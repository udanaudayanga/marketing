<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('/login/{clientid?}', 'Auth\LoginController@showLoginForm')
    ->name('login');

Route::post('/auth', 'Auth\LoginController@login')
    ->name('auth');

Route::get('/logout', 'Auth\LoginController@logout')
    ->name('logout');


Route::get('/forgot', 'Auth\ResetPinController@showForgotPinForm');

Route::post('/forgot', 'Auth\ResetPinController@resetPin')
      ->name('reset_pin');
      
Route::get('/forgot', function () {
    return view('auth.forgot');
});


Route::group(['middleware' => 'auth'],function () {

    Route::get('/dashboard', 'DashboardController@index')
    ->name('dashboard');

     Route::get('/dashboard/email', 'DashboardController@showAddEmail')
    ->name('emai_address_add_form');


     Route::post('/dashboard/email/save', 'DashboardController@saveEmailAddress')
    ->name('emai_address_add');

     Route::get('/email-list', 'DashboardController@emailList')
    ->name('emai_address_list');

    Route::get('/edit-client', 'DashboardController@editClient')
    ->name('edit_client');

     Route::post('/dashboard/client/edit', 'DashboardController@updateClient')
    ->name('update_client');




});


Route::group(['middleware' => 'auth', 'prefix' => 'jobs'],function () {

     Route::get('/create', 'JobsController@show')
    ->name('job_modal');

     Route::post('/', 'JobsController@create')
    ->name('job_create');

	Route::get('/', 'JobsController@lists')
    ->name('jobs_list');

   

    Route::get('/{id}/edit', 'JobsController@edit')
    ->name('job_edit');

     Route::post('/{id}/edit', 'JobsController@update')
    ->name('job_update');

    Route::get('/{category}/{id}', 'JobsController@view')
        ->name('jobs_view');

   Route::post('/attachments', 'AttachmentController@upload')
    ->name('job_attachments');

   Route::post('/attachments/delete', 'AttachmentController@remove')
    ->name('job_attachments_remove');


 Route::post('/save-comment/{id}', 'JobsController@saveComment')
    ->name('save_comment');

       Route::post('/log-work', 'JobsController@logWork')
    ->name('log_work');

});

Route::group(['middleware' => 'auth', 'prefix' => 'task'],function () {

    Route::get('/view/{id}', 'TasksController@view')
        ->name('task_view');

    Route::get('/{category}', 'TasksController@show')
    ->name('task_modal');

   Route::post('/', 'TasksController@create')
    ->name('task_create');

    Route::get('/{id}/delete', 'JobsController@delete')
    ->name('task_delete_model');

    Route::post('/{id}/delete', 'JobsController@remove')
    ->name('task_delete');


});


Route::group(['middleware' => 'auth', 'prefix' => 'access'],function () {

    Route::get('/', 'AccessController@lists')
        ->name('access_lists');

    Route::get('/new', 'AccessController@show')
        ->name('access_show');

    Route::post('/new', 'AccessController@create')
        ->name('access_add');

    Route::get('/edit/{id}', 'AccessController@edit')
        ->name('access_edit');

    Route::post('/edit/{id}', 'AccessController@update')
        ->name('access_update');

    Route::get('/delete/{id}', 'AccessController@delete')
        ->name('access_delete');

    Route::post('/delete/{id}', 'AccessController@destroy')
        ->name('access_destroy');

});

Route::group(['middleware' => 'auth', 'prefix' => 'ga'],function () {

    Route::get('/', 'GoogleAnalyticsController@index')->name('google_analytics');

    Route::post('/ajaxInit', 'GoogleAnalyticsController@ajaxInitial')->name('ga_ajax_init');

});

Route::group(['middleware' => 'auth', 'prefix' => 'settings'],function () {

     Route::get('/', 'SettingsController@index')->name('settings_info');

});


 /**
     * Invoice segments
     */
    Route::group(['prefix' => 'invoice'],function () {

        

        Route::get('/', 'InvoiceController@lists')
            ->name('invoice_list');
        Route::get('/{id}/download', 'InvoiceController@download')
            ->name('invoice_download');
        Route::get('/{id}/info', 'InvoiceController@info')
            ->name('invoice_info');

        Route::get('/payment/status/{id}', 'PaymentController@getPaymentStatus')
            ->name('payment_status');
        Route::post('/payment/paypal', 'PaymentController@payWithpaypal')
            ->name('payment_paypal');

        Route::get('/subscription', 'InvoiceController@subscriptionInfo')
            ->name('payment_detail');

        Route::post('/savereceiptemail', 'InvoiceController@saveRecieptEmail')
            ->name('save_receiptemail');

       
        Route::get('/payment/savemethods', 'InvoiceController@show')
            ->name('show_payment_methods');

        Route::get('/paynow/{id}', 'InvoiceController@nonSubscriptionPayments')
            ->name('non_subscribe_payments');

        Route::get('/payment/methods/edit/{id}', 'InvoiceController@editPaymentMethod')
        ->name('payment_methods_edit');

        Route::post('/payment/methods/edit/{id}', 'ComPaymentController@updatePaymentMethod')
        ->name('payment_methods_update');


        Route::get('/payment/methods/{id}/delete', 'InvoiceController@deletePaymentMethod')
    ->name('payment_methods_delete_model');

    Route::post('payment/methods/{id}/delete', 'InvoiceController@removePaymentMethod')
    ->name('payment_methods_delete');


     Route::post('/payment/comweb', 'ComPaymentController@pay')
            ->name('payment_com');

      Route::get('/payment/savecard', 'ComPaymentController@savetoken')
            ->name('payment_com_savecc');

    Route::post('/payment/methods', 'ComPaymentController@savePaymentMethod')
            ->name('save_payment_methods');

        /*    Route::post('/payment/methods', 'InvoiceController@savePaymentMethod')
            ->name('save_payment_methods');*/


            Route::get('/receiptmail/delete/{id}', 'InvoiceController@deleteRecieptEmail')
            ->name('del_recieptmail');

               Route::post('/receiptmail/delete/{id}', 'InvoiceController@remove')
            ->name('remove_recieptmail');

             Route::get('/receiptmail/edit/{id}', 'InvoiceController@editRecieptEmail')
            ->name('edit_recieptmail');

              Route::post('/receiptmail/edit/{id}', 'InvoiceController@updateRecieptEmail')
            ->name('update_recieptmail');

             Route::get('/payment/method/default/{id}', 'InvoiceController@updateDefultPaymentMethod')
            ->name('payment_markdefault');

              Route::get('/payment/method/autopay/{id}/{auto_pay}', 'InvoiceController@updateAutoPay')
            ->name('invoice_autopay');

              Route::get('/autopay/{id}/{auto_pay}', 'InvoiceController@updateAutoPayInvoice')
            ->name('invoice_updateautopay');




    });

     Route::group(['middleware' => 'auth', 'prefix' => 'webanalytics'],function () {
    
     Route::get('/', 'WebAnalyticsController@index')->name('webanalytics');
     Route::get('/info/{view_id}', 'WebAnalyticsController@info')->name('webanalytics_info');
     Route::get('/datainfo', 'WebAnalyticsController@visitors')->name('datainfo');
     Route::get('/bounceRate', 'WebAnalyticsController@bounceRate')->name('bounceRate');
     Route::get('/sessionsInfo', 'WebAnalyticsController@sessionsInfo')->name('sessionsInfo');
     Route::get('/sessionsByChannelInfo', 'WebAnalyticsController@sessionsByChannelInfo')->name('sessionsByChannelInfo');
     Route::get('/sessionRate', 'WebAnalyticsController@newSessionsRateInfo')->name('sessionRate');
     Route::get('/audienceView', 'WebAnalyticsController@audienceView')->name('audienceView');
     Route::get('/topPageViews', 'WebAnalyticsController@topPageViews')->name('topPageViews');
     Route::get('/pageViewsInfo', 'WebAnalyticsController@pageViewsInfo')->name('pageViewsInfo');
     Route::get('/visitsByDevice', 'WebAnalyticsController@visitsByDevice')->name('visitsByDevice');
     Route::get('/visitsByCountries', 'WebAnalyticsController@visitsByCountries')->name('visitsByCountries');
      Route::get('/goalInfo', 'WebAnalyticsController@goalInfo')->name('goalInfo');
      

});

      Route::group(['middleware' => 'auth', 'prefix' => 'users'],function () {
          Route::get('/', 'EmployeeController@lists')->name('users_list');
          Route::get('/new', 'EmployeeController@show')->name('employee_modal');
          Route::post('/', 'EmployeeController@create')->name('employee_create');
          Route::get('/{id}/edit', 'EmployeeController@edit')->name('employee_edit_modal');

    Route::post('/{id}/edit', 'EmployeeController@update')->name('employee_edit');

    Route::get('/{id}/delete', 'EmployeeController@delete')->name('employee_delete_modal');

    Route::post('/{id}/delete', 'EmployeeController@remove')->name('employee_delete');
     });


      Route::group(['middleware' => 'auth', 'prefix' => 'semrush'],function () {
        Route::get('/siteaudit/{projectid}', 'SemrushController@index')->name('semrush_site_audit');
        Route::get('/siteaudit/issues/{projectid}', 'SemrushController@allIssues')->name('semrush_site_audit_issues');
        Route::get('/competitor-analysis/{projectid}', 'SemrushController@compatiterAnalysis')->name('semrush_competitor_analysis');

      });


       Route::group(['middleware' => 'auth', 'prefix' => 'proposal'],function () {
          Route::get('/{projectid}', 'ProposalController@download')->name('client_marketing_proposal');
         

      });

