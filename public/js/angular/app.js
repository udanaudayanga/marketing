var app = angular.module('app', ["chart.js"], function($interpolateProvider) { $interpolateProvider.startSymbol('<%'); $interpolateProvider.endSymbol('%>'); });

app.config(['ChartJsProvider', function (ChartJsProvider) {
    // Configure all charts
    ChartJsProvider.setOptions({
        responsive: true
    });
    // Configure all line charts
    ChartJsProvider.setOptions('line', {
        showLines: true
    });
    ChartJsProvider.setOptions('doughnut', {
        cutoutPercentage: 60
    });
}]);

app.controller('gaDashboardCtrl', function ($scope, $http, $timeout) {

    $scope.chart = {
        'colors' : [ {
            'backgroundColor': [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']
        }]
    };

    $scope.init = function () {
        var req = {
            method: 'POST',
            url: 'ga/ajaxInit',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                data: ''
            }
        };
        $http(req).then(function(response){
            var result = response.data;
            if (result.status == true) {
                $scope.report = result.data;
            } else {
                alert(result.message);
            }
        }, function(){
            alert("Please try again later..");
        });
    };

});