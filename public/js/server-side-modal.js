(function ( $ ) {

    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push($.trim(this.value) || '');
            }else {
                o[this.name] = $.trim(this.value) || '';
            }
        });
        return o;
    };

    $.fn.modalRender = function() {
        $(this).click(function(e){
            e.preventDefault();
            $('.modal').remove();
            let uri = $(this).data('modal-uri');
            if (null === uri) {
                uri = $(this).href();
            }
            if (uri !== null && uri !== "#") {
                let modal = render(uri);
                modal.done(function (data) {

                    $('body').append(data);
                    $('.modal').modal('toggle');
                    init();
                });

                modal.fail(function(jqXHR, textStatus) {
                    alert( ": " + textStatus );
                });
            }
        });
    };

    let init = function () {
        $(function (){
            $(".modal form").on("submit", function(e){
                e.preventDefault();
                let form = this;
                let submission = submit(this);
                let button = $('.submit-button');
                let prevLable = button.html();
                button.html('Processing <img src="/images/loading.gif" alt=""  height="20">');
                submission.done(function(html){
                    $('.modal-content').html(html);
                });

                submission.fail(function(req) {
                    if (req.status === 422) {
                        let errors = $.parseJSON(req.responseText);
                        setErrors(form, errors);
                        button.html(prevLable);
                    }
                });
                return false;
            })
        });
    };

    let submit = function (form) {
        return $.ajax({
            url: form.action+"?modal="+  new Date().getTime(),
            data:$(form).serializeObject(),
            type: form.method,
            dataType: "html"
        });
    };

    let render = function (url) {
        return $.ajax({
            url: url+"?modal="+  new Date().getTime(),
            type: "GET",
            dataType: "html"
        });
    };

    function clearErrors() {
        $(".has-error").removeClass("has-error");
        $(".error-block").each(function(){
            $(this).remove();
        });
    }

    function setErrors(form, errors) {
        clearErrors(form);
        $(errors).each(function(i,val){
            $.each(val,function(name,message){
                $(form).find('[name="' + name + '"]').setError(message);
            });
        });
    }


    $.fn.setError = function(message)
    {
        let container =  $(this).parent();
        container.addClass('has-error');
        let errorContainer =   $('<span></span>')
                                .addClass("help-block has-error error-block")
                                .html(message);
        container.append(errorContainer)

    }

}( jQuery ));

