 
/** ****** left Menu *********************** **/

$("#menu-toggle").click(function(e) {
 "use strict";
	e.preventDefault();
		$("#wrapper").toggleClass("toggled");
});
$("#menu-toggle-2").click(function(e) {
	"use strict";
	e.preventDefault();
	$("#wrapper").toggleClass("toggled-2");
	$('#menu ul').hide();
});

/*$("#menu-toggle2").click(function(e) {
"use strict";
e.preventDefault();
		$("#wrapper").toggleClass("toggled2");
});

$("#menu-toggle-3").click(function(e) {
	"use strict";
	e.preventDefault();
	$("#wrapper").toggleClass("toggled-3");
	$('#menu ul').hide();
});

$("#menu-toggle3").click(function(e) {
"use strict";
e.preventDefault();
		$("#wrapper").toggleClass("toggled3");
});*/

function initMenu() {
	"use strict";
	$('#menu ul').hide();
	$('#menu ul').children('.current').parent().show();
	//$('#menu ul:first').show();
	$('#menu li a').click(
		function() {
			var checkElement = $(this).next();
			if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
				return false;
				}
			if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
				$('#menu ul:visible').slideUp('normal');
				checkElement.slideDown('normal');
				return false;
				}
			}
		);
	}
	$(document).ready(function() { 
	"use strict";
	initMenu();
});


/** ******  scroll top  *********************** **/
 $(document).ready(function () {
	"use strict";
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
				$('.scrollup').fadeIn();
		} else {
				$('.scrollup').fadeOut();
		}
	});

	$('.scrollup').click(function () {
		$("html, body").animate({
				scrollTop: 0
		}, 600);
		return false;
	});
});

/** ******  iswitch  *********************** **/
if ($("input.flat")[0]) {
	$(document).ready(function () {
		"use strict";
		$('input.flat').iCheck({
				checkboxClass: 'icheckbox_flat',
				radioClass: 'iradio_flat'
		});
	});
}

/** ******  tooltip  *********************** **/
$(function () {
	$('[data-toggle="tooltip"]').tooltip()
})
