@extends('layouts.app')


@section('content')
<div id="wrapper" class="client-dashboard">
 <section id="page-content-wrapper">
    <div class="container-wrapper container-fuild">
      <div class="row">
        <div class="col-sm-12">
          <div class="row clients">
            <div class="col-sm-12">
              <h2>Dashboard</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <p>Welcome,  {{ Auth::user()->name }}</p>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4">
              <section class="content">
                <fieldset class="profile">
                  <div class="row">
                    <div class="col-md-12"> <a href="#" class="pull-right"
                   data-modal-uri="{{ route('edit_client') }}" id="edit-client" style="margin-right: 10px"><i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit"></i>Edit Info</a> </div>
                    <div class="col-md-12"> <img src="http://pm.jplms.com.au/images/{{ $client->logo }}" alt="" width="150"> </div>
                  </div>
                  <div class="row">
                    <label class="col-md-6">Company Name</label>
                    <div class="col-md-6">{{ $client->company_name }}</div>
                  </div>
                  <div class="row">
                    <label class="col-md-6">ABN</label>
                    <div class="col-md-6">{{ $client->abn }}</div>
                  </div>
                  <div class="row">
                    <label class="col-md-6">Address</label>
                    <div class="col-md-6">{{ $client->address }} {{ $client->street }}  {{ $client->subrub }}  {{ $client->state }}</div>
                  </div>
                  <div class="row">
                    <label class="col-md-6">Primary Accounts</label>
                    <div class="col-md-6">xxxxx</div>
                  </div>
                </fieldset>
              </section>
            </div>
            <div class="col-md-4">
              <section class="content">
                <fieldset class="make-payment">
                  <label>Make a Payment</label>
                   <table class="table table-striped">
                    <tbody>
                      @foreach( $invoices as $invoice)
                      <tr>
                        <td>{{ $invoice->no }}</td>
                        <td class="text-right">{{--&#36;150.50--}}</td>
                        <td><a href="{{route('non_subscribe_payments', $invoice->id)}}"><button class="btn btn-sm btn-danger">Pay Now</button></a></td>
                      </tr>
                      @endforeach
                     
                    </tbody>
                  </table>
                </fieldset>
              </section>
              <section class="content">
                <fieldset class="support-request">
                   <label>Support Request</label>
                             <table class="table table-bordered table-hover in-progress">
                <tbody>
                  @foreach ( $jobs as $job)
                  <tr>
                    <td><strong>{{ $job->code }}
                      @foreach (  $job->assignees as $assignee )
                        <mark>[{{ $assignee->name}}]</mark>
                      @endforeach
                      </strong> <br>
                      <span>{{ str_limit($job->title, 33) }} </span>
                      <aside class="action">
                        <a href="task-job-update.html"><i class="fa fa-cog" aria-hidden="true"></i></a><a href="#"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        <small>
                          {{ date('m/d/Y',strtotime($job->due_date)) }}
                        </small>
                        </aside>
                        <div class="priority high" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="" data-original-title="{{ ucwords($job->priority) }}"></div>
                        </td>
                  </tr>
                   @endforeach
                  
                </tbody>
              </table>
                    <a href="#" class="btn btn-default pull-right"
                   data-modal-uri="{{ route('job_modal') }}" id="add-job" style="margin-right: 10px"><i class="fa fa-plus" aria-hidden="true"></i> Add New Request</a>
                </fieldset>
              </section>
            </div>
            <div class="col-md-4">
              <section class="content">
                <fieldset class="ms-sign">
                  <a href="https://login.live.com/" target="_blank"><img src="{{ asset('images/microsoft-sign-in.jpg') }}" alt=""></a>
                </fieldset>
              </section>
            </div>
          </div>
          <div class="row access">
            <div class="col-md-4">
              <section class="content">
                <fieldset>
                  <table class="table table-striped table-hover">
                    <tbody>
                      <tr>
                        <th scope="col">Name</th>
                        <th scope="col"></th>
                      </tr>
                      @foreach ($employees as $employee)
                      <tr>
                        <td>{{ $employee->name }}</td>
                        <td><a href="#" data-modal-uri="{{ route('employee_delete_modal', $employee->id )}}"  class="pull-right delete-employee"><i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>&nbsp;&nbsp;<a href="#" data-modal-uri="{{ route('employee_edit_modal', $employee->id ) }}"  class="pull-right edit-employee"><i class="fa fa-pencil-square-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </fieldset>
              </section>
            </div>
            <div class="col-md-4">
              <section class="content">
                <fieldset>
                  <table class="table table-striped table-hover">
                    <tbody>
                      <tr>
                        <th scope="col">Email</th>
                        <th scope="col"><i class="fa fa-lock" aria-hidden="true"></i></th>
                      </tr>
                      @foreach ($emailAddresses as $address)
                      <tr>
                        <td>{{ $address->email}}</td>
                        <td><strong><input type="password" value="{{ $address->password }}" style="border: none; background-color: transparent" class="pwd"></strong> <a href="#" class="pull-right show"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Reveal"></i></a></td>
                      </tr>
                     @endforeach
                    </tbody>
                  </table>
                </fieldset>
              </section>
            </div>
            <div class="col-md-4">
              <section class="content">
                <fieldset>
                  @if (  $access)
                  <table class="table table-striped table-hover">
                    <tbody>
                      <tr>
                        <td class="icon"><a href="task-detailed-view-pa.html"><i class="fa fa-building-o" aria-hidden="true"></i></a></td>
                        <td><strong>{{ $access->organization }}</strong> <a href="#" data-modal-uri="{{ route('access_delete', $access->id) }}" class="pull-right delete-access"><i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>&nbsp;&nbsp;<a href="#" data-modal-uri="{{ route('access_edit', $access->id) }}" class="pull-right edit-access"><i class="fa fa-pencil-square-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></i></a></td>
                      </tr>
                      <tr>
                        <td class="icon"><a href="task-detailed-view-pa.html"><i class="fa fa-address-card-o" aria-hidden="true"></i></a></td>
                        <td>{{ $access->ref_number }}</td>
                      </tr>
                      <tr>
                        <td class="icon"><a href="task-detailed-view-pa.html"><i class="fa fa-user-circle" aria-hidden="true"></i></a></td>
                        <td>{{ $access->username }}</td>
                      </tr>
                      <tr>
                        <td class="icon"><a href="task-detailed-view-pa.html"><i class="fa fa-lock" aria-hidden="true"></i></a></td>
                       <td><strong><input type="password" value="{{ $access->password }}" style="border: none; background-color: transparent" class="pwd"></strong>
                                    <a href="#" class="pull-right show">
                                        <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Reveal"></i>
                                    </a>
                                </td>
                      </tr>
                      <tr>
                        <td class="icon"><a href="task-detailed-view-pa.html"><i class="fa fa-th" aria-hidden="true"></i></a></td>
                       <td><strong><input type="password" value="{{ $access->pin }}" style="border: none; background-color: transparent" class="pwd"></strong>
                                    <a href="#" class="pull-right show">
                                        <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Reveal"></i>
                                    </a>
                                </td>
                      </tr>
                    </tbody>
                  </table>
                  @endif
                </fieldset>
              </section>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4">
              <div class="lg-tab"> <a href="#" data-toggle="modal" data-modal-uri="{{ route('employee_modal') }}"  id="add-user" class="lg"><i class="fa fa-plus" aria-hidden="true"></i> ADD <span>User</span></a>
                <div><a href="{{ route('users_list')}}"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;&nbsp;VIEW ALL</a></div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="lg-tab"> <a href="#" data-toggle="modal" data-modal-uri="{{ route('emai_address_add_form') }}"  id="add-email" class="lg"><i class="fa fa-plus" aria-hidden="true"></i> ADD <span>Email Address</span></a>
                <div><a href="{{ route('emai_address_list') }}"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;&nbsp;VIEW ALL</a></div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="lg-tab"> <a href="#" data-toggle="modal"  data-modal-uri="{{ route('access_show') }}"  id="add-access" class="lg"><i class="fa fa-plus" aria-hidden="true"></i> ADD <span>Access</span></a>
                <div><a href="{{ route('access_lists') }}"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;&nbsp;VIEW ALL</a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('scripts')
  <script>
        $(document).ready(function () {
            $('#add-access').modalRender();
            $('#add-email').modalRender();
            $('#add-user').modalRender();
            $('#add-job').modalRender();
             $('#edit-client').modalRender();

               $('.edit-employee').modalRender();
            $('.delete-employee').modalRender();

               $('.delete-access').modalRender();
            $('.edit-access').modalRender();

                $('.show').click(function () {
                var el = $(this).parent().find('.pwd');
                if (el.get(0).type === "password") {
                    el.get(0).type = "text";

                    return false;
                } else {
                    el.get(0).type = "password";

                    return false;
                }
            });

  });
    </script>
@endsection