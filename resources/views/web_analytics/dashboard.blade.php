@extends('layouts.app')


@section('content')
 <div id="wrapper">

<section id="page-content-wrapper" class="analytics">
    <div class="container-wrapper container-fuild">
      <ol class="breadcrumb">
        <li><a href="/">Dashboard</a></li>
        <li class="active">Website Analytics</li>
      </ol>
      <div class="row">
        <div class="col-sm-12">
          <section class="content">
            <fieldset>
              <ul class="default-tab">
                  @if ($website1[0]->view_id != '')
                <li><a href="{{ route('webanalytics_info',[$website1[0]->view_id]) }}" class="">Website - <em>JPLMS</em><small>{{$website1[0]->url}}</small></a></li>
                @endif
                @if ($website1[0]->semrush_project_id != '')
                <li><a href="{{ route('semrush_site_audit',[$website1[0]->semrush_project_id]) }}"><span>Site Audit Results</span></a></li>
                <li><a href="{{route('semrush_competitor_analysis',[$website1[0]->semrush_project_id])}}"><span>Competitor Analysis</span></a></li>
                <li><a href="#"><span>Keyword Performance</span></a></li>
                @endif
             {{--   @foreach ($websites as $key=>$website)
                @if ( $key == 0)
                  <li><a href="{{ route('webanalytics') }}"  class="{{ $viewId==$website->view_id?"active":"" }}" >Website - <span>{{ $website->url}}</span></a></li>
                @else
                   <li><a href="{{ route('webanalytics_info',[$website->view_id]) }}"  class="{{ $viewId==$website->view_id?"active":"" }}">Website -<span>{{ $website->url}}</span></a></li>
                @endif

                 <li><a href="{{ route('semrush_site_audit',[$website->semrush_project_id]) }}">Website -<span>{{ $website->semrush_project_id}}</span></a></li>
                           
                @endforeach
                --}}
              </ul>
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="wide-block text-center">
                        <h2>Users</h2>
                        <select name="duration" id="duration">
                          <option value="month_to_date">Month to date</option>
                          <option value="last_7_days">Last 7 days</option>
                          <option value="last_30_days">Last 30 days</option>
                         
                        </select>
                       
                        <div class="count" id="user_count_comapre"> </div>
                        <div id="chart_div" style="width: 123%; height: 650px;margin:0 -10%;" ></div>  </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="block">
                        <h2>Sessions</h2>
                     
                        <div class="count" id="session_count_comapre"> </div>
                        <div id="chart_div_sess" style="width: 128%; height: 300px;margin:70px -10% 0;"></div> </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="block">
                        <h2>Bounce Rate</h2>
                        
                        <div class="count" id="bouncerate_count_comapre">  </div>
                        <div id="chart_div_br" style="width: 128%; height: 300px;margin:70px -10% 0;"></div>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="block">
                        <h2>Sessions by Channels</h2>
                       
                        
                         <div id="chart_div_chanel" style="width: 128%; height: 300px;margin:70px -10% 0;"></div>
                       </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="block tabular">
                        <h2>Top Pages by page views</h2>
                       
                      
                        <table width="100%" class="table table-striped">
                          <thead>
                            <tr>
                              <th>Page</th>
                              <th class="text-center">Page Views</th>
                              <th class="text-center">&#37;</th>
                            </tr>
                          </thead>
                          <tbody id="top_views">
                           
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="block">
                        <h2>% New Sessions</h2>
                       
                        <div class="count" id="newsess_count_comapre"> </div>
                         <div id="chart_div_new_sessionrate" style="width: 128%; height: 300px;margin:70px -10% 0;"></div> </div>
                    </div>
                    
                    <div class="col-lg-4">
                      <div class="block tabular">
                        <h2>Audience overview</h2>
             
                      
                       <table width="100%" class="table table-striped">
                          <thead>
                            <tr>
                              <th>Metrics</th>
                              <th class="text-center">Month to date</th>
                              <th class="text-center">&#37;</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Users</td>
                              <td class="nos" id="users_count"></td>
                              <td class="success" id="users_compare" ></td>
                            </tr>
                            <tr>
                              <td>&#37; New Users</td>
                              <td class="nos" id="newusers_count"></td>
                              <td class="danger" id="newusers_compare"></td>
                            </tr>
                            <tr>
                              <td>Sessions</td>
                              <td class="nos" id="session_count"></td>
                              <td class="success" id="newsession_percentage"></td>
                            </tr>
                            
                            <tr>
                              <td>Pages /Session</td>
                              <td class="nos" id="page_session_count"></td>
                              <td class="success" id="page_session_percentage" ></td>
                            </tr>
                            <tr>
                              <td>Avd. Session Duration</td>
                              <td class="nos" id="avg_session_count"></td>
                              <td class="danger" id="avg_session_percentage"></td>
                            </tr>
                            <tr>
                              <td>Bounce rate</td>
                              <td class="nos" id="bouncerate_count"></td>
                              <td class="danger" id="bouncerate_percentage"></td>
                            </tr>
                            <tr>
                              <td>Pageview</td>
                              <td class="nos" id="pageview_count"></td>
                              <td class="success" id="pageview_percentage"></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="block tabular">
                        <h2>Goals</h2>
                        {{--<select>
                          <option>Month date to July</option>
                        </select>
                        <div class="count"> <span>30 <em class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i> 50%</em> <small>Previous 26</small> </span> </div>
                        <table width="100%" class="table table-striped">
                          <thead>
                            <tr>
                              <th>Page</th>
                              <th class="text-center">Last Month</th>
                              <th class="text-center">&#37;</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Goal 1</td>
                              <td class="nos">80</td>
                              <td class="success"><i class="fa fa-caret-up" aria-hidden="true"></i>300&#37;</td>
                            </tr>
                            <tr>
                              <td>Goal 1</td>
                              <td class="nos">76</td>
                              <td class="success"><i class="fa fa-caret-up" aria-hidden="true"></i>20&#37;</td>
                            </tr>
                            <tr>
                              <td>Goal 1</td>
                              <td class="nos">50</td>
                              <td class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i>20&#37;</td>
                            </tr>
                            <tr>
                              <td>Goal 1</td>
                              <td class="nos">25</td>
                              <td class="success"><i class="fa fa-caret-up" aria-hidden="true"></i>20&#37;</td>
                            </tr>
                            <tr>
                              <td>Goal 1</td>
                              <td class="nos">3</td>
                              <td class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i>45&#37;</td>
                            </tr>
                            <tr>
                              <td>Goal 1</td>
                              <td class="nos">50</td>
                              <td class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i>20&#37;</td>
                            </tr>
                            <tr>
                              <td>Goal 1</td>
                              <td class="nos">25</td>
                              <td class="success"><i class="fa fa-caret-up" aria-hidden="true"></i>20&#37;</td>
                            </tr>
                          </tbody>
                        </table> --}}
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="block">
                        <h2>Goal review</h2>
                       {{-- <select>
                          <option>Month date to July</option>
                        </select>
                        <div class="count"> <span>10 <em class="success"><i class="fa fa-caret-up" aria-hidden="true"></i> 50%</em> <small>Previous 26</small> </span> </div>
                        <img src="images/top-channels.jpg" class="img-responsive">  --}} </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="block">
                        <h2>Sessions by country</h2>
                      
                         
                        <table width="100%" class="table table-striped">
                          <thead>
                            <tr>
                              <th>Country</th>
                              <th class="text-center">Sessions</th>
                              <th class="text-center">&#37;</th>
                            </tr>
                          </thead>
                          <tbody id="top_country_views">
                           
                          </tbody>
                        </table>

                       </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="block">
                        <h2>Page Views</h2>
                       
                        <div class="count" id="pageview_count_comapre">  </div>
                        <div id="chart_div_pageviews"  style="width: 128%; height: 300px;margin:70px -10% 0;"></div>
                         </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="block">
                        <h2>Sessions by device</h2>
                        <div id="chart_div_devices" style="width: 128%; height: 300px;margin:70px -10% 0;"></div>
                     </div>
                    </div>
                  </div>
                </div>
                {{--<div class="col-md-3">
                  <div class="score"> <span>85%</span>
                    <p>Website Score</p>
                    <a href="#">View Detailed Report</a> </div>
                  <div class="performance"> Keyword Analysis and Performance </div>
                </div>
              </div> --}}
            </fieldset>
          </section>
        </div>
      </div>
    </div>
  </section>





  

</div>
@endsection



@section('scripts')
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart','line','bar']});
  google.charts.setOnLoadCallback(drawChart); 
  google.charts.setOnLoadCallback(drawBounceRateChart);  
  google.charts.setOnLoadCallback(drawSessionChart);
  google.charts.setOnLoadCallback(drawSessionByChanelChart);
  google.charts.setOnLoadCallback(drawNewSessionPercentageChart);
  google.charts.setOnLoadCallback(drawPageViewChart);
  google.charts.setOnLoadCallback(drawSessionByDeviceslChart);


  $(window).resize(function(){
  drawChart()
  drawBounceRateChart();
  drawSessionChart();
  drawSessionByChanelChart();
  drawNewSessionPercentageChart();
  drawPageViewChart();
  drawSessionByDeviceslChart();


  
});


  var options = {
           backgroundColor: '#f9f9f9',
          legend: { position: 'bottom' },
          is3D: true,
          tooltip: { isHtml: true },
          colors: ['#4285f4','#ff7d00']
         
        };

  function drawChart() {
      var jsonData = $.ajax({
          url: "{{route('datainfo')}}",
           type: 'GET',
          data: {'duration': $("#duration").val(), 'view_id': {{$viewId }} },
          dataType: "json",
          async: false
          }).responseText;
          
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }


    $("#duration").change(function(){
      google.charts.setOnLoadCallback(drawChart);  

    });


    //Bounce Rate
     var optionsBounce = {
          backgroundColor: '#f9f9f9',
          legend: { position: 'bottom' },
          is3D: true,
          tooltip: { isHtml: true },
          colors: ['#4285f4','#ff7d00']
          
        };


  function drawBounceRateChart() {
      var jsonData = $.ajax({
          url: "{{route('bounceRate')}}",
           type: 'GET',
          data: {'duration': $("#duration").val(), 'view_id': {{$viewId }}},
          dataType: "json",
          async: false
          }).responseText;
          
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.LineChart(document.getElementById('chart_div_br'));
      chart.draw(data, optionsBounce);
    }

     $("#duration").change(function(){
      google.charts.setOnLoadCallback(drawBounceRateChart);  

    });


    //session info
   
     var optionsSessions = {
          backgroundColor: 'transparent',
          legend: { position: 'bottom' },
          is3D: true,
          tooltip: { isHtml: true },
          colors: ['#4285f4','#ff7d00']
        
          
        };


  function drawSessionChart() {
      var jsonData = $.ajax({
          url: "{{route('sessionsInfo')}}",
           type: 'GET',
          data: {'duration': $("#duration").val(), 'view_id': {{$viewId }}},
          dataType: "json",
          async: false
          }).responseText;
          
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.LineChart(document.getElementById('chart_div_sess'));
      /*google.visualization.events.addListener(chart, 'ready', function () {
        chart_div_sess.innerHTML = '<img src="' + chart.getImageURI() + '" class="responsive">';
        console.log(chart_div.innerHTML);
      });*/
      chart.draw(data, optionsBounce);
    }

     $("#duration").change(function(){
      google.charts.setOnLoadCallback(drawSessionChart);  

    });


     //channels
     var optionsSessionsByChanel={
      
       backgroundColor: '#f9f9f9',
      pieHole: 0.4,
        
          pieSliceTextStyle: {
            color: 'black'
        }
    };

     function drawSessionByChanelChart() {
      var jsonData = $.ajax({
          url: "{{route('sessionsByChannelInfo')}}",
            type: 'GET',
          data: {'duration': $("#duration").val(), 'view_id': {{$viewId }}},
          dataType: "json",
          async: false
          }).responseText;

      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.PieChart(document.getElementById('chart_div_chanel'));
      chart.draw(data, optionsSessionsByChanel);
    }

    $("#duration").change(function(){
      google.charts.setOnLoadCallback(drawSessionByChanelChart);  

    });

//%new sessions
var optionsNewSessions = {
          backgroundColor: '#f9f9f9',
          legend: { position: 'bottom' },
          is3D: true,
          tooltip: { isHtml: true },
          colors: ['#4285f4','#ff7d00'],
          
        };
        
    function drawNewSessionPercentageChart() {
      var jsonData = $.ajax({
          url: "{{route('sessionRate')}}",
           type: 'GET',
          data: {'duration': $("#duration").val(), 'view_id': {{$viewId }}},
          dataType: "json",
          async: false
          }).responseText;
          
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.LineChart(document.getElementById('chart_div_new_sessionrate'));
      chart.draw(data, optionsNewSessions);
    }

     $("#duration").change(function(){
      google.charts.setOnLoadCallback(drawNewSessionPercentageChart);  

    });



     $.getJSON("{{route('audienceView')}}" ,{ view_id: {{$viewId }}},function(result){
       
            $("#users_count").append(result.users + " ");
            $("#users_compare").append(result.users_compare + " ");
             $("#user_count_comapre").append(result.user_count_comapre + " ");
            $("#session_count").append(result.sessions + " ");
            $("#session_count_comapre").append(result.session_count_comapre+" ");
             $("#newusers_count").append(result.newUsers + " ");
              $("#newusers_compare").append(result.newusers_compare + " ");
            $("#newsession_count").append(result.sessionPerUsers + " ");
             $("#newsession_percentage").append(result.sessionperusers_compare + " ");
            $("#page_session_count").append(result.sessionPerPage + " ");
            $("#page_session_percentage").append(result.sessionperpage_compare + " ");
            $("#avg_session_count").append(result.avgSessionDuration + " ");
            $("#avg_session_percentage").append(result.prevAvgSessionPercentage + " ");
            $("#bouncerate_count").append(result.bounceRate + "%");
            $("#bouncerate_percentage").append(result.bounceRatePercentage + '');
            $("#bouncerate_count_comapre").append(result.bouncerate_count_comapre);
            $("#pageview_count").append(result.pageViews + " ");
            $("#pageview_count_comapre").append(result.pageview_count_comapre);
             $("#pageview_percentage").append(result.pageViewsPercentage + " ");
             $("#newsess_count_comapre").append(result.newsess_count_comapre);

            
       
    });

      $("#duration").change(function(){
     $.getJSON("{{route('audienceView')}}",{duration: $("#duration").val(), view_id: {{$viewId }}}, function(result){
    
            $("#users_count").html(result.users + " ");
             $("#users_compare").html(result.users_compare + " ");
               $("#user_count_comapre").html(result.user_count_comapre + " ");
            $("#session_count").html(result.sessions + " ");
            $("#session_count_comapre").html(result.session_count_comapre);
             $("#newusers_count").html(result.newUsers + " ");
               $("#newusers_compare").html(result.newusers_compare + " ");
            $("#newsession_count").html(result.sessionPerUsers + " ");
             $("#newsession_percentage").html(result.sessionperusers_compare + " ");
            $("#page_session_count").html(result.sessionPerPage + " ");
             $("#page_session_percentage").html(result.sessionperpage_compare + " ");
            $("#avg_session_count").html(result.avgSessionDuration + " ");
            $("#avg_session_percentage").html(result.prevAvgSessionPercentage + " ");
            $("#bouncerate_count").html(result.bounceRate + "%");
            $("#bouncerate_count_comapre").append(result.bouncerate_count_comapre);
            $("#pageview_count").html(result.pageViews + " ");
            $("#pageview_percentage").html(result.pageViewsPercentage + " ");
            $("#pageview_count_comapre").html(result.pageview_count_comapre);
              $("#bouncerate_percentage").html(result.bounceRatePercentage + '');
                $("#newsess_count_comapre").html(result.newsess_count_comapre);
       
    }); 

    });


       $.getJSON("{{route('topPageViews' )}}",{ view_id: {{$viewId }}}, function(result){

            var topLinkValue = '';
            $.each( result, function( key, val ) {
               topLinkValue += '<tr>';
               topLinkValue += '<td><div class="ellipsis" title="'+val.page_path+'">'+val.page_path+'</div></td>';
               topLinkValue += '<td class="nos">'+val.view_count+'</td>';
               topLinkValue += '<td>'+val.page_path_percentage+'</td>';
               topLinkValue += '</tr>';

             
            
          });
    $("#top_views").append(topLinkValue);
       
    });


    $("#duration").change(function(){
    $.getJSON("{{route('topPageViews')}}",{duration: $("#duration").val(), view_id: "{{$viewId }}" }, function(result){
            var topLinkValue = '';
            $.each( result, function( key, val ) {
               topLinkValue += '<tr>';
               topLinkValue += '<td>'+val.page_path+'</td>';
               topLinkValue += '<td class="nos">'+val.view_count+'</td>';
               topLinkValue += '<td">'+val.page_path_percentage+'</td>';
               topLinkValue += '</tr>';

            
          });
    $("#top_views").append(topLinkValue);
       
    });

    });


    //page views
     var optionsPageViews = {
          backgroundColor: '#f9f9f9',
          legend: { position: 'bottom' },
          is3D: true,
          tooltip: { isHtml: true },
          colors: ['#4285f4','#ff7d00']
         
        };


  function drawPageViewChart() {
      var jsonData = $.ajax({
          url: "{{route('pageViewsInfo')}}",
           type: 'GET',
          data: {'duration': $("#duration").val(), 'view_id': {{$viewId }}},
          dataType: "json",
          async: false
          }).responseText;
          
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.LineChart(document.getElementById('chart_div_pageviews'));
      chart.draw(data, optionsPageViews);
    }

     $("#duration").change(function(){
      google.charts.setOnLoadCallback(drawPageViewChart);  

    });


     //devices
     var optionsSessionsByDevices={
      
       backgroundColor: '#f9f9f9',
      pieHole: 0.4,
        
          pieSliceTextStyle: {
            color: 'black'
        }
    };

     function drawSessionByDeviceslChart() {
      var jsonData = $.ajax({
          url: "{{route('visitsByDevice')}}",
            type: 'GET',
          data: {'duration': $("#duration").val(), 'view_id': {{$viewId }}},
          dataType: "json",
          async: false
          }).responseText;

      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.PieChart(document.getElementById('chart_div_devices'));
      chart.draw(data, optionsSessionsByDevices);
    }

    $("#duration").change(function(){
      google.charts.setOnLoadCallback(drawSessionByDeviceslChart);  

    });


//top_country_views
  $.getJSON("{{route('visitsByCountries' )}}",{ view_id: {{$viewId }}}, function(result){

            var topLinkValue = '';
            $.each( result, function( key, val ) {
               topLinkValue += '<tr>';
               topLinkValue += '<td><div class="ellipsis" title="'+val.page_path+'">'+val.country+'</div></td>';
               topLinkValue += '<td class="nos">'+val.view_count+'</td>';
               topLinkValue += '<td></td>';
               topLinkValue += '</tr>';

             
            
          });
    $("#top_country_views").append(topLinkValue);
       
    });


    $("#duration").change(function(){
    $.getJSON("{{route('visitsByCountries')}}",{duration: $("#duration").val(), view_id: "{{$viewId }}" }, function(result){
            var topLinkValue = '';
            $.each( result, function( key, val ) {
               topLinkValue += '<tr>';
               topLinkValue += '<td>'+val.country+'</td>';
               topLinkValue += '<td class="nos">'+val.view_count+'</td>';
               topLinkValue += '<td"></td>';
               topLinkValue += '</tr>';

            
          });
    $("#top_country_views").append(topLinkValue);
       
    });

    });


</script> 
@endsection