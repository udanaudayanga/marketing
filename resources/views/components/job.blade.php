<tr>
    <td>
     
       
        <strong>{{ $code }}</strong>
        <span>{{ str_limit($title, 33) }}</span>
        <aside class="action">
            <i class="fa fa-exclamation-circle pull-right"
               aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{ ucwords($priority) }}" style="color: {{ get_priority_color($priority) }} ">
            </i>
            <br/>
            <a href="#" data-modal-uri="{{ route('job_edit', $id ) }}" class="job-edit">
                <i class="fa fa-pencil-square-o" aria-hidden="true" data-toggle="tooltip" data-original-title="Edit"></i>
            </a>
            <a href="{{route('jobs_view', [$type, $id] )}}" class="job-view">
                <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-original-title="View"></i>
            </a>
            <a href="#" data-modal-uri="{{ route('task_delete',  $id ) }}"   class="del-job"><i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i></a>
           
        </aside>
    
        
    </td>
</tr>