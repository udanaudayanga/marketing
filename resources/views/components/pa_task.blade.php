<tr>
    <td>
        <strong>{{ $code }}</strong>
        @if($category == "all")
            <i class="{{ get_job_type_icon($type) }} pull-left success"
               aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{ ucwords($type) }}">
            </i>
        @else
            @if($status == \App\Constants\JobStatus::IN_PROGRESS)
                <i class="fa fa-bookmark info" aria-hidden="true"></i>
            @endif

            @if($status == \App\Constants\JobStatus::DONE)
                <i class="fa fa-check-circle successbright" aria-hidden="true"></i>
            @endif
        @endif
        <br>
        <i class="fa fa-exclamation-circle
        {{ get_priority_color($priority) }}  pull-right"
           aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{ ucwords($priority) }}">
        </i>

        <span>{{ str_limit($title, 33) }}</span>
        <aside class="action">
            <a href="#" data-modal-uri="{{ route('pa_task_edit_modal', $id ) }}" class="job-edit">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </a>
            <a href="{{ route('pa_task_view', [$id] ) }}" class="job-view">
                <i class="fa fa-eye" aria-hidden="true"></i>
            </a>
        </aside>
    </td>
</tr>