
        @if($mine_type == "image/jpeg" || $mine_type == "image/png" || $mine_type == "image/gif")

                @if(file_exists(public_path($path)))
                <a href="/{{$path}}" id="attachment_{{ $id }}" target="_blank">
                    <img src="/{{$path}}" alt="{{$original_name}}" height="100" width="100">
                </a>
                @else
                 <a href="https://pm.jplms.com.au/{{$path}}" id="attachment_{{ $id }}" target="_blank">
                    <img src="https://pm.jplms.com.au/{{$path}}" alt="{{$original_name}}" height="100" width="100">
                </a>
                @endif
              
                @if($delete_action == "1")
                    <a href="#" class="delete-attachment" data-id="{{ $id }}">
                        <i class="fa fa-times" aria-hidden="true"></i> delete
                    </a>
                @endif

        @else

            @if(file_exists(public_path($path)))
            <a href="/{{$path}}" id="attachment_{{ $id }}" target="_blank">
                {{ $original_name }}
            </a>
             @else
              <a href="https://pm.jplms.com.au/{{$path}}" id="attachment_{{ $id }}" target="_blank">
                {{ $original_name }}
            </a>
             @endif

            @if($delete_action == "1")
                <a href="#" class="delete-attachment" data-id="{{ $id }}">
                    <i class="fa fa-times" aria-hidden="true"></i> delete
                </a>
            @endif
        @endif