<tr>
    <td>
        @if($is_completed == '1' )
            <strike>
                @endif
                {{ $index.". ".$title }}
                @if($is_completed == '1' )
            </strike>
        @endif
    </td>
    <td>
        @if($is_completed == '1' )
            <a href="#" ><i class="fa fa fa-check-circle"> </i></a>
        @endif

    </td>
    <td>
        <label class="control-label pull-right">
            <a href="{{ route('task_view', $id) }}" ><i class="fa fa-external-link"> </i> Manage</a>
        </label>
    </td>
</tr>