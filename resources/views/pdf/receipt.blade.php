<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Invoice - {{ $invoice->no }} </title>
</head><body style="margin:0;padding:0;background:#ffffff">
<table width="600" border="0" cellpadding="0" cellspacing="0" style="width:600px;margin:0 auto;padding:0;background:#fff;border: 0px solid #ccc;">
        <tr>
          <td colspan="2" style="margin:0;padding:0 0 8px 0;text-align:left;text-align:left"><img src="{{ asset('images/jpl-consolidated-branding.png') }}" style="width:200px;" /></td>
           <td colspan="3" style="margin:0;padding:0;text-align:right;vertical-align:middle"><span style="font-family:Arial, Helvetica, sans-serif;font-size:35px;margin:0;padding:0 0 0 50px;text-align:left;color:#000000;font-weight:bold;letter-spacing:-1px;height:90px;text-transform:uppercase">Receipt</span></td>
        </tr>
        <tr>
          <td width="" style="margin:0;padding:8px 0;text-align:left;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-family:Arial, Helvetica, sans-serif;font-size:12px;margin:0;padding:0;text-align:left;color:#000000;font-weight:normal;">120/8 Wells Street <br />Southbank Vic 3006</p></td>
          <td width="" style="margin:0;padding:8px 0;text-align:left;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-family:Arial, Helvetica, sans-serif;font-size:12px;margin:0;padding:0 0 0 8px;text-align:left;color:#000000;font-weight:normal;border-left:1px solid #000"><strong>P</strong>: 03 9042 7389</p></td>
          <td width="" style="margin:0;padding:8px 0;text-align:left;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-family:Arial, Helvetica, sans-serif;font-size:12px;margin:0;padding:0 0 0 8px;text-align:left;color:#000000;font-weight:normal;border-left:1px solid #000;white-space: nowrap;"><strong>E</strong>: accounts@jplms.com.au<br />
              <strong>W</strong>: www.jplms.com.au</p></td>
               <td width="28" rowspan="3" style="margin:0;padding:0;text-align:left;">&nbsp;</td>
          <td width="" rowspan="3" style="margin:0;padding:0;text-align:left;border:1px solid #000;vertical-align:middle">
           <p style="font-family:Arial, Helvetica, sans-serif;font-size:12px;margin:0 0 5px 10px;padding:0 0 0 0;text-align:left;color:#000000;font-weight:normal;"><strong style="font-weight:bold">Date</strong>:<br />
            {{ date('d/m/Y') }}</p> <p style="font-family:Arial, Helvetica, sans-serif;font-size:12px;margin:0 0 5px 10px;padding:0 0 0 0;text-align:left;color:#000000;font-weight:normal;"><strong style="font-weight:bold">Receipt No</strong>:<br />
            {{ $invoice->no }}</p><p style="font-family:Arial, Helvetica, sans-serif;font-size:12px;margin:0 0 5px 10px;padding:0 0 0 0;text-align:left;color:#000000;font-weight:normal;"><strong style="font-weight:bold">Transaction No</strong>: <br />
            {{ $invoice->transaction_id }}</p>
            </td>
         
        </tr>
         <tr>
          <td colspan="3" style="margin:0;padding:4px 0;"><span style="margin:0;padding:4px 0;text-align:right"><span style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: normal;line-height: 1.5em; margin-bottom: 8px;padding: 8px 0;text-align:right"><strong>ABN</strong>: 65 627 783 359 <span style="padding-left:15px;"><strong>ACN</strong>: 627783359</span></span></span></td>
        </tr>
        <tr>
          <td width="149" style="margin:0;padding:8px 0;text-align:left;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-family:Arial, Helvetica, sans-serif;font-size:12px;margin:0;padding:0;text-align:left;color:#000000;font-weight:normal;"><strong>{{ $invoice->client->company_name }}</strong><br />
              {{ $invoice->client->address }}<br />
              {{ $invoice->client->street }}<br />
              {{ $invoice->client->suburb }}<br/>
              {{ $invoice->client->state }} &nbsp; {{ $invoice->client->postcode }}</p></td>
          <td colspan="2" style="margin:0;padding:8px 0;text-align:left;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-family:Arial, Helvetica, sans-serif;font-size:12px;margin:0;padding:0 0 0 8px;text-align:left;color:#000000;font-weight:normal;border-left:1px solid #000"><strong>P</strong>:  {{ $invoice->client->owner_phone }}<br />
              <strong>E</strong>: {{ $invoice->client->owner_email }}</p></td>
        </tr>
        <tr>
          <td colspan="5" style="margin:0;padding:4px 0;"><table border="0" cellspacing="0" cellpadding="0" style="width:600px;">
              <tr>
                <td style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: bold;line-height: 1.5em; margin-bottom: 8px;padding: 4px 10px 4px 0;text-align:left;border-bottom:1px solid #000"><span style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: bold;line-height: 1.5em; margin-bottom: 8px;padding: 4px 0;text-align:left">Description</span></td>
                <td width="131" style="font-family:Arial, Helvetica, sans-serif;border-bottom: 1px solid #000000;color: #000000;font-size: 12px;font-weight: bold;line-height: 1.5em; margin-bottom: 8px;padding: 4px 2px 4px 10px;text-align:right"><span style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: bold;line-height: 1.5em; margin-bottom: 8px;padding: 4px 10px;text-align:left">Amount</span></td>
              </tr>
              <tr>
                <td style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: bold;line-height: 1.5em; margin-bottom: 8px;padding: 4px 10px;text-align:left;">&nbsp;</td>
                <td style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: bold;line-height: 1.5em; margin-bottom: 8px;padding: 4px 10px;;text-align:left">&nbsp;</td>
              </tr>
               @foreach($invoice->details as $detail)
               <?php $detailInfo = str_replace("\n","<br/>",$detail->description);?>
              <tr>
                <td style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: normal;line-height: 1.5em; margin-bottom: 8px;padding: 4px 5px 4px 0;text-align:left;"><?php echo $detailInfo;?></td>
                <td style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: normal;line-height: 1.5em; margin-bottom: 8px;padding: 4px 10px;;text-align:right">{{ number_format($detail->amount, 2) }}</td>
              </tr>
                @endforeach
              @if ( count($invoice->details)<10 )
                           <?php $j = 10 - count($invoice->details); ?>
                            @for($i = 0; $i < $j; $i++)
                             <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: normal;line-height: 1.5em; margin-bottom: 8px;padding: 4px 5px 4px 12px;text-align:left;">&nbsp;</td>
                                <td style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: normal;line-height: 1.5em; margin-bottom: 8px;padding: 4px 10px;;text-align:right">&nbsp;</td>
                              </tr>
                            @endfor
                            @endif
                    
            </table></td>
        </tr>
        <tr>
          <td colspan="5" style="height:15px;margin:0;padding:0;border-bottom:2px solid #000">&nbsp;</td>
        </tr>
       <tr>
          <td colspan="5" style="height:15px;margin:0;padding:0;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:10px 0 0 0;padding:0">

              <tr>
                <td width="31%" style="vertical-align:top"><table width="90%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0">
                    <tr>
                      <td height="33" style="vertical-align:middle;text-align:left"><span style="font-family:Arial, Helvetica, sans-serif;font-size:20px;margin:0;padding:0 0 0 0;text-align:left;color:#000000;font-weight:bold;letter-spacing:-1px;height:50px;text-transform:uppercase">Payment Method</span></td>
                    </tr>
                    <tr>
                      <td height="33" style="vertical-align:middle"><p style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: normal;line-height: 1.5em;margin:10px 0 0 0;padding:0;text-align:left;clear:both;text-transform:uppercase">{{ $invoice->payment_method}}</p></td>
                    </tr>
                    <tr>
                      <td><span style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: normal;line-height: 1.5em; margin:0;padding:0;text-align:left"> </span></td>
                    </tr>
                   
                  
                  </table></td>
                <td width="29%" style="vertical-align:top;text-align:center">&nbsp;</td>
                  
                <td width="40%" style="vertical-align:top">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:240px;float:right;">
                  <tr>
                      <td width="" style="font-family:Arial, Helvetica,sans-serif;font-size:13px;margin:0;padding:0;color:#000000;text-align:left;padding:3px 5px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;">Subtotal</td>
                      <td width="" style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;padding:0;color:#000000;text-align:left;padding:3px 10px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;border-left:1px solid #c3c2c3;text-align:right"><span style="text-align:left">$</span></td>
                      <td width="" style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;padding:0;color:#000000;text-align:left;padding:3px 10px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;text-align:right">{{ number_format($invoice->total['subtotal'], 2) }}</td>
                    </tr>
                      <tr>
                      <td style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;padding:0;color:#000000;text-align:left;padding:3px 5px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;">GST</td>
                      <td style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;color:#000000;text-align:left;padding:3px 10px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;border-left:1px solid #c3c2c3;text-align:right">$</td>
                      <td style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;color:#000000;text-align:left;padding:3px 10px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;text-align:right">{{ number_format($invoice->total['gst'], 2) }}</td>
                    </tr>
                     @php
                       $cardfee = $invoice->card_fee;
                       $lateCharges = $invoice->late_charges;

                       $totalFee = $cardfee +$invoice->total['total']+$lateCharges;
                       @endphp
                     <tr>
                      <td style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;padding:0;color:#000000;text-align:left;padding:3px 5px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;">Late payment Fee 10%</td>
                      <td style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;color:#000000;text-align:left;padding:3px 10px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;border-left:1px solid #c3c2c3;text-align:right">$</td>
                      <td style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;color:#000000;text-align:left;padding:3px 10px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;text-align:right">{{ number_format($lateCharges, 2) }}</td>
                    </tr>
                     <tr>
                      <td style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;padding:0;color:#000000;text-align:left;padding:3px 5px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;">Card Fee 2%</td>
                      <td style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;color:#000000;text-align:left;padding:3px 10px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;border-left:1px solid #c3c2c3;text-align:right">$</td>
                      <td style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;color:#000000;text-align:left;padding:3px 10px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;text-align:right">{{ number_format($cardfee, 2) }}</td>
                    </tr>
                    
                  <tr>
                      <td style="font-family:Arial, Helvetica, sans-serif;  background: #eeeeee;border-bottom: 1px solid #d0d0d0;color: #000000;font-size: 12px;font-weight: bold;line-height: 1.5em; margin-bottom: 8px;padding:3px 5px;;text-align:left">Total</td>
                      <td style="font-family:Arial, Helvetica, sans-serif;font-size:13px;margin:0;color:#000000;text-align:left;padding:3px 10px;line-height:21px;border-bottom:1px solid #c3c2c3;text-align:left;border-left:1px solid #c3c2c3;text-align:right;background: #eeeeee;">$</td>
                      <td style="font-family:Arial, Helvetica, sans-serif;  background: #eeeeee;border-bottom: 1px solid #d0d0d0;color: #000000;font-size: 12px;font-weight: bold;line-height: 1.5em; margin-bottom: 8px;padding: 4px 10px;text-align:right">{{ number_format($totalFee, 2) }}</td>
                    </tr>--}}
                 
                  </table>
 <div style="clear:both"></div>
                  <p style="font-family: Arial, Helvetica, sans-serif;font-size: 22px;color: #000000; text-align: right;margin:20px 0 0 20px;padding: clear: both;display: block;float: right;text-transform: uppercase;font-weight:bold">Total ${{ number_format($totalFee, 2) }}</p></td>
             
              </tr>
            </table></td>
        </tr>
        <tr>
          <td colspan="5" style="margin:0;padding:0;"><p style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: normal;line-height: 1.5em;margin:15px 0 0 0;padding:0;text-align:center;clear:both">THANK YOU FOR YOUR BUSINESS!</p>
            <p style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;font-weight: normal;line-height: 1.5em;margin:0;padding:0;text-align:center;clear:both"> JPL Consolidated As Fenix Global Pty Ltd</p>
            <p style="font-family:Arial, Helvetica, sans-serif;color: #000000;font-size: 11px;font-weight: normal;line-height: 1.5em;margin:0;padding:0;text-align:center;clear:both">Any claims regarding this invoice must be made in writing and within 7days of date of this invoice.</p></td>
        </tr>
      </table></td>
</table></body></html>