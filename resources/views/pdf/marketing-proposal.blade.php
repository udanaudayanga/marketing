<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
<title>Marketing Suite Proporsal</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Allerta" rel="stylesheet">
<style>
body {
  margin: 0;
  padding: 0;
  background: #f1f2f2;
}
html {
  margin: 0;
  padding: 0;
  background: #f1f2f2;
}
p {
  margin: 0;
  padding: 0;
}
ul.score {
  margin: 0 0 0 25px;
  padding: 0;
}
ul.score-error, ul.score-warn, ul.score-info {
  margin-bottom: 50px;
}
ul.score-error li {
  font-family: 'Allerta', sans-serif;
  font-size: 12px;
  font-weight: normal;
  background: url(http://www.jplmarketingsolutions.com.au/proposal/bullet-fail.png) no-repeat top left;
  list-style-type: none;
  margin: 0 0 5px 0;
  padding: 0 0 0 35px;
  color: #fff;
  line-height: 15px;
  letter-spacing: -0.5;
}
ul.score-warn li {
  font-family: 'Allerta', sans-serif;
  font-size: 12px;
  font-weight: normal;
  background: url(http://www.jplmarketingsolutions.com.au/proposal/bullet-warn.png) no-repeat top left;
  list-style-type: none;
  margin: 0 0 5px 0;
  padding: 0 0 0 35px;
  color: #fff;
  line-height: 15px;
  letter-spacing: -0.5;
}
ul.score-info li {
  font-family: 'Allerta', sans-serif;
  font-size: 12px;
  font-weight: normal;
  background: url(http://www.jplmarketingsolutions.com.au/proposal/bullet-info.png) no-repeat top left;
  list-style-type: none;
  margin: 0 0 5px 0;
  padding: 0 0 0 35px;
  color: #fff;
  line-height: 15px;
  letter-spacing: -0.5;
}
p.failed {
  font-family: 'Roboto', sans-serif;
  width: 150px;
  height: 130px;
  vertical-align: middle;
  font-size: 50px;
  color: #fff;
  text-align: center;
  background: url(http://www.jplmarketingsolutions.com.au/proposal/failed.png) no-repeat top left;
  line-height: 60px;
  padding-top: 20px;
  margin: 0 auto;
}
p.failed small {
  font-family: 'Roboto Condensed', sans-serif;
  font-size: 12px;
  font-weight: bold;
  display: block;
  letter-spacing: 1px;
  line-height: 15px;
}
p.warn {
  font-family: 'Roboto', sans-serif;
  width: 150px;
  height: 130px;
  vertical-align: middle;
  font-size: 50px;
  color: #fff;
  text-align: center;
  background: url(http://www.jplmarketingsolutions.com.au/proposal/warn.png) no-repeat top left;
  line-height: 60px;
  padding-top: 20px;
  margin: 0 auto;
}
p.warn small {
  font-family: 'Roboto Condensed', sans-serif;
  font-size: 12px;
  font-weight: bold;
  display: block;
  letter-spacing: 1px;
  line-height: 15px;
}
p.info {
  font-family: 'Roboto', sans-serif;
  width: 150px;
  height: 130px;
  vertical-align: middle;
  font-size: 50px;
  color: #fff;
  text-align: center;
  background: url(http://www.jplmarketingsolutions.com.au/proposal/info.png) no-repeat top left;
  line-height: 60px;
  padding-top: 20px;
  margin: 0 auto;
}
p.info small {
  font-family: 'Roboto Condensed', sans-serif;
  font-size: 12px;
  font-weight: bold;
  display: block;
  letter-spacing: 1px;
  line-height: 15px;
}
.setup ul li {
  font-family: 'Allerta', sans-serif;
  font-size: 10px;
  font-weight: normal;
  background: url(http://www.jplmarketingsolutions.com.au/proposal/bullet-1.png) no-repeat left 2px;
  list-style-type: none;
  margin: 0 0 5px 0;
  padding: 0 0 0 20px;
  color: #000;
  line-height: 10px;
  letter-spacing: -0.5;
}
.optimisation ul li {
  font-family: 'Allerta', sans-serif;
  font-size: 10px;
  font-weight: normal;
  background: url(http://www.jplmarketingsolutions.com.au/proposal/bullet-2.png) no-repeat left 2px;
  list-style-type: none;
  margin: 0 0 5px 0;
  padding: 0 0 0 20px;
  color: #000;
  line-height: 10px;
  letter-spacing: -0.5;
}
.compliance {
  height: 300px;
}
.compliance ul li {
  font-family: 'Allerta', sans-serif;
  font-size: 10px;
  font-weight: normal;
  background: url(http://www.jplmarketingsolutions.com.au/proposal/bullet-3.png) no-repeat left 2px;
  list-style-type: none;
  margin: 0 0 5px 0;
  padding: 0 0 0 20px;
  color: #000;
  line-height: 10px;
  letter-spacing: -0.5;
}
.seo ul li {
  font-family: 'Allerta', sans-serif;
  font-size: 10px;
  font-weight: normal;
  background: url(http://www.jplmarketingsolutions.com.au/proposal/bullet-4.png) no-repeat left 2px;
  list-style-type: none;
  margin: 0 0 5px 0;
  padding: 0 0 0 20px;
  color: #000;
  line-height: 10px;
  letter-spacing: -0.5;
}
.agreement p {
  font-family: 'Allerta', sans-serif;
  font-size: 8px;
  font-weight: normal;
  line-height: 0.6em;
  margin: 0 0 6px 0;
  padding: 0 15px;
}
.agreement ul {
  margin: 0 0 0 10px;
  padding: 0 15px;
  line-height: 0.7em;
}
.agreement ul li {
  font-family: 'Allerta', sans-serif;
  font-size: 10px;
  font-weight: normal;
  line-height: 0.7em;
  margin-bottom: 6px;
}
.agreement ol {
  margin:0 0 0 10px;
  padding:0 15px;
  line-height: 0.7em;
}
.agreement ol li {
  font-family: 'Allerta', sans-serif;
  font-size: 9px;
  font-weight: normal;
  line-height: 0.7em;
}
.agreement ul li p {
  font-size: 8px;
  padding: 0;
  line-height: 0.7em;
}
.agreement ul li p {
  font-size: 8px;
  padding: 0;
  line-height: 0.7em;
}
.agreement ul li ol {
  margin:0 0 0 10px;
  padding: 0;
}
.agreement ul li ol li {
  font-size: 8px;
  margin-bottom: 4px;
  line-height: 0.7em;
}
.agreement ul li ol li ol.alpha {
  list-style-type:lower-alpha;
  margin:0 0 0 10px;
  padding: 0;
}
.agreement ul li ol li ol.alpha li {
  font-size: 8px;
  line-height: 0.5em;
}
.summary {
  margin-top: -30px;
}
.summary p {
  font-family: 'Allerta', sans-serif;
  font-size: 9px;
  font-weight: normal;
  line-height: 0.7em;
}
.summary fieldset {
  margin: 0 10px 15px;
  padding: 25px 0 0;
}
.summary fieldset legend {
  font-family: 'Allerta', sans-serif;
  font-size: 9px;
  background:#355362;
  color:#fff;
  padding: 0 15px 2px;
}
.summary table {
  font-family: 'Allerta', sans-serif;
  font-size: 9px;
  font-weight: normal;
  line-height: 0.7em;
}
.summary table td {
  border-collapse:collapse;
}
.summary .account table td input {
  width: 120px;
  height: 7px;
  padding: 4px 5px 3px;
  border:1px solid #000;
}

</style>
</head>
<body>
<div style="width:600px;margin:0 auto;padding:0;background:#f1f2f2;height:775px;">
  <div style="margin:60px 47px 40px 0;position:relative"> <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-top.png" style="position:absolute;margin:-15px 0 0 128px;">
    <p style="font-family:'Anton', sans-serif;font-size:60px;font-weight:normal;margin:0 10px 0 0;padding:0;line-height:60px;text-align:right;text-transform:uppercase;letter-spacing:2px;">Marketing Suite</p>
    <p style="font-family:'Roboto Condensed', sans-serif;font-size:60px;font-weight:normal;letter-spacing:-2px;text-transform:uppercase;margin:0 10px 0 0;padding:0;line-height:40px;text-align:right;color:#f26522;">Proporsal</p>
    <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-right.png" style="position:absolute;margin: -18px 0 0 560px;"> </div>
  <div style="margin:0;padding:0;border-collapse:collapse;width:600px;"> <img src="http://www.jplmarketingsolutions.com.au/proposal/main-img.jpg" style="width:600px;margin:0;padding:0;border-collapse:collapse"> </div>
  <div style="margin:0;padding:0;background:#f26522 url(http://www.jplmarketingsolutions.com.au/proposal/landing-bg.png) no-repeat center;background-size:cover;width:600px;height:158px;border-collapse:collapse;">
    <div style="padding: 60px 0 0 40px;">
      <p style="font-family:'Roboto', sans-serif;font-size:20px;font-weight:normal;margin:0;padding:0 0 0 15px;line-height:15px;color:#fff;font-weight:bold;border-left: 3px solid #fff">1300 345 954<br>
        <small style="font-family:'Roboto', sans-serif;font-size:12px;font-weight:normal;margin:0;padding:0;line-height:11px;color:#fff;font-weight:normal;"> www.jplms.com.au<br>
        enquiries@jplms.com.au</small></p>
    </div>
  </div>
  <div style="margin:0;padding:0;border-collapse:collapse;width:600px;">
    <div style="width:50%;float:left"><img src="http://www.jplmarketingsolutions.com.au/proposal/jplms.png" style="margin: 50px 0 0 20px"></div>
    <div style="width:50%;float:left;text-align:right"><img src="http://www.jplmarketingsolutions.com.au/proposal/jpl-logo.png" style="margin: 50px 20px 0 0"></div>
  </div>
</div>
<div style="width:600px;margin:0 auto;clear:both;height:775px;background:url(http://www.jplmarketingsolutions.com.au/proposal/bg-blue.png) no-repeat top center;background-size:100%;">
  <div style="margin:0 47px 40px 0;position:relative;text-align:left;padding-top:20px"> <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-top.png" style="position:absolute;margin:15px 0 0 33px;left:0">
    <p style="font-family:'Anton', sans-serif;font-size:40px;font-weight:normal;letter-spacing:1px;margin:40px 0 0 30px;line-height:30px;padding:0;text-transform:uppercase;color:#fff">COMPLIANCE SCORE</p>
    <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-right.png" style="position:absolute;margin: -23px 0 0 340px;left:0">
    <p style="font-family:'Roboto', sans-serif;font-size:15px;font-weight:normal;letter-spacing:0;margin-left:33px;padding:0;line-height:10px;color:#fff">www.jplms.com.au</p>
    <div style="width:120px;height:120px;position:absolute;right:0;top:10px;margin:0 -30px 0 0;background:url(http://www.jplmarketingsolutions.com.au/proposal/green-circle.png) no-repeat top center;background-size:cover;">
      <p style="font-family:'Anton', sans-serif;font-size:40px;font-weight:normal;margin:0;padding:0;line-height:60px;text-align:center;color:#8dc63f;padding-top:0px">{{$totalScore}}</p>
      <p style="font-family:'Anton', sans-serif;font-size:12px;position:absolute;left:55px;top:27px;margin:-0 0 0 30px;color:#fff">%</p>
    </div>
  </div>
  <div style="height:595px;">
    <div style="clear:both;">
      <div style="width:250px;float:left;border-right: 1px solid #fff">
        <p class="failed">{{$snaperrorcount}}<br>
          <small style="text-transform:uppercase">Failed Checks</small></p>
      </div>
      <div style="width:350px;float:left;margin-bottom:30px;">
        <ul class="score-error">
          @foreach ($issueErrorInfoAll as $error)
          <li>{{ $error['title_page']}}</li>
        @endforeach
        </ul>
      </div>
    </div>
    <div style="clear:both;">
      <div style="width:250px;float:left;border-right: 1px solid #fff">
        <p class="warn">{{$snapwarncount}}<br>
          <small style="text-transform:uppercase">Warnings</small></p>
      </div>
      <div style="width:350px;float:left;margin-bottom:30px;">
        <ul class="score-warn">
           @foreach ($issueInfoWarnAll as $warn)
           <li>{{ $warn['title_page']}}</li>
           @endforeach
        </ul>
      </div>
    </div>
    <div style="clear:both;">
      <div style="width:250px;float:left;border-right: 1px solid #fff">
        <p class="info">{{$snapnoticecount}}<br>
          <small style="text-transform:uppercase">Notices</small></p>
      </div>
      <div style="width:350px;float:left;margin-bottom:30px;">
        <ul class="score-info">
           @foreach ($issueInfoNoticAll as $notice)
           <li>{{ $notice['title_page']}}</li>
           @endforeach
        </ul>
      </div>
    </div>
  </div>
  <div style="height:70px;">
    <div style="width:75%;float:left;text-align:left;font-family:'Roboto Condensed', sans-serif;font-size:13px;font-weight:normal;margin: 0 0 0 0;">
      <p style="margin:0;padding:30px 0 0 20px;letter-spacing:3px">www.jplms.com.au <strong style="padding:0 20px;">|</strong> info@jplms.com.au</p>
    </div>
    <div style="width:20%;float:left;text-align:right"><img src="http://www.jplmarketingsolutions.com.au/proposal/jpl-logo.png" style="margin: 20px 0 0 0;height:40px"></div>
  </div>
</div>

<div style="width:600px;margin:0 auto;clear:both;height:775px;page-break-before:left;background:url(http://www.jplmarketingsolutions.com.au/proposal/bg-ash.png) no-repeat top center;background-size:100%;">
  <div style="margin:0 47px 40px 0;position:relative;text-align:left;padding-top:20px"> <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-top.png" style="position:absolute;margin:15px 0 0 33px;left:0">
    <p style="font-family:'Anton', sans-serif;font-size:40px;font-weight:normal;letter-spacing:1px;margin:40px 0 0 30px;line-height:35px;padding:0;text-transform:uppercase;color:#355362">works/inclusions</p>
    <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-right.png" style="position:absolute;margin: -23px 0 0 340px;left:0"> </div>
    <div style="height:600px;">
      <div class="setup">
      <img src="http://www.jplmarketingsolutions.com.au/proposal/setup.png" style="position:absolute;margin: -23px 0 0 440px;left:0">
      <ul>
        <li>Setup Account in JPL Marketing Suite (JPLMS).</li>
        <li>Redevelop site to meet latest design and development trends/Requirement.</li>
        <li>Make sure essential pages and contact information are present.</li>
        <li>Install Google Analytics.</li>
        <li>Apply and submit sitemap to Google Search Console and Bing Webmaster Tools.</li>
        <li>Create/Update and upload robot.txt.</li>
        <li>Install an SSL certificate.</li>
        <li>Check and optimize for Browser and Device compatibility (Mobile).</li>
        <li>Claim your business online (Google Business).</li>
        <li>Add Structured Data Markup to your website.</li>
      </ul>
      </div>
      <div style="width:350px; height:4px;background:#bcbec0;margin: 50px 0 50px 40px"></div>
      <div class="optimisation">
        <img src="http://www.jplmarketingsolutions.com.au/proposal/optimisation.png" style="position:absolute;margin: -103px 0 0 440px;left:0">
        <ul style="width:280px;float:left">
          <li>Title tags application</li>
          <li>Description and Keywords in Meta tags and permalinks</li>
          <li>Proper placemen of headings (h1 to h6) and integration with keywords</li>
          <li>Creating image links and optimizing the Alt text and anchor links</li>
          <li>Creating proper robots.txt files for access prevention of specific folders</li>
          <li>Creating XML sitemaps for the website and optimizing cross-linking</li>
          <li>Design and implementation of customized404 page with a search box</li>
          <li>Links to all the major social platforms</li>
          <li>Adding RSS feeds and the compliance - Validation of html code with W3C standards</li>
        </ul>
        <ul style="width:200px;float:left">
          <li>Social Bookmarking</li>
          <li>Social Networking Profiles</li>
          <li>Directory Submissions</li>
          <li>Article Submissions</li>
          <li>Blog Commenting</li>
          <li>Forum Posts/Links</li>
          <li>Inforgraphic Submission</li>
          <li>Image Submissions</li>
          <li>Slide sharing</li>
          <li>Video Marketing</li>
          <li>Web 2.0 Properties</li>
          <li>Document and PDF Submissions</li>
        </ul>
      </div>
  </div>
  
  <div style="height:70px;">
    <div style="width:75%;float:left;text-align:left;font-family:'Roboto Condensed', sans-serif;font-size:13px;font-weight:normal;margin: 0 0 0 0;">
      <p style="margin:0;padding:30px 0 0 20px;letter-spacing:3px">www.jplms.com.au <strong style="padding:0 20px;">|</strong> info@jplms.com.au</p>
    </div>
    <div style="width:20%;float:left;text-align:right"><img src="http://www.jplmarketingsolutions.com.au/proposal/jpl-logo.png" style="margin: 20px 0 0 0;height:40px"></div>
  </div>
</div>

<div style="width:600px;margin:0 auto;clear:both;height:775px;page-break-before:left;background:url(http://www.jplmarketingsolutions.com.au/proposal/bg-ash.png) no-repeat top center;background-size:100%;">
  <div style="margin:0 47px 40px 0;position:relative;text-align:left;padding-top:20px"> <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-top.png" style="position:absolute;margin:15px 0 0 33px;left:0">
    <p style="font-family:'Anton', sans-serif;font-size:40px;font-weight:normal;letter-spacing:1px;margin:40px 0 0 30px;line-height:35px;padding:0;text-transform:uppercase;color:#355362">works/inclusions</p>
    <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-right.png" style="position:absolute;margin: -23px 0 0 340px;left:0"> </div>
    <div style="height:600px;">
      <div class="compliance">
        <img src="http://www.jplmarketingsolutions.com.au/proposal/compliance.png" style="position:absolute;margin: -123px 0 0 440px;left:0">
        <ul style="width:230px;float:left">
          <li>Site Crawl.</li>
          <li>Check metadata.</li>
          <li>Check status errors to fix or redirect.</li>
          <li>Check xml sitemap and robots file.</li>
          <li>Analyze competitor performance comparison.</li>
          <li>Review back link report.</li>
          <li>Check website's loading speed.</li>
          <li>Check for broken links.</li>
          <li>Validate your site's HTML and CSS.</li>
          <li>Review your goal keywords.</li>
          <li>Optimize page titles, meta descriptions, and content (including images!) for your goal keywords.</li>
          <li>Check internal website linking.</li>
          <li>Page optimizations.</li>
        </ul>
        <ul style="width:230px;float:left">
        <li>Ensure each page has a unique target keyword.</li>
          <li>Perform a content review to make sure pages are using heading tag properly.</li>
          <li>Review website's design to ensure it still performs as expected on desktop and mobile device.</li>
          <li>Check for 301 and 302 redirects.</li>
          <li>Check for broken links and other crawl errors.</li>
          <li>Check your URLs to ensure each pages URL aligns with its target keywords.</li>
          <li>Review page content and make sure every page's keyword is incorporating synonyms.</li>
          <li>Page content review, ensure every page has at least 500 words. Remove any pages that can't be improved.</li>
          <li>Hosting environment health check.</li>
        </ul>
      </div>
      <div style="width:350px; height:4px;background:#bcbec0;margin: 50px 0 50px 40px;float:left;clear:both"></div>
      <div class="seo" style="width:600px">
        <img src="http://www.jplmarketingsolutions.com.au/proposal/seo.png" style="position:absolute;margin: 3px 0 0 440px;left:0">
        <ul>
        </ul>
      </div>
  </div>
  
  <div style="height:70px;">
    <div style="width:75%;float:left;text-align:left;font-family:'Roboto Condensed', sans-serif;font-size:13px;font-weight:normal;margin: 0 0 0 0;">
      <p style="margin:0;padding:30px 0 0 20px;letter-spacing:3px">www.jplms.com.au <strong style="padding:0 20px;">|</strong> info@jplms.com.au</p>
    </div>
    <div style="width:20%;float:left;text-align:right"><img src="http://www.jplmarketingsolutions.com.au/proposal/jpl-logo.png" style="margin: 20px 0 0 0;height:40px"></div>
  </div>
</div>

<div style="width:600px;margin:0 auto;clear:both;height:775px;page-break-before:left;background:#fff;background-size:100%;">
  <div style="margin:0 47px 40px 0;position:relative;text-align:left;padding-top:20px"> <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-top.png" style="position:absolute;margin:15px 0 0 33px;left:0">
    <p style="font-family:'Anton', sans-serif;font-size:40px;font-weight:normal;letter-spacing:1px;margin:40px 0 0 30px;line-height:35px;padding:0;text-transform:uppercase;color:#355362">The Grid</p>
    <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-right.png" style="position:absolute;margin: -23px 0 0 200px;left:0"> </div>
  <div style="height:600px;">
    <p style="font-family: 'Allerta', sans-serif;font-size: 10px;font-weight: normal;line-height: 10px;padding:0 30px;margin-top:-20px;">"The Grid" is a marketing discipline focused on growing visibility in organic (non-paid) search engine results. The Grid encompasses both the technical and creative elements required to improve rankings, drive traffic, and increase awareness in search engines. Its function is to cast a wider yet focused SEO net that your primary domain, to target specific products, services or keywords to help enhance the credibility and searchability of your business. The branches of the grid provide for a more focused and tailored approach to specific industry and keyword sectors, allowing this strategy to emerse itself in the specific requirements while keeping your primary domain focused on the overall growth objectives. </p>
    <img src="http://www.jplmarketingsolutions.com.au/proposal/grid.png" style="position:relative;margin: 50px 0 0 10px;left:0">
    <div style="margin: 0 0 0 125px;">
      <p style="float:left;font-family: 'Allerta', sans-serif;font-size: 12px;font-weight: normal;line-height: 14px;padding:15px 25px;background:#d1d3d4">Setup<br>
        Monthly Subscription<br>
        Debit Date<br>
        Minimum Term<br>
        Cancellation</p>
       <p style="float:left;font-family: 'Allerta', sans-serif;font-size: 12px;font-weight: normal;line-height: 14px;padding:15px 25px;background:#e6e7e8">$0<br>
       $500 p/m<br>
       5th of each Month<br>
       None<br>
       30 Days Notice</p>
    </div>
  </div>
  <div style="height:70px;background:#fff">
    <div style="width:75%;float:left;text-align:left;font-family:'Roboto Condensed', sans-serif;font-size:13px;font-weight:normal;margin: 0 0 0 0;">
      <p style="margin:0;padding:30px 0 0 20px;letter-spacing:3px">www.jplms.com.au <strong style="padding:0 20px;">|</strong> info@jplms.com.au</p>
    </div>
    <div style="width:20%;float:left;text-align:right"><img src="http://www.jplmarketingsolutions.com.au/proposal/jpl-logo.png" style="margin: 20px 0 0 0;height:40px"></div>
  </div>
</div>

<div style="width:600px;margin:0 auto;clear:both;height:775px;page-break-before:left">
  <div style="margin:20px 47px 40px 0;position:relative;text-align:left"> <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-top.png" style="position:absolute;margin:15px 0 0 33px;left:0">
    <p style="font-family:'Anton', sans-serif;font-size:40px;font-weight:normal;letter-spacing:1px;margin:40px 0 0 30px;line-height:35px;padding:0;text-transform:uppercase;color:#355362">Summary</p>
    <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-right.png" style="position:absolute;margin: -23px 0 0 200px;left:0"> </div>
  <div style="height:610px;" class="summary">
    <fieldset style="border:1px solid #355362;height:110px;">
      <legend>Subscription Details</legend>
      <div style="width:50%;float:left">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;font-size:10px;margin-left:8px;">
          <tr>
            <td style="text-align:right;padding:5px 15px 4px 0;width:120px">Setup</td>
            <td>$5,000</td>
          </tr>
          <tr>
            <td style="text-align:right;padding:5px 15px 4px 0">Monthly Subscription</td>
            <td>$2,000 p/m</td>
          </tr>
          <tr>
            <td style="text-align:right;padding:5px 15px 4px 0">Debit Date</td>
            <td>5th of each Month</td>
          </tr>
          <tr>
            <td style="text-align:right;padding:5px 15px 4px 0">Minimum Term</td>
            <td>4 Months</td>
          </tr>
          <tr>
            <td style="text-align:right;padding:5px 15px 4px 0">Cancellation</td>
            <td>30 Days Notice</td>
          </tr>
        </table>
      </div>
      <div style="width:50%;float:left">
      <fieldset style="border:1px solid #355362;height:60px;margin:0 20px 0 0;">
         <legend style="font-size:8px;">Add Ons</legend>
         <table border="0" cellspacing="0" cellpadding="0" style="padding:0 15px;width:250px">
          <tr>
            <td style="padding:3px;width:70px">&nbsp;</td>
            <td style="padding:3px;width:50px">Qty</td>
            <td style="padding:3px;width:150px">Monthly Total</td>
          </tr>
          <tr>
            <td style="padding:3px"><label style="padding-bottom:2px">The Grid</label><input type="checkbox" value="input type checkbox" style="margin:5px 15px 0px 5px" /></td>
            <td style="padding:3px"><input type="text" value="" style="width:20px;height: 7px;margin:0 5px;padding: 4px 5px 3px;border:1px solid #000;"></td>
            <td style="padding:3px"><input type="text" value=""  style="width:70px;height: 7px;margin:0 5px;padding: 4px 5px 3px;border:1px solid #000;"/></td>
          </tr>
      </table>

      </fieldset>
      <p style="font-size:7px;margin:5px 20px 0 0;padding:0;text-align:right">*price excluding GST</p>
      </div>
    </fieldset>
    <fieldset style="border:1px solid #355362;height:170px;" class="account">
      <legend>Subscription Details</legend>
      <div style="width:50%;float:left">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
          <tr>
            <td style="text-align:right;padding-right:15px;width:100px">Company Name</td>
            <td><input type="text" value="" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:15px">Street</td>
            <td><input type="text" value="" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:15px">State</td>
            <td><input type="text" value="" /></td>
          </tr>
        </table>
        <div style="position:absolute;transform: rotate(-90deg);margin:0;top:20px;font-family: 'Allerta', sans-serif;font-size:9px;">Address</div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
          <tr>
            <td style="text-align:right;padding-right:15px;width:100px">First Name</td>
            <td><input type="text" value="" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:15px">Phone</td>
            <td><input type="text" value="" /></td>
          </tr>
        </table>
        <div style="position:absolute;transform: rotate(-90deg);margin:0;top:85px;font-family: 'Allerta', sans-serif;font-size:9px;">Admin</div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="text-align:right;padding-right:15px;width:100px">First Name</td>
            <td><input type="text" value="" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:15px">Phone</td>
            <td><input type="text" value="" /></td>
          </tr>
        </table>
        <div style="position:absolute;transform: rotate(-90deg);margin:0;top:135px;font-family: 'Allerta', sans-serif;font-size:9px;">Admin</div>

      </div>
      <div style="width:50%;float:left">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
          <tr>
            <td style="text-align:right;padding-right:15px;width:100px">ABN</td>
            <td><input type="text" value="" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:15px">Suburb</td>
            <td><input type="text" value="" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:15px">Postcode</td>
            <td><input type="text" value="" /></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
          <tr>
            <td style="text-align:right;padding-right:15px;width:100px">First Name</td>
            <td><input type="text" value="" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:15px">Phone</td>
            <td><input type="text" value="" /></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="text-align:right;padding-right:15px;width:100px">First Name</td>
            <td><input type="text" value="" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:15px">Phone</td>
            <td><input type="text" value="" /></td>
          </tr>
        </table>
      </div>
    </fieldset>
    <fieldset style="border:1px solid #355362;height:218px;width:52%;float:left">
      <legend>Payment Details</legend>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;padding:0 15px;">
          <tr>
            <td style="text-align:right;padding-right:10px;width:87px;vertical-align:middle;padding-bottom:8px;">Cardholders Name</td>
            <td style="padding-bottom:8px;"><input type="text" value="" style="width:159px;height: 7px;margin:0 5px 0 5px;padding: 4px 5px 3px;border:1px solid #000;" /><br>
            <span style="font-size:7px;top:0;margin:0 5px 8px 5px">as shown on card</span></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:10px;vertical-align:middle">Card Number</td>
            <td><input type="text" value="" style="width:30px;height: 7px;margin:0 1px 1px 5px;padding: 4px 5px 3px;border:1px solid #000;" /><input type="text" value="" style="width:30px;height: 7px;margin:0 1px 1px 0px;padding: 4px 5px 3px;border:1px solid #000;" /><input type="text" value="" style="width:30px;height: 7px;margin:0 1px 1px 0px;padding: 4px 5px 3px;border:1px solid #000;" /><input type="text" value="" style="width:30px;height: 7px;margin:0 1px 1px 0px;padding: 4px 5px 3px;border:1px solid #000;" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:10px;">Expiry</td>
            <td><input type="text" value="" style="width:30px;height: 7px;margin:0 1px 8px 5px;padding: 4px 5px 3px;border:1px solid #000;" /><input type="text" value="" style="width:30px;height: 7px;margin:0 1px 0 0;padding: 4px 5px 3px;border:1px solid #000;" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:10px;vertical-align:middle">CVV</td>
            <td><input type="text" value="" style="width:30px;height: 7px;margin:0 1px 8px 5px;padding: 4px 5px 3px;border:1px solid #000;" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:10px;">Signature</td>
            <td><input type="text" value="" style="width:159px;height: 60px;margin:0 5px 8px;padding: 4px 5px 3px;border:1px solid #000;" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:10px;">Date</td>
            <td><input type="text" value=""  style="width:120px;height: 7px;margin:0 5px 8px;padding: 4px 5px 3px;border:1px solid #000;" /></td>
          </tr>
        </table>
    </fieldset>
    <fieldset style="border:3px solid #355362;height:215px;width:40%;float:left">
      <legend>Direct Debit Authorisation</legend>
      <p style="font-family: 'Allerta', sans-serif;font-size: 8px;font-weight: normal;padding:0 15px;line-height:8px">We acknowledge that this is direct debit arrangement is governed by the terms of the direct debit request service agreement overleaf</p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:25px;padding:0 15px;">
          <tr>
            <td style="text-align:right;padding-right:10px;width:50px;">Name</td>
            <td><input type="text" value="" style="width:120px;height: 7px;margin:0 5px 8px;padding: 4px 5px 3px;border:1px solid #000;" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:10px;">Signature</td>
            <td><input type="text" value="" style="width:120px;height: 70px;margin:0 5px 8px;padding: 4px 5px 3px;border:1px solid #000;" /></td>
          </tr>
          <tr>
            <td style="text-align:right;padding-right:10px;">Date</td>
            <td><input type="text" value=""  style="width:120px;height: 7px;margin:0 5px 8px;padding: 4px 5px 3px;border:1px solid #000;" /></td>
          </tr>
        </table>
    </fieldset>
  </div>
  <div style="height:70px;">
    <div style="width:75%;float:left;text-align:left;font-family:'Roboto Condensed', sans-serif;font-size:13px;font-weight:normal;margin: 0 0 0 0;">
      <p style="margin:0;padding:30px 0 0 20px;letter-spacing:3px">www.jplms.com.au <strong style="padding:0 20px;">|</strong> info@jplms.com.au</p>
    </div>
    <div style="width:20%;float:left;text-align:right"><img src="http://www.jplmarketingsolutions.com.au/proposal/jpl-logo.png" style="margin: 20px 0 0 0;height:40px"></div>
  </div>
</div>

<div style="width:600px;margin:0 auto;clear:both;height:775px;page-break-before:left">
  <div style="margin:20px 47px 40px 0;position:relative;text-align:left"> <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-top.png" style="position:absolute;margin:15px 0 0 33px;left:0">
    <p style="font-family:'Anton', sans-serif;font-size:40px;font-weight:normal;letter-spacing:1px;margin:40px 0 0 30px;line-height:35px;padding:0;text-transform:uppercase;color:#355362">Service Agreement</p>
    <img src="http://www.jplmarketingsolutions.com.au/proposal/hd-right.png" style="position:absolute;margin: -23px 0 0 350px;left:0"> </div>
  <div style="height:600px;" class="agreement">
    <div style="width:50%;float:left;margin-top:-10px">
      <ul>
      <p style="padding-left:0">The meaning of words printed like this in this Direct Debit RequestService Agreement is explained in 8 below.</p>
      
        <li>Debiting your account
          <ol>
            <li>By signing a direct debit request, you have authorised us to arrange for funds to be debited from your account.</li>
            <li>We will arrange for your financial institution to debit your account in accordance with your instructions given to us in the Payment Option Schedule. If, however, a debit payment is due on a day:
            <ol class="alpha">
              <li>which is not contained in a particular month; or</li>
              <li>which is not a business day, then the debit payment will be made on the last day of that month or on the preceding business day respectively. If you are uncertain as to when a debit pay ment will be processed, you should contact your financial institution for assistance.</li>
            </ol>
            </li>
          </ol>
        </li>
        <li>Changes by us
        <p>We may vary any details of this agreement or the direct debit request at any time (including cancelling it). We will give you notice in writing of any such change at least fourteen (14) days before the change takes effect.</p></li>
        <li>Changes by you <p>You may request to stop or defer a debit payment or alter, suspend or cancel the direct debit request at any time. When we receive such a request, we will inform you of our notification requirements for such a request.</p></li>
        <li>Your Obligations
          <ol>
            <li>It is your responsibility to ensure that there are sufficient clear
funds available in your account to allow a debit payment to be
made in accordance with the direct debit request.</li>
            <li>If there are insufficient clear funds in your account to meet a
debit payment:
              <ol class="alpha">
                <li>you may be charged a fee and/or interest by your
       financial institution;</li>
                <li>you may also incur fees or charges imposed or incurred
        by us, and</li>
                <li>you must arrange for the debit payment to be made by
        another method or arrange for sufficient clear funds to be in
        your account by an agreed time so that we can process the
       debit payment.</li>
              </ol>
            </li>
            <li>You should check your account statement to verify that the amounts debited from your account are correct.</li>
            <li>If we are liable to pay goods and services tax ("GST") on a supply made by us in connection with this agreement, then you agree to pay us on demand an amount equal to the consideration payable or the supply multiplied by the prevailing GST rate.</li>
          </ol>
        </li>
        </ul>
      
    </div>
    <div style="width:50%;float:left;margin-top:0">
    <ul>
      <li>Dispute
        <ol>
          <li>If you believe that there has been an error in debiting your account, you should notify us immediately.</li>
          <li>We will investigate and deal promptly and in good faith with any such query, claim or complaint. If your query, claim or complaint cannot be resolved to your satisfaction in that call, we will inform you at that time of the length of time which we estimate the investigation will take.</li>
          <li>If we conclude as a result of our investigations that your account has been incorrectly debited we will adjust your account (including interest and charges) accordingly by directly crediting your account or sending you a refund cheque at our discretion. We will also notify you of the adjustment either orally or in writing.</li>
          <li>If we conclude as a result of our investigations that your account has not been incorrectly debited we will respond to your query by providing you with reasons and any evidence for this finding.</li>
          <li>Any queries you may have about an error made in debiting your account should be directed to us in the first instance so that we can attempt to resolve the matter between us and you. If we cannot resolve the matter you can still refer it to your financial institution which will obtain details from you of the disputed transaction and may lodge a claim on your behalf.</li>
        </ol>
      </li>
      <li>Accounts
        <ol>
          <li>You should check:
            <ol class="alpha">
              <li>with your financial institution whether direct debiting is
             available from your account as direct debiting is not
             available on all accounts offered by financial institutions;</li>
              <li>that your account details which you have provided to us
           are correct by checking them against a recent account
          statement;</li>
              <li>with your financial institution if you are uncertain about
      either of the above matters before completing the direct
      debit request.</li>
            </ol>
          </li>
        </ol>
      </li>
      <li>Confidentiality
        <ol>
          <li>We will keep any information (including your account details) in your direct debit request confidential. We will make reasonable efforts to keep any such information that we have about you secure and to ensure that any of our employees or agents who have access to information about you do not make any unauthorised use, modification, reproduction or disclosure of that information.</li>
          <li>We will only disclose information that we have about you:
            <ol class="alpha">
              <li>to the extent specifically required or authorised by law; or</li>
              <li>for the purposes of this agreement (including disclosing information in connection with any query or claim); or</li>
              <li>with your implied or express consent.</li>
            </ol>
          </li>
        </ol>
      </li>
      </ul>
    </div>    
  
  </div>
  <div style="height:70px;">
    <div style="width:75%;float:left;text-align:left;font-family:'Roboto Condensed', sans-serif;font-size:13px;font-weight:normal;margin: 0 0 0 0;">
      <p style="margin:0;padding:30px 0 0 20px;letter-spacing:3px">www.jplms.com.au <strong style="padding:0 20px;">|</strong> info@jplms.com.au</p>
    </div>
    <div style="width:20%;float:left;text-align:right"><img src="http://www.jplmarketingsolutions.com.au/proposal/jpl-logo.png" style="margin: 20px 0 0 0;height:40px"></div>
  </div>
</div>



</body>
</html>