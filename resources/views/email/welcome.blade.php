@extends('email.layout')

@section('body')
    <p style="font-family:Arial, Helvetica, sans-serif;font-size:24px;line-height:34px;color:#000;padding:0;margin:0 0 20px 0;font-weight:bold;">Welcome,</p>
    <p style="font-family:Arial, Helvetica, sans-serif;font-size:13px;line-height:18px;color:#333;padding:0;margin:0 0 20px 0">
        Hi {{ $name }},
        <br/><br/>
        Please use bellow PIN in order to access your JPLMS account. <br>
        <strong> {{ $pin }}</strong>
    </p>
    <p style="font-family:Arial, Helvetica, sans-serif;font-size:13px;line-height:18px;color:#333;padding:0;margin:0 0 20px 0">
    <div align="center" class="button-container center" style="Margin-right: 10px;Margin-left: 10px;">
        <div style="line-height:15px;font-size:1px">&nbsp;</div>
       
        <a href="https://marketing.jplms.com.au/login" target="_blank" style="color: #ffffff; text-decoration: none;">
   
            <!--[if mso]>
            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://pm.jplms.com.au/login" style="height:42px; v-text-anchor:middle; width:146px;" arcsize="60%" strokecolor="#C7702E" fillcolor="#C7702E" >
                <w:anchorlock/><center style="color:#ffffff; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:16px;">
            <![endif]-->
            <!--[if !mso]><!-->
            <div style="color: #ffffff; background-color: #C7702E; border-radius: 25px; -webkit-border-radius: 25px; -moz-border-radius: 25px; max-width: 126px; width: 25%; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 5px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; text-align: center;">
                <!--<![endif]-->
                <span style="font-size:16px;line-height:32px;"><span style="font-size: 14px; line-height: 28px;" data-mce-style="font-size: 14px;" mce-data-marked="1">Login</span></span>
                <!--[if !mso]><!-->
            </div>
            <!--<![endif]-->
            <!--[if mso]>
            </center>
            </v:roundrect>
            <![endif]-->
        </a>
        <div style="line-height:10px;font-size:1px">&nbsp;</div>
    </div>
    </p>
@endsection