@extends('email.involayout')

@section('body')
    <p style="font-family:Arial, Helvetica, sans-serif;font-size:13px;line-height:34px;color:#000;padding:0;margin:0 0 20px 0;font-weight:normal;">Dear {{ $invoice->client->owner_name }},</p>
    <p style="font-family:Arial, Helvetica, sans-serif;font-size:13px;line-height:18px;color:#000;padding:0;margin:0 0 20px 0">Please find attached reciept <strong>{{ $invoice->no }}</strong>.</p>
    <p style="font-family:Arial, Helvetica, sans-serif;font-size:13px;line-height:18px;color:#000;padding:0;margin:0 0 20px 0">
    Regards,<br />
    JPLMS Accounts<br />
    accounts@jplms.com.au<br /><br />
    <img src="{{ asset('/images/jplms-logo.png') }}" />
    </p>
      <p style="font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:11px;color:#000;padding:0;margin:0 0 20px 0;font-weight:normal;">IMPORTANT:<br />
This e-mail (and any attachments to this e-mail) is for the exclusive use of the person, firm or corporation to which it is intended and may contain information that by law is privileged, confidential or protected by copyright. If you are not the intended recipient or the person responsible for delivering this e-mail to the intended recipient, you are notified that any use, disclosure, distribution, printing or copying of this e-mail transmission is prohibited by law and that the contents must be kept strictly confidential. If you have received this e-mail in error, kindly notify us immediately on 0405 275 022 or respond to the sender by return e-mail. The original transmission and copies of this e-mail must be deleted. JPLMS accepts no responsibility for any viruses this e-mail may contain. This notice should not be removed.</p>
    </p>
@endsection