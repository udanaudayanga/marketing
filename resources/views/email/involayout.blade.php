<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>JPLMS Client Interface Email</title>
</head>
<body style="background:#e7e7e7;">
<div>

    <table width="680" border="0" align="center" cellpadding="0" cellspacing="0" style="width:680px;margin:0 auto;background:#fff;">
        <tr>
            <td style="width:680px;height:80px;vertical-align:top;background:#e0e0e0"><img src="{{ asset('/images/jplms-client-interface-logo.png') }}" width="343" height="65" alt="JPLMS Client Interface" style="margin:20px;" /></td>
        </tr>
        <tr>
            <td style="padding:45px 30px 30px 30px;">
                @yield('body')
            </td>
        </tr>
        <tr>
            <td style="width:680px;height:50px;vertical-align:bottom;background:#000;border-top:5px solid #ccc;line-height:50px;text-align:center"><p style="font-family:Arial, Helvetica, sans-serif;font-size:11px;color:#fff;padding:0;margin:0 0 0 20px">Copyright &copy; 2017 JPL Marketing Solutions. All rights reserved.</p></td>
        </tr>
    </table>
</div>
</body>
</html>
