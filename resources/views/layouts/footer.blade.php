 <footer>
      <p><strong>JPL CONSOLIDATED PTY LTD</strong><br>
  <strong>ABN</strong>: 65 627 783 359 <strong>ACN</strong>: 627783359<br>
  120/8 Wells Street Southbank Vic 3006 Australia<br>
  03 9042 7389<br>
  <a href="mailto:accounts@jplms.com.au">accounts@jplms.com.au</a><br>
  <a href="#" data-toggle="modal" data-target="#terms">Terms and Conditions</a></p>
  <p>&copy; Copyright <script type="text/javascript">document.write(new Date().getFullYear());</script> <span>JPLMS</span>. All rights reserved.</p>
    </footer>


    <div class="modal fade" id="terms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <h4 class="modal-title">Terms and Conditions</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="terms">
        <ul>
          <li>Definitions
            <ul>
              <li><strong>Agreement</strong>: These Terms and Conditions, as well as the Proposal (signed or unsigned), or verbal, written or email instructions to commence work.</li>
              <li><strong>JPLMS</strong>: JPLMS, the supplier of the web development solution, as well as any individual working for JPLMS, shareholder or director.</li>
              <li><strong>Purchaser</strong>: The company, person or entity purchasing these services as listed in the agreement and signature page of the Proposal or otherwise indicated through the Agreement to commence work.</li>
              <li><strong>Package</strong>: Refers to the product, service, web development solution, website, system or whatever the Purchaser is purchasing from JPLMS.</li>
              <li><strong>Payment</strong>: Any and all monies as well as the full amount of any contra services payable from the Purchaser to JPLMS for the service to be performed.</li>
              <li><strong>Proposal</strong>: Any document created by JPLMS that contains an offer to do work including an overview of the work and the prices for such work.</li>
              <li><strong>Service</strong>: Website construction, design work, search engine optimisation, consulting, advice or any other service or product provision by JPLMS</li>
            </ul>
          </li>
          <li>Property ownership
            <ul>
              <li>The Purchaser owns, and is responsible for, all content inserted into the Package, including:
                <ul>
                  <li>Graphics, including logos, images, templates, "look and feel" to the extent to which the Purchaser owns the Graphics prior to insertion onto the Package.</li>
                  <li>Text and intellectual property.<br />
                    The Purchaser warrants that it has full rights to publish all content on its website, and indemnifies JPLMS against any and all claims from any third parties in respect to any content published on the Purchaser's site. </li>
                </ul>
              </li>
              <li>The Purchaser, upon full Payment, obtains a royalty free, perpetual licence to:
                <ul>
                  <li>Own and operate the Package as developed by JPLMS.</li>
                  <li>Develop, and continue to grow the Package, whether with JPLMS or with any developer or programmer of the Purchaser's choosing.</li>
                  <li>Use any elements which are developed through open source licence or previously by JPLMS that are used in the Package for no additional payment other than specifically agreed without affecting the licence or ownership of those elements.</li>
                </ul>
              </li>
              <li>JPLMS retains the right to continue to sell, develop or otherwise use any elements used in the Package for example, components built on open source.</li>
            </ul>
          </li>
          <li>Processing, Payment and Debt collection
            <ul>
              <li>Purchaser guarantees full Payment for the Package.</li>
              <li>Ownership of the Package only passes to the Purchaser when the final Payment has been received in full by JPLMS.</li>
              <li>If any costs are incurred by JPLMS to collect outstanding Payment the Purchaser indemnifies JPLMS from those costs, eg Legal expenses and debt collection fees.</li>
              <li>In the event of non-payment of any monies from the Purchaser to JPLMS, JPLMS reserves the right to take down the website of the Purchaser until such time as the monies are repaid. The Purchaser acknowledges that it cannot challenge this and JPLMS is not liable for any losses while the website is offline. If domain registrations or any other service lapses due to non-payment the Purchaser is liable for any costs in re-registering those services, if it is possible. The Purchaser is liable for all costs to get the website operational after being taken down, and indemnifies JPLMS against any costs or losses due to this event.</li>
              <li>In the event that the Purchaser takes longer than three months to provide feedback to JPLMS at any time the Package is deemed to be completed and payment is due in full. There are no refunds in this instance.</li>
              <li>Any payments not made by their due date will incur a 10% surcharge.</li>
              <li>Early exit from fixed signed contract requires minimum 30 days’ notice and will incur a cancellation fee of 30% of the remaining contract balance.</li>
              <li>All service subscriptions are ongoing and subject to a 4 month minimum term (unless stated otherwise within the service agreement)</li>
              <li>All subscriptions require 30 days’ notice for cancellation </li>
               <li>If your initial payment authorisation is revoked, your subscription will be terminated. We reserve the right to reject any subscription order at any time</li>
               <li>Service subscriptions renew automatically (5th of each month) unless you cancel according to our cancellation policy. Payment for digital subscriptions will be direct debited from your nominated financial institution or payment method, all subscriptions automatically renew unless cancelled</li>
               <li>It is your responsibility to provide valid payment details, and ensure that your payment details are up to date. You can manage these online through your Client Portal</li>
               <li>If your payment method is invalid, or your payment is otherwise rejected, your subscription may automatically be cancelled and unlimited access revoked</li>
               <li>Unless specified otherwise in the Cancellation policy, all charges are non-refundable</li>
               <li>A 2% fee will be added to your monthly subscription cost if you pay by MasterCard, Visa, or American Express card</li>
               <li>All direct payment gateways adhere to the standards set by PCI-DSS as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa, MasterCard, American Express and Discover</li>
            </ul>
          </li>
          <li>Hosting
            <ul>
              <li>Where JPLMS arranges for hosting of the Package:
                <ul>
                  <li>It is inherent in how the internet is built that from time to time your website and/or Emails will be down. This is normal, and JPLMS is not responsible for any losses the Purchaser may suffer, including lost sales.</li>
                  <li>JPLMS may move your website hosting, for example when upgrading equipment.</li>
                </ul>
              </li>
              <li>The Purchaser warrants that it has full authority to publish everything on its website and indemnifies JPLMS for any costs, liabilities or damages as a result of the Purchasers package. This includes copywrite infringement, violation of any censorship laws and breach of any other laws.</li>
              <li>JPLMS has no responsibility or liability for the deletion or failure to store email messages and any losses the Purchaser may suffer, including lost sales. Some messages may not be processed due to space constraints or outbound message limitations. The Purchaser agrees that JPLMS is not responsible or liable for the deletion or failure to store messages (emails) or other information and that it is the responsibility of the Purchaser to ensure any and all email messages and content are sufficiently backed up and stored.</li>
            </ul>
          </li>
          <li>Timeframe
            <ul>
              <li>If the Purchaser would like a fixed timeline for the completion of the Package, they must obtain specific, written and signed agreement from a Director of JPLMS prior to signing the contract for the site. Timelines requested or provided after the contract has been agreed to are for information only and subject to delay at any point.</li>
              <li>In any timeline, JPLMS will only be able to complete the Package if the Purchaser supplies any and all required information prior to or at the times required under the timeline.</li>
              <li>There are no guarantees that timelines will be met, and no grounds for compensation if that occurs.</li>
            </ul>
          </li>
          <li>Service level
            <ul>
              <li>Unless specifically agreed to in a written Service Level Agreement, there is no minimum or maximum time for JPLMS to enact changes on any website, including correcting errors.</li>
              <li>Operating hours are between 8:30am to 5:30pm Monday to Friday (excluding public Holidays), all works completed after hours incur Express Rate Fee as a minimum per hour charge.</li>
            </ul>
          </li>
          <li>Support levels
            <ul>
              <li>Hourly rates
                <ul>
                  <li>Normal rate is &#36;150 plus GST per hour, with a one week guaranteed turn around</li>
                  <li>Express rate is &#36;200 plus GST per hour for same day turn around or for changes to be made after hours</li>
                  <li>Emergency rate is &#36;250 plus GST per hour for immediate turnaround</li>
                  <li>Rates will increase over time;JPLMS reserves the right to do so without providing notice</li>
                </ul>
              </li>
              <li>Three Month Service Period
                <ul>
                  <li>JPLMS provides a free Three Month Service Period on the Package. Please use this time to ensure the Package works as per your wishes</li>
                </ul>
              </li>
              <li>Warranty
                <ul>
                  <li>JPLMS provides a limited lifetime guarantee on the sites code integrity and basic site function and operation</li>
                  <li>The warranty is void if the site is no longer hosted with JPLMS or a third party development team access and modifies the sites code</li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>
      </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>