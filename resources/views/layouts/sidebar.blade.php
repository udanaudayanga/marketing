<div id="sidebar-wrapper">
    <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
        <li class="{{ Request::is('dashboard*') ? 'active' : '' }}"><a href="{{ route('dashboard') }}"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
        <li class="{{ Request::is('invoice*') ? 'active' : '' }}"><a href="{{ route('invoice_list') }}"><i class="fa fa-user-circle"></i><span>Account</span></a>
        
        </li>
        {{--<li class="{{ Request::is('access*') ? 'active' : '' }}"><a href="{{ route('access_lists') }}"><i class="fa fa-key"></i><span>Access</span></a></li>--}}
        {{--<li><a href="#"><i class="fa fa-envelope"></i><span>Email</span></a></li>--}}
        <li class="{{ Request::is('webanalytics*') ? 'active' : '' }}"><a href="{{ route('webanalytics') }}"><i class="fa fa-line-chart"></i><span>Analytics<i class="fa fa-caret-down" aria-hidden="true"></i></span> </a>
        <ul>
            @foreach ( $websiteList as $website)
            @if( $website->view_id != '')
                <li><a href="{{ route('webanalytics_info',[$website->view_id]) }}">- {{ $website->url}}</a></li>
            @elseif(  $website->semrush_project_id != '' )
                <li><a href="{{ route('semrush_site_audit',[$website->semrush_project_id]) }}">- {{ $website->url}}</a></li>
            @endif
            @endforeach
        
        </ul>
      </li>
        <li class="{{ Request::is('jobs*') ? 'active' : '' }}"><a href="{{ route('jobs_list') }}"><i class="fa fa-history"></i><span>Support</span></a></li>
        <li class="{{ Request::is('settings*') ? 'active' : '' }}"><a href="{{ route('settings_info')}}"><i class="fa fa-cog"></i><span>Settings</span></a></li>
        <li><a href="#"><i class="fa fa-file"></i><span>Files</span></a></li> 
    </ul>
</div>