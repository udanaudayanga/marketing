<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JPLMS - Marketing Suite</title>
    <link href="{{ url('/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/css/dropzone.min.css') }}" rel="stylesheet" type="text/css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="app">
     @include('layouts.header')
   

        @include('layouts.sidebar')

        @yield('content')


   @include('layouts.footer')
</div>

<!-- Scripts -->

<script src="{{ url('/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/jquery.nicescroll.min.js') }}" type="text/javascript"></script> 
<script src="{{ url('/js/custom.js') }}" type="text/javascript"></script> 
<!-- form validation -->
<script src="{{ url('/js/parsley.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('/js/app.js')}}"></script>
<script type="text/javascript" src="{{ url('/js/dropzone.min.js')}}"></script>
<script type="text/javascript" src="{{ url('/js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{ url('/js/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{ url('/js/server-side-modal.js?ver=1.1')}}"></script>
<script type="text/javascript" src="{{ url('/js/icheck.min.js')}}"></script>

<!-- dataTable --> 
<script src="{{ url('js/jquery.dataTables.min.js') }}" type="text/javascript"></script> 
<script src="{{ url('js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script> 
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
@yield('scripts')
</body>
</html>
