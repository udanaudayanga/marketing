<header>
        <div class="container-fluid">
            <div class="row">
                <h1><a href="{{ route('dashboard')}}"><img src="{{ asset('images/jplms-branding.png') }}" alt="JPLMS - Marketing Suite"
                                                  width="120px"></a></h1>
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" id="menu-toggle"><span
                                class="glyphicon glyphicon-th-large" aria-hidden="true"></span></button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <button class="navbar-toggle collapse in" data-toggle="collapse" id="menu-toggle-2"><span
                                        class="glyphicon glyphicon-th-large" aria-hidden="true"></span></button>
                        </li>
                    </ul>
                </div>
                <h2>Marketing Suite</h2>
                <ul class="action-panel">
                    <li>Logged in as <span>{{ Auth::user()->name }}</span></li>
                    <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i></a></li>
                </ul>
            </div>
        </div>
    </header>