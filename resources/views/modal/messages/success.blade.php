

        <div class="modal-body">
            <div class="alert alert-success">
                <p><strong><i class="fa fa-check-circle"></i> Success</strong>: {{ $message }}</p>
            </div>
        </div>


        <div class="modal-footer">
            <button type="submit" id="reload" class="btn btn-lg btn-cancel" data-dismiss="modal">
                {{ isset($action_button)?$action_button:"Okay" }}
            </button>
        </div>


<script type="text/javascript"> 
        $(function(){
            $("#reload").click(function(){
               
                window.location.reload();

            })
        });
</script>