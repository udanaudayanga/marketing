    <div class="modal fade in" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block; padding-left: 15px;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="{{ route('save_payment_methods') }}" class="form-horizontal form-label-left" novalidate="" method="post" id="payment_methods">
                 {{ csrf_field() }}



        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <h4 class="modal-title">Add New Credit Card Details</h4>
        </div>
        <div class="modal-body">
          <div class="accounts">
            <div class="payments">
              <fieldset class="payment-mothod">
                <div class="form-wrapper">
                  <div class="col-sm-9 col-sm-offset-1">
                    <div class="">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="cardNumber"> CARD NUMBER</label>
                            <div class="input-group">
                              <input type="number" class="form-control" required="required" id="cardNumber" name="card_number" maxlength="19" placeholder="Valid Card Number"
                                required autofocus />
                              <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span> </div>
                            <img src="{{ asset('images/cc.png') }}" alt="Credit Card Icon" class="icons"> </div>
                        </div>
                      </div>
                      {{--<div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="cardName"> Card Type</label>
                            <div class="input-group">
                              <select class="form-control" name="card_type" required>
                                <option value="">Select Card Type</option>
                                <option value="Visa">Visa</option>
                                <option value="Master">Master</option>
                             
                              </select>
                              <span class="input-group-addon"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span> </div>
                          </div>
                        </div>
                      </div>
                      <br>--}}
                      <div class="row">
                        <div class="col-md-4 pull-left">
                          <label for="expityMonth"> EXPIRY DATE</label>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-3">
                           <div class="form-group">
                            <select class="form-control" name="exp_date_month" required>
                              <option value="">MM</option>
                              @for( $i= 01; $i<=12; $i++)
                              <option value="{{ sprintf ("%02u", $i) }}">{{ sprintf ("%02u", $i) }}</option>
                              @endfor;
                            </select>
                          </div>
                        
                        </div>
                        <div class="col-md-1">
                          <div class="form-group">/</div>
                        </div>
                        <div class="col-md-3">
                           <select class="form-control" name="exp_date_year"  required>
                              <option value="">YYYY</option>
                              @for ($i = date('y'); $i <= date('y') + 10; $i++)
                              <option value="{{ $i }}">{{ $i }}</option>
                              @endfor
                            </select>
                        </div>
                        <div class="col-md-3 pull-right cvv">
                          <div class="form-group">
                            <label for="cvCode"> CVV CODE</label>
                            <input type="text" class="form-control" id="cvCode" name="cvv_code" placeholder="CVV" required />
                          </div>
                          <img src="{{ asset('images/cvv.png') }}" alt="CVV Icon" class="icons"> </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="cardName"> Name On Card</label>
                            <div class="input-group">
                              <input type="text" class="form-control" id="cardName"  name="name_on_card" placeholder="Card Name" required autofocus />
                              <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> </div>
                          </div>
                        </div>
                      </div>
                     {{-- <br>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="address">Street Address</label>
                            <div class="input-group">
                              <input type="text" class="form-control" id="address" name="street_address" placeholder="Street Address" required autofocus />
                              <span class="input-group-addon"><span class="glyphicon glyphicon-pushpin"></span></span> </div>
                          </div>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="form-group col-sm-4">
                          <label for="suburb">Suburb</label>
                          <input type="text" class="form-control" name="suburb"  required="">
                          <ul class="parsley-errors-list" id="parsley-id-4349">
                          </ul>
                        </div>
                        <div class="form-group col-sm-4">
                          <label for="state">State</label>
                          <select class="form-control" name="state">
                            <option value="">Select</option>
                            <option value="Australian Capital Territory">Australian Capital Territory</option>
                            <option value="New South Wales">New South Wales</option>
                            <option value="Northern Territory">Northern Territory</option>
                          </select>
                        </div>
                        <div class="form-group col-sm-4">
                          <label for="postcode">Postcode</label>
                          <input type="text" class="form-control" name="postcode">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="email">Email</label>
                            <div class="input-group">
                              <input type="email" class="form-control" name="email" id="email" placeholder="Email Address"
                                required autofocus />
                              <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span> </div>
                          </div>
                        </div>
                      </div>--}}
                      <br>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>
                              <input type="checkbox" name="use_next_time">
                              &nbsp;&nbsp;Save card for next time</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
            </div>
          </div>
        </div> <input type="hidden" value="{{ $client->id}}" name="client_id">
        
        <div class="modal-footer">
          <button type="submit" class="btn btn-lg btn-default submit-button">Add New Credit Card</button>
          <button type="submit" class="btn btn-lg btn-cancel" data-dismiss="modal">Cancel</button>
        </div>

         </form>
      </div>
     



                    
                


            </div>
        </div>
