    <div class="modal fade" id="addnewclient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
                <form action="{{ route('update_recieptmail', $line->id) }}" method="post" novalidate="" class="form-horizontal form-label-left">
                @if ($errors->any())
                    <div class="alert alert-error">
                        <p><strong><i class="fa fa-ban"></i> </strong>{{ $errors->first() }}</p>
                    </div>
                @endif
                    <input type="hidden" name="invoice_id" value="{{ $line->id }}">
                    {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h4 class="modal-title">{{ $line->email }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-wrapper">
                        <div class="col-sm-11 col-sm-offset-1">
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="title">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="email" value="{{ $line->email}}" required="required">
                                    <span class="required">*</span> </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-lg btn-default submit-button">Update</button>
                    <button type="submit" class="btn btn-lg btn-cancel" data-dismiss="modal">Cancel</button>
                </div>
                </form>
            </div>

        </div>
    </div>
