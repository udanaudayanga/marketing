    <div class="modal fade in" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block; padding-left: 15px;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="{{ route('update_client') }}" class="form-horizontal form-label-left" novalidate="" method="post" id="access">
                 {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                        <h4 class="modal-title">Edit Client</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-wrapper">
                            <div class="col-sm-11 col-sm-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="organisation">Company Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="{{$client->company_name}}" class="form-control" required="required" name="company_name">
                                        <span class="required">*</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="ref-number">ABN</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="{{$client->abn}}" class="form-control" name="abn">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="address">Address</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="{{$client->address}}" class="form-control" name="address">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-sm-3" for="street">Street</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="{{$client->street}}" class="form-control" name="street">
                                    </div>
                                </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-3" for="suburb">Suburb</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="{{$client->suburb}}" class="form-control" name="suburb">
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-lg btn-default submit-button">Update</button>
                        <button type="submit" class="btn btn-lg btn-cancel" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>