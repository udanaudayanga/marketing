  <div class="modal fade in" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block; padding-left: 15px;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="{{ route('task_create') }}" class="form-horizontal form-label-left" novalidate="" method="post" id="task">
                    {{ csrf_field() }}
                    <input type='hidden' name='_method' value='post'>
                    <input type="hidden" name="status" value="todo">
                    <input type="hidden" name="type" value="sub">
                    <input type="hidden" name="job_id" value="0">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                        <h4 class="modal-title">Add New Task</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-wrapper">
                            <div class="col-sm-11 col-sm-offset-1">
                              {{--<div class="form-group">
                                    <label class="control-label col-sm-3" for="address">Job</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="jobs" name="job_id">
                                            <option value=""> Select a Job</option>
                                            @foreach($jobs as $job)
                                                <option value="{{ $job->id }}"> {{ $job->title }}</option>
                                            @endforeach
                                        </select>
                                       </div>
                                </div>--}}
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="address">Client</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="clients" name="client_id">
                                            <option value=""> Select a Client</option>
                                            @foreach($clients as $client)
                                                <option value="{{ $client->id }}"> {{ $client->company_name }}</option>
                                            @endforeach
                                        </select>
                                       </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="title">Title</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="title" required="required">
                                        <span class="required">*</span> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="due_date">Due Date</label>
                                    <div class="col-sm-5">
                                        <input type="text" name="due_date" class="form-control has-feedback-left calendar-single" placeholder="dd/mm/yyyy" data-parsley-id="9604"><ul class="parsley-errors-list" id="parsley-id-9604"></ul>
                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span> </div>

                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Priority</label>
                                    <div class="col-sm-8">
                                        <select name="priority" class="form-control" data-parsley-id="4336">
                                            <option value="">Select Priority</option>
                                            @foreach($priorities as $priority)
                                                <option value="{{ $priority }}">{{ ucwords($priority) }}</option>
                                            @endforeach
                                        </select><ul class="parsley-errors-list" id="parsley-id-4336"></ul>
                                        <span class="required">*</span> </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Assign to</label>
                                    <div class="col-sm-5">
                                        <select name="assignees" id="assignees"  class="form-control" data-parsley-id="2078" data-placeholder="Select Assignees">
                                           <option value="">Select Employee</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                            @endforeach
                                        </select><ul class="parsley-errors-list" id="parsley-id-2078"></ul>
                                        <span class="required">*</span> </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3">Description</label>
                                    <div class="col-sm-8">
                                        <textarea name="description" class="form-control" rows="10" data-parsley-id="4075"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="attachments">Attachement</label>
                                    <div class="col-sm-8">
                                        <div id="attachments"  class="dropzone" name="attachments"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-lg btn-default submit-button">Create</button>
                        <button type="submit" class="btn btn-lg btn-cancel" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function () {
            // Dropzone.autoDiscover = false;
            $('.calendar-single').daterangepicker({
                singleDatePicker: true,
                calender_style: "calendar-single",
                format: "DD/MM/YYYY"
            });

            let dropzone = new Dropzone("#attachments", {
                url: '{{ route('job_attachments')}}',
                headers: {
                    'X-CSRF-Token': '{{csrf_token()}}'
                },
                addRemoveLinks: true,
                init: function () {
                    this.on("removedfile", function(file) { console.log(file.name); });
                },
                //autoProcessQueue: false,
            });

            dropzone.on('success', function(file, response) {
                let $element = $("<input type='hidden' value='" + response.id + "' name='attachments[]' class='uploaded-file-id' '>");
                $(file.previewTemplate).append($element);
            });

            dropzone.on("removedfile", function(file) {
                let uploaded_id = $(file.previewTemplate).children('.uploaded-file-id').val();
                $.post("{{ route('job_attachments_remove') }}", { '_token': '{{csrf_token()}}',id: uploaded_id } );
            });
            //$("#assignees").select2();
        });

        if ($("input.flat")[0]) {
            $(document).ready(function () {
                "use strict";
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_flat',
                    radioClass: 'iradio_flat'
                });
            });
        }
    </script>