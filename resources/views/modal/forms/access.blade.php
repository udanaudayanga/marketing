    <div class="modal fade in" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block; padding-left: 15px;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="{{ route('access_add') }}" class="form-horizontal form-label-left" novalidate="" method="post" id="access">
                 {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                        <h4 class="modal-title">Add Access</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-wrapper">
                            <div class="col-sm-11 col-sm-offset-1">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="organisation">Organisation</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="organization">
                                        <span class="required">*</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="ref-number">Membership/ Ref. Number</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="ref_number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="username">Username</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="username">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="password">Password</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="pin">Pin</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" name="pin">
                                    </div>
                                </div>
                               
                                <input type="hidden" name="type" value="{{request()->route('type')}}">
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-lg btn-default submit-button">Add Access</button>
                        <button type="submit" class="btn btn-lg btn-cancel" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>