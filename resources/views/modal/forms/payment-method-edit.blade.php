    <div class="modal fade in" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block; padding-left: 15px;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="{{ route('payment_methods_update',[$method->id]) }}" class="form-horizontal form-label-left" novalidate="" method="post" id="payment_methods">
                 {{ csrf_field() }}


        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <h4 class="modal-title">Edit Credit Card Details</h4>
        </div>
        <div class="modal-body">
          <div class="accounts">
            <div class="payments">
              <fieldset class="payment-mothod">
                <div class="form-wrapper">
                  <div class="col-sm-9 col-sm-offset-1">
                    <div class="">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="cardNumber"> CARD NUMBER</label>
                            <div class="input-group">
                              <input type="text" class="form-control" id="cardNumber" name="card_number"  disabled value="{{ $method->card_number}}" maxlength="19" placeholder="Valid Card Number"
                                required autofocus />
                              <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span> </div>
                            <img src="{{ asset('images/cc.png') }}" alt="Credit Card Icon" class="icons"> </div>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-4 pull-left">
                          <label for="expityMonth"> EXPIRY DATE</label>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" name="exp_date_month" value="{{ $method->exp_date_month}}"  id="expityMonth" placeholder="MM" required />
                          </div>
                        </div>
                        <div class="col-md-1">
                          <div class="form-group">/</div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <input type="text" class="form-control" id="expityYear" name="exp_date_year" value="{{ $method->exp_date_year}}" placeholder="YY" required />
                          </div>
                        </div>
                        <div class="col-md-3 pull-right cvv">
                          <div class="form-group">
                            <label for="cvCode"> CVV CODE</label>
                            <input type="text" class="form-control" id="cvCode" name="cvv_code" value="{{ $method->cvv_code}}" placeholder="CVV" required />
                          </div>
                          <img src="{{ asset('images/cvv.png') }}" alt="CVV Icon" class="icons"> </div>
                      </div>
                      <br><input type="hidden" value="{{ $method->token}}" class="form-control" id="token" name="token" />
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="cardName"> Name On Card</label>
                            <div class="input-group">
                              <input type="text" class="form-control" id="cardName"  name="name_on_card"  value="{{ $method->name_on_card}}" placeholder="Card Name" required autofocus />
                              <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> </div>
                          </div>
                        </div>
                      </div>
                      <br>
                     {{-- <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="address">Street Address</label>
                            <div class="input-group">
                              <input type="text" class="form-control" id="address" value="{{ $method->street_address}}" name="street_address" placeholder="Street Address" required autofocus />
                              <span class="input-group-addon"><span class="glyphicon glyphicon-pushpin"></span></span> </div>
                          </div>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="form-group col-sm-4">
                          <label for="suburb">Suburb</label>
                          <input type="text" class="form-control" value="{{ $method->suburb}}" name="suburb"  required="">
                          <ul class="parsley-errors-list" id="parsley-id-4349">
                          </ul>
                        </div>
                        <div class="form-group col-sm-4">
                          <label for="state">State</label>
                          <select class="form-control" name="state">
                            <option value="">Select</option>
                            <option value="Australian Capital Territory" {{ $method->state == 'Australian Capital Territory'?"selected":"" }}>Australian Capital Territory</option>
                            <option value="New South Wales" {{ $method->state == 'New South Wales'?"selected":"" }}>New South Wales</option>
                            <option value="Northern Territory" {{ $method->state == 'Northern Territory'?"selected":"" }}>Northern Territory</option>
                          </select>
                        </div>
                        <div class="form-group col-sm-4">
                          <label for="postcode">Postcode</label>
                          <input type="text" class="form-control" value="{{ $method->postcode}}" name="postcode">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="email">Email</label>
                            <div class="input-group">
                              <input type="email" class="form-control" value="{{ $method->email}}" name="email" id="email" placeholder="Email Address"
                                required autofocus />
                              <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span> </div>
                          </div>
                        </div>
                      </div>--}}
                      <br>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>
                              <input type="checkbox" name="set_default"   {{ $method->set_default == 1?"checked":"" }} >
                              &nbsp;&nbsp;Save card for next time</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
            </div>
          </div>
        </div> <input type="hidden" value="{{ $client->id}}" name="clientsubmit-button_id">
        <input type="hidden" name="id" value="{{ $method->id }}">
        <input type="hidden" value="Visa" name="card_type">
        <div class="modal-footer">
          <button type="submit" class="btn btn-lg btn-default submit-button">Update Credit Card</button>
          <button type="submit" class="btn btn-lg btn-cancel" data-dismiss="modal">Cancel</button>
        </div>
         </form>
                 
               


            </div>
        </div>
    </div>