    <div class="modal fade in" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block; padding-left: 15px;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="{{ route('job_update', $job->id) }}" class="form-horizontal form-label-left" novalidate="" method="post">
                 {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $job->id }}" id="job_id">
                    <input type="hidden" name="status" value="{{ $job->status }}">
                 <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h4 class="modal-title">{{ $job->title }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-wrapper">
                        <div class="col-sm-11 col-sm-offset-1">
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="title">Job Id</label>
                                <div class="col-sm-8">
                                    <label><strong>{{ $job->code }}</strong></label>
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="title">Title</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="title" required="required" value="{{ $job->title }}">
                                    <span class="required">*</span> </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="due_date">Due Date</label>
                                <div class="col-sm-5">
                                    <input type="text" name="due_date" class="form-control has-feedback-left calendar-single" value="{{ $job->due_date }}" placeholder="dd/mm/yyy" data-parsley-id="9604"><ul class="parsley-errors-list" id="parsley-id-9604"></ul>
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span> </div>
                                <div class="col-sm-3">
                                    <label class="control-label pull-right">
                                        <div class="icheckbox_flat" style="position: relative;">
                                        <input type="checkbox" value="1" name="next_status" data-parsley-mincheck="2" class="flat" style="position: absolute; opacity: 0;">
                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        @if($job->status == \App\Constants\JobStatus::TODO)
                                            &nbsp;&nbsp;<strong>Start</strong>
                                         @elseif($job->status == \App\Constants\JobStatus::IN_PROGRESS)
                                            &nbsp;<strong>Complete</strong>
                                         @else
                                             <strong>&ensp;Archive</strong>
                                        @endif
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Type</label>
                                <div class="col-sm-8">
                                    <select name="type" class="form-control" data-parsley-id="2625">
                                        <option value="">Select Type</option>
                                        @foreach($types as $type)
                                            @if ($type != "all")
                                                @if ($type != "sub" )
                                                    <option value="{{ $type }}" {{ $job->type == $type?"selected":"" }}>{{ ucwords($type) }}</option>
                                                @else
                                                     <option value="{{ $type }}" {{ $job->type == $type?"selected":"" }}>Task</option>
                                                @endif

                                            @endif
                                        @endforeach
                                    </select><ul class="parsley-errors-list" id="parsley-id-2625"></ul>
                                    <span class="required">*</span> </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Assign</label>
                                <div class="col-sm-5">
                                    <select name="assignees" id="assignees" class="form-control" data-parsley-id="2078">
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}" {{ in_array($employee->id, $job->assignees)?"selected":"" }}>{{ $employee->name }}</option>
                                        @endforeach
                                    </select><ul class="parsley-errors-list" id="parsley-id-2078"></ul>
                                    <span class="required">*</span> </div>
                            </div>
                             <div class="alert-warning highlight">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Add Note</label>
                                <div class="col-sm-8">
                                    <textarea name="comment" class="form-control" rows="3" placeholder="Note"></textarea>
                                </div>
                              </div>
                           
                          </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Priority</label>
                                <div class="col-sm-8">
                                    <select name="priority" class="form-control" data-parsley-id="4336">
                                        <option value="">Select Priority</option>
                                        @foreach($priorities as $priority)
                                            <option value="{{ $priority }}" {{ $job->priority == $priority?"selected":"" }}>{{ ucwords($priority) }}</option>
                                        @endforeach
                                    </select><ul class="parsley-errors-list" id="parsley-id-4336"></ul>
                                    <span class="required">*</span>
                                </div>
                            </div>
                           
                           
                           
                         
                            <div class="form-group">
                                <label class="control-label col-sm-3">Description</label>
                                <div class="col-sm-8">
                                    <textarea name="description" class="form-control" rows="10" data-parsley-id="4075">{{ $job->description }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="attachments">Attachement</label>
                                <div class="col-sm-8">
                                    <div id="attachments"  class="dropzone" name="attachments"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="well">
                                    @foreach( $job->attachments as $attachment)
                                        @component('components.attachment')

                                            @slot('id')
                                                {{ $attachment->id }}
                                            @endslot

                                            @slot('mine_type')
                                                {{ $attachment->mine_type }}
                                            @endslot

                                            @slot('original_name')
                                                {{ $attachment->original_name }}
                                            @endslot

                                            @slot('path')
                                                {{ $attachment->path }}
                                            @endslot

                                            @slot('delete_action')
                                                {{ 1 }}
                                            @endslot

                                        @endcomponent
                                    @endforeach
                                </div>
                            </div>

             
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-lg btn-default submit-button">Save</button>
                    <button type="submit" class="btn btn-lg btn-cancel" data-dismiss="modal">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            init();
            // Dropzone.autoDiscover = false;
            $('.calendar-single').daterangepicker({
                singleDatePicker: true,
                calender_style: "calendar-single",
                format: "DD/MM/YYYY"
            });

            let dropzone = new Dropzone("#attachments", {
                url: '{{ route('job_attachments')}}',
                headers: {
                    'X-CSRF-Token': '{{csrf_token()}}'
                },
                addRemoveLinks: true,
                init: function () {
                    this.on("removedfile", function(file) { console.log(file.name); });
                },
                //autoProcessQueue: false,
            });

            dropzone.on('success', function(file, response) {
                let $element = $("<input type='hidden' value='" + response.id + "' name='attachments[]' class='uploaded-file-id' '>");
                $(file.previewTemplate).append($element);
            });

            dropzone.on("removedfile", function(file) {
                let uploaded_id = $(file.previewTemplate).children('.uploaded-file-id').val();
                $.post("{{ route('job_attachments_remove') }}", { '_token': '{{csrf_token()}}',id: uploaded_id } );
            });

                $(".delete-attachment").click(function(){
                    var id = $(this).data('id');
                    var job_id = "{{ $job->id }}";
                    $.post("{{ route('job_attachments_remove') }}", { '_token': '{{csrf_token()}}',id: id, job_id: job_id } );
                    $(this).remove();
                    $("#attachment_"+id).remove();
                });

             //   $("#assignees").select2();


        });

        var count = ('{{ count($job->tasks) }}'*1)+1;

        $(document).ready(function(){
                $("#add-job-task").click(function(){

                var task = $("#new-job-task").val();
                if(task !== "") {
                    $("#task-container").append('<tr><td>'+ count +'. '+ task+' <input type="hidden" name="tasks[]" value="'+task+'"> </td> <td><label class="control-label pull-right"><a href="#" class="delete-new-job-task"><i class="fa fa-trash-o"></i></a></label></td></tr>');
                    $("#new-job-task").val("");
                    count++;
                }
                init();
            });

            /*
            $("#add-job-task").click(function(){ 
                var el = $("#new-job-task");
                var task = el.val();
                var actionEl = $(this);

                if(task !== "") {
                    actionEl.html('<i class="fa fa-plus" aria-hidden="true"></i> Adding');
                    $.post('{{ route('task_create') }}', {"_token":'{{csrf_token()}}', "description": task, "job_id": '{{ $job->id }}' }, function (data) {
                        $("#task-container").append('<tr><td>'+ count +'. '+ task+' <input type="hidden" name="tasks[]" value="'+task+'"> </td><td><label class="control-label pull-right"><input type="checkbox" value="" data-parsley-mincheck="3" class="flat"></label> </td> <td><label class="control-label pull-right"><a href="#" class="delete-new-job-task"><i class="fa fa-trash-o"></i></a></label></td></tr>');
                        el.val("");
                        actionEl.html('<i class="fa fa-plus" aria-hidden="true"></i> Add');
                        count++;
                        init();
                    });
                }
            });*/
        });
        function init() {
            $('.delete-new-job-task').click(function () {
                $(this).parent().parent().parent().remove();
                count--;
                if (count <= 0) {
                    count = 1;
                }
            });
            if ($("input.flat")[0]) {
                $(document).ready(function () {
                    "use strict";
                    $('input.flat').iCheck({
                        checkboxClass: 'icheckbox_flat',
                        radioClass: 'iradio_flat'
                    });
                });
            }
        }
    </script>