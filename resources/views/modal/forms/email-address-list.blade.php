    <div class="modal fade in" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block; padding-left: 15px;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="{{ route('emai_address_add') }}" class="form-horizontal form-label-left" novalidate="" method="post" id="access">
                 {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                        <h4 class="modal-title">Add Email Address</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
              <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Email Address">
              </div>
              <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password">
              </div>
            </div><input type="hidden" name="client_id" class="form-control" value="{{ $client->id}}">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-lg btn-default submit-button">Add Email Address</button>
                        <button type="submit" class="btn btn-lg btn-cancel" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>