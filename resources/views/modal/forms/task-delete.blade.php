    <div class="modal fade" id="delete_task" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
                <form action="{{ route('task_delete', $job->id) }}" method="post" novalidate="" class="form-horizontal form-label-left">
                @if ($errors->any())
                    <div class="alert alert-error">
                        <p><strong><i class="fa fa-ban"></i> </strong>{{ $errors->first() }}</p>
                    </div>
                @endif
                    <input type="hidden" name="invoice_id" value="{{ $job->id }}">
                    {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h4 class="modal-title">{{ $job->title }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-wrapper">
                        <h3>Are you sure You want to delete this task ?</h3>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-lg btn-default submit-button">Delete</button>
                    <button type="submit" class="btn btn-lg btn-cancel" data-dismiss="modal">Cancel</button>
                </div>
                </form>
            </div>

        </div>
    </div>
