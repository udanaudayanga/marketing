    <div class="modal fade" id="addnewclient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
                <form action="{{ route('access_destroy', $access->id) }}" method="post" novalidate="" class="form-horizontal form-label-left">
                    <input type="hidden" name="user_id" value="{{ $access->id }}">
                    {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h4 class="modal-title">Delete Access - ({{ $access->organization }})</h4>
                </div>
                <div class="modal-body">
                    <div class="form-wrapper">
                        <h3>Do you really want to delete this access?</h3>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-lg btn-default submit-button">Delete</button>
                    <button type="submit" class="btn btn-lg btn-cancel" data-dismiss="modal">Cancel</button>
                </div>
                </form>
            </div>

        </div>
    </div>
