@extends('layouts.auth')

@section('body-classes','login')

@section('content')
    <section class="login-wrapper container">
        <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <h3>Forgot Password</h3>
            <section class="content">
                <!-- ALERT MESSAGES STYLE -->
                <!--
                <!-- ALERT MESSAGES END -->
                @if ($errors->any())
                    <div class="alert alert-error">
                        <p><strong><i class="fa fa-ban"></i> </strong>{{ $errors->first() }}</p>
                    </div>
                @endif

                @if(session('success'))
                    <div class="alert alert-success">
                        <p><strong><i class="fa fa-check-circle"></i></strong>{{session('success')}}</p>
                    </div>

                @endif

                <form class="form-horizontal" action="{{ route('reset_pin') }}" method="post">
                    {{ csrf_field() }}
                    <p>Please enter your email address. We will send a new pin to your inbox</p>
                    <br>
                    <div class="form-group">
                        <label for="email" class="col-xs-4 col-sm-4 control-label">Email</label>
                        <div class="col-xs-6 col-sm-7">
                            <input type="text" class="form-control" name="email" placeholder="Email Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-7">
                            <button type="submit" class="btn btn-lg btn-default">Send</button>
                        </div>
                    </div>
                </form>
            </section>
            <footer>
                <p>© Copyright <script type="text/javascript">document.write(new Date().getFullYear());</script>2017 <span>JPLMS</span>. All rights reserved.</p>
            </footer>
        </div>
    </section>
@endsection