@extends('layouts.auth')

@section('body-classes','login pin')

@section('content')

    <section class="login-wrapper container">
        <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <h3>Enter Your Pin</h3>
            <section class="content">
                <form class="form-horizontal" action="{{ route('auth') }}" method="post" id="auth">
                    {{ csrf_field() }}
                    <!-- ALERT MESSAGES STYLE -->
                    <!--div class="alert alert-success">
                        <p><strong><i class="fa fa-check-circle"></i> Success</strong>: Sucess message goes here</p>
                      </div>
                      <div class="alert alert-warning">
                        <p><strong><i class="fa fa-exclamation-circle"></i> Warn</strong>: Warn message goes here</p>
                      </div>

                    <!-- ALERT END -->

                    @if ($errors->any())
                        <div class="alert alert-error">
                            <p><strong><i class="fa fa-ban"></i> </strong>{{ $errors->first() }}</p>
                        </div>
                    @endif

                    <div class="col-sm-3 col-md-3">
                        <div class="form-group">
                            <input type="password" name="pin[1]" class="form-control inputs" maxlength="1" size="1">
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <div class="form-group">
                            <input type="password" name="pin[2]" class="form-control inputs" maxlength="1" size="1">
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <div class="form-group">
                            <input type="password" name="pin[3]" class="form-control inputs" maxlength="1" size="1">
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <div class="form-group">
                            <input type="password" name="pin[4]" class="form-control inputs" maxlength="1" size="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4 pull-left text-left"><a href="/forgot" class="pwd">Forgot Pin?</a></div>
                        <span class="input-group-btn  pull-left col-md-offset-2"></span>
                        <div class="col-sm-4 pull-right text-right">
                            <button type="submit" class="btn btn-lg btn-default ">Go</button>
                        </div>
                    </div>
                    <input type="hidden" name="client_id" value="{{ app('request')->input('clientid') }}">
                </form>
            </section>
            <footer>
                <p>&copy; Copyright <script type="text/javascript">document.write(new Date().getFullYear());</script> <span>JPLMS</span>. All rights reserved.</p>
            </footer>
        </div>
    </section>

@endsection

@section('scripts')

    <script type="text/javascript" >
        $(function() {
            var count = 0;
            $(".inputs").keyup(function () {
                if (this.value.length === this.maxLength) {
                    var inputs = $(this).closest('form').find(':input');
                    inputs.eq( inputs.index(this)+ 1 ).focus();
                    count++;
                    if (count === 4)  {
                        $("#auth").submit();
                    }
                }
            });
        });
    </script>
@endsection
