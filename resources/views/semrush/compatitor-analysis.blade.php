@extends('layouts.app')


@section('content')

<div id="wrapper">
  
  <section id="page-content-wrapper" class="analytics competitor">
    <div class="container-wrapper container-fuild">
     <ol class="breadcrumb">
        <li><a href="/">Dashboard</a></li>
        <li class="active">Website Analytics</li>
      </ol>
      <div class="row">
        <div class="col-sm-12">
          <section class="content">
              <ul class="default-tab">
                 @if ($website1[0]->view_id != '')
                 <li><a href="{{ route('webanalytics_info',[$website1[0]->view_id]) }}" class="">Website - <em>JPLMS</em><small>{{$website1[0]->url}}</small></a></li>
                 @endif
                @if ($website1[0]->semrush_project_id != '')
                <li><a href="{{ route('semrush_site_audit',[$website1[0]->semrush_project_id]) }}"><span>Site Audit Results</span></a></li>
                <li><a href="{{route('semrush_competitor_analysis',[$website1[0]->semrush_project_id])}}"><span>Competitor Analysis</span></a></li>
                <li><a href="#"><span>Keyword Performance</span></a></li>
                @endif
              </ul>
              <div class="domain-overview">
                <div class="row">
                  <h3>Domain Overview "<strong>{{$domain}}</strong>"</h3>
                  <div class="col-md-3">
                    <ul>
                      @foreach ($organicSearchOverviews as $organicSearchOverview)
                      <li>
                        <h4><i class="fa fa-circle info" aria-hidden="true"></i>Organic Search</h4>
                      </li>
                      <li class="trafic">
                        <h5>{{ $organicSearchOverview['Organic Traffic']}}<span>0%</span></h5>
                        <aside>Trafic</aside>
                      </li>
                      <li>SEMrush Rank<span>{{ $organicSearchOverview['Rank']}}<em></em></span></li>
                      <li>Keywords<span>{{ $organicSearchOverview['Organic Keywords']}} <em class="danger">-17%</em></span></li>
                      <li>Trafic Cost<span>${{ $organicSearchOverview['Organic Cost']}}<em>0%</em></span></li>

                      @endforeach
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <ul>
                      @foreach ($organicSearchOverviews as $organicSearchOverview)
                      <li>
                        <h4><i class="fa fa-circle orange" aria-hidden="true"></i>Paid Search</h4>
                      </li>
                      <li class="trafic">
                        <h5>{{ $organicSearchOverview['Adwords Traffic']}}<span>0%</span></h5>
                        <aside>Trafic</aside>
                      </li>
                      <li>Keywords<span>{{ $organicSearchOverview['Adwords Keywords']}}<em class="danger">-17%</em></span></li>
                      <li>Trafic Cost<span>{{ $organicSearchOverview['Adwords Cost']}}<em>0%</em></span></li>
                       @endforeach
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <ul>
                      @foreach ($backlinkOverviews as $backlinkOverview)
                      <li>
                        <h4><i class="fa fa-circle success" aria-hidden="true"></i>Backlinks</h4>
                      </li>
                      <li class="trafic">
                        <h5>{{ $backlinkOverview['total']}}</h5>
                        <aside class="margin-left">Total BackLinks</aside>
                      </li>
                      <li>Referring Domains<span>{{ $backlinkOverview['domains_num']}}</span></li>
                      <li>Referring IPs<span>{{ $backlinkOverview['ips_num']}}</span></li>
                      @endforeach
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <ul>
                      <li>
                        <h4><i class="fa fa-circle danger" aria-hidden="true"></i>Display Advetising</h4>
                      </li>
                      <li class="trafic">
                        <h5>8</h5>
                        <aside class="margin-left">total Ads</aside>
                      </li>
                      <li>Publishers<span>34</span></li>
                      <li>Advertisers<span>0</span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <fieldset>
                <div class="row">
                  <div class="col-md-6">
                    <h3>Organic Keywords</h3>
                    <div class="chart">
                      <div id="donutchart"></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="chart">
                      <h3>
                        <label><i class="fa fa-circle info" aria-hidden="true"></i> Organic</label>
                        <label><i class="fa fa-circle orange" aria-hidden="true"></i> Paid</label>
                      </h3>
                      <div id="linechart"></div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <div class="row">
                  <div class="col-md-6">
                    <h3>Top Organic Keywords <span>({{ count($organicKeywords)}})</span>
                      <button class="btn btn-sm btn-success">Live Update</button>
                    </h3>
                    <table width="100%" class="table table-striped">
                      <thead>
                        <tr>
                          <th>Keyword</th>
                          <th>Pos.</th>
                          <th>Volume</th>
                          <th>CPC(USD)</th>
                          <th>Trafic</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php $i = 0; @endphp
                        @foreach ($organicKeywords as $organicKeyword)
                        @if ($i<5)
                        <tr>
                          <td><div class="ellipsis"><a href="#">{{ $organicKeyword['Keyword']}}</a></div></td>
                          <td>{{ $organicKeyword['Position']}} <span>({{ $organicKeyword['Previous Position']}})</span></td>
                          <td>{{ $organicKeyword['Search Volume']}}</td>
                          <td>{{ $organicKeyword['CPC']}}</td>
                          <td><div class="progress skill-bar">
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $organicKeyword['Traffic (%)']}}" aria-valuemin="0" aria-valuemax="100" ></div>
                            </div></td>
                        </tr>
                        @endif
                         @php $i = $i+1; @endphp
                        @endforeach
                      </tbody>
                    </table>
                    <button class="btn btn-sm btn-dark">view full report</button>
                  </div>
                  <div class="col-md-6">
                    <h3>Organic Position Distribution</h3>
                    <div class="chart">
                      <div id="barchart"></div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <h3>Main Organic Competitirs <span>(76)</span>
                      <button class="btn btn-sm btn-success">Live Update</button>
                    </h3>
                    <table width="100%" class="table table-striped">
                      <thead>
                        <tr>
                          <th>Competitir</th>
                          <th>Com. Level</th>
                          <th>Com. Keywords</th>
                          <th>SE Keywords</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach( $mainCompetitors as $mainCompetitor)
                        <tr>
                          <td><div class="ellipsis"><a href="#">{{ $mainCompetitor['Domain']}}</a></div></td>
                          <td><div class="progress skill-bar">
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $mainCompetitor['Competitor Relevance']}}" aria-valuemin="0" aria-valuemax="100" ></div>
                            </div></td>
                          <td>{{ $mainCompetitor['Common Keywords']}}</td>
                          <td>{{ $mainCompetitor['Organic Keywords']}}</td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                    <button class="btn btn-sm btn-dark">view full report</button>
                  </div>
                  <div class="col-md-6">
                    <h3>Competitive Position Map</h3>
                    <div class="chart">
                      <div id="map"></div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <div class="row">
                  <div class="col-md-12">
                    <h3>Backlinks</h3>
                    <table width="100%" class="table table-striped">
                      <thead>
                        <tr>
                          <th>Referring page title/ Referring page URL</th>
                          <th>Anchor text/ Link URL</th>
                          <th>Type</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($backlinks as $backlink)
                        <tr>
                          <td>{{ $backlink['source_title'] }}<br>
                            <a href="{{ $backlink['source_url'] }}">{{ $backlink['source_url'] }}</a></td>
                          <td><a href="{{ $backlink['target_url'] }}">{{ $backlink['target_url'] }}</a></td>
                          <td><span class="alert alert-info">image</span></td>
                        </tr>
                        @endforeach
                       
                      </tbody>
                    </table>
                    <button class="btn btn-sm btn-dark pull-right">view full report</button>
                    <br><br><br><br>
                    <h3>Top Anchors</h3>
                    <table width="100%" class="table table-striped">
                      <thead>
                        <tr>
                          <th>Anchors</th>
                          <th>Domains</th>
                          <th>Backlinks</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($anchors as $anchor)
                        <tr>
                          <td>{{ $anchor['anchor'] }}</td>
                          <td>{{ $anchor['domains_num'] }}</td>
                          <td>{{ $anchor['backlinks_num'] }}</td>
                        </tr>
                        @endforeach
                       
                      </tbody>
                    </table>
                    <button class="btn btn-sm btn-dark pull-right">view full report</button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <h3>Referring Domains</h3>
                    <table width="100%" class="table table-striped">
                      <thead>
                        <tr>
                          <th>Root Domain</th>
                          <th>Backlinks</th>
                          <th>IP/Country</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($referingDomains as $referingDomain)
                        <tr>
                          <td><i class="fa fa-arrow-right" aria-hidden="true"></i> <a href="#">{{ $referingDomain['domain']}}</a></td>
                          <td>{{ $referingDomain['backlinks_num']}}</td>
                          <td><img src="https://lipis.github.io/flag-icon-css/flags/4x3/{{ $referingDomain['country']}}.svg" width="18">{{ $referingDomain['ip']}}</td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                    <button class="btn btn-sm btn-dark pull-right">view full report</button>
                  </div>
                  <div class="col-md-6">
                    <h3>Index Pages</h3>
                    <table width="100%" class="table table-striped">
                      <thead>
                        <tr>
                          <th>Title and URL</th>
                          <th>Domains</th>
                          <th>Backlinks</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($indexBackLinks as $indexBackLink)
                        <tr>
                          <td><a href="{{ $indexBackLink['source_url']}}">{{ $indexBackLink['source_url']}}</a></td>
                          <td>{{ $indexBackLink['domains_num']}}</td>
                          <td>{{ $indexBackLink['backlinks_num']}}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <button class="btn btn-sm btn-dark pull-right">view full report</button>
                  </div>
                </div>
              </fieldset>
          </section>
        </div>
      </div>
    </div>
  </section>
</div>


@endsection

 

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('.progress .progress-bar').css("width",
      function() {
        return $(this).attr("aria-valuenow") + "%";
      }
    )
   });
</script> 
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['AU',     11],
      ['US',      2],
      ['Mobile',  2],
      ['CA', 2],
      ['FR',    7]
    ]);

    var options = {
      backgroundColor: 'transparent',
      pieHole: 0.4,
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
  }
  $(window).resize(function(){
    drawChart();
  });
</script> 
<!-- line chart --> 
<script type="text/javascript">
  google.charts.load('current', {'packages':['line']});
  google.charts.setOnLoadCallback(drawChart);

   function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Year', 'Organic', 'Paid'],
      ['2004',  1000,      400],
      ['2005',  1170,      460],
      ['2006',  660,       1120],
      ['2007',  1030,      540]
    ]);

    var options = {
      colors: ['#0053C4', '#E84009'],
     // title: 'Company Performance',
      curveType: 'function',
      backgroundColor: { fill:'transparent' },
      legend: { position: 'none' }
    };

  var chart = new google.visualization.LineChart(document.getElementById('linechart'));
    chart.draw(data, options);
  }
  $(window).resize(function(){
    drawChart();
  });
 </script> 
 <!-- bar chart --> 
 <script type="text/javascript">
  google.charts.load('current', {packages: ['corechart', 'bar']});
  google.charts.setOnLoadCallback(drawBasic);
  
  function drawBasic() {
  
        var data = new google.visualization.DataTable();
        data.addColumn('timeofday', '');
        data.addColumn('number', '');
  
        data.addRows([
          [{v: [8, 0, 0], f: '8 am'}, 1],
          [{v: [9, 0, 0], f: '9 am'}, 2],
          [{v: [10, 0, 0], f:'10 am'}, 3],
          [{v: [11, 0, 0], f: '11 am'}, 4],
          [{v: [12, 0, 0], f: '12 pm'}, 5],
          [{v: [13, 0, 0], f: '1 pm'}, 6],
          [{v: [14, 0, 0], f: '2 pm'}, 7],
          [{v: [15, 0, 0], f: '3 pm'}, 8],
          [{v: [16, 0, 0], f: '4 pm'}, 9],
          [{v: [17, 0, 0], f: '5 pm'}, 10],
        ]);
  
        var options = {
          //title: 'Motivation Level Throughout the Day',
          backgroundColor: 'transparent',
          legend: { position: 'bottom' },
          hAxis: {
            //title: 'Time of Day',
            //format: 'h:mm a',
            viewWindow: {
              min: [7, 30, 0],
              max: [17, 30, 0]
            }
          },
          vAxis: {
            title: 'Rating (Organic Keywords (N))'
          }
        };
  
        var chart = new google.visualization.ColumnChart(
          document.getElementById('barchart'));
        chart.draw(data, options);
      }
      $(window).resize(function(){
        drawChart();
      });
</script> 
<!-- map chart --> 
<script type="text/javascript">
  google.charts.load('current', {
    'packages':['geochart'],
    // Note: you will need to get a mapsApiKey for your project.
    // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
    'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
  });
  google.charts.setOnLoadCallback(drawRegionsMap);

  function drawRegionsMap() {
    var data = google.visualization.arrayToDataTable([
      ['Country', 'Popularity'],
      ['Germany', 200],
      ['United States', 300],
      ['Brazil', 400],
      ['Canada', 500],
      ['France', 600],
      ['RU', 700]
    ]);

    var options = {
      backgroundColor: 'transparent',  
    };

    var chart = new google.visualization.GeoChart(document.getElementById('map'));

    chart.draw(data, options);
  }
  $(window).resize(function(){
    drawChart();
  });
</script>
@endsection