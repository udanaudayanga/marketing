@extends('layouts.app')


@section('content')

<div id="wrapper">
  
  <section id="page-content-wrapper" class="analytics audit">
    <div class="container-wrapper container-fuild">
      <ol class="breadcrumb">
        <li><a href="/">Dashboard</a></li>
        <li class="active">Website Analytics</li>
      </ol>
      <div class="row">
        <div class="col-sm-12">
          <section class="content">
            <fieldset>
              <ul class="default-tab">
                 @if ($website1[0]->view_id != '')
                 <li><a href="{{ route('webanalytics_info',[$website1[0]->view_id]) }}" class="">Website - <em>JPLMS</em><small>{{$website1[0]->url}}</small></a></li>
                 @endif
                @if ($website1[0]->semrush_project_id != '')
                <li><a href="{{ route('semrush_site_audit',[$website1[0]->semrush_project_id]) }}"><span>Site Audit Results</span></a></li>
                <li><a href="{{route('semrush_competitor_analysis',[$website1[0]->semrush_project_id])}}"><span>Competitor Analysis</span></a></li>
                <li><a href="#"><span>Keyword Performance</span></a></li>
                @endif
              </ul>
              <div class="row">
                <div class="col-md-4">
                  <div class="total-score">
                    <h3>Total Score <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="The total score of the website is based on the ratio of found issues to the number of performed checks."></i></h3>
                    <div class="chart_wrapper">
                      <input id="chart_anim" type="text" class="dial" data-fgColor="#D71F22" data-bgColor="#f1f1f1" />
                      <span>%</span>
                       <em>{{ $response['current_snapshot']['quality']['delta']}}%</em>
                        </div>
                    <hr>
                    <h3>Crawled Pages <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Here you can view the total number of pages crawled by SEMrushBot. The bar chart shows the distribution of pages by their status."></i></h3>
                    <div class="count">{{ $response['pages_crawled']}}</div>
                    <div id="piechart"></div>
                  </div>
             
                </div>
                <div class="col-md-8">
                  <div class="row site-result">
                    <div class="col-sm-4">
                      <h3>Errors <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Here you can view the total number of pages crawled by SEMrushBot. The bar chart shows the distribution of pages by their status."></i></h3>
                      <h5><a href="#" class="danger">{{ $response['errors']}}</a><span><a href="#">{{ $response['errors_delta']}}</a></span></h5>
                      <div id="curve_chart" style="width: 200px; height: 300px"></div>
                    </div>
                    <div class="col-sm-4">
                      <h3>Warnings <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Here you can view the total number of pages crawled by SEMrushBot. The bar chart shows the distribution of pages by their status."></i></h3>
                      <h5><a href="#" class="warning">{{ $response['warnings']}}</a><span><a href="#">{{ $response['warnings_delta']}}</a></span></h5>
                      <div id="curve_chart2" style="width: 200px; height: 300px"></div>
                    </div>
                    <div class="col-sm-4">
                      <h3>Notices <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Here you can view the total number of pages crawled by SEMrushBot. The bar chart shows the distribution of pages by their status."></i></h3>
                      <h5><a href="#" class="info">{{ $response['notices']}}</a><span><a href="#">{{ $response['notices_delta']}}</a></span></h5>
                      <div id="curve_chart3" style="width: 200px; height: 300px"></div>
                    </div>
                  </div>
                  <hr>
                  
                  <div class="row thematic-score">
                    <h3>Thematic Score</h3>
                    <div class="col-sm-4">
                      <div class="score">
                      <h4>Crawlability  <span class="alert alert-success">New</span></h4>
                      <div id="crawlability" class="chart-score"></div>
                      <span class="percentage">{{ $crawlability }}%</span> 
                      </div>
                    </div>
                    <div class="col-sm-4">
                    <div class="score">
                      <h4>HTTPS</h4>
                      <div id="https" class="chart-score"></div>
                      <span class="percentage">{{ $https }}%</span> 
                    </div>
                    </div>
                    <div class="col-sm-4">
                    <div class="score">
                      <h4>International SEO</h4>
                      <div id="seo" class="chart-score"></div>
                      <span class="percentage">{{ $seo}}%</span> 
                    </div>
                    </div>
                    <div class="col-sm-4">
                    <div class="score">
                      <h4>Performance</h4>
                      <div id="per" class="chart-score"></div>
                      <span class="percentage">{{ $performance }}%</span> 
                      </div>
                      </div>
                    <div class="col-sm-4">
                    <div class="score">
                      <h4>Internal Linking <span class="alert alert-success">New</span></h4>
                      <div id="link" class="chart-score"></div>
                      <span class="percentage">{{ $link }}%</span> 
                     </div>
                     </div>
                  </div>
                 <hr>
                  
                  <div class="top-issues">
                  <h3>Top Issues <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Here you can view the total number of pages crawled by SEMrushBot. The bar chart shows the distribution of pages by their status."></i></h3>
                     @if (isset($issueInfo[0]['info']))
                    <div class="row">
                      <div class="col-lg-12">
                        <label>{{ $issueInfo[0]['info']}}</label>
                        <div class="progress skill-bar">
                          <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{$issueInfo[0]['percentage']}}" aria-valuemin="0" aria-valuemax="100" ></div>
                          </div>
                          <small class="val">{{$issueInfo[0]['percentage']}}% of total issues </small> 
                      </div>
                    </div>
                    @endif
                       @if (isset($issueInfo[1]['info']))
                    <div class="row">
                      <div class="col-lg-12">
                        <label>{{ $issueInfo[1]['info']}}</label>
                        <div class="progress skill-bar">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{$issueInfo[1]['percentage']}}" aria-valuemin="0" aria-valuemax="100" ></div>
                          </div>
                          <small class="val">{{$issueInfo[1]['percentage']}}% of total issues</small>
                      </div>
                    </div>
                    @endif
                    @if (isset($issueInfo[2]['info']))
                    <div class="row">
                      <div class="col-lg-12">
                        <label>{{ $issueInfo[2]['info']}}</label>
                        <div class="progress skill-bar">
                          <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{{$issueInfo[2]['percentage']}}" aria-valuemin="0" aria-valuemax="100" ></div>
                          </div>
                          <small class="val">{{$issueInfo[2]['percentage']}}% of total issues</small>
                     </div>
                    </div>
                    @endif
                 
                  <a href="#" data-toggle="modal"  data-modal-uri="{{ route('semrush_site_audit_issues',$website1[0]->semrush_project_id) }}"  id="add-access" class="lg btn btn-default btn-sm">View all issues</a> </div>
                </div>
                
               
              </div>
            </fieldset>
          </section>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

 

@section('scripts')

<script src="{{ url('/js/jquery.knob.js')}}" type="text/javascript"></script> 
<script type="text/javascript">
  $(document).ready(function() {

     $('#add-access').modalRender();
     
    $('.progress .progress-bar').css("width",
      function() {
        return $(this).attr("aria-valuenow") + "%";
      }
    )
   });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    /* create jQuery knob with .dial class */
    $(".dial").knob({
      'readOnly': true
    });
      animateChart('chart_anim', 0, {{ $response['current_snapshot']['quality']['value']}});  
    /* Update circle value when input.animate changes */
    $("input.dial").change(function () {
      $(this).parent().siblings(".circle").find(".pie_value").text($(this).val() + "%");
    })
  
    /* Animate chart from start_val to end_val */
    function animateChart (chart_id, start_val, end_val) {
      $({chart_value: start_val}).animate({chart_value: end_val}, {
        duration: 1000, // 1 second animation
        easing: 'swing',
        step: function () {
          $('#' + chart_id).val(Math.ceil(this.chart_value)).trigger('change');
        },
        complete: function () {
          /* complete */
          console.log('complete');
        }
      });
    };
  });
</script> 
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartErrors);

       var jsonDataErrors = <?php echo $errorInfoData ?>;

      function drawChartErrors() {
     
          var data = google.visualization.arrayToDataTable(jsonDataErrors);

        var options = {
          //title: 'Company Performance',
          //curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
        
      }

      $(window).resize(function(){
        //drawChartErrors();
      });
    </script> 
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartWarn);

       var jsonDataWarn = <?php echo $warnInfoData ?>;

      function drawChartWarn() {
      /* var data = google.visualization.arrayToDataTable([
         jsonDataWarn
        ]);*/

          var data = google.visualization.arrayToDataTable(jsonDataWarn);


        var options = {
          //title: 'Company Performance',
          curveType: 'function',
          legend: { position: 'bottom' }

        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart2'));

        chart.draw(data, options);
        
      }

      $(window).resize(function(){
        //drawChartWarn();
      });
    </script> 
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartNotices);
      var jsonDataNotices = <?php echo $noticeInfoData ?>;
      function drawChartNotices() {
        /*var data = google.visualization.arrayToDataTable([
         jsonDataNotices
        ]);*/

         var data = google.visualization.arrayToDataTable(jsonDataNotices);



        var options = {
          //title: 'Company Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart3'));

        chart.draw(data, options);
        
      }

      $(window).resize(function(){
       // drawChartNotices();
      });
    </script> 
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartCrawled);
      $(window).resize(function(){
        drawChartCrawled();
        drawChartCrawlability();
        drawChartHttps();
        drawChartSEO();
        drawChartPerformance();
        drawChartLinking();

      });

      //Crawled

      function drawChartCrawled() {

        var data = google.visualization.arrayToDataTable([
          ['Pages', 'Crawled Pages'],
          ['Healthy',     {{ $response['healthy']}}],
          ['Broken',      {{ $response['broken']}}],
          ['Have issues',  {{ $response['haveIssues']}}],
          ['Redirects', {{ $response['redirected']}}],
          ['Blocked',    {{ $response['blocked']}}]
        ]);
        
        var options = {
          'backgroundColor': 'transparent',
           legend: { position: 'bottom' },
          animation: {
              duration: 1000,
              easing: 'in',
              startup: true
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }

       
    //Crawlability
      google.charts.setOnLoadCallback(drawChartCrawlability);
      function drawChartCrawlability() {
        var data = google.visualization.arrayToDataTable([
          ['Status', 'Crawlability '],
          ['None',     {{ $nonCrawlability}}],
          ['Crawlability ',  {{ $crawlability}} ],
        ]);

        var options = {
          'backgroundColor': 'transparent',
          pieHole: 0.4,
          legend: 'none',
          pieSliceText: "none",
          enableInteractivity : false,
          colors: ['#e0440e', '#ccc'],
          animation: {
            duration: 1000,
            easing: 'in',
            startup: true
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('crawlability'));
        chart.draw(data, options);
        // initial value
        var percent = 0;
        // start the animation loop
        var handler = setInterval(function(){
            // values increment
            percent += 1;
            // apply new values
            data.setValue(0, 1, percent);
            data.setValue(1, 1, 100 - percent);
            // update the pie
            chart.draw(data, options);
            // check if we have reached the desired value
            if (percent !=0 && percent >  {{ $crawlability}})
                // stop the loop
                clearInterval(handler);
        }, 30);
      }


      //draw HTTPS chart
        google.charts.setOnLoadCallback(drawChartHttps);
         function drawChartHttps() {
        var data = google.visualization.arrayToDataTable([
          ['Status', 'Crawlability '],
          ['None',     {{ $nonhttps}}],
          ['HTTPS ',   {{ $https}}],
        ]);

        var options = {
          //title: 'My Daily Activities',
          'backgroundColor': 'transparent',
          pieHole: 0.4,
          legend: 'none',
          pieSliceText: "none",
          enableInteractivity : false,
          colors: ['#e0440e', '#ccc'],
          animation: {
            duration: 1000,
            easing: 'in',
            startup: true
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('https'));
        chart.draw(data, options);
        // initial value
        var percent = 0;
        // start the animation loop
        var handler = setInterval(function(){
            // values increment
            percent += 1;
            // apply new values
            data.setValue(0, 1, percent);
            data.setValue(1, 1, 100 - percent);
            // update the pie
            chart.draw(data, options);
            // check if we have reached the desired value
            if (percent !=0 && percent >={{ $https}})
                // stop the loop
                clearInterval(handler);
        }, 30);
      }

 //draw SEO chart
        google.charts.setOnLoadCallback(drawChartSEO);
         function drawChartSEO() {
        var data = google.visualization.arrayToDataTable([
          ['Status', 'Crawlability '],
          ['None',     {{ $nonseo}}],
          ['SEO ',   {{ $seo}}],
        ]);

        var options = {
          //title: 'My Daily Activities',
          'backgroundColor': 'transparent',
          pieHole: 0.4,
          legend: 'none',
          pieSliceText: "none",
          enableInteractivity : false,
          colors: ['#e0440e', '#ccc'],
          animation: {
            duration: 1000,
            easing: 'in',
            startup: true
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('seo'));
        chart.draw(data, options);
        // initial value
        var percent = 0;
        // start the animation loop
        var handler = setInterval(function(){
            // values increment
            percent += 1;
            // apply new values
            data.setValue(0, 1, percent);
            data.setValue(1, 1, 100 - percent);
            // update the pie
            chart.draw(data, options);
            // check if we have reached the desired value
            if (percent !=0 && percent >={{ $seo}})
                // stop the loop
                clearInterval(handler);
        }, 30);
      }



 //draw Performance chart
        google.charts.setOnLoadCallback(drawChartPerformance);
         function drawChartPerformance() {
        var data = google.visualization.arrayToDataTable([
          ['Status', 'Crawlability '],
          ['None',     {{ $nonperformance}}],
          ['Performance ',   {{ $performance}}],
        ]);

        var options = {
          //title: 'My Daily Activities',
          'backgroundColor': 'transparent',
          pieHole: 0.4,
          legend: 'none',
          pieSliceText: "none",
          enableInteractivity : false,
          colors: ['#e0440e', '#ccc'],
          animation: {
            duration: 1000,
            easing: 'in',
            startup: true
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('per'));
        chart.draw(data, options);
        // initial value
        var percent = 0;
        // start the animation loop
        var handler = setInterval(function(){
            // values increment
            percent += 1;
            // apply new values
            data.setValue(0, 1, percent);
            data.setValue(1, 1, 100 - percent);
            // update the pie
            chart.draw(data, options);
            // check if we have reached the desired value
            if (percent !=0 && percent >={{ $performance}})
                // stop the loop
                clearInterval(handler);
        }, 30);
      }


      //draw internal linking chart
        google.charts.setOnLoadCallback(drawChartLinking);
         function drawChartLinking() {
        var data = google.visualization.arrayToDataTable([
          ['Status', 'Crawlability '],
          ['None',     {{ $nonlink}}],
          ['SEO ',   {{ $link}}],
        ]);

        var options = {
          //title: 'My Daily Activities',
          'backgroundColor': 'transparent',
          pieHole: 0.4,
          legend: 'none',
          pieSliceText: "none",
          enableInteractivity : false,
          colors: ['#e0440e', '#ccc'],
          animation: {
            duration: 1000,
            easing: 'in',
            startup: true
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('link'));
        chart.draw(data, options);
        // initial value
        var percent = 0;
        // start the animation loop
        var handler = setInterval(function(){
            // values increment
            percent += 1;
            // apply new values
            data.setValue(0, 1, percent);
            data.setValue(1, 1, 100 - percent);
            // update the pie
            chart.draw(data, options);
            // check if we have reached the desired value
            if (percent !=0 && percent >={{ $link}})
                // stop the loop
                clearInterval(handler);
        }, 30);
      }


    </script>
 
@endsection