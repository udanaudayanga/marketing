  <div class="modal fade in" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block; padding-left: 15px;">
        <div class="modal-dialog modal-lg" role="document">
           <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
        <h4 class="modal-title">Top Issues</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
          <div class="issues-info">
            <!-- Nav tabs -->
           
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all">
                <div class="danger">
                
                  <table class="table table-hover table-striped">
                    <tbody>
                    @foreach ($issueInfoAll as $issue)
                      <tr>
                        <td width="50"><i class="fa fa-times-circle" aria-hidden="true"></i></td>
                        <td width="150"><strong>{{ $issue['title']}}</strong></td>
                        <td>{{ $issue['title_page']}}</td>
                      </tr>
                    @endforeach
                      
                    </tbody>
                  </table>

                </div>
             {{--   <div class="warning">
                  <h3>Warnnings</h3>
                  <table class="table table-hover table-striped">
                    <tbody>
                      <tr>
                        <td width="50"><i class="fa fa-exclamation-triangle"></i></td>
                        <td width="150"><strong>Images</strong></td>
                        <td>47 images don't have alt attributes</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-exclamation-triangle"></i></td>
                        <td><strong>Uncompressed</strong></td>
                        <td>28 uncompressed pages</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-exclamation-triangle"></i></td>
                        <td><strong>Title Tags</strong></td>
                        <td>3 pages don't have enough text within the title tags</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-exclamation-triangle"></i></td>
                        <td><strong>H1 Tags</strong></td>
                        <td>3 pages don't have an h1 heading</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="notice">
                  <h3>Notices</h3>
                  <table class="table table-hover table-striped">
                    <tbody>
                      <tr>
                        <td width="50"><i class="fa fa-info-circle" aria-hidden="true"></i></td>
                        <td width="150"><strong>Crawling</strong></td>
                        <td>3 pages are blocked from crawling</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-info-circle" aria-hidden="true"></i></td>
                        <td><strong>Internal Link</strong></td>
                        <td> have only one incoming internal link</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-info-circle" aria-hidden="true"></i></td>
                        <td><strong>H1 Tags</strong></td>
                        <td>1 page has more than one h1 tag</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div> 
              <div role="tabpanel" class="tab-pane danger" id="error">
                <div class="danger">
                  <h3>Errors</h3>
                  <table class="table table-hover table-striped">
                    <tbody>
                      <tr>
                        <td width="50"><i class="fa fa-times-circle" aria-hidden="true"></i></td>
                        <td width="150"><strong>Meta Title</strong></td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-times-circle" aria-hidden="true"></i></td>
                        <td><strong>Meta Description</strong></td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-times-circle" aria-hidden="true"></i></td>
                        <td><strong>Incorrect Page</strong></td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-times-circle" aria-hidden="true"></i></td>
                        <td><strong>Sitemap</strong></td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-times-circle" aria-hidden="true"></i></td>
                        <td><strong>Viewport</strong></td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </td>
                      </tr>
                    </tbody>
                  </table>

                </div>
              </div>
              <div role="tabpanel" class="tab-pane warning" id="warning">
                <div class="warning">
                  <h3>Warnnings</h3>
                  <table class="table table-hover table-striped">
                    <tbody>
                      <tr>
                        <td width="50"><i class="fa fa-exclamation-triangle"></i></td>
                        <td width="150"><strong>Images</strong></td>
                        <td>47 images don't have alt attributes</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-exclamation-triangle"></i></td>
                        <td><strong>Uncompressed</strong></td>
                        <td>28 uncompressed pages</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-exclamation-triangle"></i></td>
                        <td><strong>Title Tags</strong></td>
                        <td>3 pages don't have enough text within the title tags</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-exclamation-triangle"></i></td>
                        <td><strong>H1 Tags</strong></td>
                        <td>3 pages don't have an h1 heading</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane info" id="notice">
                <div class="notice">
                  <h3>Notices</h3>
                  <table class="table table-hover table-striped">
                    <tbody>
                      <tr>
                        <td width="50"><i class="fa fa-info-circle" aria-hidden="true"></i></td>
                        <td width="150"><strong>Crawling</strong></td>
                        <td>3 pages are blocked from crawling</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-info-circle" aria-hidden="true"></i></td>
                        <td><strong>Internal Link</strong></td>
                        <td> have only one incoming internal link</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-info-circle" aria-hidden="true"></i></td>
                        <td><strong>H1 Tags</strong></td>
                        <td>1 page has more than one h1 tag</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>--}}
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
               
        </div>
    </div>
