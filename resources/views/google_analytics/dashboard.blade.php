@extends('layouts.app')

@section('content')
    <link href="{{ url('/css/custom.css') }}" rel="stylesheet" type="text/css">
    <section id="page-content-wrapper">
        <div ng-app="app" ng-controller="gaDashboardCtrl" class="container-wrapper container-fuild" ng-init="init();">
            <div class="row">
                <div class="col-md-12">
                    <div class="row clients">
                        <div class="col-md-12">
                            <h2>Google Analytics</h2>
                        </div>
                    </div>

                    {{-- Visits Count --}}
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Visits</h4>
                        </div>
                        <div class="col-md-8">
                            <canvas class="chart chart-line" chart-data="report.visits.data" chart-labels="report.visits.label"
                                    chart-series="report.visits.series"></canvas>
                        </div>
                        <div class="col-md-3" >
                            <table class="table custom_table">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Visits</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="visit in report.visits.table">
                                        <td><%visit.date%></td>
                                        <td><%visit.visit_count%></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    {{-- Visits by device --}}
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Visits by device</h4>
                        </div>
                        <div class="col-md-8">
                            <canvas class="chart chart-doughnut" chart-data="report.visitsByDevice.data" chart-labels="report.visitsByDevice.label" chart-colors="chart.colors">
                            </canvas>
                        </div>
                        <div class="col-md-3" >
                            <table class="table custom_table">
                                <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>visits</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="user in report.visitsByDevice.table">
                                    <td><%user.category%></td>
                                    <td><%user.visits%></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    {{-- Page visits and count --}}
                    <div class="row">
                        <div class="col-md-12">
                            <h4>What pages do your users visit? </h4>
                        </div>
                        <div class="col-md-5" >
                            <table class="table custom_table">
                                <thead>
                                <tr>
                                    <th>Page</th>
                                    <th>Pageviews</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="pageView in report.topViewedPages.table">
                                    <td><%pageView.page_path%></td>
                                    <td><%pageView.view_count%></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <script src="{{ url('/js/angular/angular.min.js') }}"></script>
    <script src="{{ url('/js/angular/Chart.min.js') }}"></script>
    <script src="{{ url('/js/angular/angular-chart.min.js') }}"></script>
    <script src="{{ url('/js/angular/app.js') }}"></script>
@endsection