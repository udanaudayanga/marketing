@extends('layouts.app')


@section('content')
<div id="wrapper" class="client-dashboard">
  <section id="page-content-wrapper">
    <div class="container-wrapper container-fuild">
      <ol class="breadcrumb">
        <li><a href="dashboard.html">Dashboard</a></li>
        <li class="active">Users</li>
      </ol>
      <div class="row">
        <div class="col-md-12">
          <h2 class="pull-left">All Users</h2>
          <a href="{{ route('dashboard') }}" class="btn btn-grey pull-right"><i class="fa fa-chevron-left" aria-hidden="true" ></i> Back</a>
          <section class="content">
            <fieldset>
              <div class="table-responsive-">
                <table class="table sotring-table table-striped" width="100%">
                  <thead>
                    <tr>
                      <th width="14%"> Name</th>
                      <th width="10%">Email</th>
                      <th width="10%"></th>
                       <th width="10%"></th>
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($employees as $employee)
                    <tr>
                        <td>{{ $employee->name }}</td>
      
                        <td>{{ $employee->email }}</td>
                        <td>{{ ucwords($employee->role)  }}</td>
                        <td>

                          
                            <a href="#" data-modal-uri="{{ route('employee_edit_modal', $employee->id ) }}" class="edit-employee">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> edit info
                            </a>
                            |
                            <a href="#" data-modal-uri="{{ route('employee_delete_modal', $employee->id )}}" class="delete-employee">
                                <i class="fa fa-times" aria-hidden="true"></i> delete
                            </a>
                          
                        </td>
                    </tr>
                    @endforeach
                 
                  </tbody>
                </table>
              </div>
            </fieldset>
          </section>
        </div>
      </div>
    </div>
  </section>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        $(function () {
            $('.edit-employee').modalRender();
            $('.delete-employee').modalRender();
        });
    </script>
@endsection