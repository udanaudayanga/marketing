@extends('layouts.app')


@section('content')
<div id="wrapper" class="accounts">

  <section id="page-content-wrapper">
    <div class="container-wrapper container-fuild">
      <div class="row">
        <div class="col-sm-12">
          <a href="{{ route('invoice_list') }}"><button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#add-new"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</button></a>
          <ol class="breadcrumb">
            <li><a href="accounts.html">HUD</a></li>
            <li>Invoices</li>
            <li class="active">Pay</li>
          </ol>
          <br>
          <ul class="default-tab">
            <li><a href="accounts-invoices.html" class="active">Invoices</a></li>
            <li><a href="accounts-payment-details.html">Payment Details</a></li>
          </ul>
          <h3>Payment Details</h3>
          <section class="content payments">
            <fieldset>
              <div class="">
                <table class="table sotring-table table-striped payment-details" width="100%">
                  <thead>
                    <tr>
                      <th>Invoice Date</th>
                      <th>Invoice ID</th>
                      <th>Description</th>
                      <th class="text-right">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($invoice->details as $detail)
                    <tr>
                      <td>{{ date('d/m/Y',strtotime($invoice->invoice_date))}} </td>
                      <td>{{ $invoice->no }}</td>
                      <td>{{ $detail->description }}</td>
                      <td class="text-right">${{ number_format($detail->amount,2) }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="text-right"><strong>Total</strong></td>
                      <td class="text-right"><strong>${{ number_format($invoice->total['subtotal'], 2) }}</strong></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </fieldset>
          </section>
          <h3>Select Payment Method</h3>
          <section class="content payments">
            <fieldset class="payment-mothod">
              <div class="form-wrapper">
                <div class="col-sm-6 col-sm-offset-3">
                  <form role="form">
                  <div class="payment-option">
                    <label>
                      <input type="radio" name="1" id="open" /><br>
                      Credit Card<br>
                      <img src="{{ asset('images/cc-types.png')}}" alt="Credit Card" class="img-responsive">
                    </label>
                    <label>
                      <input type="radio" name="1" id="close" /><br>
                      Paypal<br>
                      <img src="{{ asset('images/paypal.png')}}" alt="Paypal" class="img-responsive">
                    </label>
                  </div>
           
                  <div class="con">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="cardNumber"> CARD NUMBER</label>
                          <div class="input-group">
                            <input type="text" class="form-control" id="cardNumber" placeholder="Valid Card Number"
                                required autofocus />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span> </div>
                          <img src="{{ asset('images/cc.png')}}" alt="Credit Card Icon" class="icons">
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-4 pull-left">
                        <label for="expityMonth"> EXPIRY DATE</label>
                      </div>
                      <div class="clear"></div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" class="form-control" id="expityMonth" placeholder="MM" required />
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">/</div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <input type="text" class="form-control" id="expityYear" placeholder="YY" required />
                        </div>
                      </div>
                      <div class="col-md-3 pull-right cvv">
                        <div class="form-group">
                          <label for="cvCode"> CVV CODE</label>
                          <input type="password" class="form-control" id="cvCode" placeholder="CVV" required />
                        </div>
                        <img src="{{ asset('images/cvv.png')}}" alt="CVV Icon" class="icons">
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label><input type="checkbox">&nbsp;&nbsp;Save card for next time</label>
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-12">
                      <div class="col-md-6 total text-left">
                      Total     
                      </div>
                      <div class="col-md-6 total text-right">
                      $2,500.00     
                      </div>
                      </div>
                    </div>
                    <br>
                    <div class="row payment-btn">
                      <div class="col-md-12">
                        <div class="form-group">
                          <button class="btn btn-success">Make Payment</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              
                     <form data-parsley-validate class="form-horizontal form-label-left" action="{{route('payment_paypal')}}" method="post">
                         {{ csrf_field() }}

                         @if ($message = Session::get('success'))
    <div class="w3-panel w3-green w3-display-container">
        <span onclick="this.parentElement.style.display='none'"
                class="w3-button w3-green w3-large w3-display-topright">&times;</span>
        <p>{!! $message !!}</p>
    </div>
    <?php Session::forget('success');?>
    @endif
@if ($message = Session::get('error'))
    <div class="w3-panel w3-red w3-display-container">
        <span onclick="this.parentElement.style.display='none'"
                class="w3-button w3-red w3-large w3-display-topright">&times;</span>
        <p>{!! $message !!}</p>
    </div>
    <?php Session::forget('error');?>
    @endif
                  <div class="con2">
                    <div class="row">
                      <div class="col-md-12">
                      <div class="col-md-6 total text-left">
                      Total     
                      </div>
                      <div class="col-md-6 total text-right">
                         <input name="invo_id" value="{{$invoice->id}}" type="text">
                        <input name="amount" value="{{$invoice->total['total']}}" type="hidden">
                      ${{ number_format($invoice->total['total'], 2) }}
                      </div>
                      </div>
                    </div>
                    <br>
                    <div class="row payment-btn">
                      <div class="col-md-12">
                        <div class="form-group">
                          <button class="btn btn-success">Make Payment</button>
                        </div>
                      </div>
                    </div>
                  </div>
</form>
                  
                  
    
    
                </div>
              </div>
            </fieldset>
          </section>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){
     $('#add-access').modalRender();
      $("#open").click(function(){
      $(".con").animate({height: "toggle"}, 500);
      $(".con2").hide();
    });
    $("#close").click(function(){
      $(".con").hide();
      $(".con2").animate({height: "toggle"}, 500);
    });
  });;
</script>
    @stop






        