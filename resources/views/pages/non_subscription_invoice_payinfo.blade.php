@extends('layouts.app')


@section('content')
<div id="wrapper" class="accounts">

  <section id="page-content-wrapper">
    <div class="container-wrapper container-fuild">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li><a href="{{ route('dashboard')}}">HUD</a></li>
            <li class="active">Invocie Non Subscription Payment</li>
          </ol>
          <br>
          <ul class="default-tab">
            <li><a href="{{ route('invoice_list')}}" class="active">Invoices</a></li>
            <li><a href="{{ route('payment_detail')}}">Payment Details</a></li>
          </ul>
          <br>
          <section class="content payments details">
            <fieldset class="">
              <div class="col-sm-6">
                <h3>JPL Distribution Pty Ltd Merchant Facility<br>
                  <strong>ID</strong>: 0011257785</h3>
              </div>
              <div class="col-sm-6">
                <h3>Receipt Email</h3>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-8">
                      <input type="text" class="form-control">
                    </div>
                    <label class="col-sm-4 control-label"><a href="#"><i class="fa fa-plus" aria-hidden="true"></i> Add</a></label>
                  </div>
                </div>
                <br>
                <div class="row">
                  @foreach ($recieptEmails as $recieptEmail)
                  <div class="col-sm-12">
                    <div class="col-sm-8"> {{ $recieptEmail->email }} </div>
                    <label class="col-sm-4" ><a href="#" data-toggle="modal" data-target="#edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> edit</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> delete</a></label>
                  </div>
                  @endforeach
                </div>
              </div>
            </fieldset>
          </section>
          <h3>Payment Details</h3>
          <section class="content payments">
            <fieldset>
              <div class="">
                <table class="table sotring-table table-striped payment-details" width="100%">
                  <thead>
                    <tr>
                      <th>Invoice Date</th>
                      <th>Invoice ID</th>
                      <th>Description</th>
                      <th class="text-right">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($invoice->details as $detail)
                    <tr>
                      <td>{{ date('d/m/Y',strtotime($invoice->invoice_date))}}</td>
                      <td>{{ $invoice->no }}</td>
                      <td>{{ $detail->description }}</td>
                      <td class="text-right">${{ number_format($detail->amount,2) }}</td>
                    </tr>
                     @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="sm text-right">Sub Total</td>
                      <td class="sm text-right">${{ number_format($invoice->total['subtotal'], 2) }}</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="sm text-right">GST</td>
                      <td class="sm text-right">${{ number_format($invoice->total['gst'], 2) }}</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="text-right"><strong>Total</strong></td>
                      <td class="text-right"><strong>${{ number_format($invoice->total['total'], 2) }}</strong></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </fieldset>
          </section>
          <h3>Select Payment Method</h3>
          <section class="content payments details">
            <fieldset>
              @foreach ($paymentMethods as $method)
              @if ( $method->set_default == 1)
              <div class="select-card active">
              @else
              <div class="select-card">
              @endif
                <div class="row">
                  @if ( $method->card_type == 'Visa')
                  <div class="col-sm-8"> <img src="{{ asset('images/visa-sm.png') }}" alt="Visa">
                  @elseif ($method->card_type == 'Master')
                  <div class="col-sm-8"> <img src="{{ asset('images/master-sm.png') }}" alt="Master">
                  @endif
                    @php $masked = str_pad(substr($method->card_number, -4), strlen($method->card_number), '*', STR_PAD_LEFT); @endphp
                    <h4>{{ $method->card_type }} ({{ $masked }})<br>
                      <span>Expiration Date {{ $method->exp_date_month }}/{{ $method->exp_date_year }}</span> </h4>
                     @if ( $method->set_default == 1)<p><span class="label">DEFAULT</span><a href="#">Disable as Default</a></p>@endif
                  </div>
                  <div class="col-sm-4 text-right">
                    <h5>{{ $method->name_on_card }}</h5>
                    <a href="#" data-modal-uri="{{ route('payment_methods_edit', $method->id) }}" class="pull-right edit-access"><i class="fa fa-pencil-square-o"></i> edit</a>&nbsp;&nbsp;&nbsp;<a href="#" data-modal-uri="{{ route('payment_methods_delete_model', $method->id ) }}"   class="del-card"><i class="fa fa-trash-o"></i> delete</a> </div>
                </div>
              </div>
              @endforeach
              
             <a href="#" data-toggle="modal"  data-modal-uri="{{ route('show_payment_methods') }}"  id="add-access" class="lg"> <button class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Add New Payment Method</button></a>
            </fieldset>
          </section>
          <section class="content payments">
            <fieldset class="payment-mothod">
              <br>
               {{-- <form data-parsley-validate class="form-horizontal form-label-left" action="{{route('payment_paypal')}}" method="post">--}}

               <form data-parsley-validate class="form-horizontal form-label-left" action="{{route('payment_com')}}" method="post">
                         {{ csrf_field() }}

              <div class="row">
                <div class="col-md-6 col-md-offset-3">
                  <div class="col-md-6 total text-left"> Total </div>
                  <div class="col-md-6 total text-right">  <input name="invo_id" value="{{$invoice->id}}" type="hidden">
                        <input name="amount" value="{{$invoice->total['total']}}" type="hidden">
                      ${{ number_format($invoice->total['total'], 2) }} 
                  </div>
                </div>
              </div>
              <br>
              <div class="row payment-btn">
                <div class="col-md-6 col-md-offset-3">
                  <div class="form-group">
                    <button class="btn btn-success">Make Payment</button>
                  </div>
                </div>
              </div>
              </form>
            </fieldset>
          </section>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){

       $('#add-access').modalRender();
        $('.edit-access').modalRender();
 $('.del-card').modalRender();
 
      $("#open").click(function(){
      $(".con").animate({height: "toggle"}, 500);
      $(".con2").hide();
    });
    $("#close").click(function(){
      $(".con").hide();
      $(".con2").animate({height: "toggle"}, 500);
    });



  });
</script>
    @stop






        