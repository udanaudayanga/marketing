@extends('layouts.app')

@section('content')
 <div id="wrapper" class="client-dashboard">
        <section id="page-content-wrapper">
            <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
        <li class="active">Support</li>
      </ol>
            <div class="container-wrapper container-fuild">
             

                <a href="#" class="btn btn-default pull-right"
                   data-modal-uri="{{ route('job_modal') }}" id="add-job" style="margin-right: 10px">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New Request</a>

              
               
                <div class="row">
                    <div class="col-sm-4">
                        <h2 class="text-center">Received</h2>
                        <section class="content">
                            <fieldset>
                                <table class="table table-bordered table-hover todo">
                                    <tbody> 
                                    @foreach($jobs['todo'] as $job)
                                   
                                        @component('components.job')

                                            @slot('code')
                                              {{ $job->code }}
                                              @foreach (  $job->assignees as $assignee )
                                                <mark>[{{ $assignee->name}}]</mark>
                                              @endforeach
                                            @endslot

                                            @slot('status')
                                            {{ $job->status }}
                                            @endslot

                                            @slot('title')
                                              {{ $job->title }}
                                            @endslot

                                            @slot('priority')
                                              {{ $job->priority }}
                                            @endslot

                                            @slot('id')
                                              {{ $job->id }}
                                            @endslot

                                           

                                            @slot('type')
                                                {{ $job->type }}
                                            @endslot

                                        @endcomponent
                                    @endforeach
                                    </tbody>
                                </table>
                            </fieldset>
                        </section>
                    </div>
                    <div class="col-sm-4">
                        <h2 class="text-center">Working</h2>
                        <section class="content">
                            <fieldset>
                                <table class="table table-bordered table-hover in-progress">
                                    <tbody>
                                    @foreach($jobs['inProgress'] as $job)
                                        @component('components.job')

                                        @slot('code')
                                        {{ $job->code }}
                                         @foreach (  $job->assignees as $assignee )
                                        <mark>[{{ $assignee->name}}]</mark>
                                        @endforeach
                                        @endslot

                                        @slot('status')
                                        {{ $job->status }}
                                        @endslot

                                        @slot('title')
                                        {{ $job->title }}
                                        @endslot

                                        @slot('priority')
                                        {{ $job->priority }}
                                        @endslot

                                        @slot('id')
                                        {{ $job->id }}
                                        @endslot

                                       

                                        @slot('type')
                                            {{ $job->type }}
                                        @endslot

                                        @endcomponent
                                    @endforeach
                                    </tbody>
                                </table>
                            </fieldset>
                        </section>
                    </div>
                    <div class="col-sm-4">
                        <h2 class="text-center">Resolved</h2>
                        <section class="content">
                            <fieldset>
                                <table class="table table-bordered table-hover done">
                                    <tbody>
                                    @foreach($jobs['done'] as $job)
                                        @component('components.job')

                                            @slot('code')
                                                {{ $job->code }}
                                                 @foreach (  $job->assignees as $assignee )
                                                <mark>[{{ $assignee->name}}]</mark>
                                                @endforeach
                                            @endslot

                                            @slot('status')
                                                {{ $job->status }}
                                            @endslot

                                            @slot('title')
                                                {{ $job->title }}
                                            @endslot

                                            @slot('priority')
                                                {{ $job->priority }}
                                            @endslot

                                            @slot('id')
                                                {{ $job->id }}
                                            @endslot

                                           

                                            @slot('type')
                                                {{ $job->type }}
                                            @endslot

                                        @endcomponent
                                    @endforeach
                                    </tbody>
                                </table>
                            </fieldset>
                        </section>
                    </div>
                </div>
            </div>
        </section>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

        function updateQueryString(key, value, url) {
            if (!url) url = window.location.href;
            let re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
                hash;

            if (re.test(url)) {
                if (typeof value !== 'undefined' && value !== null)
                    return url.replace(re, '$1' + key + "=" + value + '$2$3');
                else {
                    hash = url.split('#');
                    url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
                    if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                        url += '#' + hash[1];
                    return url;
                }
            }
            else {
                if (typeof value !== 'undefined' && value !== null) {
                    var separator = url.indexOf('?') !== -1 ? '&' : '?';
                    hash = url.split('#');
                    url = hash[0] + separator + key + '=' + value;
                    if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                        url += '#' + hash[1];
                    return url;
                }
                else
                    return url;
            }
        }

        $(function () {
            $('#add-job').modalRender();
            $('#add-task').modalRender();
            $('.job-edit').modalRender();
            $('.del-job').modalRender();
          
        });
    </script>

    @stop