@extends('layouts.app')


@section('content')
<div id="wrapper" class="dashboard-email">
  <section id="page-content-wrapper">
    <div class="container-wrapper container-fuild">
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
        <li class="active">Email Addresses</li>
      </ol>
      <div class="row">
        <div class="col-md-12">
          <h2 class="pull-left">Email Addresses</h2>
          <a href="{{ route('dashboard') }}" class="btn btn-grey pull-right"><i class="fa fa-chevron-left" aria-hidden="true" ></i> Back</a>
          <section class="content">
            <fieldset class="email">
              <div class="row">
                @foreach($emailAddresses as $address)
                <div class="col-sm-4">
                  <div class="email-block">{{ $address->email }} <span>{{ $address->password}}</span></div>
                </div>
                   @endforeach          
              </div>
            </fieldset>
          </section>
        </div>
      </div>
    </div>
  </section>
  </div>

 

@endsection


