@extends('layouts.app')

@section('content')
    <div id="wrapper" class="client-dashboard">
 
        <section id="page-content-wrapper">
            <div class="container-wrapper container-fuild"> <a href="#" data-modal-uri="{{ route('job_edit',  $job->id ) }}"  class="btn btn-default pull-right job-edit" style="margin-left: 10px"><i class="fa fa-edit"></i> Edit Job</a>&nbsp; <a href="{{ url()->previous() }}" class="btn btn-grey pull-right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
                <ol class="breadcrumb">
                    <li><a href="#">Client</a></li>
                    <li class="active">{{ $job->client->company_name }}</li>
                </ol>
                <div class="row">
                    <div class="col-sm-12">
                        <h2>{{ $job->title }}</h2>
                    </div>
                    <div class="col-sm-12">
                        <section class="content">
                            <form data-parsley-validate class="form-horizontal form-label-left">
                                <div class="form-wrapper">
                                    <div id="msg">
                        @if (session('message'))
                            <p>{{ session('message') }}</p>
                        @endif
                    </div>
                                    <div class="">
                                          <div class="form-group">
                    <div class="row status">
                                              <div class="col-sm-12">
                                                    <label class="control-label pull-left">
                                                        <Strong>Job ID:</Strong>&nbsp;&nbsp;<i>{{ $job->code }}</i>&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;
                                                    </label>
                                                    <label class="control-label pull-right">
                                                        <Strong>Status:</Strong>&nbsp;&nbsp;<i @if($job->status == 'done') class="completed" @endif >{{ ucfirst($job->status) }}</i>&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;
                                                    </label>
                                                </div>
                                            </div>
                                            </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3" for="title">Title</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="title" disabled value="{{ $job->title }}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3" for="created">Created By</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="created" disabled value="{{ $job->creator->name }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3">Priority</label>
                                                    <div class="col-sm-9">
                                                            <input type="text" class="form-control" name="created" disabled value="{{ ucfirst($job->priority) }}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3" for="created-date">Created Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="created-date" disabled value="{{ $job->created_at->format('d/m/Y') }}" class="form-control has-feedback-left calendar-single" placeholder="mm/dd/yyyy">
                                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3">Type</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" name="created" disabled value="{{ ucfirst($job->type) }}">
                                                        </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-3">Client</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" name="created" disabled value="{{ ucfirst($job->client->company_name ) }}">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3" for="due-date">Due Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="due-date" class="form-control has-feedback-left calendar-single" placeholder="mm/dd/yyyy" disabled value="{{ date('d/m/Y',strtotime($job->due_date)) }}">
                                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span> </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3">Assign To</label>
                                                    <div class="col-sm-9">
                                                        <ol>
                                                            @foreach($job->assignees as $assignee )
                                                                <li> {{ $assignee->name }}</li>
                                                            @endforeach
                                                        </ol>
                                                    </div>
                                                </div>

                                            </div>
                                             <div class="form-group">
                                            <div class="row">
                                                
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3">Assignees Histroy</label>
                                                    <div class="col-sm-9">
                                                        <ol>

                                                            @foreach($job->allAssignees as $assignee )
                                                          
                                                                <li> {{ $assignee->name }}</li>
                                                                 @if(!$loop->last)
                                                                <li><i class="fa fa-angle-right"></i></li>
                                                                @endif
                                                            @endforeach
                                                        </ol>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label class="control-label col-sm-12">Description</label>
                                                        <div class="col-sm-12">
                                                            <textarea class="form-control" rows="25" disabled="">{{ $job->description }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label class="control-label col-sm-12">Tasks</label>
                                                        <div class="col-sm-12">
                                                            <table width="100%" class="table table-striped">
                                                                <tbody id="task-container">
                                                                @foreach($job->tasks  as $key => $task)
                                                                    @component('components.sub_task_list')

                                                                        @slot('index')
                                                                            {{ $key+1 }}
                                                                        @endslot

                                                                        @slot('title')
                                                                            {{ $task->title }}
                                                                        @endslot

                                                                        @slot('id')
                                                                            {{ $task->id }}
                                                                        @endslot

                                                                        @slot('is_completed')
                                                                            {{ $task->is_completed }}
                                                                        @endslot

                                                                    @endcomponent
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label class="control-label col-sm-12">Attachement</label>
                                                        <div class="col-sm-12">
                                                            <div class="well">
                                                                @foreach( $job->attachments as $attachment)
                                                                    @component('components.attachment')

                                                                        @slot('id')
                                                                            {{ $attachment->id }}
                                                                        @endslot

                                                                        @slot('mine_type')
                                                                            {{ $attachment->mine_type }}
                                                                        @endslot

                                                                        @slot('original_name')
                                                                            {{ $attachment->original_name }}
                                                                        @endslot

                                                                        @slot('path')
                                                                            {{ $attachment->path }}
                                                                        @endslot

                                                                        @slot('delete_action')
                                                                            {{ 0 }}
                                                                        @endslot

                                                                    @endcomponent
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                                    <div class="comments">
                                      <h5>Activity</h5>
                                      @foreach( $job->comments as $comment)
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <h6><span>{{ $comment->name }}</span> added a comment - {{ date('d/m/Y H:i',strtotime($comment->updated_at))}}
                                              @if(user()->isAdmin() && $comment->hours>0)
                                                <em class="logged-time">Logged {{ $comment->hours}}hrs</em>
                                              @elseif( $comment->created_by == user()->id  && $comment->hours>0)
                                                <em class="logged-time">Logged {{ $comment->hours}}hrs</em>
                                             @endif

                                          </h6>
                                          <p>{{ $comment->comments}}</p>
                                          <ul>
                                           
                                            <li><a href="#">edit</a></li>
                                            <li><a href="#">delete</a></li>
                                          </ul>
                                        </div>
                                      </div>
                                      @endforeach
                                      <form data-parsley-validate class="form-horizontal form-label-left" action="{{ route('save_comment',$job->id) }}" method="post">
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <h3><a role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-plus"></i> Add Note</a></h3>
                                      </div>
                                      <br>
                                      <br>
                                      <br>
                                          {{ csrf_field() }}
                                      <div class="col-sm-12 collapse" id="collapseExample">
                                        <textarea class="form-control" name="comment" rows="4"></textarea>
                                        <button type="submit" class="btn btn-lg btn-dark">Comment</button>
                                      </div>
                                    </div>

                                </div>
                            </form>
                        </section>
                    </div>
                
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        $(function () {
            $('.job-edit').modalRender();

            $('.calendar-single').daterangepicker({
                singleDatePicker: true,
                calender_style: "calendar-single"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#log-time').click(function(){
                let date = $("#log-date");
                let hours = $("#log-hours");
                let job = $("#job-id");
                let hasErrors = false;

                $(".has-error").removeClass("has-error");
                $(".error-block").each(function(){
                    $(this).remove();
                });

                if (date.val() === "") {
                    date.setError("Please add log date!");
                    hasErrors = true;
                }
                if (hours.val() === "") {
                    hours.setError("Please add log hours!");
                    hasErrors = true;
                }
                if (hasErrors === true) {
                    return;
                }
                let token = '{{ csrf_token() }}';
                $.post('{{ route('log_work') }}', { '_token':token, date: date.val(), hours: hours.val(), job_id: job.val() }, function (data) {
                    $('#log-total').val(data.total);
                    hours.val("");
                    date.val("");
                }, 'json');
            });

        });

        var disqus_config = function () {
            this.page.url =  '{{\Request::url()}}';
            this.page.identifier = 1*'{{ $job->id }}';
        };

        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://pmjplmscomau.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
@endsection