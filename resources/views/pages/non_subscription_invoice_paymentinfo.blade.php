@extends('layouts.app')


@section('content')
<div id="wrapper" class="accounts">

  <section id="page-content-wrapper">
    <div class="container-wrapper container-fuild">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li><a href="{{ route('dashboard')}}">HUD</a></li>
            <li class="active">Invocie Non Subscription Payment</li>
          </ol>
          <br>
          <ul class="default-tab">
            <li><a href="{{ route('invoice_list')}}" class="active">Invoices</a></li>
            <li><a href="{{ route('payment_detail')}}">Payment Details</a></li>
          </ul>
          <br>
          <section class="content payments details">
              @if (session('message'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                   <p><strong>{{ session('message') }}</strong></p>
                </div>
                 @endif 

              @if ($message = Session::get('success'))

                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                   <p><strong>{!! $message !!}</strong></p>
                </div>

              <?php Session::forget('success');?>
             @endif
            @if ($message = Session::get('error'))

            <div class="alert alert-danger alert-block">
                              <button type="button" class="close" data-dismiss="alert">×</button> 
                             <p><strong>{!! $message !!}</strong></p>
                          </div>
   
            <?php Session::forget('error');?>
           @endif
            <fieldset class="">
              <div class="col-sm-6">
                <h3>JPL Distribution Pty Ltd Merchant Facility<br>
                  <strong>ID</strong>: 0011257785</h3>
              </div>
              <div class="col-sm-6">
                <h3>Receipt Email</h3>
                  <div id="loading"></div>
                <form id="receipt_form">
                   <input type="hidden" name="client_id" id="client_id" value="{{ $client->id }}">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-8">
                      <input type="text" name="email" id="email" class="form-control">
                    </div>
                    <label class="col-sm-4 control-label"><a href="#" id="add_line"><i class="fa fa-plus" aria-hidden="true"></i> Add</a></label>
                  </div>
                </div>
              </form>
                <br>
                <div class="row">
                  @foreach ($recieptEmails as $recieptEmail)
                  <div class="col-sm-12">
                    <div class="col-sm-8"> {{ $recieptEmail->email }} </div>
                     <label class="col-sm-4" ><a href="#" data-modal-uri="{{ route('edit_recieptmail', $recieptEmail->id) }}" class="edit-email"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> edit</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" data-modal-uri="{{ route('del_recieptmail', $recieptEmail->id) }}" class="edit-access"><i class="fa fa-trash-o" aria-hidden="true"></i> delete</a></label>
                  </div>
                  @endforeach
                </div>
              </div>
            </fieldset>
          </section>
          <h3>Payment Details</h3>
          <section class="content payments">
            <fieldset>
              <div class="">
                <table class="table sotring-table table-striped payment-details" width="100%">
                  <thead>
                    <tr>
                      <th>Invoice Date</th>
                      <th>Invoice ID</th>
                      <th>Description</th>
                      <th class="text-right">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $linecount = 0; @endphp
                    @foreach($invoice->details as $detail)
                    @if ( $linecount == 0)
                    <tr>
                      <td>{{ date('d/m/Y',strtotime($invoice->invoice_date))}}</td>
                      <td>{{ $invoice->no }}</td>
                      <td>{{ $detail->description }}</td>
                      <td class="text-right">${{ number_format($detail->amount,2) }}</td>
                    </tr>
                    @else 
                      <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>{{ $detail->description }}</td>
                      <td class="text-right">${{ number_format($detail->amount,2) }}</td>
                    </tr>
                    @endif
                     @php $linecount = $linecount+1; @endphp
                     @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="sm text-right">Sub Total</td>
                      <td class="sm text-right">${{ number_format($invoice->total['subtotal'], 2) }}</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="sm text-right">GST</td>
                      <td class="sm text-right">${{ number_format($invoice->total['gst'], 2) }}</td>
                    </tr>
                    @php
                      $totalWithLatePay = 0;
                      $cardfee         = 0;
                      $latePayment      = 0;
                      $diff =  round((time()-strtotime($invoice->due_date))/(60 * 60 * 24));

                      if ($diff >0)
                      {
                        $latePayment        =  $invoice->total['total']*0.10;
                        $totalWithLatePay   = $latePayment +$invoice->total['total'];
                        $cardfee            = $totalWithLatePay*0.02;
                        $totalFee           = $totalWithLatePay+$cardfee;
                      }else
                      {
                        $cardfee = $invoice->total['total']*0.02;

                        $totalFee = $cardfee +$invoice->total['total'];
                      }

                       
                       @endphp
                     <tr>
                      <td></td>
                      <td></td>
                      <td class="sm text-right">Late payment Fee (10%)</td>
                      <td class="sm text-right">${{ number_format($latePayment, 2) }}</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="sm text-right">Card Fee 2%</td> 

                      
                      <td class="sm text-right">${{ number_format($cardfee, 2) }}</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="text-right"><strong>Total</strong></td>
                      <td class="text-right"><strong>${{number_format($totalFee, 2)}}{{-- number_format($invoice->total['total'], 2) --}}</strong></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </fieldset>
          </section>
          <h3>Select Payment Method</h3>
          <section class="content payments details select-type">
            <fieldset>
              <div class="col-md-4 col-sm-offset-2">
                <div class="select-card">
                  <a href="#" id="cc_payment">
                  <img src="{{ asset('images/cards.jpg')}}" alt="">
                  <div class="clearfix"></div>
                  Credit Card
                  </a>
                </div>
              </div>
              <div class="col-md-4">
                <div class="select-card">
                  <a href="#" id="paypal_payment"><img src="{{ asset('images/paypal.jpg') }}" alt="">
                  <div class="clearfix"></div>
                  PayPal</a>
                </div>
              </div>
            </fieldset>
          </section>

     <section class="content payments details" id="payment_details" style="display: none;" >
            <fieldset>
              @foreach ($paymentMethods as $method)
              <div class="select-card">
                              <input type="radio" id="{{$method->id}}" class="radio-css" name="radio-name" value="{{ $method->id}}" tabindex="-1">
                <label for="{{$method->id}}" class="label-cc">
                <div class="row">
                  <div class="col-sm-7"> 
                  	@if ( $method->card_type == 'VISA')
                  	<img src="{{ asset('images/visa-sm.png') }}" alt="Visa">
                  	 @elseif ($method->card_type == 'MASTERCARD')
                  	  <img src="{{ asset('images/master-sm.png') }}" alt="Master">
                  @endif

                    <h4>@if ( $method->card_type == 'VISA') Visa  @elseif ($method->card_type == 'MASTERCARD') Master @else {{ $method->card_type}} @endif  
                    	({{ $method->card_number }})<br>
                      <span>Expiration Date {{ $method->exp_date_month }}/{{ $method->exp_date_year }}</span> </h4>
                    <p>@if ( $method->set_default == 1)<span class="label label-success">DEFAULT</span>@endif @if ( $method->set_default == 0)<a href="{{ route('payment_markdefault', $method->id )}}">Make as Default</a>@endif</p>
                  </div>
                  <div class="col-sm-5 text-right">
                    <h5>{{ $method->name_on_card }}</h5>
                    <a href="#" data-modal-uri="{{ route('payment_methods_edit', $method->id) }}" class="edit-access"><i class="fa fa-pencil-square-o"></i> edit</a>&nbsp;&nbsp;&nbsp;<a href="#" data-modal-uri="{{ route('payment_methods_delete_model', $method->id ) }}"   class="del-card"><i class="fa fa-trash-o"></i> delete</a> </div>
                </div>
                </label>
              </div>

              @if ( $method->set_default == 1)
<input type="hidden" value="{{$method->id}}" id="defult_id">
            @endif
                @endforeach

              <a href="#" data-toggle="modal"  data-modal-uri="{{ route('show_payment_methods') }}"  id="add-access" class="lg"> <button class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Add New Payment Method</button></a>
            </fieldset>
          </section>




   
          <section class="content payments">
            <fieldset class="payment-mothod">
              <br>

              <div id="form_paypal"  style="display: none;">
              <form data-parsley-validate class="form-horizontal form-label-left" action="{{route('payment_paypal')}}" method="post">
              
                         {{ csrf_field() }}

              <div class="row">
                <div class="col-md-6 col-md-offset-3">
                  <div class="col-md-6 total text-left"> Total </div>
                  <div class="col-md-6 total text-right">  <input name="invo_id" value="{{$invoice->id}}" type="hidden">
                        <input name="amount" value="{{$totalFee}}" type="hidden">
                      ${{ number_format($totalFee, 2) }} 
                  </div>
                </div>
              </div>
              <br>
              <div class="row payment-btn">
                <div class="col-md-6 col-md-offset-3">
                  <div class="form-group">
                    <button class="btn btn-success">Make Payment</button>
                  </div>
                </div>
              </div>
              </form>
               </div>

               <div id="form_cc" style="display: none;">
               <form data-parsley-validate class="form-horizontal form-label-left" action="{{route('payment_com')}}" method="post">
               {{ csrf_field() }}
                    <input type="hidden" value="" name="card_id" id="card_token">
              <div class="row">
                <div class="col-md-6 col-md-offset-3">
                  <div class="col-md-6 total text-left"> Total </div>
                  <div class="col-md-6 total text-right">  <input name="invo_id" value="{{$invoice->id}}" type="hidden">
                        <input name="amount" value="{{$totalFee}}" type="hidden">
                        <input name="card_fee" value="{{$cardfee}}" type="hidden">
                        <input name="late_payment" value="{{$latePayment}}" type="hidden">
                      ${{ number_format($totalFee, 2) }}
                  </div>
                </div>
              </div>
			

              <br>
              <div class="row payment-btn">
                <div class="col-md-6 col-md-offset-3">
                  <div class="form-group">
                    <button id="makepay_cc" class="btn btn-success submit-button">Make Payment</button>
                  </div>
                </div>
              </div>
              </form>
               </div>



            </fieldset>
          </section>
        </div>
      </div>
    </div>
  </section>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){

      var default_id = $("#defult_id").val();


      $("#card_token").val(default_id);

      if (  $("#card_token").val() == '')
      {
        $('#makepay_cc').attr("disabled", "disabled");
      }

      $('#add-access').modalRender();
      $('.edit-access').modalRender();
      $('.edit-email').modalRender();
      $('.del-card').modalRender();
 
      $("#open").click(function(){
      $(".con").animate({height: "toggle"}, 500);
      $(".con2").hide();
    });
    $("#close").click(function(){
      $(".con").hide();
      $(".con2").animate({height: "toggle"}, 500);
    });

    $('input[name=radio-name]').click(function(){

    	$("#card_token").val( $('input[name=radio-name]:checked').val());
      $('#makepay_cc').removeAttr("disabled");


    });

$("#cc_payment").click(function(e){

	$("#form_paypal").hide();
	$("#form_cc").show();
	$("#payment_details").show();
	$("#cc_payment").addClass('active');
	$("#paypal_payment").removeClass('active');
	e.preventDefault();
});

$("#paypal_payment").click(function(e){

	$("#form_paypal").show();
	$("#form_cc").hide();
	$("#payment_details").hide();
	$("#cc_payment").removeClass('active');
	$("#paypal_payment").addClass('active');
	e.preventDefault();

});


 $("#add_line").click(function(){

      var client_id = $('#client_id').val();
      var email     = $('#email').val();
      var image     = '{{ asset('images/loading.gif') }}';

       $('#loading').html('<img src="'+image+'"> Saving...');

        $.post('{{ route('save_receiptemail') }}', {"_token":'{{csrf_token()}}', "client_id": client_id, "email": email }, function (data) {

           window.location.reload();           
        });
  });


  });
</script>
    @stop






        