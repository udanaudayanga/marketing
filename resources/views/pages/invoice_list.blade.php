  @extends('layouts.app')


@section('content')
 <div id="wrapper" class="access">
     <section id="page-content-wrapper">
    <div class="container-wrapper container-fuild">
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
        <li class="active">Account</li>
      </ol>
        <ul class="default-tab">
            <li><a href="{{ route('invoice_list')}}" class="active">Invoices</a></li>
            <li><a href="{{ route('payment_detail')}}">Payment Details</a></li>
          </ul>
      <div class="row">
           <section class="content">
            <fieldset>
              <div class="table-responsive-">
                 @if ($message = Session::get('success'))
                 <div class="overlay-alert">
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>
                    <p><i class="fa fa-check" aria-hidden="true"></i><br>
                       <strong>{!! $message !!}</strong></p>
                    <div class="info">
                      <p>Thank you for your payment</p>
                    <a href="{{ route('invoice_list')}}" class="btn btn-success">Continue</a>
                  </div>
                </div>
                
                </div>
               <?php Session::forget('success');?>
                  @endif
              @if ($message = Session::get('error'))

              <div class="overlay-alert">
                
                <div class="alert alert-danger alert-dismissible" role="alert">
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>
                   <p><i class="fa fa-times-circle" aria-hidden="true"></i><br>
                   <strong>{!! $message !!}</strong></p>
                   <div class="info">
                     <p>Transaction status uncompleted.</p>
                   <a href="{{ route('invoice_list')}}" class="btn btn-danger">Please try again!</a>
                  </div>
                </div>
                </div>

                  <div class="w3-panel w3-red w3-display-container">
                      <span onclick="this.parentElement.style.display='none'"
                              class="w3-button w3-red w3-large w3-display-topright">&times;</span>
                      <p>{!! $message !!}</p>
                  </div>
                  <?php Session::forget('error');?>
                  @endif
                <table class="table sotring-table table-striped" width="100%">
                  <thead>
                    <tr>
                      <th>Invoice Date</th>
                      <th>Invoice ID</th>
                      <th>Amount</th>
                      <th>Status</th>
                      <th>Inv. Status</th>
                      <th>Terms</th>
                      <th>Due Date</th>
                      <th>Overdue</th>
                     <th></th>
                       <th>Auto Pay</th>
                      <th></th>
                      <th></th>

                    </tr>
                  </thead>
                  <tbody>
                     @foreach($invoices as $invoice)
                    <tr>
                      <td> {{ date('d/m/Y',strtotime($invoice->invoice_date)) }} </td>
                      <td>{{ $invoice->no }}</td>
                    
                       @php 
                        $total = $invoice->getTotalAttribute();
                      @endphp
                      <td><strong>${{ number_format($total['total'],2)}}</strong></td>
                       <?php $diff =  round((time()-strtotime($invoice->due_date))/(60 * 60 * 24));?>
                         <td>
                       {{$invoice->status}}
                      </td>
                      <td>
                        @if ( $invoice->status == 'UNPAID' && $diff>0 )
                            <span class="alert alert-danger">OVERDUE</span>
                        @elseif ( $invoice->invoice_status == 'pending')
                          <span class="alert alert-warning">PENDING</span>
                        @elseif ( $invoice->invoice_status == 'invoiced')
                          <span class="alert alert-success">INVOICED</span>
                          @endif
                      </td>
                     
                       <td>@if( $invoice->terms == 0) Due Date @else {{ $invoice->terms }} days @endif</td>
                        <td>{{ date('d/m/Y',strtotime($invoice->due_date)) }}</td>
                       
                        @if ($diff >0  && $invoice->status == 'UNPAID' )
                        <td>{{ $diff }} days</td>
                        @else
                        <td></td>
                        @endif
                        <td class="action"><a href="{{route('invoice_download', $invoice->id)}}"><i class="fa fa fa-file-pdf-o"></i> View as PDF</a></td>
                         
                        <td class="action-btn">
                          @if ( $invoice->status == 'UNPAID' )
                        <label class="switch">
                        <input class="auto_invoice" value="{{ $invoice->id}}" type="checkbox" {{ $invoice->auto_pay?'checked':'' }}>
                        <span class="slider round"></span>
                      </label>
                       @endif
                    </td> 
                      <td class="action">
                      @if ( $invoice->status == 'UNPAID' )
                        <a href="{{route('non_subscribe_payments', $invoice->id)}}"><i class="fa fa-money"></i> <strong>PAY</strong></a>
                       @endif
                      </td>
                     
                      <td class="action"> @if ( $invoice->status == 'PAID' )<a href="{{ url('/receipts/receipt-'.$invoice->no .'.pdf') }}" target="_blank"><i class="fa fa fa-file-pdf-o"></i> Receipt</a> @endif </td>
                         
                    </tr>
                     @endforeach
                    
                  </tbody>
                </table>
              </div>
            </fieldset>
          </section>
      </div>
    </div>
  </section>
     </div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('.sotring-table').DataTable( {
      "bLengthChange": false,
      "bPaginate": true,
      "bFilter": true,
      "info": true,
      "scrollX": true,
      "pagingType": "full_numbers",
      "aoColumnDefs": [ {
        'bSortable': false,
        'aTargets': [8]
      } ]
    } );
  } );


  $(".auto_invoice").click(function(){

    var invoid =  $(this).val();
    if ( $(this).is(":checked")){

      var url = '/invoice/autopay/'+invoid+'/1';
    }
    else{
       var url = '/invoice/autopay/'+invoid+'/0';
    }

    $.get(url, function (data) {

               window.location.reload();           
     });
})
  
</script>
@stop
        