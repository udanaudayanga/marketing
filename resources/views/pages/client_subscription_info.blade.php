@extends('layouts.app')


@section('content')
<div id="wrapper" class="accounts">
<section id="page-content-wrapper">
    <div class="container-wrapper container-fuild">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li><a href="{{ route('dashboard')}}">Accounts</a></li>
            <li class="active">Payment Details</li>
          </ol>
          <br>
          <ul class="default-tab">
            <li><a href="{{ route('invoice_list')}}" >Invoices</a></li>
            <li><a href="{{ route('payment_detail')}}" class="active">Payment Details</a></li>
            
          </ul>
          <br>
          <section class="content payments details">
            <fieldset class="">
              <div class="col-sm-6">
                <h3>JPL Distribution Pty Ltd Merchant Facility<br>
                  <strong>ID</strong>: 0011257785</h3>
                  <h3>Subscription Details</h3>
                <div class="row">
                @if (count($subscriptions)>0)
                @foreach ($subscriptions as $subscription)
                <div class="col-lg-6">
                  <p class="subscription"><strong>Item</strong>: {{ $subscription->package->name}}<br>
                  <strong>Payment Type</strong>: {{ $subscription->payment_type}}<br>
                  <strong>Payment Date</strong>: {{ $subscription->payment_date}}th of each month<br>
                  <strong>Subscription Length</strong>: {{ $subscription->subscription_length}}<br>
                  <strong>Amount</strong>: ${{ $subscription->amount}} (inc GST)</p>
                </div>
                @endforeach
                @endif
                </div>
                  
                 
              </div>
              <div class="col-sm-6">
                <h3>Receipt Email</h3>
                <div id="loading"></div>
                <form id="receipt_form">
                   <input type="hidden" name="client_id" id="client_id" value="{{ $client->id }}">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-8">
                      <input type="text" name="email" id="email" class="form-control">
                    </div>
                    <label class="col-sm-4 control-label"><a href="#" id="add_line"><i class="fa fa-plus" aria-hidden="true"></i> Add</a></label>
                  </div>
                </div>
              </form>
                <br>
                <div class="row">
                  @foreach ($recieptEmails as $recieptEmail)
                  <div class="col-sm-12">
                    <div class="col-sm-8"> {{ $recieptEmail->email }} </div>
                    <label class="col-sm-4" ><a href="#" data-modal-uri="{{ route('edit_recieptmail', $recieptEmail->id) }}" class="edit-email"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> edit</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" data-modal-uri="{{ route('del_recieptmail', $recieptEmail->id) }}" class="edit-access"><i class="fa fa-trash-o" aria-hidden="true"></i> delete</a></label>
                  </div>
                  @endforeach
                </div>

                <fieldset> 
                <h3>Payment Settings</h3>
                <div class="row">
                
                <div class="col-md-6"></div>
                <div class="col-md-6"><p class="text-left">Current Setting:</p></div>
                <div class="col-md-6"><label class="control-label">Subscription</label></div>
                <div class="col-md-3">
                  <label class="switch"><strong class="pull-left">Off</strong>
                  <input type="checkbox" id="current_settings" {{ $client->has_subscribed?'checked':'' }}>
                  <span class="slider round"></span> <strong class="pull-right">On</strong></label>
                </div>
                
                </div>
                <div class="row">
                  <div class="col-md-6"><label class="control-label">Invoices</label></div>
                  <div class="col-md-3">
                    <label class="switch"><strong class="pull-left">Off</strong>
                    <input type="checkbox" id="auto_pay_invoice" class="auto_invoice" {{ $client->invoice_autopay?'checked':'' }}>
                    <span class="slider round"></span> <strong class="pull-right">On</strong></label>
                  </div>
                  <div class="col-md-1"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="By selecting “Yes” you authorize JPLMS to deduct invoice totals on their due date using your nominated payment method, this can be disabled at anytime"></i></div>
                </div>
                </fieldset>
                           
              
              </div>
            </fieldset>
          </section>
          <h3>Payment Settings</h3>
          <section class="content payments details">
            <fieldset>
              @foreach ($paymentMethods as $method)
             {{-- @if ( $method->set_default == 1)
              <div class="select-card active">
              @else
              <div class="select-card">
              @endif
                <div class="row">
                  @if ( $method->card_type == 'VISA')
                  <div class="col-sm-8"> <img src="{{ asset('images/visa-sm.png') }}" alt="Visa">
                  @elseif ($method->card_type == 'MASTERCARD')
                  <div class="col-sm-8"> <img src="{{ asset('images/master-sm.png') }}" alt="Master">
                  @endif
                    
                    <h4>{{ $method->card_type }} ({{ $method->card_number }})<br>
                      <span>Expiration Date {{ $method->exp_date_month }}/{{ $method->exp_date_year }}</span> </h4>
                     @if ( $method->set_default == 1)<p><span class="label">DEFAULT</span><a href="#">Disable as Default</a></p>@endif
                  </div>
                  <div class="col-sm-4 text-right">
                    <h5>{{ $method->name_on_card }}</h5>
                    <a href="#" data-modal-uri="{{ route('payment_methods_edit', $method->id) }}" class="pull-right edit-access"><i class="fa fa-pencil-square-o"></i> edit</a>&nbsp;&nbsp;&nbsp;<a href="#" data-modal-uri="{{ route('payment_methods_delete_model', $method->id ) }}"   class="del-card"><i class="fa fa-trash-o"></i> delete</a> </div>
                </div>
              </div>
--}}

                <div class="select-card">
                              <input type="radio" id="{{$method->id}}" class="radio-css" name="radio-name" value="{{ $method->id}}" tabindex="-1">
                <label for="{{$method->id}}" class="label-cc">
                <div class="row">
                  <div class="col-sm-7"> 
                    @if ( $method->card_type == 'VISA')
                    <img src="{{ asset('images/visa-sm.png') }}" alt="Visa">
                     @elseif ($method->card_type == 'MASTERCARD')
                      <img src="{{ asset('images/master-sm.png') }}" alt="Master">
                  @endif

                    <h4>@if ( $method->card_type == 'VISA') Visa  @elseif ($method->card_type == 'MASTERCARD') Master @else {{ $method->card_type}} @endif  
                      ({{ $method->card_number }})<br>
                      <span>Expiration Date {{ $method->exp_date_month }}/{{ $method->exp_date_year }}</span> </h4>
                    <p>@if ( $method->set_default == 1)<span class="label label-success">DEFAULT</span>@endif @if ( $method->set_default == 0)<a href="{{ route('payment_markdefault', $method->id )}}">Make as Default</a>@endif</p>
                  </div>
                  <div class="col-sm-5 text-right">
                    <h5>{{ $method->name_on_card }}</h5>
                    <a href="#" data-modal-uri="{{ route('payment_methods_edit', $method->id) }}" class="edit-access"><i class="fa fa-pencil-square-o"></i> edit</a>&nbsp;&nbsp;&nbsp;<a href="#" data-modal-uri="{{ route('payment_methods_delete_model', $method->id ) }}"   class="del-card"><i class="fa fa-trash-o"></i> delete</a> </div>
                </div>
                </label>
              </div>
              @endforeach
              
             <a href="#"  data-modal-uri="{{ route('show_payment_methods') }}"  id="add-access" class="lg"> <button class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Add New Payment Method</button></a>
            </fieldset>
          </section>
        </div>
      </div>
    </div>
  </section>
</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    $('#add-access').modalRender();
     $('.edit-access').modalRender();
      $('.del-card').modalRender();
       $('.edit-email').modalRender();
    
   $("#add_line").click(function(){

      var client_id = $('#client_id').val();
      var email     = $('#email').val();
      var image     = '{{ asset('images/loading.gif') }}';

       $('#loading').html('<img src="'+image+'"> Saving...');

        $.post('{{ route('save_receiptemail') }}', {"_token":'{{csrf_token()}}', "client_id": client_id, "email": email }, function (data) {

           window.location.reload();           
        });


    });


$(".auto_invoice").click(function(){

    var autoPay =0;
    if ( $(this).is(":checked"))
      var url = '{{ route('invoice_autopay', [$client->id,1]) }}';
    else
     var url = '{{ route('invoice_autopay', [$client->id,0]) }}';

    $.get(url, function (data) {

               window.location.reload();           
     });
})
  

</script>
    @stop






        