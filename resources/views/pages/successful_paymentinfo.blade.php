@extends('layouts.app')


@section('content')
<div id="wrapper" class="accounts">

  <section id="page-content-wrapper">
    <div class="container-wrapper container-fuild">
      <div class="row">
        <div class="col-sm-12">

          <ol class="breadcrumb">
            <li><a href="{{ route('dashboard')}}">HUD</a></li>
             <li class="active">Payment Confirmation</li>
          </ol>
     
          <br>
          <section class="content payments details">
            <fieldset class="">
              <div class="col-sm-12 text-center">
                <h2>Thank you for your payment</h2>
                <h3 style="text-align:center;margin-bottom:0"><strong>Transaction ID</strong>: {{ $invoice->transaction_id}}</h3>
              </div>
            </fieldset>
          </section>
          <h3>Payment Details</h3>
     
          <section class="content payments">
            <fieldset>
              <div class="">
                <table class="table sotring-table table-striped payment-details" width="100%">
                  <thead>
                    <tr>
                      <th>Invoice Date</th>
                      <th>Invoice ID</th>
                      <th>Description</th>
                      <th class="text-right">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $linecount = 0; @endphp
                    @foreach($invoice->details as $detail)
                    @if ( $linecount == 0)
                    <tr>
                      <td>{{ date('d/m/Y',strtotime($invoice->invoice_date))}}</td>
                      <td>{{ $invoice->no }}</td>
                      <td>{{ $detail->description }}</td>
                      <td class="text-right">${{ number_format($detail->amount,2) }}</td>
                    </tr>
                    @else 
                      <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>{{ $detail->description }}</td>
                      <td class="text-right">${{ number_format($detail->amount,2) }}</td>
                    </tr>
                    @endif
                     @php $linecount = $linecount+1; @endphp
                     @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="sm text-right">Sub Total</td>
                      <td class="sm text-right">${{ number_format($invoice->total['subtotal'], 2) }}</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="sm text-right">GST</td>
                      <td class="sm text-right">${{ number_format($invoice->total['gst'], 2) }}</td>
                    </tr>
                    @php
                      $totalWithLatePay = 0;
                      $cardfee          = $invoice->card_fee;
                      $latePayment      = $invoice->late_charges;
                      $diff =  round((time()-strtotime($invoice->due_date))/(60 * 60 * 24));

                      $totalFee = $cardfee +$invoice->total['total']+$latePayment;

                       
                       @endphp
                     <tr>
                      <td></td>
                      <td></td>
                      <td class="sm text-right">Late payment Fee (10%)</td>
                      <td class="sm text-right">${{ number_format($invoice->late_charges,2) }}</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="sm text-right">Card Fee 2%</td> 

                      
                      <td class="sm text-right">${{ number_format($invoice->card_fee,2) }}</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td class="text-right"><strong>Total</strong></td>
                      <td class="text-right"><strong>${{ number_format($totalFee,2) }}</strong></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </fieldset>
          </section>
  
        </div>
      </div>
    </div>
  </section>
</div>

@endsection







        