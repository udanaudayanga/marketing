@extends('layouts.app')


@section('content')
 <div id="wrapper" class="access">
     <section id="page-content-wrapper">
    <div class="container-wrapper container-fuild"><a href="#" class="btn btn-default pull-right" data-modal-uri="{{ route('access_show') }}" id="add-access">
                    <i class="fa fa-plus" aria-hidden="true" ></i> Add Access
                </a>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
        <li class="active">Access</li>
      </ol>
      <div class="row">
            @foreach($accesses as $access)
            <div class="col-sm-4">
                <section class="content">
                    <fieldset>
                        <table class="table table-striped table-hover">
                            <tbody>
                            <tr>
                                <td class="icon"><a href="#"><i class="fa fa-building-o" aria-hidden="true"></i></a></td>
                                <td><strong>{{ $access->organization }}</strong>
                                    <a href="#" data-modal-uri="{{ route('access_delete', $access->id) }}" class="pull-right delete-access">
                                        <i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Delete"></i>
                                    </a>&nbsp;&nbsp;
                                    <a href="#" data-modal-uri="{{ route('access_edit', $access->id) }}" class="pull-right edit-access">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="icon"><a href="#"><i class="fa fa-address-card-o" aria-hidden="true"></i></a></td>
                                <td> {{ $access->ref_number }}</td>
                            </tr>
                            <tr>
                                <td class="icon"><a href="#"><i class="fa fa-user-circle" aria-hidden="true"></i></a></td>
                                <td>{{ $access->username }}</td>
                            </tr>
                            <tr>
                                <td class="icon"><a href="#"><i class="fa fa-lock" aria-hidden="true"></i></a></td>
                                <td><strong><input type="password" value="{{ $access->password }}" style="border: none; background-color: transparent" class="pwd"></strong>
                                    <a href="#" class="pull-right show">
                                        <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Reveal"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="icon"><a href="#"><i class="fa fa-th" aria-hidden="true"></i></a></td>
                                <td><strong><input type="password" value="{{ $access->pin }}" style="border: none; background-color: transparent" class="pwd"></strong>
                                    <a href="#" class="pull-right show">
                                        <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Reveal"></i>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </section>
            </div>
             @endforeach
       
      </div>
    </div>
  </section>
     </div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function () {
            $('#add-access').modalRender();
            $('.delete-access').modalRender();
            $('.edit-access').modalRender();

            $('.show').click(function () {
                var el = $(this).parent().find('.pwd');
                if (el.get(0).type === "password") {
                    el.get(0).type = "text";
                    return false;
                } else {
                    el.get(0).type = "password";
                     return false;
                }
            });
        });
    </script>
@endsection