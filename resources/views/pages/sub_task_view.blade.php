@extends('layouts.app')

@section('content')
    <div id="wrapper" class="client-dashboard">
    
        <section id="page-content-wrapper">
            <div class="container-wrapper container-fuild"> <a href="#" data-modal-uri="{{ route('job_edit',  $job->id ) }}"  class="btn btn-default pull-right job-edit" style="margin-left: 10px"><i class="fa fa-edit"></i> Edit Job</a>&nbsp;<a href="{{ url()->previous() }}" class="btn btn-grey pull-right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
                <ol class="breadcrumb">
                    {{--<li><a href="#">{{ $job->parent->code }}</a></li>
                    <li class="active">{{ $job->parent->title }}</li>--}}
                </ol>
                <div class="row">
                    <div class="col-sm-12">
                        <h2>{{ $job->title }}</h2>
                    </div>
                    <div class="col-sm-8">
                        <section class="content">
                            <form data-parsley-validate class="form-horizontal form-label-left">
                                <div class="form-wrapper">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label pull-left">
                                                        <Strong>Job ID:</Strong>&nbsp;&nbsp;<i>{{ $job->code }}</i>&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;
                                                    </label>
                                                    <label class="control-label pull-right">
                                                        <Strong>Done:</Strong>&nbsp;&nbsp;
                                                        <label class="control-label pull-right">
                                                            <div class="icheckbox_flat" style="position: relative;">
                                                                <input type="checkbox" value="1" name="next_status" data-parsley-mincheck="2" class="flat" style="position: absolute; opacity: 0;" {{ ($job->status == \App\Constants\JobStatus::DONE)?"checked":"" }}>
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                            </div>
                                                        </label>&nbsp;
                                                        &nbsp;&nbsp;&nbsp;
                                                    </label>
                                                </div>
                                            </div>
                                            </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3" for="title">Title</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="title" value="{{ $job->title }}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3" for="created-date">Created Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="created-date" disabled value="{{ $job->created_at->format('d-m-Y') }}" class="form-control has-feedback-left calendar-single" placeholder="mm/dd/yyyy">
                                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3">Priority</label>
                                                    <div class="col-sm-9">
                                                        <select name="priority" class="form-control" data-parsley-id="4336">
                                                            <option value="">Select Priority</option>
                                                            @foreach($priorities as $priority)
                                                                <option value="{{ $priority }}" {{ $job->priority == $priority?"selected":"" }}>{{ ucwords($priority) }}</option>
                                                            @endforeach
                                                        </select><ul class="parsley-errors-list" id="parsley-id-4336"></ul>
                                                        <span class="required">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3" for="due-date">Due Date</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="due-date" class="form-control has-feedback-left calendar-single" placeholder="mm/dd/yyyy"  value="{{ $job->due_date }}">
                                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span> </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label col-sm-3">Assignees</label>
                                                    <div class="col-sm-9">
                                                        <ol>
                                                            @foreach($job->assignees as $assignee )
                                                                <li> {{ $assignee->name }}</li>
                                                                  @if(!$loop->last)
                                                                <li><i class="fa fa-angle-right"></i></li>
                                                                @endif
                                                            @endforeach
                                                        </ol>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label class="control-label col-sm-12">Description</label>
                                                        <div class="col-sm-12">
                                                            <textarea class="form-control" rows="8" >
                                                                {{ $job->description }}
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="attachments">Attachement</label>
                                                <div class="col-sm-8">
                                                    <div id="attachments"  class="dropzone" name="attachments"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="well">
                                                    @foreach( $job->attachments as $attachment)
                                                        @component('components.attachment')

                                                            @slot('id')
                                                                {{ $attachment->id }}
                                                            @endslot

                                                            @slot('mine_type')
                                                                {{ $attachment->mine_type }}
                                                            @endslot

                                                            @slot('original_name')
                                                                {{ $attachment->original_name }}
                                                            @endslot

                                                            @slot('path')
                                                                {{ $attachment->path }}
                                                            @endslot

                                                            @slot('delete_action')
                                                                {{ 1 }}
                                                            @endslot

                                                        @endcomponent
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 text-center">
                                                    <a href="#" data-modal-uri="{{ route('task_delete_model',$job->id) }}" id="delete_task"><button type="button" class="btn btn-lg btn-default">Delete</button></a>

                                                    <button type="submit" class="btn btn-lg btn-default">Update</button>
                                                    <button type="reset" class="btn btn-lg btn-cancel" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </form>
                        </section>
                    </div>
                    <div class="col-sm-4">
                        <section class="content">
                            <div>
                                <ul class="nav nav-tabs" role="tablist">
{{--
                                    <li class="active"><a href="#worklog" aria-controls="worklog" role="tab" data-toggle="tab">Work Log</a></li>
--}}
                                    <li class="active"><a href="#notes" aria-controls="notes" role="tab" data-toggle="tab">Q&A</a></li>

                                </ul>
                                <div class="tab-content">
                                  {{--  <div role="tabpanel" class="tab-pane active fade  in" id="worklog">
                                        <div class="form-wrapper">
                                            <input type="hidden" value="{{ $job->id }}" id="job-id">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="control-label col-sm-12" for="date">Date <em>*</em></label>
                                                    <div class="col-sm-12">
                                                        <input type="text" name="date" id="log-date" class="form-control has-feedback-left calendar-single" placeholder="mm/dd/yyyy">
                                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span> </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label class="control-label" for="hours">Hours <em>*</em></label>
                                                        <input type="text" class="form-control" name="hours" id="log-hours">
                                                    </div>
                                                    <div class="col-sm-6"><br>
                                                        <br>
                                                        <a href="#" id="log-time">Add</a></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label class="control-label"></label>
                                                        <input type="text" class="form-control total-hrs" id="log-total" value="{{ $job->workLog() }}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--}}
                                    
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        $(function () {

            $('.calendar-single').daterangepicker({
                singleDatePicker: true,
                calender_style: "calendar-single"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });


        });

        var disqus_config = function () {
            this.page.url =  '{{\Request::url()}}';
            this.page.identifier = 1*'{{ $job->id }}';
        };

        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://pmjplmscomau.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();

            $(function () {
                init();
                // Dropzone.autoDiscover = false;
                $('.calendar-single').daterangepicker({
                    singleDatePicker: true,
                    calender_style: "calendar-single",
                    format: "DD/MM/YYYY"
                });

                let dropzone = new Dropzone("#attachments", {
                    url: '{{ route('job_attachments')}}',
                    headers: {
                        'X-CSRF-Token': '{{csrf_token()}}'
                    },
                    addRemoveLinks: true,
                    init: function () {
                        this.on("removedfile", function(file) { console.log(file.name); });
                    },
                    //autoProcessQueue: false,
                });

                dropzone.on('success', function(file, response) {
                    let $element = $("<input type='hidden' value='" + response.id + "' name='attachments[]' class='uploaded-file-id' '>");
                    $(file.previewTemplate).append($element);
                });

                dropzone.on("removedfile", function(file) {
                    let uploaded_id = $(file.previewTemplate).children('.uploaded-file-id').val();
                    $.post("{{ route('job_attachments_remove') }}", { '_token': '{{csrf_token()}}',id: uploaded_id } );
                });

                $(".delete-attachment").click(function(){
                    var id = $(this).data('id');
                    var job_id = "{{ $job->id }}";
                    $.post("{{ route('job_attachments_remove') }}", { '_token': '{{csrf_token()}}',id: id, job_id: job_id } );
                    $(this).remove();
                    $("#attachment_"+id).remove();
                });

                $("#assignees").select2();


            });

        var count = ('{{ count($job->tasks) }}'*1)+1;

        $(document).ready(function(){
            $("#add-job-task").click(function(){
                var el = $("#new-job-task");
                var task = el.val();
                var actionEl = $(this);

                if(task !== "") {
                    actionEl.html('<i class="fa fa-plus" aria-hidden="true"></i> Adding');
                    $.post('{{ route('task_create') }}', {"_token":'{{csrf_token()}}', "description": task, "job_id": '{{ $job->id }}' }, function (data) {
                        $("#task-container").append('<tr><td>'+ count +'. '+ task+' <input type="hidden" name="tasks[]" value="'+task+'"> </td><td><label class="control-label pull-right"><input type="checkbox" value="" data-parsley-mincheck="3" class="flat"></label> </td> <td><label class="control-label pull-right"><a href="#" class="delete-new-job-task"><i class="fa fa-trash-o"></i></a></label></td></tr>');
                        el.val("");
                        actionEl.html('<i class="fa fa-plus" aria-hidden="true"></i> Add');
                        count++;
                        init();
                    });
                }
            });
        });
        function init() {
            $('.delete-new-job-task').click(function () {
                $(this).parent().parent().parent().remove();
                count--;
                if (count <= 0) {
                    count = 1;
                }
            });
            if ($("input.flat")[0]) {
                $(document).ready(function () {
                    "use strict";
                    $('input.flat').iCheck({
                        checkboxClass: 'icheckbox_flat',
                        radioClass: 'iradio_flat'
                    });
                });
            }
        }

         $(function () {
         
            $('#delete_task').modalRender();
           
        });
    </script>
@endsection