<?php

namespace App\Console\Commands;

use URL;
use Session;

use App\Models\Card;
use App\Models\Invoice;
use App\Models\Client;

use App\Mail\InvoiceReceipt;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class autoPay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically Pay invoices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();


        $this->curlObj = curl_init();
        // configure cURL proxy options by calling this function
        $this->ConfigureCurlProxy();

        // configure cURL certificate verification settings by calling this function
        $this->ConfigureCurlCerts();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

       $this->testing();
        //
    }

    private function testing()
    {
        $invoices = Invoice::where('status','UNPAID')
        			->where('due_date',date('Y-m-d'))
              ->where('auto_pay',1)
              ->get();
        
        foreach ($invoices as $invoiceInfo)
        {
            $id      = $invoiceInfo->id;
            $invoice = Invoice::find($id);

            $totalWithLatePay  = 0;
            $cardfee           = 0;
            $latePayment       = 0;

            $diff =  round((time()-strtotime($invoice->due_date))/(60 * 60 * 24));

          //late payment fee
          if ($diff >0)
          {
            $latePayment        = $invoice->total['total']*0.10;
            $totalWithLatePay   = $latePayment +$invoice->total['total'];
            $cardfee            = $totalWithLatePay*0.02;
            $totalFee           = $totalWithLatePay+$cardfee;
          }else
          {
            $cardfee = $invoice->total['total']*0.02;
            $totalFee = $cardfee +$invoice->total['total'];
          }
          

            $client      = $invoice->client;
            $cards       = $client->cards; 
            $defaultCard = null;
            foreach ( $cards as $card )
            {
                if ( $card->set_default ==1)
                {
                    $defaultCard = $card;
                    break;
                }
            }


      if ( $defaultCard == null )
      {
      		break;
      }else{

      	  $data                                                          = [];
	      $data['apiOperation']                                          = 'PAY';
	      $data['sourceOfFunds']['type']                                 = 'CARD';
	      $data['sourceOfFunds']['provided']['card']['securityCode']     = $defaultCard->cvv_code;
	      $data['sourceOfFunds']['token']                                = $defaultCard->token;
	      $data['order']['amount']                                       = round($totalFee,2);
	      $data['order']['description']                                  = $invoice->no;
	      $data['order']['currency']                                     = 'AUD';

	      $method                             = 'PUT';
	      $transId                            = rand ( 20 , 999 );
	      $customUri                          = "/order/" . $id.'/transaction/'.$transId;
	      $request1                           = $this->ParseRequest($data);
	      $requestUrl                         = $this->FormRequestUrl( $customUri);
	      $response                           = $this->SendTransaction($request1, $method,$requestUrl);
	      $responseArray                      = json_decode($response, TRUE);


	       if (array_key_exists("result", $responseArray))
	        $result = $responseArray["result"];

	       if ( $result == 'ERROR' ||  $result == 'FAILURE') {
	       		echo 'Sorry, Your payment is failed due to following reason.<br/>'.$responseArray['error']['explanation'];
		  		break;
	    	}
	    	else{
	    		 if (array_key_exists("gatewayCode", $responseArray['response']))
			        $resultStatus = $responseArray['response']["gatewayCode"];

			      if ( $resultStatus == 'DECLINED' || $resultStatus == 'EXPIRED_CARD' || $resultStatus == 'TIMED_OUT' || $resultStatus == 'ACQUIRER_SYSTEM_ERROR' || $resultStatus == 'UNSPECIFIED_FAILURE' || $resultStatus == 'UNKNOWN'  )
			      {
			        if (isset($responseArray['response']['acquirerMessage']))
			        	  Storage::put('error-failer.txt', 'Sorry, Your payment is failed due to following reason.<br/>'.$responseArray['response']['acquirerMessage']);
			        
			        else
			        	 Storage::put('error-failer.txt', 'Sorry, Your payment is failed.');
			        break;

			      }
			    
			    if ($resultStatus == 'APPROVED') {
			    
			      $invoice->status          = InvoiceStatus::PAID;
			      $invoice->payment_method  = 'CREDIT CARD';
			      $invoice->card_fee        = $cardfee;
			      $invoice->late_charges    = $latePayment;
			      $invoice->transaction_id  = $responseArray['transaction']['acquirer']['transactionId'];
			      $invoice->save();
			      $this->sendinvoicemail( $invoice->id);
		       
			    }
	   		 }

      }
      
    		

        }
       
        echo 'testing----------------'; 
    }


       // Send transaction to payment server
  public function SendTransaction( $request, $method,$requestUrl) {
    // The below sets the HTTP operation type
    if ($method == "GET") {
      curl_setopt($this->curlObj, CURLOPT_HTTPGET, 1);
    }
    else if ($method == "POST") {
      // NOTE: POST operations are currently not supported in this version
      // [Snippet] howToPost - start
      curl_setopt($this->curlObj, CURLOPT_POST, 1);
      // [Snippet] howToPost - end
      curl_setopt($this->curlObj, CURLOPT_POSTFIELDS, $request);
      // [Snippet] howToSetHeaders - start
      curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
      curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
      // [Snippet] howToSetHeaders - end
    }
    else if ($method == "PUT") {
      // [Snippet] howToPut - start
      curl_setopt($this->curlObj, CURLOPT_CUSTOMREQUEST, "PUT");
      // [Snippet] howToPut - end
      curl_setopt($this->curlObj, CURLOPT_POSTFIELDS, $request);
      curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
      curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
    }

   

        // [Snippet] howToSetURL - start
    // call the function below to construct the URL for sending the transaction
    curl_setopt($this->curlObj, CURLOPT_URL, $requestUrl);
        // [Snippet] howToSetURL - end

    // [Snippet] howToSetCredentials - start
    // set the API Password in the header authentication field.
    curl_setopt($this->curlObj, CURLOPT_USERPWD, env('API_USERNAME') . ":" . env('API_PASSWORD'));
    // [Snippet] howToSetCredentials - end

    // tells cURL to return the result if successful, of FALSE if the operation failed
    curl_setopt($this->curlObj, CURLOPT_RETURNTRANSFER, TRUE);

    // this is used for debugging only. This would not be used in your integration, as DEBUG should be set to FALSE
  /* if ($merchantObj->GetDebug()) {
      curl_setopt($this->curlObj, CURLOPT_HEADER, TRUE);
      curl_setopt($this->curlObj, CURLINFO_HEADER_OUT, TRUE);
    }*/

    // [Snippet] executeSendTransaction - start
    // send the transaction
    $response = curl_exec($this->curlObj);
    // [Snippet] executeSendTransaction - end

    // this is used for debugging only. This would not be used in your integration, as DEBUG should be set to FALSE
    /*if ($merchantObj->GetDebug()) {
      $requestHeaders = curl_getinfo($this->curlObj);
      if (array_key_exists("request_header", $requestHeaders))
        $response = $requestHeaders["request_header"] . $response;
    }*/

    // assigns the cURL error to response if something went wrong so the caller can echo the error
    if (curl_error($this->curlObj))
      $response = "cURL Error: " . curl_errno($this->curlObj) . " - " . curl_error($this->curlObj);

    // respond with the transaction result, or a cURL error message if it failed
    return $response;
  }


    // Check if proxy config is defined, if so configure cURL object to tunnel through
 protected function ConfigureCurlProxy() {
    // If proxy server is defined, set cURL options
    if ( env('PROXYSERVER') != "") {
      curl_setopt($this->curlObj, CURLOPT_PROXY, env('PROXYSERVER'));
      curl_setopt($this->curlObj,env('PROXYCURLOPTION'), env('PROXYCURLVALUE'));
    }
    // If proxy authentication is defined, set cURL option
    if (env('PROXYAUTH') != "")
      curl_setopt($this->curlObj, CURLOPT_PROXYUSERPWD, env('PROXYAUTH'));
  }
  // [Snippet] howToConfigureProxy - end

  // [Snippet] howToConfigureSslCert - start
  // configure the certificate verification related settings on the cURL object
  protected function ConfigureCurlCerts() {
    // if user has given a path to a certificate bundle, set cURL object to check against them
    if (env('CERTIFICATE_PATH') != "")
      curl_setopt($this->curlObj, CURLOPT_CAINFO,env('CERTIFICATE_PATH') );

    curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYPEER, env('CERTIFICATE_VERIFY_PEER'));
    curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYHOST, env('CERTIFICATE_VERIFY_HOST'));
  }


  public function FormRequestUrl($customUri) {
    $gatewayUrl = env('GATEWAY_URL');
    $gatewayUrl .= "/version/" . env('VERSION');
    $gatewayUrl .= "/merchant/" . env('MERCHANT_ID');
    $gatewayUrl .= $customUri;
   
    return $gatewayUrl;
  }
  // [Snippet] howToConfigureURL - end

    // [Snippet] howToConvertFormData - start
  // Takes a multidimensional associative array
  //
  // Recursive function to unset empty arrays from a
  // multidimensional array up to the highest level
  public function RemoveEmptyValues($array) {
    foreach ($array as $i => $value) {
      // If member is an array
      if (is_array($array[$i])) {
        // if array has no members, unset array
        if (count($array[$i]) == 0)
          unset($array[$i]);
        // if array has members, recurse and pass in the array
        // recursive function will then loop through all members of this array
        else {
          // overwrite old array with new structure
          $array[$i] = $this->RemoveEmptyValues($array[$i]);
          // if array is empty unset it
          if (count($array[$i]) == 0)
            unset($array[$i]);
        }
      }
      // if member not an array
      else {
        // if member variable is empty, unset it
        if ($array[$i] == "")
          unset($array[$i]);
      }
    }
    return $array;
  }

  
  // Creates the JSON encoded transaction body from an associative array
  // Remember to make it check if the array member is empty, then remove it from json if it is
  public function ParseRequest($formData) {
    $request = "";
    if (count($formData) == 0)
      return "";
    $formData = $this->RemoveEmptyValues($formData);
    $request = json_encode($formData);
    return $request;
  }



	public function sendinvoicemail($id)
    {
        $invoice = Invoice::find($id);
        $client = $invoice->client->get();
        $mails  = $client->reciptEmails()->get();

        $toMails = [];

        foreach ( $mails as $mail)
        {
        	$toMails[] = $mail->email; 
        }
       
        $fileName = "receipt-".$invoice->no.".pdf";
        \PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif','isHtml5ParserEnabled'=>true]); 
        $pdf = \PDF::loadView('pdf.receipt', compact('invoice'))
            ->setPaper(array(0, 0, 595, 841), 'portrait');
       
        $pdf->save('./receipts/'.$fileName);

        Mail::to($toMails)
        
           ->bcc('james@jplms.com.au')
           ->send(new InvoiceReceipt($invoice));

             
    }
}
