<?php

namespace App\Constants;


interface JobTypes
{
    const ALL="all";
    const DESIGN="design";
    const DEV="dev";
    const SEO="seo";
    const SERVER="server";
    const SUPPORT="support";
    const SUB="sub";
}