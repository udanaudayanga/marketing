<?php

namespace App\Constants;


interface Priority
{
    const LOW = "low";
    const MEDIUM = "medium";
    const HIGH = "high";
    const IMMEDIATE = "immediate";
}