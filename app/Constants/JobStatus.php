<?php

namespace App\Constants;


interface JobStatus
{
    const TODO = "todo";
    const IN_PROGRESS = "inprogress";
    const DONE = "done";
    const ARCHIVED = "archived";
}