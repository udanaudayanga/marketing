<?php

namespace App\Constants;


interface AccessTypes
{
    const ADMIN="admin";
    const PA="pa";
    const CLIENT="client";
    
}