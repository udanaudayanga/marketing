<?php

namespace App\Constants;


interface InvoiceStatus
{
    const PAID = "paid";
    const UNPAID = "unpaid";
    const PARTIALY_PAID = "partialy_paid";
    const INVOICED      = 'invoiced';
    const PENDING      = 'pending';
}