<?php

namespace App\Constants;


interface UserRoles
{
    const ADMIN = "admin";
    const CLIENT = "client";
    const CLIENTUSER = "clientuser";
    const DEVELOPER = "developer";
    const DESIGNER = "designer";
    const SERVER = "server";
    const SEO = "seo";
    const SUPPORT = "support";
    const PA = "pa";
}