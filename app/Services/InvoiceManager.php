<?php

namespace App\Services;

use Auth;
use App\Constants\InvoiceStatus;
use App\Models\CreditNote;
use App\Models\Invoice;
use App\Models\InvoiceDetails;
use App\Models\InvoiceView;
use App\Models\Client;
use App\Models\SubscriberPackage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class InvoiceManager
{
    /**
     * @var Invoice
     */
    private $invoice;
    /**
     * @var InvoiceDetails
     */
    private $invoiceDetails;
    /**
     * @var CreditNote
     */
    private $creditNote;
    /**
     * @var ClientService
     */
    private $clientService;

    public function __construct(Invoice $invoice, InvoiceDetails $invoiceDetails, CreditNote $creditNote, ClientService $clientService)
    {

        $this->invoice = $invoice;
        $this->invoiceDetails = $invoiceDetails;
        $this->creditNote = $creditNote;
        $this->clientService = $clientService;
    }

     function getClient()
    {
      
       //$client  = Client::find(Auth::user()->client_id);
       if ( Auth::user()->role == 'admin')
       {
            $client_id = session('client_id');
            $client    = Client::find( $client_id);
       
       }else
       {
             $client  = Client::find(Auth::user()->client_id);
       }
       return $client;
    }

    public function lists()
    {

      // $client     = new Client;
      // $client     = $client->findClient(Auth::user()->id);

        $client = $this->getClient();
       
       $invoices   = $client->invoices();

       /*$invoiceList = [];
       $invoices1 = $invoices->get();
       foreach ( $invoices1 as $invoice)
       {

        $invoiceIdMax = $this->invoice->getMaxVersionInvoice( $invoice->parent_id);
           if (  $invoice->id == $invoiceIdMax )
            {

               $invoiceList[]  = $invoice;

            }
       }

       return $invoiceList;*/

 //dd( $invoices);
        return $invoices
             ->orderBy('id', 'desc')
            ->paginate(20);

    }

    public function find($id)
    {
        return $this->invoice
            ->with('client','details', 'creditNotes', 'history', 'parent')
            ->where('id', $id)
            ->first();
    }

  /*  function getClient()
    {
       $client     = new Client;
       $client     = $client->findClient(Auth::user()->id);

       return  $client;
    }*/


     public function subscriptionInfo($subsPackageId)
    {
       
       $package = new SubscriberPackage();
       $subscriptionPackage = $package->find($subsPackageId);
      
       return  $subscriptionPackage;
    }


    public function saveRecieptEmail()
    {
        
    }

   


 
}
