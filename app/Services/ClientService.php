<?php

namespace App\Services;

use App\Constants\UserRoles;
use App\Models\Client;

class ClientService
{
    /**
     * @var UserService
     */
    private $user;
    /**
     * @var Client
     */
    private $client;

    public function __construct(UserService $user, Client $client)
    {
        $this->user = $user;
        $this->client = $client;
    }

    public function create($data)
    {
        //TODO apply user account
      /*  $this->user = $this->user->create(
            $this->resolveUserData($data)
        );*/
        $this->client->create(
            $data
        );

        return $this->client;
    }

   /* private function resolveUserData($data)
    {
        return [
            'name' => $data['company_name'],
            'email' => $data['email'],
            'pin' => $data['pin'],
            'role' => UserRoles::CLIENT,
        ];
    }*/

    public function all()
    {
        return $this->client->with('users')
            ->paginate(10);
    }

    public function lists()
    {
        return $this->client->all(['id','company_name']);
    }

    public function get($id)
    {
        return $this->client->with('users')
            ->where('id',$id)->first();
    }

    public function update($id, $data)
    {
        $client = $this->client->find($id);
       // $this->user->updateClientInfo($data);

        $client->update($data);
    }

    public function find($id)
    {
        return $this->client->find($id);
    }

    public function remove($id)
    {
        $this->client->destroy($id);
    }


}
