<?php

namespace App\Services;


use App\Constants\UserRoles;
use App\Mail\ForgotPin;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class UserService
{

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }


    public function create($data)
    {
        if (isset($data['pin'])) {
            $pin = $data['pin'];
        } else {
            $pin = $this->generateUniquePin();
        }

        return $this->user->create([
            'name' => $data['name'],
            'client_id' => $data['client_id'],
            'email' => $data['email'],
            'determiner' => $this->determiner($pin),
            'pin' => $this->encodePin($pin),
            'role' => $data['role'],
        ]);
    }

    
    public function generateUniquePin()
    {
        $randPin =  rand(1000,9999);
        $determiner = $this->determiner($randPin);

        if ($this->user->has($determiner)) {
            return $this->generateUniquePin();
        } else {
          return $randPin;
        }
    }

    public function sendNewPin($email)
    {
        $pin = $this->generateUniquePin();

        $user = $this->user->findByEmail($email);
        $user->determiner = $this->determiner($pin);
        $user->pin = $this->encodePin($pin);
        $user->save();

        Mail::to($user)
           ->send(new ForgotPin($user->name, $pin));
    }

    public function determiner($pin)
    {
        return sha1($pin);
    }

    public function encodePin($pin)
    {
        return  bcrypt($pin);
    }

    public function updateClientInfo($data)
    {
        $user = $this->user->find($data['user_id']);
        $user->name = $data['company_name'];
        $user->email = $data['email'];
        $user->save();
    }

    public function employees($id)
    {
        return $this->user->where('client_id', $id)
              ->paginate(10);
    }

    public function employeesList()
    {
        return $this->user->get(['id','name']);
    }

    public function find($id)
    {
        return $this->user->find($id);
    }

    public function update($id, $attributes)
    {
        $user = $this->user->find($id);
        $user->update($attributes);
    }

    public function remove($id)
    {
        $this->user->destroy($id);
    }


}