<?php

namespace App\Services;

use App\Models\Attachment;
use App\Models\Job;

class AttachmentService
{
    /**
     * @var Attachment
     */
    private $attachment;
    /**
     * @var Job
     */
    private $job;

    public function __construct(Attachment $attachment, Job $job)
    {
        $this->attachment = $attachment;
        $this->job = $job;
    }

    public function add($attachment)
    {
        return $this->attachment->create($attachment);
    }

    public function remove($id, $jobId)
    {
       try {
           if ($jobId != null) {
               /** @var Job $job */
               $job = $this->job->find($jobId);
               $job->attachments()->detach($id);
           }
           $attachment = $this->attachment->find($id);
           unlink($attachment->path);
           $attachment->delete();
           return "success";
       } catch (\Exception $e) {
           return "fail";
       }
    }
}
