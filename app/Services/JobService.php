<?php

namespace App\Services;

use Auth;
use App\Constants\JobStatus;
use App\Models\Job;
use App\Models\Client;


class JobService
{
    /**
     * @var Job
     */
    private $job;
  /**
     * @var Client
     */
    private $client;

    /**
     * @var AttachmentService
     */
    private $attachmentService;

      /**
     * @var CommentService
     */
    private $commentService;

  

    public function __construct(Job $job, AttachmentService $attachmentService, Client $client,CommentService $commentService)
    {
        $this->job               = $job;
        $this->attachmentService = $attachmentService;
         $this->commentService = $commentService;
        $this->client            = $client;

    }

     function getClient()
    {
       if ( Auth::user()->role == 'admin')
       {
            $client_id = session('client_id');
            $client    = Client::find( $client_id);
         
       }else
       {
             $client  = Client::find(Auth::user()->client_id);
       }
     
       return $client;
    }

    public function all()
    {
        $todo       = [];
        $inProgress = [];
        $done       = [];


      // $client     = new Client;
      // $client     = $client->findClient(Auth::user()->id);
      $client = $this->getClient();
       $jobs       = $client->jobs();
       $jobs->where('parent', 0)
            ->where('type', 'support')
            ->orderBy('created_at','DESC');

       $jobs = $jobs->get()->unique();
       //dd($jobs);
       $jobs->each(function ($item) use (&$todo, &$inProgress, &$done) {

            if ( $item->status == JobStatus::TODO ) {
                $item = $this->find($item->id); 
                $todo[] = $item;
            }

            if ( $item->status == JobStatus::IN_PROGRESS ) {
               $item = $this->find($item->id); 
                $inProgress[] = $item;
            }

            if ( $item->status == JobStatus::DONE ) {
                $item = $this->find($item->id); 
                $done[] = $item;
            }
        });

     //  dd($todo);

     return compact('todo','inProgress','done');
 //dd($inProgress);
       //return $jobs;
    }

    public function create($attributes)
    {

        $attributes['due_date'] = date("Y-m-d");//, strtotime(str_replace('/', '-',$attributes['due_date']) ));
        $attributes['created_by'] = user()->id;
        // $client = new client;
        // $client     = $client->findClient(Auth::user()->id);

        $client = $this->getClient();
        $attributes['client_id']  = $client->id;

        /** @var Job $job */
        $job = $this->job->create($attributes);
        if (isset($attributes['attachments'])) {
            $attachments = $attributes['attachments'];
            $job->attachments()->attach($attachments);
        }
       /* if (isset($attributes['assignees'])) {
            $job->assignees()->attach(array_wrap($attributes['assignees']));
        }
        if (isset($attributes['tasks'])) {
           $job->tasks()->insert($this->processTask($attributes, $job->id));
            
            //add asignees for task
            $tasks = $job->tasks;
            foreach ( $tasks as $task)
            {
                $jobTask = $this->find($task->id);
                $jobTask->assignees()->attach(array_wrap($attributes['assignees']));
            }
          
        }*/

        
        return $job;
    }

    public function createSubTasks($attributes)
    {
        $job            = $this->create($attributes);
        $job->parent    = $attributes['job_id'];
        $job->save();
        return $job;
    }

    public function update($id, $attributes)
    {
        /** @var Job $job */
        $job                    = $this->find($id);
        $attributes['status']   = $this->getStatus(isset($attributes['next_status']), $job->status);
         //$client = new client;
         //$client     = $client->findClient(Auth::user()->id);

         $client = $this->getClient();
        $attributes['client_id']  = $client->id;
        //$attributes['due_date']  = date('Y-m-d', strtotime($attributes['due_date']));

        $job->update($attributes);
        if (isset($attributes['attachments'])) {
            $attachments = $attributes['attachments'];
            $job->attachments()->syncWithoutDetaching($attachments);
        }
        if (isset($attributes['assignees'])) {
            foreach ( $job->assignees as $assignee)
            {
                $assigneeId = $assignee->id;
                 $job->assignees()->updateExistingPivot($assigneeId, ['status'=>'old']);
            }
           
            $job->assignees()->syncWithoutDetaching(array_wrap($attributes['assignees']));
        }
         if (isset($attributes['tasks'])) {
            $job->tasks()->insert($this->processTask($attributes, $job->id));
        }
        if (isset($attributes['comment'])) {

            $commentDetail['comments']      = $attributes['comment'];
          //  $commentDetail['hours']         = $attributes['hours'];
            $commentDetail['created_by']    = user()->id;
            $commentDetail['name']          = user()->name;
            $commentDetail['created_at']    = date('Y-m-d H:i:s');
            $commentDetail['updated_at']    = date('Y-m-d H:i:s');
            $commentId                      = $job->comments()->insertGetId($commentDetail);
            $job->comments()->syncWithoutDetaching(array_wrap($commentId));

            
        }
        
    }

    public function find($id)
    {
        return $this->job
            ->with('assignees', 'tasks')
            ->where('id', $id)
            ->first();
    }

    private function getStatus($setNextStatus, $status)
    {
        if (!$setNextStatus) {
            return $status;
        }
        switch ($status){
            case JobStatus::TODO:
                return JobStatus::IN_PROGRESS;
            case JobStatus::IN_PROGRESS;
                return JobStatus::DONE;
            case JobStatus::DONE;
                return JobStatus::ARCHIVED;
        }
    }

    public function findFullDetails($id)
    {
        return $this->job
            ->with(['assignees', 'creator', 'attachments', 'client', 'parent','comments'])
            ->where('id', $id)
            ->first();
    }

  
    private function processTask($attributes, $jobId)
    {
        $tasks = $attributes['tasks'];
        $tasksToSave = [];
        foreach ($tasks as $task) {
            $newTask['title'] = $task;
            $newTask['parent'] = $jobId;
            $newTask['type'] = 'sub';
            $newTask['client_id'] = $attributes['client_id'];
            $newTask['due_date'] = $attributes['due_date'];
            $newTask['priority'] = $attributes['priority'];
            $newTask['description'] = $task;
            $newTask['status'] = 'todo';
            if (isset( $attributes['created_by']))
                $newTask['created_by'] = $attributes['created_by'];
            else
                $newTask['created_by'] = Auth::user()->id;

            $newTask['created_at'] = date('Y-m-d H:i:s');
            $newTask['updated_at'] = date('Y-m-d H:i:s');

            $tasksToSave[]=$newTask;
        }
        return $tasksToSave;
    }

    //delete task 
    public function remove($id)
    {
        $this->job->destroy($id);
    }

    public function saveComment($id, $attributes)
    {
         $job = $this->find($id); 
         if (isset($attributes['comment'])) {

            $commentDetail['comments']      = $attributes['comment'];
           
            $commentDetail['created_by']    = user()->id;
            $commentDetail['name']          = user()->name;
            $commentDetail['created_at']    = date('Y-m-d H:i:s');
            $commentDetail['updated_at']    = date('Y-m-d H:i:s');
            $commentId                      = $job->comments()->insertGetId($commentDetail);
            $job->comments()->syncWithoutDetaching(array_wrap($commentId));

       }
    }


     public function activeJobs()
    {
        $todo       = [];
        $inProgress = [];
        $done       = [];
 
       $client = $this->getClient();
       $jobs       = $client->jobs();
       $jobs->where('parent', 0)
            ->where('type', 'support')
            ->where('status', JobStatus::TODO)
            ->orderBy('created_at','DESC');

       $jobs = $jobs->get()->unique();

       return $jobs;
    }

  
}
