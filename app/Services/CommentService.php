<?php

namespace App\Services;

use App\Models\Comment;
use App\Models\Job;

class CommentService
{
    /**
     * @var comment
     */
    private $comment;
    /**
     * @var Job
     */
    private $job;

    public function __construct(Comment $comment, Job $job)
    {
        $this->comment = $comment;
        $this->job = $job;
    }

    public function add($comment)
    {
        return $this->comment->create($comment);
    }

   

    public function remove($id, $jobId)
    {
       try {
           if ($jobId != null) {
               /** @var Job $job */
               $job = $this->job->find($jobId);
               $job->comments()->detach($id);
           }
           $comment = $this->comment->find($id);
           $comment->delete();
           return "success";
       } catch (\Exception $e) {
           return "fail";
       }
    }
}
