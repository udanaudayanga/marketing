<?php

namespace App\Services;

use Auth;
use App\Models\Access;
use App\Models\Client;

class AccessService
{
    /**
     * @var Access
     */
    private $access;

   
    public function __construct(Access $access)
    {
        $this->access = $access;
        
    }

     function getClient()
    {
       if ( Auth::user()->role == 'admin')
       {
            $client_id = session('client_id');
            $client    = Client::find( $client_id);

       }else
       {
             $client  = Client::find(Auth::user()->client_id);
       }
       return $client;
    }


    public function create($attributes)
    {
      //   $client     = new Client;
       $client     = $this->getClient();//$client->findClient(Auth::user()->id);

        $attributes['client_id'] = $client->id;
        $attributes['type'] = 'client';
        $this->access->create($attributes);
    }

    public function lists()
    {
        //$access = $this->access->get();

       /*  $client     = new Client;
       $client     = $client->findClient(Auth::user()->id);*/

       $client     = $this->getClient();
        
        return $this->access->where('client_id', $client->id)->get();
    }

    public function find($id)
    {
        return $this->access->find($id);
    }

    public function destroy($id)
    {
        return $this->access->destroy($id);
    }

    public function update($attributes)
    {
        $access =  $this->access->find($attributes['id']);
        $access->fill($attributes);
        $access->save();
    }

      public function firstRecord($client_id)
    {
       
        return $this->access->where('client_id', $client_id)->first();
    }
}
