<?php

namespace App\Services;

use Auth;
use App\Models\Card;
use App\Models\Client;

class CardService
{
    /**
     * @var Access
     */
    private $card;

   
    public function __construct(Card $card)
    {
        $this->card = $card;
        
    }

     function getClient()
    {
     
        if ( Auth::user()->role == 'admin')
       {
            $client_id = session('client_id');
            $client    = Client::find( $client_id);
        

       }else
       {
             $client  = Client::find(Auth::user()->client_id);
       }

       return $client;
    }

    public function create($attributes)
    {
        // $client     = new Client;
       //$client     = $client->findClient(Auth::user()->id);

        $client = $this->getClient();

        $attributes['client_id'] = $client->id;
        //$attributes['type'] = 'client';
        $this->card->create($attributes);
    }

  
    public function find($id)
    {
        return $this->card->find($id);
    }

    public function destroy($id)
    {
        return $this->card->destroy($id);
    }

    public function update($attributes)
    {
       
        $card =  $this->card->find($attributes['id']);
        $card->fill($attributes);
        $card->save();
    }
}
