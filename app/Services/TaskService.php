<?php

namespace App\Services;

use App\Models\Task;

class TaskService
{
    /**
     * @var Task
     */
    private $task;

    /**
     * TaskService constructor.
     * @param Task $task
     */
    public function __construct(Task $task)
    {

        $this->task = $task;
    }
    public function create($data)
    {
        return $this->task->create($data);
    }
}
