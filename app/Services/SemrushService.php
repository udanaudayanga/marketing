<?php

namespace App\Services;

use Auth;
use App\Models\SemrushResponse;
use App\Models\Client;

class SemrushService
{
    /**
     * @var Access
     */
    private $semrushresponse;

   
    public function __construct(SemrushResponse $semrushresponse)
    {
        $this->semrushresponse = $semrushresponse;
        
    }

     function getClient()
    {
    
      if ( Auth::user()->role == 'admin')
       {
            $client_id = session('client_id');
            $client    = Client::find( $client_id);
           
       }else
       {
             $client  = Client::find(Auth::user()->client_id);
       }
    
       return $client;
    }

    public function create($attributes)
    {
        $client                  = $this->getClient();
        $attributes['client_id'] = $client->id;
        $this->semrushresponse->create($attributes);
    }

  
    public function find($projectId, $description)
    {
        return $this->semrushresponse
        ->where('website',$projectId)
        ->where('description',$description)->get()->last();
    }


    public function clientRecords($projectId)
    {
        return $this->semrushresponse->clientRecords($projectId);
    }

    
}
