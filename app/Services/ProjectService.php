<?php

namespace App\Services;


use App\Models\Project;

class ProjectService
{
    /**
     * @var Project
     */
    private $project;

    public function __construct(Project $project)
    {
        $this->project = $project;
    }
    public function create($data)
    {
        $this->project->create($data);
    }

    public function all()
    {
        return $this->project
            ->with('client')
            ->paginate(10);
    }

    public function get($id)
    {
        return $this->project
            ->with('client')
            ->where('id',$id)
            ->first();
    }

    public function update($data)
    {
        $project = $this->project->find($data['id']);
        $project->update($data);
    }

    public function lists($client = null)
    {
        $projects =  $this->project;
        if ($client != null) {
            $projects = $projects->where('client_id', $client);
        }
        return $projects->get(['id','name']);
    }

    public function byClient($client)
    {
        return $this->project
            ->where('client_id', $client)
            ->get();
    }

    public function find($id)
    {
        return $this->project->find($id);
    }

    public function remove($id)
    {
        return $this->project->destroy($id);
    }
}