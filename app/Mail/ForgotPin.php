<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPin extends Mailable
{
    use Queueable, SerializesModels;


    public $pin;
    public $name;


    public function __construct($name, $pin)
    {
        $this->name = $name;
        $this->pin = $pin;
    }


    public function build()
    {
        return $this->view('email.forgot_pin');
    }
}
