<?php

namespace App\Mail;

use App\Models\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceReceipt extends Mailable
{
    use Queueable, SerializesModels;


    public $invoice;
 

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
       
       
    }


    public function build()
    {
        $fileName = "receipt-".$this->invoice->no.".pdf";
        return  $this->from('accounts@jplms.com.au')
         ->view('email.receipt_email')
          ->attach('./receipts/'.$fileName);  


    }
}
