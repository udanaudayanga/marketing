<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    public $pin;

    /**
     * Create a new message instance.
     *
     * @param string $name
     * @param int $pin
     */
    public function __construct($name, $pin)
    {
        $this->name = $name;
        $this->pin = $pin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.welcome');
    }
}
