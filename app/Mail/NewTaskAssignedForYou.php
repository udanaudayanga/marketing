<?php

namespace App\Mail;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewTaskAssignedForYou extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Job
     */
    public $job;
    /**
     * @var
     */

    public $name;

    /**
     * Create a new message instance.
     *
     * @param $name
     * @param Job $job
     */
    public function __construct($name, Job $job)
    {
        $this->job = $job;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.new_job');
    }
}
