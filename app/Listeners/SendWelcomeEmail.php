<?php

namespace App\Listeners;

use App\Events\EmployeeRegistered;
use App\Mail\Welcome;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendWelcomeEmail
{

    /**
     * Handle the event.
     *
     * @param  EmployeeRegistered  $event
     * @return void
     */
    public function handle(EmployeeRegistered $event)
    {
        $user = $event->getUser();
        $pin = $event->getPin();

        Mail::to($user)
            ->send(new Welcome($user->name, $pin));
    }
}
