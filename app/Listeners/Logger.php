<?php

namespace App\Listeners;

use App\Events\CreditNoteAdded;
use App\Events\CreditNoteDeleted;
use App\Events\CreditNoteEdited;
use App\Events\InvoiceEdited;
use App\Events\InvoiceGenerated;
use App\Models\Logger as DBLogger;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Logger
{
    /**
     * Create the event listener.
     *
     */
    public function __construct( )
    {

    }

    /**
     * Handle the event.
     *
     * @param  InvoiceGenerated  $event
     * @return void
     */
    public function invoiceGenerated(InvoiceGenerated $event)
    {
        $invoice = $event->invoice;
        $this->log(
            $invoice->parent_id,
            $invoice->no." created by ".user()->first_name
        );
    }

    public function invoiceEdited(InvoiceEdited $event)
    {
        $invoice = $event->invoice;
        $this->log(
            $invoice->parent_id,
            $invoice->no." edited by ".user()->first_name
        );
    }

    public function creditNoteAdded(CreditNoteAdded $event)
    {
        $invoice = $event->creditNote->invoice_id;
        $this->log(
            $invoice,
            " Credit note added by ".user()->first_name
        );

    }

    public function creditNoteDeleted(CreditNoteDeleted $event)
    {
        $invoice = $event->creditNote->invoice_id;
        $this->log(
            $invoice,
            " Credit note deleted by ".user()->first_name
        );
    }

    public function log($invoice_id, $log)
    {
        $logger = new DBLogger;
        $logger->resource_id = $invoice_id;
        $logger->log = $log;
        $logger->type = "invoice";
        $logger->save();
    }
}
