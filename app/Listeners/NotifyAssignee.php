<?php

namespace App\Listeners;

use App\Events\JobCreated;
use App\Mail\NewTaskAssignedForYou;
use App\Services\UserService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class NotifyAssignee
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * Create the event listener.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Handle the event.
     *
     * @param  JobCreated  $event
     * @return void
     */
    public function handle(JobCreated $event)
    {
        $job = $event->getTask();
        $users = $job->assignees()->get();
        foreach ($users as $user) {
            Mail::to($user)
                ->send(new NewTaskAssignedForYou($user->name, $job));
        }

    }
}
