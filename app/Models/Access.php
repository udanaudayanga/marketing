<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
	 protected $table = 'client_accesses';
    protected $fillable = ['organization',
                            'ref_number',
                            'username',
                            'password',
                            'pin',
                             'type',
                             'client_id'
                           ];

}
