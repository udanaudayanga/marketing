<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditNote extends Model
{
    protected $fillable = [
        "invoice_id",
        "paid_date",
       // "description",
        //"gst",
        "amount"
    ];

    public $timestamps = false;
}
