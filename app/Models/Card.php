<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = [
    	"client_id",
        "card_type",
        "card_number",
        "exp_date_year",
        "exp_date_month",
        "cvv_code",
        "name_on_card",
        "token",
        "use_next_time",
       // "street_address",
      //  "suburb",
      //  "state",
      //  "postcode",
      //  "email",
        "set_default"
       
    ];

  	public function client()
   {

   		return $this->belongsTo(Client::class);

   }
  
}
