<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'client_id',
        'code',
        'name'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class,'client_id');
    }
}
