<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SemrushResponse extends Model
{
    protected $fillable = [
        "client_id",
        "website",
        "year",
        "month",
        "description",
        "response"
       
    ];


    function clientRecords( $projectId)
    {
    	return $this->where('website',$projectId);

    }
   // public $timestamps = false;

  
}
