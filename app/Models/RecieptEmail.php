<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecieptEmail extends Model
{
    protected $fillable = [
    	"client_id",
      "email"
        
       
    ];

  	public function client()
   {

   		return $this->belongsTo(Client::class);

   }
  
}
