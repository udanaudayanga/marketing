<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = ['original_name','path','mine_type'];

    public function jobs()
    {
        return $this->belongsToMany(Job::class, 'job_attachments');
    }
}
