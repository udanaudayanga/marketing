<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{

    protected $fillable = [
        "client_id",
        "url",
        "view_id"

    ];



    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function websiteByViewId($viewId)
    {
    	 return $this->where('view_id', $viewId)
    	              ->get();
                     
    }


    public function websiteBySemProId($proId)
    {
    	 return $this->where('semrush_project_id', $proId)
    	              ->get();
                     
    }


   

    
}
