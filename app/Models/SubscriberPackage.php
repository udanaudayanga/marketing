<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriberPackage extends Model
{
    protected $fillable = [
        "name",
        "price"
       
    ];

    public $timestamps = false;

  
}
