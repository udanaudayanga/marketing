<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailAddress extends Model
{
    protected $fillable = [
    	"client_id",
      "email",
      "password"
        
       
    ];

  	public function client()
   {

   		return $this->belongsTo(Client::class);

   }
  
}
