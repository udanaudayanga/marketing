<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        "client_id",
        "sales_id",
        "auto_pay_start_year",
        "auto_pay_start_month",
        "payment_type",
        "payment_date",
        "amount",
        "subscription_package",
        "agreement",
        "subscription_length",
        "subscription_end_date",
        "invo_gen_month",
        "invo_gen_year",
        "status",
        "manage_email_essentials_qty",
        "manage_email_office_qty",
        "manage_email_office_premium_qty",
        "manage_email_adds_management",
        "custom_desc",
    ];

 

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function package()
    {
        return $this->belongsTo(SubscriberPackage::class,"subscription_package","id");
    }

    public function salesuser()
    {
         return $this->belongsTo(User::class,"sales_id", 'id');
    }
 

    public function details()
    {
        
      //  return $this->hasMany(InvoiceDetails::class);
    }

   


    
}
