<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'company_name',
        'abn',
        'address',
        'street',
        'suburb',
        'state',
        'postcode',
        'owner_name',
        'owner_phone',
        'owner_email',
        'admin_name',
        'admin_phone',
        'admin_email',
        'billing_name',
        'billing_phone',
        'billing_email',
        'user_id',
        'has_subscribed',
        'invoice_autopay',

    ];

    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

     public function findClient( $userId )
    {
        return $this->where('user_id',$userId)->first();
    }


     public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

     public function websites()
    {
        return $this->hasMany(Website::class);
    }

   public function cards()
   {

    return $this->hasMany(Card::class);

   }

   public function reciptEmails()
   {

    return $this->hasMany(RecieptEmail::class);

   }


   public function emailsAddresses()
   {

    return $this->hasMany(EmailAddress::class);

   }

public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }
  


}
