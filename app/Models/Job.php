<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Job extends Model
{
    protected $fillable = ['title',
                        'due_date',
                        'type',
                        'client_id',
                        'priority',
                        'description',
                        'request_type',
                        'created_by',
                        'reporter_name',
                        'status'];

    public function getCodeAttribute()
    {
        try {
            $code = substr($this->client->company_name, 0, 3);
        } catch (\Exception $e) {
            $code = substr($this->title, 0, 3);
        }
        $id = str_pad($this->id, 4, '0', STR_PAD_LEFT);
        return Str::upper($code)."-#".$id;
    }

    public function client()
    {
       return $this->belongsTo(Client::class);
    }

    public function assignees()
    {
       return $this->belongsToMany(User::class, "job_assignees")->wherePivot('status', 'current');
    }

     public function allAssignees()
    {
       return $this->belongsToMany(User::class, "job_assignees");
    }

    public function creator()
    {
       return $this->belongsTo(User::class, 'created_by');
    }

    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'job_attachments');
    }

    public function tasks()
    {
        return $this->hasMany(Job::class, 'parent');
    }

    public function parent()
    {
        return $this->belongsTo(Job::class, 'id', 'parent');
    }

        public function comments()
    {
        return $this->belongsToMany(Comment::class, 'job_comments');
    }

}
