<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable = ['comments','created_by'];

    public function jobs()
    {
        return $this->belongsToMany(Job::class, 'job_comments');
    }
}
