<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $prefix =  "JPLMS";
    protected $fillable = [
        "client_id",
        "invoice_date",
        "due_date",
        "version",
        "terms",
        "parent_id",
        "gst_exp",
        "status",
        "payment_method",
        "schedule_date",
        "transaction_id",
        'card_fee',
        'late_charges',
        'auto_pay',
    ];

    public function setParentId()
    {
        $this->parent_id = $this->id;
        $this->save();
    }

    public function getNoAttribute()
    {
        $id = str_pad($this->parent_id, 5, '0', STR_PAD_LEFT);
        return $this->prefix.$id.numbers_to_chars($this->version);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function parent()
    {
        return $this->belongsTo(Invoice::class, 'parent_id');
    }

    public function creditNotes()
    {
        return $this->hasMany(CreditNote::class);
    }

    public function details()
    {
        
        return $this->hasMany(InvoiceDetails::class);
    }

    public function getBalanceAttribute()
    {
        $paid = $this->parent->creditNotes->sum('amount');
        $gst = $this->parent->creditNotes->sum('gst');
        return $this->total['total']-($paid+$gst);
    }
    public function getDepositAttribute()
    {
        $paid = $this->parent->creditNotes->sum('amount');
        $gst = $this->parent->creditNotes->sum('gst');
        return $paid+$gst;
    }

    public function getTotalAttribute()
    {
        $subtotal = $this->details->sum('amount');
        $gst = $this->details->sum('gst');

        $gst = $this->gst_exp!=1?$gst:0;
        $total = $subtotal+$gst;
        return compact('subtotal','gst','total','paid');
    }

    public function getNextVersion($id)
    {
        if ($id == 0) {
            return 0;
        }
        $invoice = $this->find($id);
        return $invoice->version+1;
    }

    public function history()
    {
        return $this->hasMany(Logger::class, "resource_id")
            ->where('type', 'invoice');
    }

    public function getMonthlyInvoices($year, $month)
    {
        return $this->whereMonth('invoice_date', $month)
                     ->whereYear('invoice_date', $year)
                     ->where('status','PAID');
                    

    }

    public function getMonthlyAllInvoices($year, $month)
    {
        return $this->whereMonth('invoice_date', $month)
                     ->whereYear('invoice_date', $year);
    }


     public function getMaxVersionInvoice( $id)
    {
        return $this->where('parent_id',$id)
               ->max('id');
    }
    


    //get client monthly paid invoices
   /* public function getClientMonthlyInvoices($clientId, $year, $month)
    {
        return $this->whereMonth('invoice_date', $month)
                     ->whereYear('invoice_date', $year)
                     ->where('client_id', $clientId)
                     ->where('status','PAID')
                      ->where('is_reconcile',0);
                    

    }*/

    
}
