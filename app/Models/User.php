<?php

namespace App\Models;

use App\Constants\UserRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'pin','determiner','role','rate', 'client_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pin', 'remember_token',
    ];

    public function getFirstNameAttribute()
    {
        return ucfirst(name($this->name)->first_name);
    }

    public function getLastNameAttribute()
    {
        return ucfirst(name($this->name)->last_name);
    }

    public function jobs()
    {
        return $this->belongsToMany(Job::class, "job_assignees")
            ->wherePivotIn('user_id', array_wrap($this->id));
    }

    public function actAs($role)
    {
        return $this->role == $role;
    }

    public function isAdmin()
    {
        return $this->role == UserRoles::ADMIN;
    }
    public function isClient()
    {
        return $this->role == UserRoles::CLIENT;
    }
     public function isClientUser()
    {
        return $this->role == UserRoles::CLIENTUSER;
    }

    public function getAuthPin()
    {
        return $this->pin;
    }

    public function has($determiner)
    {
        return $this->where('determiner', $determiner)->count() > 0;
    }

    public function findByEmail($email)
    {
        return $this->where('email', $email)->first();
    }

    


 
}
