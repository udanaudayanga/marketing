<?php

namespace App\Foundation\Auth;

use App\Services\UserService;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class PinBasedUserProvider extends EloquentUserProvider
{


    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials)) {
            return;
        }

        /** @var UserService $userService */
        $userService = app(UserService::class);

        $query = $this->createModel()->newQuery();

        $query->where("determiner",
            $userService->determiner($credentials['pin'])
        )->whereNotIn('role',['developer','designer','server','seo','support','pa','sales']);

        return $query->first();
    }


    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $plain = $credentials['pin'];

        return $this->hasher->check($plain, $user->getAuthPin());
    }
}