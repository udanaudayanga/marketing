<?php

namespace App\Foundation\Http;

use  \Illuminate\Http\Request as BaseRequest;

class Request extends BaseRequest
{
    public function forModal()
    {
        return !$this->get('modal');
    }
}