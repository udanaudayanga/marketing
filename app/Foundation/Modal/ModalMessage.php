<?php

namespace App\Foundation\Modal;


use App\Foundation\Content\MessageContract;

class ModalMessage implements MessageContract
{

    private $modal;

    private $templateRoot;

    public function __construct(Modal $modal)
    {
        $this->modal = $modal;
        $this->templateRoot = "modal.messages.";
    }

    private function build($message, $template)
    {
        return $this->modal->create()
            ->variables(['message'=>$message])
            ->template( $this->templateRoot.$template)
            ->render();
    }

    public function success($message)
    {
       return $this->build($message,"success");
    }

    public function error($message)
    {
        return $this->build($message,"error");
    }

    public function warning($message)
    {
        return $this->build($message,"warning");
    }

    public function info($message)
    {
        return $this->build($message,"info");
    }


}