<?php

namespace App\Foundation\Modal;


class Modal
{
    protected $attribute = [];
    private $template;

    public static function create()
    {
        $modal = new static;
        return $modal;
    }

    public function title($title)
    {
        $this->attribute['title'] = $title;
        return $this;
    }

    public function action($action)
    {
        $this->attribute = compact('action');
        return $this;
    }

    public function variables(array $vars)
    {
        $this->attribute['vars'] = $vars;
        return $this;
    }

    public function template($template)
    {
        $this->template = $template;
        return $this;
    }

    public function buttons(array $button)
    {
        $this->attribute['action_button'] = $button[0];
        $this->attribute['cancel_button'] = isset($button[1])?$button[1]:[];
        return $this;
    }

    public function render()
    {
        $vars = isset($this->attribute['vars'])?$this->attribute['vars']:[];

        return view($this->template)
            ->with($this->attribute)
            ->with($vars);
    }


}