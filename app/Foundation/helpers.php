<?php
use App\Constants\Priority;
use App\Constants\JobTypes;
use App\Models\User;
use App\Services\UserService;
use Carbon\Carbon;


/**
 * @param string $name
 * @return string
 * @throws Exception
 *  by default logged user's name return
 */
function name($name){

    $splitted_name = [];

    try {
        preg_match('#^(\w+\.)?\s*([\'\’\w]+)\s+([\'\’\w]+)\s*(\w+\.?)?$#', $name, $splitted_name);

        list($name, $prefix, $first_name, $last_name) = $splitted_name;
        $name = (object) compact('name','prefix','first_name', 'last_name', 'suffix');
        return $name;
    } catch (\Exception $e) {
        return $name = (object) ['first_name' => $name, 'last_name'=> ""];
    }

}

/**
 * @return \Illuminate\Contracts\Auth\Authenticatable|\Illuminate\Foundation\Application|mixed|null
 */
function user() {

    if (!auth()->guest()) {
        return auth()->user();
    }
    return app(User::class);
}

/**
 * This will return unique pin for authentication
 * @return int
 */
function generate_pin() {

    /** @var UserService $userService */
    $userService = app(UserService::class);
    return $userService->generateUniquePin();
}

/**
 * This will return priority
 * @param $priority
 * @return string
 */
function get_priority_color($priority) {

    switch($priority) {
        case Priority::IMMEDIATE;
            return "#FB1018";
        case Priority::HIGH;
            return "#ff714b";
        case Priority::MEDIUM;
            return "#14befd";
        case Priority::LOW;
            return "#F3D90A";
    }
    return null;
}


/**
 * This will return font awesome icon base on type
 * @param $type
 * @return string font awesome icon
 */
function get_job_type_icon($type) {

    switch($type) {
        case JobTypes::DESIGN;
            return "fa fa-file-image-o";
        case JobTypes::DEV;
            return "fa fa-file-code-o";
        case JobTypes::SEO;
            return "fa fa-search";
        case JobTypes::SERVER;
            return "fa fa-server";
        case JobTypes::SUPPORT;
            return "fa fa-history";
        case JobTypes::SUB;
            return "fa fa-list";
    }
}

/**
 * @param $fully_qualified_namespace = fully qualified namespace of the constant interface
 * @param bool|false $to_lower = change to lower case of not
 * @return array
 */
function constants_to_array($fully_qualified_namespace, $to_lower = false)
{
    $reflectedClass = new \ReflectionClass($fully_qualified_namespace);
    $constants = $reflectedClass->getConstants();
    if ($to_lower) {
        return array_change_key_case($constants, CASE_LOWER);
    }
    return $constants;
}

/**
 * Converts numbers to asciis
 *
 * @param $no
 * @return string
 */
function numbers_to_chars($no)
{
    $result = "";
    if ($no == 0) {
        return $result;
    }

    foreach(str_split($no) as $int) {
        $result = $result.chr($int+64);
    }
    return $result;
}

/**
 * Return today
 * @return string
 */
/*function today(){
    return Carbon::now()->format('d/m/Y');
}*/