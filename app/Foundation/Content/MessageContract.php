<?php

namespace App\Foundation\Content;


interface MessageContract
{
    public function success($message);
    public function error($message);
    public function warning($message);
    public function info($message);

}