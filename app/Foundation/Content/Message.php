<?php

namespace App\Foundation\Content;


class Message implements MessageContract
{

    private function negotiateContent($message, $type)
    {
        return [];
    }

    public function success($message)
    {
        return $this->negotiateContent($message, 'success');
    }

    public function error($message)
    {
        return $this->negotiateContent($message, 'error');
    }

    public function warning($message)
    {
        return $this->negotiateContent($message, 'warning');
    }

    public function info($message)
    {
        return $this->negotiateContent($message, 'info');
    }
}