<?php

namespace App\Http\Controllers;

use App\Services\AttachmentService;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class AttachmentController extends Controller
{
    /**
     * @var AttachmentService
     */
    private $attachmentService;

    private $destinationPath = 'uploads';


    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function upload(Request $request)
    {
         $attachment = $this->saveAttachment($request->file('file'));
         return [
           'id' => $attachment->id,
           'originalName' => $attachment->original_name,
         ];
    }

    public function remove(Request $request)
    {
        $attachmentId = $request->get('id');
        $jobId = $request->get('job_id');
        return $this->attachmentService->remove($attachmentId, $jobId);
    }

    private function saveAttachment(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $fileName = sha1(rand(11111,99999)).'.'.$extension;
        $file->move($this->destinationPath, $fileName);
        $mine_type = $file->getClientMimeType();
        $original_name = $file->getClientOriginalName();
        $path = $this->destinationPath."/".$fileName;
        return $this->attachmentService->add(compact('original_name', 'mine_type', 'path'));
    }
}
