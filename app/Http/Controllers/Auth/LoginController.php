<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function validateLogin(Request $request)
    {
       //client id save in session for admin access
       session(['client_id' =>  $request->get('client_id')]);
      
        $this->validate($request, [
            'pin.*' => 'required',
        ],[
            'pin.*.required' => 'Please enter valid pin'
        ]);
    }


    protected function attemptLogin(Request $request)
    {

        $input = $request->all();
        $pin = implode('', $input['pin']);

        return $this->guard()->attempt(
            compact('pin'), $request->has('remember')
        );
    }
}
