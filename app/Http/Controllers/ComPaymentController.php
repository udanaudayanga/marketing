<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use URL;
use Session;
use Redirect;
use Input;

use App\Models\Card;
use App\Services\InvoiceManager;
use App\Constants\InvoiceStatus;
use App\Services\CardService;
use App\Mail\InvoiceReceipt;
use Illuminate\Support\Facades\Hash;
use Facades\App\Foundation\Modal\Modal;
use Facades\App\Foundation\Modal\ModalMessage;
use Illuminate\Support\Facades\Mail;

class ComPaymentController extends Controller
{
	 /**
     * @var InvoiceManager
     */
    private $invoiceManager;

    protected $curlObj;

    /**
     * @var CardService
     */
    private $cardService;

    //
    public function __construct(InvoiceManager $invoiceManager, CardService $cardService)
    {
      ini_set( 'serialize_precision', -1 );

    	$this->curlObj = curl_init();
    	// configure cURL proxy options by calling this function
    	$this->ConfigureCurlProxy();

    	// configure cURL certificate verification settings by calling this function
    	$this->ConfigureCurlCerts();

        $this->invoiceManager = $invoiceManager;
         $this->cardService = $cardService;
	}

	public function __destruct() 
	{
    // free cURL resources/session
    curl_close($this->curlObj);
    }


	public function pay(Request $request)
    {
      $invoId = $request->get('invo_id');
    	$invoice = $this->invoiceManager->find($request->get('invo_id'));

      $card = Card::find($request->get('card_id'));
      $data                                                          = [];
      $data['apiOperation']                                          = 'PAY';
      $data['sourceOfFunds']['type']                                 = 'CARD';
      $data['sourceOfFunds']['provided']['card']['securityCode']     = $card->cvv_code;
      $data['sourceOfFunds']['token']                                = $card->token;
      $data['order']['amount']                                       = round($request->get('amount'),2);
      $data['order']['description']                                  = $invoice->no;
      $data['order']['currency']                                     = 'AUD';




      $method                             = 'PUT';


      $transId                            = rand ( 20 , 999 );
      $customUri                          = "/order/" . $request->get('invo_id').'/transaction/'.$transId;
      $request1                           = $this->ParseRequest($data);
      $requestUrl                         = $this->FormRequestUrl( $customUri);
      $response                           = $this->SendTransaction($request1, $method,$requestUrl);
      $responseArray                      = json_decode($response, TRUE);

      $responseArr = [];
      $responseArr['ABORTED'] = 'Transaction aborted by payer';
      $responseArr['ACQUIRER_SYSTEM_ERROR'] = ' Acquirer system error occurred processing the transaction';
      $responseArr['APPROVED'] = 'Transaction Approved';
      $responseArr['APPROVED_AUTO'] = 'The transaction was automatically approved by the gateway. it was not submitted to the acquirer.';
      $responseArr['APPROVED_PENDING_SETTLEMENT'] = 'Transaction Approved - pending batch settlement';
      $responseArr['AUTHENTICATION_FAILED'] = '3D Secure authentication failed';
      $responseArr['BALANCE_AVAILABLE'] = 'A balance amount is available for the card, and the payer can redeem points.';
      $responseArr['BALANCE_UNKNOWN'] = 'A balance amount might be available for the card. Points redemption should be offered to the payer.';
      $responseArr['BLOCKED'] = ' Transaction blocked due to Risk or 3D Secure blocking rules';
      $responseArr['CANCELLED'] = 'Transaction cancelled by payer';
      $responseArr['DECLINED']                = 'Transaction declined by issuer';
      $responseArr['DECLINED_AVS']            = 'Transaction declined due to address verification';
      $responseArr['DECLINED_AVS_CSC']        = 'Transaction declined due to address verification and card security code';
      $responseArr['DECLINED_CSC']            = 'Transaction declined due to card security code';
      $responseArr['DECLINED_DO_NOT_CONTACT'] = 'Transaction declined - do not contact issuer';
      $responseArr['DECLINED_INVALID_PIN'] = 'Transaction declined due to invalid PIN';
      $responseArr['DECLINED_PAYMENT_PLAN'] = 'Transaction declined due to payment plan';
      $responseArr['DECLINED_PIN_REQUIRED'] = 'Transaction declined due to PIN required';
      $responseArr['DEFERRED_TRANSACTION_RECEIVED'] = 'Deferred transaction received and awaiting processing';
      $responseArr['DUPLICATE_BATCH'] = 'Transaction declined due to duplicate batch';
      $responseArr['EXCEEDED_RETRY_LIMIT'] = 'Transaction retry limit exceeded';
      $responseArr['EXPIRED_CARD'] = 'Transaction declined due to expired card';
      $responseArr['INSUFFICIENT_FUNDS'] = 'Transaction declined due to insufficient funds';
      $responseArr['INVALID_CSC'] = 'Invalid card security code';
      $responseArr['LOCK_FAILURE'] = 'Order locked - another transaction is in progress for this order';
      $responseArr['NOT_ENROLLED_3D_SECURE'] = 'Card holder is not enrolled in 3D Secure';
      $responseArr['NOT_SUPPORTED'] = 'Transaction type not supported';
      $responseArr['NO_BALANCE'] = 'A balance amount is not available for the card. The payer cannot redeem points.';
      $responseArr['PARTIALLY_APPROVED'] = 'The transaction was approved for a lesser amount than requested. The approved amount is returned in order.totalAuthorizedAmount.';
      $responseArr['PENDING'] = 'Transaction is pending';
      $responseArr['REFERRED'] = 'Transaction declined - refer to issuer';
      $responseArr['SUBMITTED'] = 'The transaction has successfully been created in the gateway. It is either awaiting submission to the acquirer or has been submitted to the acquirer but the gateway has not yet received a response about the success or otherwise of the payment.';
      $responseArr['SYSTEM_ERROR'] = 'Internal system error occurred processing the transaction';
      $responseArr['TIMED_OUT'] = 'The gateway has timed out the request to the acquirer because it did not receive a response. Points redemption should not be offered to the payer.';
      $responseArr['UNKNOWN'] = 'The transaction has been submitted to the acquirer but the gateway was not able to find out about the success or otherwise of the payment. If the gateway subsequently finds out about the success of the payment it will update the response code.';
      $responseArr['UNSPECIFIED_FAILURE'] = 'Transaction could not be processed';



    if (array_key_exists("result", $responseArray))
        $result = $responseArray["result"];


      if ( $result == 'ERROR' ||  $result == 'FAILURE') {

      if (array_key_exists("error", $responseArray['response'])){
         \Session::put('error', 'Sorry, Your payment is failed due to following reason.<br/>'.$responseArray['error']['explanation']);
      }

      else if (array_key_exists("gatewayCode", $responseArray['response'])){
        $resultStatus = $responseArray['response']["gatewayCode"];
         \Session::put('error', 'Sorry, Your payment is failed due to following reason.<br/>'.$responseArr[$resultStatus]);
      } else
             \Session::put('error', 'Sorry, Your payment is failed.');
        
           
       return Redirect::route('non_subscribe_payments',$request->get('invo_id'));
    
    }


      if (array_key_exists("gatewayCode", $responseArray['response']))
        $resultStatus = $responseArray['response']["gatewayCode"];

      /* if ( $resultStatus == 'DECLINED' || $resultStatus == 'EXPIRED_CARD' || $resultStatus == 'TIMED_OUT' || $resultStatus == 'ACQUIRER_SYSTEM_ERROR' || $resultStatus == 'UNSPECIFIED_FAILURE' || $resultStatus == 'UNKNOWN'  )
      {
        if (isset($responseArray['response']['acquirerMessage']))
          \Session::put('error', 'Payment failed.'.$responseArray['response']['acquirerMessage']);
        else
             \Session::put('error', 'Sorry, Your payment is failed.');
      
       return Redirect::route('non_subscribe_payments',$request->get('invo_id'));

      }*/

    
    if ($resultStatus == 'APPROVED') {
   // \Session::put('success', 'Payment success');
    
      $invoice->status          = InvoiceStatus::PAID;
      $invoice->payment_method  = 'CREDIT CARD';
      $invoice->card_fee        = $request->get('card_fee');
      $invoice->late_charges    = $request->get('late_payment');
      $invoice->transaction_id  = $responseArray['transaction']['acquirer']['transactionId'];
      $invoice->save();

       $this->sendinvoicemail( $invoId);


         return view('pages.successful_paymentinfo')
            ->with(compact('invoice','client'));

       //return Redirect::route('payment_success');
       // return Redirect::route('invoice_list');
    }

    
}


    public function savePaymentMethod(Request $request)
    {
          $attributes = $request->all();
          $attributes['use_next_time'] = isset($request['use_next_time'])?1:0;
          $cardFirstDigits = substr($request['card_number'],0, 6);
          $cardLastDigits  = substr($request['card_number'], -4);
          $cardLength      = strlen($request['card_number'])-4;

          $newFormattedNumber = str_pad($cardFirstDigits, $cardLength, '*',STR_PAD_RIGHT );
         $newFormattedNumber .= $cardLastDigits;
          $attributes['card_number']   =  $newFormattedNumber;

         
          
          $method                                                        = 'POST';
          $data                                                          = [];
          $data['sourceOfFunds']['type']                                 = 'CARD';
          
          $data['sourceOfFunds']['provided']['card']['number']           =  $request['card_number'];
          $data['sourceOfFunds']['provided']['card']['expiry']['month']  =  $request['exp_date_month'];
          $data['sourceOfFunds']['provided']['card']['expiry']['year']   =  $request['exp_date_year'];
          $data['sourceOfFunds']['provided']['card']['securityCode']     =  $request['cvv_code'];
      

          $customUri                          = "/token";
          $request                            = $this->ParseRequest($data);
          $requestUrl                         = $this->FormRequestUrl( $customUri);
          $response                           = $this->SendTransaction($request, $method,$requestUrl);
          $responseArray                      = json_decode($response, TRUE);

         if (array_key_exists("result", $responseArray))
        $result = $responseArray["result"];

        if ( $result == 'ERROR') {
       return ModalMessage::success("Error in your card details. Please check with following details.<br/>".$responseArray['error']['explanation']);
      }

      if ($result == 'SUCCESS' && $responseArray['status'] == 'VALID') {
         $attributes['card_type'] =   $responseArray['sourceOfFunds']['provided']['card']['brand'];
         $attributes['token'] =   $responseArray['token'];
         
          $card = new Card();
          $card->create($attributes);

          //save card in session


          return ModalMessage::success("Payment method successfully saved!");
      
      }

        
    }

	public function sendinvoicemail($id)
    {
        $invoice = $this->invoiceManager->find($id);
        $client = $this->invoiceManager->getClient();
        $mails  = $client->reciptEmails()->get();

        $toMails = [];

        foreach ( $mails as $mail)
        {
        	$toMails[] = $mail->email; 
        }
       
        $fileName = "receipt-".$invoice->no.".pdf";
        \PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif','isHtml5ParserEnabled'=>true]); 
        $pdf = \PDF::loadView('pdf.receipt', compact('invoice'))
            ->setPaper(array(0, 0, 595, 841), 'portrait');
       
        $pdf->save('./receipts/'.$fileName);

        Mail::to($toMails)
        
           ->bcc('james@jplms.com.au')
           ->send(new InvoiceReceipt($invoice));

             
    }

  
   // Send transaction to payment server
  public function SendTransaction( $request, $method,$requestUrl) {
    // The below sets the HTTP operation type
    if ($method == "GET") {
      curl_setopt($this->curlObj, CURLOPT_HTTPGET, 1);
    }
    else if ($method == "POST") {
      // NOTE: POST operations are currently not supported in this version
      // [Snippet] howToPost - start
      curl_setopt($this->curlObj, CURLOPT_POST, 1);
      // [Snippet] howToPost - end
      curl_setopt($this->curlObj, CURLOPT_POSTFIELDS, $request);
      // [Snippet] howToSetHeaders - start
      curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
      curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
      // [Snippet] howToSetHeaders - end
    }
    else if ($method == "PUT") {
      // [Snippet] howToPut - start
      curl_setopt($this->curlObj, CURLOPT_CUSTOMREQUEST, "PUT");
      // [Snippet] howToPut - end
      curl_setopt($this->curlObj, CURLOPT_POSTFIELDS, $request);
      curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
      curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
    }

   

		// [Snippet] howToSetURL - start
    // call the function below to construct the URL for sending the transaction
    curl_setopt($this->curlObj, CURLOPT_URL, $requestUrl);
		// [Snippet] howToSetURL - end

    // [Snippet] howToSetCredentials - start
    // set the API Password in the header authentication field.
    curl_setopt($this->curlObj, CURLOPT_USERPWD, env('API_USERNAME') . ":" . env('API_PASSWORD'));
    // [Snippet] howToSetCredentials - end

    // tells cURL to return the result if successful, of FALSE if the operation failed
    curl_setopt($this->curlObj, CURLOPT_RETURNTRANSFER, TRUE);

    // this is used for debugging only. This would not be used in your integration, as DEBUG should be set to FALSE
  /* if ($merchantObj->GetDebug()) {
      curl_setopt($this->curlObj, CURLOPT_HEADER, TRUE);
      curl_setopt($this->curlObj, CURLINFO_HEADER_OUT, TRUE);
    }*/

    // [Snippet] executeSendTransaction - start
    // send the transaction
    $response = curl_exec($this->curlObj);
    // [Snippet] executeSendTransaction - end

    // this is used for debugging only. This would not be used in your integration, as DEBUG should be set to FALSE
    /*if ($merchantObj->GetDebug()) {
      $requestHeaders = curl_getinfo($this->curlObj);
      if (array_key_exists("request_header", $requestHeaders))
        $response = $requestHeaders["request_header"] . $response;
    }*/

    // assigns the cURL error to response if something went wrong so the caller can echo the error
    if (curl_error($this->curlObj))
      $response = "cURL Error: " . curl_errno($this->curlObj) . " - " . curl_error($this->curlObj);

    // respond with the transaction result, or a cURL error message if it failed
    return $response;
  }


    // Check if proxy config is defined, if so configure cURL object to tunnel through
 protected function ConfigureCurlProxy() {
    // If proxy server is defined, set cURL options
    if ( env('PROXYSERVER') != "") {
      curl_setopt($this->curlObj, CURLOPT_PROXY, env('PROXYSERVER'));
      curl_setopt($this->curlObj,env('PROXYCURLOPTION'), env('PROXYCURLVALUE'));
    }
    // If proxy authentication is defined, set cURL option
    if (env('PROXYAUTH') != "")
      curl_setopt($this->curlObj, CURLOPT_PROXYUSERPWD, env('PROXYAUTH'));
  }
  // [Snippet] howToConfigureProxy - end

  // [Snippet] howToConfigureSslCert - start
  // configure the certificate verification related settings on the cURL object
  protected function ConfigureCurlCerts() {
    // if user has given a path to a certificate bundle, set cURL object to check against them
    if (env('CERTIFICATE_PATH') != "")
      curl_setopt($this->curlObj, CURLOPT_CAINFO,env('CERTIFICATE_PATH') );

    curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYPEER, env('CERTIFICATE_VERIFY_PEER'));
    curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYHOST, env('CERTIFICATE_VERIFY_HOST'));
  }


  public function FormRequestUrl($customUri) {
    $gatewayUrl = env('GATEWAY_URL');
    $gatewayUrl .= "/version/" . env('VERSION');
    $gatewayUrl .= "/merchant/" . env('MERCHANT_ID');
    $gatewayUrl .= $customUri;
   
    return $gatewayUrl;
  }
  // [Snippet] howToConfigureURL - end

	// [Snippet] howToConvertFormData - start
  // Takes a multidimensional associative array
  //
  // Recursive function to unset empty arrays from a
  // multidimensional array up to the highest level
  public function RemoveEmptyValues($array) {
    foreach ($array as $i => $value) {
      // If member is an array
      if (is_array($array[$i])) {
        // if array has no members, unset array
        if (count($array[$i]) == 0)
          unset($array[$i]);
        // if array has members, recurse and pass in the array
        // recursive function will then loop through all members of this array
        else {
          // overwrite old array with new structure
          $array[$i] = $this->RemoveEmptyValues($array[$i]);
          // if array is empty unset it
          if (count($array[$i]) == 0)
            unset($array[$i]);
        }
      }
      // if member not an array
      else {
        // if member variable is empty, unset it
        if ($array[$i] == "")
          unset($array[$i]);
      }
    }
    return $array;
  }

  
  // Creates the JSON encoded transaction body from an associative array
  // Remember to make it check if the array member is empty, then remove it from json if it is
  public function ParseRequest($formData) {

    $request = "";
    if (count($formData) == 0)
      return "";

    $formData = $this->RemoveEmptyValues($formData);
    $request = json_encode($formData);

    return $request;
  }


public function updatePaymentMethod(Request $request)
    {

          $method                                                        = 'POST';
          $data                                                          = [];
          $data['sourceOfFunds']['type']                                 = 'CARD';
          
          $data['sourceOfFunds']['token']                                =  $request['token'];
          $data['sourceOfFunds']['provided']['card']['expiry']['month']  =  $request['exp_date_month'];
          $data['sourceOfFunds']['provided']['card']['expiry']['year']   =  $request['exp_date_year'];
         
      

          $customUri                          = "/token";
          $requestcom                         = $this->ParseRequest($data);
          $requestUrl                         = $this->FormRequestUrl( $customUri);
          $response                           = $this->SendTransaction($requestcom, $method,$requestUrl);
          $responseArray                      = json_decode($response, TRUE);

        

         if (array_key_exists("result", $responseArray))
        $result = $responseArray["result"];

        if ( $result == 'ERROR') {
       return ModalMessage::success("Error in your card details. Please check.");
    }

    if ($result == 'SUCCESS' && $responseArray['status'] == 'VALID') {
        $attributes = $request->all();
        $attributes['set_default'] = isset($request['set_default'])?1:0;
       
         $this->cardService->update($attributes);
        return ModalMessage::success("Payment method successfully updated!");
    
    }



      
    }


	
}
