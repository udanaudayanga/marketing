<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Client;
use App\Models\Website;
use App\Services\SemrushService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Facades\App\Foundation\Modal\Modal;
use Facades\App\Foundation\Modal\ModalMessage;

class SemrushController extends Controller {

	  private $semrushService;
     public function __construct( SemrushService $semrushService)
    {
      $this->semrushService  = $semrushService;
    }

     function getClient()
    {
       
       if ( Auth::user()->role == 'admin')
       {
            $client_id = session('client_id');
            $client    = Client::find( $client_id);
       }else
       {
             $client  = Client::find(Auth::user()->client_id);
       }
       return $client;
    }

    public function index( $projectId ) 
    {

       $this->saveRequests($projectId);

       $website = new Website();
       $website1 = $website->websiteBySemProId($projectId);
       
       $semrushInfo = $this->semrushService->find($projectId, 'info');
       $response    = json_decode($semrushInfo->response, true);


       $totalScore  = $response['current_snapshot']['quality']['value'];
       $totalScoreSub = 100-$totalScore;


       $semrushsnapshots = $this->semrushService->find($projectId, 'snapshots');
       $snapshots_response    = json_decode($semrushsnapshots->response, true);
 

        $errorInfo    = [];
        $warnInfo     = [];
        $noticeInfo   = [];

        $errorInfo[] = ['Date','Issues'];
        $warnInfo[] = ['Date','Issues'];
        $noticeInfo[] = ['Date','Issues'];
       
        $s = 0;
       


        foreach (  $snapshots_response['snapshots'] as $snapshot)
        {

             $semrushsnapshotInfo = $this->semrushService->find($projectId, $snapshot['snapshot_id']);
             $response_snapinfo    = json_decode($semrushsnapshotInfo->response, true);

              

              $snaperrorcount = 0;
              $snapwarncount  = 0;
              $snapnoticecount  = 0;


              foreach(  $response_snapinfo['errors'] as $snaperror)
              {
                  $snaperrorcount = $snaperrorcount+ $snaperror['count'];
              } 
              $errordates = date('Y-m-d', (int)$response_snapinfo['finish_date']/1000);

              $errorInfo[] = [$errordates, (int)$snaperrorcount];

              foreach(  $response_snapinfo['warnings'] as $snapwarn)
              {
                  $snapwarncount = $snapwarncount+ $snapwarn['count'];
              } 
              $warndates = date('Y-m-d', $response_snapinfo['finish_date']/1000);
              
              $warnInfo[] = [$warndates, (int)$snapwarncount];

              foreach(  $response_snapinfo['notices'] as $snapnotice)
              {
                  $snapnoticecount = $snapnoticecount+ $snapnotice['count'];
              } 
              $noticedates   = date('Y-m-d', $response_snapinfo['finish_date']/1000);
              $noticeInfo[]  = [$noticedates, (int)$snapnoticecount];

                $s = $s+1;

          }

          $errorInfoData    = json_encode($errorInfo);
          $warnInfoData     = json_encode($warnInfo);
          $noticeInfoData   = json_encode($noticeInfo);

          $errorcount = [];
  
       $j = 0;

       $totalErrorCount = 0;

      foreach ($response['current_snapshot']['topIssues'] as $issueId)
      {

       foreach ( $response['current_snapshot']['errors'] as $error)
       {
          $totalErrorCount = $totalErrorCount+$error['count'];
       }

     }



        foreach ($response['current_snapshot']['topIssues'] as $issueId)
       {
          foreach ( $response['current_snapshot']['errors'] as $error)
          {
              if ( $error['id'] ==$issueId )
              {
                $errorcount[$issueId] = $error['count'];
                $j = $j+1;
              }

              if($j == 3)
                break;
          }

       }



       $crawlability =  $response['current_snapshot']['thematicScores']['crawlability']['value'];
       $nonCrawlability = 100-$crawlability;

       $https   = $response['current_snapshot']['thematicScores']['https']['value'];
       $nonhttps = 100-$https;
     

       $seo     = $response['current_snapshot']['thematicScores']['intSeo']['value'];
       $nonseo = 100-$seo;

       $performance = $response['current_snapshot']['thematicScores']['performance']['value'];
       $nonperformance = 100-$performance;


       $link =  $response['current_snapshot']['thematicScores']['linking']['value'];
       $nonlink = 100 - $link;

       $semrushIssuesInfo = $this->semrushService->find($projectId, 'issues');
      $response_issue_info    = json_decode($semrushIssuesInfo->response, true);
 
     
       $issueInfo  = [];

       $count = 0;

       foreach ($response['current_snapshot']['topIssues'] as $issueId)
       {
          foreach ( $response_issue_info['issues'] as $issueDetails)
          {
              if ( $issueDetails['id'] == $issueId   && isset($errorcount[$issueId])){

                 $issueInfo[$count]['info'] =$errorcount[$issueId].' '.$issueDetails['title'];
                 $issueInfo[$count]['percentage'] = round(($errorcount[$issueId]*100)/$totalErrorCount);
                 $count = $count+1;


              }

              if( $count == 3)
                break;
          
          }
       }

         return view('semrush.siteaudit')
            ->with(compact('response','crawlability','nonCrawlability','https','nonhttps','seo','nonseo','performance','nonperformance','link','nonlink','website1','issueInfo','errorInfoData','warnInfoData','noticeInfoData','totalScore','totalScoreSub'));


    }


       public function allIssues($projectId)
    {
        $semrushInfo = $this->semrushService->find($projectId, 'info');
        $response    = json_decode($semrushInfo->response, true);

        $errorInfo =[];

        foreach ($response['current_snapshot']['topIssues'] as $issueId)
       {
          foreach ( $response['current_snapshot']['errors'] as $error)
          {
               $errorInfo[$error['id']] = $error['count'];
          }
        }

  
        $semrushIssuesInfo = $this->semrushService->find($projectId, 'issues');
        $response_issue_info    = json_decode($semrushIssuesInfo->response, true);

        $issueInfoAll  = [];

        $count = 0;

       
          foreach ( $response_issue_info['issues'] as $issueDetails)
          {
            if (array_key_exists($issueDetails['id'],$errorInfo))
            {

              $issueInfo = [];
             $issueInfo['title'] =$issueDetails['title'];
             $issueInfo['title_page'] =str_replace("##count##",$errorInfo[$issueDetails['id']],$issueDetails['title_page']);
             
            $issueInfoAll[] = $issueInfo;

            }
             
            
          
          }
    
        return Modal::create()
            ->title("All Issues")
            ->template('semrush.all_issues')
            ->buttons(["Delete", "Cancel"])
            ->variables(compact('issueInfoAll'))
            ->render();
    }


    function callAPI($method, $url, $data){
	   $curl = curl_init();

	   switch ($method){
	      case "POST":
	         curl_setopt($curl, CURLOPT_POST, 1);
	         if ($data)
	            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	         break;
	      case "PUT":
	         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
	         if ($data)
	            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
	         break;
	      default:
	         if ($data)
	            $url = sprintf("%s?%s", $url, http_build_query($data));
	   }

	   // OPTIONS:
	   curl_setopt($curl, CURLOPT_URL, $url);
	  /* curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	      'APIKEY: e0b48df151f2e1b6fdcaf703652d1734',
	      'Content-Type: application/json',
	   ));*/

      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'APIKEY: '.env('SEMRUSH_API_KEY'),
        'Content-Type: application/json',
     ));

     
	   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	   // EXECUTE:
	   $result = curl_exec($curl);
	   if(!$result){die("Connection Failure");}
	   curl_close($curl);
	   return $result;
	}



  function saveProject($projectName, $url)
  {
     
     $apiKey  = env('SEMRUSH_API_KEY');
     $url  = 'https://api.semrush.com/management/v1/projects?key='.$apiKey.'&project_name='.$projectName.'&url='.$url;

      
     $get_data = $this->callAPI('GET', $url, false);
     $response = $get_data;

     $response    = json_decode($response, true);

     $data              = [];
     $data['project_id'] = Auth::user()->client_id;
     $data['website']   = $projectId;
     $data['year'] = date('Y');
     $data['month'] =  date('m');
     $data['description'] =  'info';
     $data['response'] =  $response;

     $this->semrushService ->create($data);

     return $response;

  }




  function saveInfo($projectId)
  {
     
     $apiKey  = env('SEMRUSH_API_KEY');
     $url  = 'https://api.semrush.com/reports/v1/projects/'.$projectId.'/siteaudit/info?key='.$apiKey;
      
     $get_data = $this->callAPI('GET', $url, false);
     $response = $get_data;

     $data              = [];
     $data['client_id'] = Auth::user()->client_id;
     $data['website']   = $projectId;
     $data['year'] = date('Y');
     $data['month'] =  date('m');
     $data['description'] =  'info';
     $data['response'] =  $response;

     $this->semrushService ->create($data);

     return $response;

  }


   function saveSnapshots($projectId)
  {
     
     $apiKey  = env('SEMRUSH_API_KEY');
     
     $snapshot_url      = 'https://api.semrush.com/reports/v1/projects/'.$projectId.'/siteaudit/snapshots?key='.$apiKey;
     $get_data_snapshot = $this->callAPI('GET', $snapshot_url, false);
     $response = $get_data_snapshot;
     $snapshots_response = json_decode($get_data_snapshot, true);

     $data              = [];
     $data['client_id'] = Auth::user()->client_id;
     $data['website']   = $projectId;
     $data['year'] = date('Y');
     $data['month'] =  date('m');
     $data['description'] =  'snapshots';
     $data['response'] =  $response;

     $this->semrushService ->create($data);


      foreach ( $snapshots_response['snapshots'] as $snapshot)
      {

            $snapshotinfo_url = 'https://api.semrush.com/reports/v1/projects/'.$projectId.'/siteaudit/snapshot?key='.$apiKey.'&snapshot_id='.$snapshot['snapshot_id'];
            $get_data_snapshotinfo = $this->callAPI('GET', $snapshotinfo_url, false);
            $response_s  = $get_data_snapshotinfo;
            $response_snapinfo = json_decode($get_data_snapshotinfo, true);


           $dataS              = [];
           $dataS['client_id'] = Auth::user()->client_id;
           $dataS['website']   = $projectId;
           $dataS['year'] = date('Y');
           $dataS['month'] =  date('m');
           $dataS['description'] =  $snapshot['snapshot_id'];
           $dataS['response'] =  $response_s;

           $this->semrushService ->create($dataS);

      }
  }


  function saveIssues($projectId)
  {
     
     $apiKey  = env('SEMRUSH_API_KEY');
     $issueInfoUrl  = 'https://api.semrush.com/reports/v1/projects/'.$projectId.'/siteaudit/meta/issues?key='.$apiKey;
      
     $get_data_issues = $this->callAPI('GET', $issueInfoUrl, false);
   
   
    $response =  $get_data_issues;

     $data              = [];
     $data['client_id'] = Auth::user()->client_id;
     $data['website']   = $projectId;
     $data['year'] = date('Y');
     $data['month'] =  date('m');
     $data['description'] =  'issues';
     $data['response'] =  $response;

     $this->semrushService->create($data);


  }



  function saveRequests($projectId)
  {
    //check project Id is exsit

    //check client has record for this month if today >=15

    //if there is no records save following lines
    $clientRecords   = $this->semrushService->clientRecords($projectId);
    if ( $clientRecords->count() == 0)
    {
        //save data in db
          $this->saveInfo($projectId);
          $this->saveSnapshots($projectId);
          $this->saveIssues($projectId);

       //get data from db and load page
          
        
    }else{
        
     
        if ( date('d')>= 15)
        {
          //check current month has data
          $hasRecords = $clientRecords->where('month',date('m'))
                        ->where('year',date('Y'))
                        ->count();

          if( $hasRecords ==0)
          {
            //save data in db
            $this->saveInfo($projectId);
            $this->saveSnapshots($projectId);
            $this->saveIssues($projectId);
          }
        

        }

    }

  
  }
   

  //Domain Analysis
  function compatiterAnalysis($projectId )
  {
      $website = new Website();
      $website1 = $website->websiteBySemProId($projectId);

      //$projectName = 'gterlato.com.au';
      $apiKey      = env('SEMRUSH_API_KEY');

     // $domain = str_replace("www.","",$website1[0]->url);
   

      $domain  = 'precisionmail.com.au';
      
      //organic keywords
      $organicKeywords  = $this->getOrganicKeywords( $domain);

      //Backlinks
      $backlinks        = $this->getBackLinks($domain);


      //get anchors
      $anchors          = $this->getTopAnchors( $domain );

      //get index backlinks
      $indexBackLinks   = $this->getIndexBacklink( $domain );


      //reffering domain
      $referingDomains   = $this->getReferingDomains( $domain );

      //main competitors
      $mainCompetitors  =   $this->getMainCompetitors($domain);

      //backlink overview
       $backlinkOverviews  = $this->getBackLinkOverview($domain);

       //organic search overview
       $organicSearchOverviews  = $this->getOrganicSearchOverview($domain);
      // dd($organicSearchOverviews);

     return view('semrush.compatitor-analysis')
            ->with(compact('website1','domain','organicKeywords','backlinks','anchors','indexBackLinks','referingDomains','mainCompetitors','backlinkOverviews','organicSearchOverviews'));

  } 


  //backlink overview
   function getBackLinkOverview($domain)
  {
     
     $apiKey  = env('SEMRUSH_API_KEY');
     $url  = ' https://api.semrush.com/analytics/v1/?key='.$apiKey.'&target='.$domain.'&type=backlinks_overview&target_type=root_domain&export_columns=total,domains_num,urls_num,ips_num,ipclassc_num,texts_num,follows_num,forms_num,forms_num,nofollows_num,forms_num,frames_num,images_num,score,trust_score';
       
   //  $dataString  = $this->callAPI('GET', $url, false);

     $dataString  =  'total;domains_num;urls_num;ips_num;ipclassc_num;texts_num;follows_num;forms_num;forms_num;nofollows_num;forms_num;frames_num;images_num;score;trust_score
216;14;118;16;16;206;213;0;0;3;0;0;10;17;22';
   
    
     return $this->parseDataIntoArray($dataString);

  }



  //backlink overview
   function getOrganicSearchOverview($domain)
  {
     
     $apiKey  = env('SEMRUSH_API_KEY');
     $url  = 'https://api.semrush.com/?type=domain_rank&key='.$apiKey.'&domain='.$domain.'&database=au&&export_columns=Dn,Rk,Or,Ot,Oc,Ad,At,Ac,Um,am,Cm';
       
   //  $dataString  = $this->callAPI('GET', $url, false);

     $dataString  =  'Domain;Rank;Organic Keywords;Organic Traffic;Organic Cost;Adwords Keywords;Adwords Traffic;Adwords Cost
precisionmail.com.au;1336443;45;13;30;0;0;0';
   
    
     return $this->parseDataIntoArray($dataString);

  }




  //domain analysis - BACKLINKS
  function getBackLinks($domain)
  {
     
     $apiKey  = env('SEMRUSH_API_KEY');
     $url  = 'https://api.semrush.com/analytics/v1/?key='.$apiKey.'&target='.$domain.'&type=backlinks&target_type=root_domain&display_limit=10';
           
   //  $dataString  = $this->callAPI('GET', $url, false);

     $dataString  =  'page_score;source_title;source_url;target_url;anchor;external_num;internal_num;first_seen;last_seen
                      65;Data Made Easy - DataTools;https://datatools.com.au/;http://www.precisionmail.com.au/;;29;48;1501845005;1561341528
                      65;Data Made Easy - DataTools;https://datatools.com.au/;https://www.precisionmail.com.au/;;29;48;1526789102;1560865296
                      64;RSPCA Victoria - Corporate partners and sponsors;https://rspcavic.org/about-us/corporate-partners-and-sponsors;https://www.precisionmail.com.au/;;45;117;1535045793;1547729949
                      57;Awards 2019 - Fundraising Institute Australia;https://fia.org.au/professional-development/awards-2019/;https://www.precisionmail.com.au/;;20;193;1534697645;1548716514
                      38;Contact Us - DataTools;https://datatools.com.au/contact-us/;http://www.precisionmail.com.au/;;30;33;1501420630;1561131689
                      38;Contact Us - DataTools;https://datatools.com.au/contact-us/;https://www.precisionmail.com.au/;;30;33;1526789102;1560865296
                      21;Tasmania | RSPCA Million Paws Walk;https://www.millionpawswalk.com.au/TAS;https://www.precisionmail.com.au/;;91;92;1497938055;1553144566
                      18;Northern Territory | RSPCA Million Paws Walk;https://www.millionpawswalk.com.au/NT;https://www.precisionmail.com.au/;;86;93;1497938524;1557709542
                      16;Welcome to the Northern Darts Association;http://www.ndadarts.org.au/;https://www.precisionmail.com.au/;left;11;53;1526789102;1560865296
                      16;Welcome to the Northern Darts Association;http://www.ndadarts.org.au/;http://www.precisionmail.com.au/;left;11;53;1498322685;1561334392';
   
    
     return $this->parseDataIntoArray($dataString);

  }


  function getOrganicKeywords( $domain)
  {

     $apiKey  = env('SEMRUSH_API_KEY');
     $url     = 'https://api.semrush.com/?type=domain_organic&key='.$apiKey.'&domain='.$domain.'&database=au';
           
   //  $dataString  = $this->callAPI('GET', $url, false);

     $dataString  =  'Keyword;Position;Previous Position;Position Difference;Search Volume;CPC;Url;Traffic (%);Traffic Cost (%);Competition;Number of Results;Trends
                      mail house melbourne;5;5;0;110;3.02;https://www.precisionmail.com.au/;38.46;53.33;0.42;89000000;0.36,0.64,0.79,0.64,0.50,1.00,0.79,1.00,0.64,0.79,0.79,0.50
                      precision printing;3;3;0;40;0.00;https://www.precisionmail.com.au/;23.07;0.00;0.05;135000000;0.27,0.08,0.65,0.04,0.04,0.04,0.04,0.04,0.65,0.04,0.04,1.00
                      mail house sydney;10;10;0;70;4.77;https://www.precisionmail.com.au/;15.38;33.33;0.59;127000000;0.05,0.05,0.10,0.05,0.05,0.33,1.00,0.67,0.05,0.05,0.81,0.05
                      printing and direct mail services;8;8;0;70;0.00;https://www.precisionmail.com.au/;15.38;0.00;0.00;165000000;0.00,0.00,0.00,0.00,0.00,0.00,1.00,0.00,0.00,0.50,0.50,0.00
                      direct mail melbourne;5;5;0;20;2.72;https://www.precisionmail.com.au/;7.69;6.66;0.64;53500000;0.14,0.71,0.14,0.43,0.14,0.14,0.14,0.57,0.14,1.00,1.00,0.43
                      precision group of companies;91;71;-20;10;0.00;https://www.precisionmail.com.au/;0.00;0.00;0.00;105000000;0.20,0.20,1.00,0.20,0.60,0.40,0.40,0.20,0.80,0.20,0.20,0.20
                      the mail house;13;13;0;70;3.58;https://www.precisionmail.com.au/;0.00;6.66;0.33;7700000000;0.29,1.00,0.14,0.43,0.14,0.14,0.14,0.43,0.14,0.71,0.71,0.43
                      printed xmas cards;36;36;0;10;2.79;https://www.precisionmail.com.au/xmas-cards;0.00;0.00;1.00;16800000;1.00,0.14,0.00,0.00,0.00,0.05,0.05,0.43,0.05,0.14,0.14,0.24
                      precision it;89;47;-42;170;0.00;https://www.precisionmail.com.au/;0.00;0.00;0.00;416000000;0.52,1.00,0.81,0.81,0.67,1.00,0.67,0.81,0.81,1.00,1.00,1.00
                      inkjet printing services;50;50;0;40;2.64;https://www.precisionmail.com.au/print-plastic-wrapping-mail;0.00;0.00;0.60;41400000;0.14,0.43,0.14,1.00,0.14,0.14,0.14,1.00,0.14,0.43,0.43,0.71
                      product distribution;25;25;0;210;2.89;https://www.precisionmail.com.au/blog3;0.00;0.00;0.12;1220000000;0.36,0.44,0.44,0.54,0.67,1.00,0.67,0.44,0.67,0.54,0.67,0.54
                      plastic storage envelopes;35;35;0;70;0.00;https://www.precisionmail.com.au/print-plastic-wrapping-mail;0.00;0.00;1.00;26600000;0.00,1.00,0.00,0.50,0.50,0.50,0.00,0.50,0.00,0.50,1.00,0.50
                      house mailbox;40;40;0;40;0.53;https://www.precisionmail.com.au/;0.00;0.00;0.82;116000000;0.33,0.33,0.33,0.33,0.67,1.00,0.67,0.33,0.67,0.33,0.33,0.33
                      direct mail management;24;24;0;70;0.00;https://www.precisionmail.com.au/print-management;0.00;0.00;0.57;1200000000;0.20,0.20,0.40,0.20,0.20,1.00,0.20,0.20,0.20,0.20,0.20,0.20
                      house print;43;43;0;10;0.52;https://www.precisionmail.com.au/;0.00;0.00;0.37;4560000000;1.00,0.50,0.25,0.25,0.25,0.25,0.25,0.25,0.25,0.50,0.50,0.25
                      image deduplication software;24;24;0;90;0.00;https://www.precisionmail.com.au/print-plastic-wrapping-mail;0.00;0.00;0.21;499000;0.00,1.00,1.00,1.00,0.00,0.00,0.00,0.00,0.00,1.00,0.00,1.00
                      print your own xmas cards;52;52;0;90;2.74;https://www.precisionmail.com.au/xmas-cards;0.00;0.00;1.00;20900000;1.00,0.33,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.33,0.33,0.67
                      shrink wrap service;83;83;0;10;2.41;https://www.precisionmail.com.au/print-plastic-wrapping-mail;0.00;0.00;0.48;29100000;1.00,0.22,0.11,0.11,0.11,0.11,0.11,0.11,0.11,0.22,0.22,0.11
                      direct mail services;67;67;0;10;5.82;https://www.precisionmail.com.au/print-plastic-wrapping-mail;0.00;0.00;0.58;2620000000;0.16,0.03,0.03,0.81,0.03,0.44,0.03,0.03,0.44,0.06,1.00,0.03
                      intelligen;78;78;0;170;0.00;https://www.precisionmail.com.au/resources-directmail-precision;0.00;0.00;0.00;1390000;0.66,0.54,0.29,1.00,0.36,0.12,0.05,0.07,0.05,0.03,0.03,0.05
                      personalised xmas cards;66;66;0;10;2.75;https://www.precisionmail.com.au/xmas-cards;0.00;0.00;1.00;7170000;0.36,0.00,0.00,0.15,0.02,0.00,0.02,0.12,0.02,0.07,0.05,1.00
                      black plastic wrap;40;40;0;10;0.79;https://www.precisionmail.com.au/print-plastic-wrapping-mail;0.00;0.00;0.92;316000000;0.33,0.33,0.33,0.33,0.67,1.00,0.67,0.33,0.33,0.33,0.33,0.33
                      describe palm trees;22;22;0;70;0.00;https://www.precisionmail.com.au/xmas-cards?lightbox=dataItem-j9z3vb8f1;0.00;0.00;0.00;18400000;0.50,0.50,0.50,0.50,1.00,1.00,1.00,0.50,1.00,0.50,0.50,0.50
                      beach christmas cards;78;78;0;20;1.92;https://www.precisionmail.com.au/xmas-cards?lightbox=dataItem-j9z3vb8e1;0.00;0.00;0.99;191000000;1.00,0.00,0.00,0.00,0.01,0.01,0.00,0.01,0.01,0.01,0.03,0.01
                      xmas signatures;17;17;0;90;0.00;https://www.precisionmail.com.au/xmas-cards;0.00;0.00;0.19;2310000;1.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1.00,1.00
                      print management;80;80;0;140;5.50;https://www.precisionmail.com.au/print-management;0.00;0.00;0.46;3360000000;0.43,0.67,0.67,0.81,0.52,1.00,0.67,0.67,0.67,0.43,0.52,0.52
                      direct mail;44;44;0;320;5.66;https://www.precisionmail.com.au/;0.00;0.00;0.39;200000000;0.44,0.67,0.82,1.00,0.82,1.00,0.82,0.67,0.82,1.00,1.00,0.82
                      email xmas cards;95;95;0;10;0.89;https://www.precisionmail.com.au/xmas-cards?lightbox=dataItem-j9z3vb8e1;0.00;0.00;0.73;34600000;1.00,0.09,0.18,0.00,0.00,0.00,0.00,0.00,0.00,0.09,0.09,0.09
                      precision storage;44;0;-44;90;0.00;https://www.precisionmail.com.au/resources-directmail-precision;0.00;0.00;0.00;397000000;0.00,0.50,1.00,0.50,0.00,0.00,0.50,0.50,0.50,0.50,0.00,0.50
                      direct mail australia;30;30;0;10;7.60;https://www.precisionmail.com.au/;0.00;0.00;0.59;436000000;0.11,0.11,1.00,0.11,0.44,0.22,0.33,0.11,0.33,0.11,0.11,0.11
                      best souvlaki;81;81;0;10;0.00;https://www.precisionmail.com.au/xmas-cards?lightbox=dataItem-j9z3y8fb1;0.00;0.00;0.01;13100000;0.25,0.25,1.00,0.25,0.50,0.25,0.75,0.25,0.75,0.25,0.25,0.25
                      xmas palm tree;98;98;0;70;0.40;https://www.precisionmail.com.au/xmas-cards?lightbox=dataItem-j9z3vb8f1;0.00;0.00;0.46;13200000;0.33,0.33,0.33,0.00,0.00,1.00,0.00,0.33,0.67,0.33,0.33,0.33
                      melbourne souvlaki;85;76;-9;10;0.00;https://www.precisionmail.com.au/xmas-cards?lightbox=dataItem-j9z3y8fb1;0.00;0.00;0.02;414000;0.22,1.00,0.11,0.22,0.11,0.11,0.11,0.33,0.11,0.56,0.44,0.33
                      home mail;61;61;0;390;0.22;https://www.precisionmail.com.au/;0.00;0.00;0.01;1210000000;0.81,0.81,0.81,1.00,0.67,0.81,0.81,0.67,0.81,0.67,0.67,0.81
                      warehousing of shampoo;86;86;0;90;0.00;https://www.precisionmail.com.au/servicesub2;0.00;0.00;0.00;1150000;1.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00
                      postal envelope;39;39;0;110;0.48;https://www.precisionmail.com.au/print-plastic-wrapping-mail;0.00;0.00;0.46;35100000;0.50,0.64,0.64,0.79,0.64,1.00,0.79,0.79,0.64,0.79,0.79,0.64
                      digital postal mail;21;21;0;70;0.00;https://www.precisionmail.com.au/print-plastic-wrapping-mail;0.00;0.00;0.00;108000000;0.00,0.50,0.00,0.00,0.00,1.00,0.00,0.00,0.00,0.00,0.50,0.00
                      mail house brisbane;12;12;0;10;2.69;https://www.precisionmail.com.au/;0.00;0.00;0.50;40300000;0.14,0.29,0.14,1.00,0.14,0.14,0.14,1.00,0.14,0.57,0.57,0.71
                      document scanning melbourne;81;81;0;10;1.85;https://www.precisionmail.com.au/mail-house-precision-mail;0.00;0.00;0.41;1800000;0.20,0.20,0.40,0.20,0.20,1.00,0.20,0.20,0.20,0.20,0.20,0.20
                      variable data direct mail;86;86;0;90;0.00;https://www.precisionmail.com.au/print-plastic-wrapping-mail;0.00;0.00;0.00;212000000;0.00,0.00,0.00,0.00,0.00,0.00,0.00,1.00,0.00,0.00,0.00,0.00
                      precision ink;76;76;0;10;0.00;https://www.precisionmail.com.au/servicesub3;0.00;0.00;0.15;105000000;1.00,0.33,0.33,0.67,0.33,0.33,0.33,0.67,0.33,0.33,0.33,0.67
                      direct mail sydney;19;19;0;20;6.13;https://www.precisionmail.com.au/;0.00;0.00;0.45;54300000;1.00,0.33,0.33,0.67,0.33,0.33,0.33,0.67,0.33,0.33,0.33,0.33
                      mountain christmas cards;37;37;0;70;0.00;https://www.precisionmail.com.au/xmas-cards?lightbox=dataItem-j9z3vb8e1;0.00;0.00;0.14;103000000;0.14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1.00
                      shrink wrap melbourne;84;84;0;40;2.41;https://www.precisionmail.com.au/print-plastic-wrapping-mail;0.00;0.00;0.97;724000;0.20,0.20,0.40,0.20,0.20,1.00,0.20,0.20,0.20,0.20,0.20,0.20
                      mailhouse;39;39;0;210;5.77;https://www.precisionmail.com.au/;0.00;0.00;0.47;568000;0.65,0.54,1.00,1.00,0.65,1.00,1.00,0.81,0.81,1.00,1.00,0.65';
   
    
     return $this->parseDataIntoArray($dataString);

  }


  function getTopAnchors( $domain )
  {

    $apiKey  = env('SEMRUSH_API_KEY');
     $url     = 'https://api.semrush.com/analytics/v1/?key='.$apiKey.'&target='.$domain.'&type=backlinks_anchors&target_type=root_domain&display_limit=10';

           
   //  $dataString  = $this->callAPI('GET', $url, false);

     $dataString  =  'anchor;domains_num;backlinks_num;first_seen;last_seen
                      <EmptyAnchor>;8;100;1526789102;1560865296
                      left;1;85;1496059286;1561358777
                      <EmptyAnchor>;4;12;1497938055;1561359677
                      http://www.precisionmail.com.au;3;6;1486980671;1561264574
                      www.precisionmail.com.au;2;4;1556972722;1561156243
                      website;1;2;1550415195;1559516701
                      precision mail;2;2;1511952697;1561268050
                      precisionmail.com.au;1;1;1539737910;1545713740
                      precision;1;1;1528926846;1550988399
                      precision group;1;1;1549117898;1561135413';
   
    
    return $this->parseDataIntoArray($dataString);

  }


  //REFERRING DOMAINS -- this is need to check-----------------

  function getReferingDomains( $domain )
  {

    $apiKey  = env('SEMRUSH_API_KEY');
     $url     = 'https://api.semrush.com/analytics/v1/?key='.$apiKey.'&target='.$domain.'&type=backlinks_refdomains&target_type=root_domain';

           https://api.semrush.com/analytics/v1/?key='.$apiKey.'&type=backlinks_matrix&targets[]='.$domain.'&targets[]=amazon.com&target_types[]=root_domain&target_types[]=root_domain&display_limit=10
   //  $dataString  = $this->callAPI('GET', $url, false);

     $dataString  =  'domain;backlinks_num;domain_score;domain_trust_score;first_seen;last_seen;ip;country
ndadarts.org.au;172;1;2;1496059286;1561358777;67.225.158.88;us
datatools.com.au;6;17;19;1501420630;1561341528;104.154.24.170;us
rspcavic.org;6;31;39;1535000636;1561359677;101.0.104.155;au
eruditetechnologies.com.au;6;16;20;1526789102;1561156243;103.11.53.197;au
dlook.com.au;4;37;34;1526789102;1561264574;168.1.15.187;au
melbournebd.com.au;4;13;11;1486980671;1560865296;55.51.218.60;us
yellowbook.com.au;4;10;8;1516426001;1560865296;55.51.218.60;us
millionpawswalk.com.au;3;24;36;1497938055;1557709542;52.62.108.178;au
strokefoundation.org.au;3;30;39;1511952697;1561135413;61.14.101.67;au
scimitar.com.au;2;1;0;1526789102;1561268050;54.252.148.183;au';
   
    
    return $this->parseDataIntoArray($dataString);

  }


  //Index backlinks

  function getIndexBacklink( $domain )
  {
    
    $apiKey  = env('SEMRUSH_API_KEY');
     $url     = 'https://api.semrush.com/analytics/v1/?key='.$apiKey.'&target='.$domain.'&type=backlinks_pages&target_type=root_domain&display_limit=10';

          
   //  $dataString  = $this->callAPI('GET', $url, false);

     $dataString  =  'source_url;source_title;response_code;backlinks_num;domains_num;external_num;internal_num;last_seen
http://www.precisionmail.com.au/;;301;101;10;0;0;1560865296
http://precisionmail.com.au/;;301;1;1;0;0;1544805553
https://precisionmail.com.au/;;301;0;0;0;0;1545647059';
   
    
    return $this->parseDataIntoArray($dataString);

  }



  function getMainCompetitors($domain)
  {

    $apiKey  = env('SEMRUSH_API_KEY');
     $url     = 'https://api.semrush.com/?type=domain_organic_organic&key='.$apiKey.'&display_limit=10&domain='.$domain.'&database=au';

       
   //  $dataString  = $this->callAPI('GET', $url, false);

     $dataString  =  'Domain;Competitor Relevance;Common Keywords;Organic Keywords;Organic Traffic;Organic Cost;Adwords Keywords
directmailservices.com.au;0.40;4;30;37;139;0
bcmail.com.au;0.39;4;68;99;398;0
aomail.com.au;0.35;3;50;11;64;0
familymailhouse.com.au;0.35;4;55;51;97;0
centrica.com.au;0.34;3;31;11;61;0
dmo.com.au;0.30;5;76;74;272;0
mmailm.com.au;0.25;2;98;71;78;0
precisionprinting.co.uk;0.23;1;18;3;4;0
precisionprint.com.au;0.23;1;48;27;5;0
directmm.com.au;0.22;3;86;107;352;0';
   
   
      return $this->parseDataIntoArray($dataString);


  }



  function parseDataIntoArray($dataString)
  {
    $data  = preg_split("/\r\n|\n|\r/", $dataString);

     $dataNew    = [];

     $headers    = preg_split("/;/", $data[0]);
     $count      = count($headers);

      foreach ($data  as $key=>$value )  
      {
        if ( $key > 0)
        {
          $dNew  = [];
          $line    = preg_split("/;/", $value);
          for( $i =0; $i<$count;$i++)
          {
            $dNew[$headers[$i]]  = $line[$i];

          }

          $dataNew[]    =  $dNew;

        }

      } 
    
      return $dataNew;

  }


  
}
