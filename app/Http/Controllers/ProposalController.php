<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Client;
use App\Models\Website;
use App\Services\SemrushService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class ProposalController extends Controller {

	 private $semrushService;

     public function __construct( SemrushService $semrushService )
    {
       $this->semrushService  = $semrushService;
    }



    function getClient()
    {
       
       if ( Auth::user()->role == 'admin')
       {
            $client_id = session('client_id');
            $client    = Client::find( $client_id);
       }else
       {
             $client  = Client::find(Auth::user()->client_id);
       }
       return $client;
    }

    
    public function download($projectId)
    {

       $website       = new Website();
       $website1      = $website->websiteBySemProId($projectId);
       
       $semrushInfo   = $this->semrushService->find($projectId, 'info');
       $response      = json_decode($semrushInfo->response, true);

   

       $totalScore    = $response['current_snapshot']['quality']['value'];
       $totalScoreSub = 100-$totalScore;

       $semrushsnapshots   = $this->semrushService->find($projectId, 'snapshots');
       $snapshots_response = json_decode($semrushsnapshots->response, true);

         $errorInfo    = [];
        $warnInfo     = [];
        $noticeInfo   = [];

        $errorInfo[] = ['Date','Issues'];
        $warnInfo[] = ['Date','Issues'];
        $noticeInfo[] = ['Date','Issues'];
       
        $s = 0;
       


        foreach (  $snapshots_response['snapshots'] as $snapshot)
        {

             $semrushsnapshotInfo = $this->semrushService->find($projectId, $snapshot['snapshot_id']);
             $response_snapinfo    = json_decode($semrushsnapshotInfo->response, true);

              

              $snaperrorcount = 0;
              $snapwarncount  = 0;
              $snapnoticecount  = 0;


              foreach(  $response_snapinfo['errors'] as $snaperror)
              {
                  $snaperrorcount = $snaperrorcount+ $snaperror['count'];
              } 
              $errordates = date('Y-m-d', (int)$response_snapinfo['finish_date']/1000);

              $errorInfo[] = [$errordates, (int)$snaperrorcount];

              foreach(  $response_snapinfo['warnings'] as $snapwarn)
              {
                  $snapwarncount = $snapwarncount+ $snapwarn['count'];
              } 
              $warndates = date('Y-m-d', $response_snapinfo['finish_date']/1000);
              
              $warnInfo[] = [$warndates, (int)$snapwarncount];

              foreach(  $response_snapinfo['notices'] as $snapnotice)
              {
                  $snapnoticecount = $snapnoticecount+ $snapnotice['count'];
              } 
              $noticedates   = date('Y-m-d', $response_snapinfo['finish_date']/1000);
              $noticeInfo[]  = [$noticedates, (int)$snapnoticecount];

                $s = $s+1;

          }

          $errorInfoData    = json_encode($errorInfo);
          $warnInfoData     = json_encode($warnInfo); 
          $noticeInfoData   = json_encode($noticeInfo);

          $errorcount = [];
  
       $j = 0;

       $errorInfoDe =[];
       $warnInfoDe =[];
       $noticeInfoDe =[];

      foreach ($response['current_snapshot']['topIssues'] as $issueId)
       {
          foreach ( $response['current_snapshot']['errors'] as $error)
          {
               $errorInfoDe[$error['id']] = $error['count'];
          }

           foreach ( $response['current_snapshot']['warnings'] as $warn)
          {
               $warnInfoDe[$warn['id']] = $warn['count'];
          }

           foreach ( $response['current_snapshot']['notices'] as $notices)
          {
               $noticeInfoDe[$notices['id']] = $notices['count'];
          }
        }



        $semrushIssuesInfo = $this->semrushService->find($projectId, 'issues');
      $response_issue_info    = json_decode($semrushIssuesInfo->response, true);

      $issueErrorInfoAll  = [];

       $issueInfoWarnAll  = [];

        $issueInfoNoticAll  = [];

        $count = 0;

       
          foreach ( $response_issue_info['issues'] as $issueDetails)
          {
            if (array_key_exists($issueDetails['id'],$errorInfoDe))
            {

              $key = $issueDetails['id'];
               if( $errorInfoDe[$key]>0){

                   $issueInfo = [];
                   $issueInfo['title'] =$issueDetails['title'];
            
                   $issueInfo['title_page'] =str_replace("##count##",$errorInfoDe[$key],$issueDetails['title_page']);
             
                   $issueErrorInfoAll[] = $issueInfo;

               }


            


             
            }

            elseif (array_key_exists($issueDetails['id'],$warnInfoDe))
            {

                $key = $issueDetails['id'];
               if( $warnInfoDe[$key]>0){

                   $issueInfo = [];
                   $issueInfo['title'] =$issueDetails['title'];
            
                   $issueInfo['title_page'] =str_replace("##count##",$warnInfoDe[$key],$issueDetails['title_page']);
             
                   $issueInfoWarnAll[] = $issueInfo;

               }
           

            }

             elseif (array_key_exists($issueDetails['id'],$noticeInfoDe))
            {

                $key = $issueDetails['id'];
               if( $noticeInfoDe[$key]>0){

                   $issueInfo = [];
                   $issueInfo['title'] =$issueDetails['title'];
            
                   $issueInfo['title_page'] =str_replace("##count##",$noticeInfoDe[$key],$issueDetails['title_page']);
             
                   $issueInfoNoticAll[] = $issueInfo;

              }
           

            }
          
          }

     /*  $totalErrorCount = 0;

      foreach ($response['current_snapshot']['topIssues'] as $issueId)
      {

       foreach ( $response['current_snapshot']['errors'] as $error)
       {
          $totalErrorCount = $totalErrorCount+$error['count'];
       }

     }*/


        $fileName = "proposal-".$projectId.".pdf";
        \PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif','isHtml5ParserEnabled'=>true]); 
        $pdf = \PDF::loadView('pdf.marketing-proposal',compact('totalScore','snaperrorcount','snapwarncount','snapnoticecount','issueErrorInfoAll','issueInfoWarnAll','issueInfoNoticAll'))
            ->setPaper(array(0, 0, 595, 841), 'portrait');
       
      return $pdf->download($fileName);

    }
    

  
}
