<?php

namespace App\Http\Controllers;

use Redirect;

use App\Constants\Terms;
use App\Constants\InvoiceStatus;
use App\Events\CreditNoteAdded;
use App\Events\CreditNoteEdited;
use App\Events\InvoiceEdited;
use App\Events\InvoiceGenerated;
//use App\Foundation\Http\Request;
use App\Http\Requests\CreditNoteRequest;
use App\Http\Requests\InvoiceRequest;
use App\Models\CreditNote;
use App\Models\Client;
use App\Models\Invoice;
use App\Models\RecieptEmail;
use App\Models\Subscription;
use App\Models\Card;
use App\Models\Logger;
use App\Mail\InvoiceFromJPLMS;
use App\Services\InvoiceManager;
use App\Services\CardService;
use Barryvdh\DomPDF\PDF;
use Facades\App\Foundation\Modal\Modal;
use Facades\App\Foundation\Modal\ModalMessage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;


class InvoiceController extends Controller
{
    /**
     * @var InvoiceManager
     */
    private $invoiceManager;

      /**
     * @var CardService
     */
    private $cardService;

    public function __construct(InvoiceManager $invoiceManager, CardService $cardService)
    {

        $this->invoiceManager = $invoiceManager;
        $this->cardService = $cardService;
    }
    public function lists()
    {
    	
        $invoices = $this->invoiceManager->lists();
     
        return view('pages.invoice_list')
            ->with(compact('invoices'));
    }

    

    public function view($id)
    {
        $invoice = $this->invoiceManager->find($id);
        return view('pages.invoice_view')
            ->with(compact('invoice'));
    }

    public function download($id)
    {
        $invoice = $this->invoiceManager->find($id);
        $fileName = "invoice-".$invoice->no.".pdf";
        \PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif','isHtml5ParserEnabled'=>true]); 
        $pdf = \PDF::loadView('pdf.invoice', compact('invoice'))
            ->setPaper(array(0, 0, 595, 841), 'portrait');

        
        return $pdf->download($fileName);
    }

    public function info($id)
    {

        $invoice = $this->invoiceManager->find($id);
        return view('pages.invoice_payinfo')
            ->with(compact('invoice'));

    }


    public function subscriptionInfo()
    {
        $client = $this->invoiceManager->getClient();
        //$subscriptions = $client->subscriptions()->get(); 
        $recieptEmails = $client->reciptEmails()->get();
         $paymentMethods = $client->cards()->get();

        /*foreach ( $subscriptions as $subscription)
        {
            dd($subscription->package);
           // dd($subscription->salesuser->name);
        }*/
        
        if ($client->has_subscribed == 1)
        {

            $subscriptionPackage = $this->invoiceManager->subscriptionInfo($client->subs_package_id);

            $subscriptions = $client->subscriptions()->get();
        }else
        {
              $subscriptionPackage = null;
              $subscriptions = null;
        }
        return view('pages.client_subscription_info')
            ->with(compact('subscriptionPackage','client','recieptEmails','paymentMethods','subscriptions'));

    }

    public function saveRecieptEmail(Request $request)
    {
         $attributes = $request->all();

         $recieptEmail = new RecieptEmail();
         $recieptEmail->create($attributes);

         return redirect('invoice/subscription');
     

    }

      public function editRecieptEmail($id)
    {

         $line = RecieptEmail::find($id);

      //  $invoice = $this->invoiceManager->find($id);
        return Modal::create()
            ->title("Edit Reciept Email")
            ->template('modal.forms.recieptemail-edit')
            ->buttons(["Delete", "Cancel"])
            ->variables(compact('line'))
            ->render();
    }

     public function updateRecieptEmail(Request $request, $id)
    {
        $line = RecieptEmail::find($id);
        $line->update($request->all());

        return ModalMessage::success("Email successfully deleted!");
    }


     public function deleteRecieptEmail($id)
    {

         $line = RecieptEmail::find($id);

      //  $invoice = $this->invoiceManager->find($id);
        return Modal::create()
            ->title("Delete Reciept Email")
            ->template('modal.forms.recieptemail-delete')
            ->buttons(["Delete", "Cancel"])
            ->variables(compact('line'))
            ->render();
    }

      public function remove($id)
    {
        $line = RecieptEmail::find($id);
        $line->forceDelete();

        return ModalMessage::success("Email successfully deleted!");
    }



 
    public function updateAutoPay($id,$request)
    {
          $client = Client::find($id);
          $client->invoice_autopay= $request;
          $client->save();
          //if client auto_pay invoices = off, update all pending invoices auto_pay off
          //if client auto_pay invoices = on, update all pending invoices auto_pay on
          $invoices = $client->invoices->where('status','UNPAID');

          foreach($invoices as $invoice)
          {
            $invoice->auto_pay = $request;
            $invoice->save();
          }
          
       

    }

 /*public function nonSubscriptionPayments($id)
    {

        $invoice = $this->invoiceManager->find($id);
         $client = $this->invoiceManager->getClient();
        $recieptEmails = $client->reciptEmails()->get();
         $paymentMethods = $client->cards()->get();
        return view('pages.non_subscription_invoice_payinfo')
            ->with(compact('invoice','recieptEmails','paymentMethods'));
    }*/


    public function nonSubscriptionPayments($id)
    {
        $client = $this->invoiceManager->getClient();
 
        $invoice = $this->invoiceManager->find($id);

        if ( $invoice == null)
            return Redirect::route('invoice_list');
        if ( $invoice->client_id !== $client->id ||  $invoice->status == 'PAID') 
          return Redirect::route('invoice_list');

        //$client = $this->invoiceManager->getClient();
        $recieptEmails = $client->reciptEmails()->get();
        $paymentMethods = $client->cards()->get();
        return view('pages.non_subscription_invoice_paymentinfo')
            ->with(compact('invoice','recieptEmails','paymentMethods','client'));
    }



    //save card info in db
     public function show()
    {
         $client = $this->invoiceManager->getClient();
        return Modal::create()
            ->title("Add an access")
            ->template('modal.forms.payment-methods')
            ->buttons(["Create", "Cancel"])
            ->variables(compact('client'))
            ->render();
    }

	public function savePaymentMethod(Request $request)
    {
         $attributes = $request->all();
          $attributes['set_default'] = isset($request['set_default'])?1:0;
          $attributes['card_number'] =  $request['card_number'];//Hash::make($request['card_number'], ['rounds' => 12]);
         //  $request->session()->forget('card_info'); 
         // $request->session()->push('card_info',$attributes);


         $card = new Card();
         $card->create($attributes);
        return ModalMessage::success("Payment method successfully saved!");
    }


    public function editPaymentMethod($id)
    {
        $method = $this->cardService->find($id);
        $client = $this->invoiceManager->getClient();
        return Modal::create()
            ->title("Edit an Payment Method")
            ->template('modal.forms.payment-method-edit')
            ->variables(compact('method','client'))
            ->buttons(["Edit", "Cancel"])
            ->render();
    }

    public function updatePaymentMethod(Request $request)
    {

        $this->cardService->update($request->all());
        return ModalMessage::success("Payment method successfully updated!");
    }

    public function deletePaymentMethod( $id )
    {
        $card = $this->cardService->find($id);
        return Modal::create()
            ->title("Delete Payment Methods")
            ->template('modal.forms.card-delete')
            ->buttons(["Delete", "Cancel"])
            ->variables(compact('card'))
            ->render();
    }


     public function updateDefultPaymentMethod($id)
    {
        $data = [];
        $data['set_default'] = 1;
        $data['id'] = $id;
        $this->cardService->update($data);

        Card::where('id', '!=', $id)->update(['set_default'=>0]);

         return back()->with('message','Default payment method successfully updated!');

     
    }



    public function removePaymentMethod($id)
    {
         $this->cardService->destroy($id);

        return ModalMessage::success("Payment method deleted successfully deleted!");
    }


    public function updateAutoPayInvoice($id,$request)
    {
          $invoice = Invoice::find($id);
          $invoice->auto_pay = $request;
          $invoice->save();
     }




}
