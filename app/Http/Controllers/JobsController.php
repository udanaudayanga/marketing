<?php

namespace App\Http\Controllers;

use App\Constants\Priority;
use App\Constants\JobTypes;
use App\Events\JobCreated;
use App\Foundation\Modal\Modal;
use App\Http\Requests\JobRequest;
use App\Mail\NewTaskAssignedForYou;
use App\Services\ClientService;
use App\Services\ProjectService;
use App\Services\JobService;
use App\Services\UserService;
use Facades\App\Foundation\Modal\ModalMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class JobsController extends Controller
{
    /**
     * @var JobService
     */
    private $jobService;
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var ProjectService
     */
    private $projectService;
    /**
     * @var ClientService
     */
    private $clientService;
  

    public function __construct(JobService $jobService, UserService $userService, ProjectService $projectService, ClientService $clientService)
    {
        $this->jobService = $jobService;
        $this->userService = $userService;
        $this->projectService = $projectService;
        $this->clientService = $clientService;
     }

    public function show()
    {
        $employees = $this->userService->employeesList();
        $priorities = constants_to_array(Priority::class);
        $types = constants_to_array(JobTypes::class);
        $clients = $this->clientService->lists();

        return Modal::create()
            ->title("Add New Jobs")
            ->template('modal.forms.job')
            ->buttons(["Create", "Cancel"])
            ->variables(compact('employees', 'types', 'priorities', 'clients'))
            ->render();
    }

    public function create(JobRequest $request)
    {
       
        $job = $this->jobService->create(
            $request->all()
        );
        event(new JobCreated($job));
        //event(new getTask());
        return ModalMessage::success("Job successfully created!");
    }

    public function edit($id)
    {

        $employees = $this->userService->employeesList();
        $priorities = constants_to_array(Priority::class);
        $types = constants_to_array(JobTypes::class);
        $clients = $this->clientService->lists();
        $job = $this->jobService->find($id);


        $job->assignees = current($job->assignees->pluck('id'));

      

        if ($job->type != 'sub'){
        return Modal::create()
            ->title("Add New Job")
            ->template('modal.forms.job_edit')
            ->buttons(["Create", "Cancel"])
            ->variables(compact('employees', 'types', 'priorities', 'clients', 'job'))
            ->render();
        }else {
             return Modal::create()
            ->title("Add New Job")
            ->template('modal.forms.task_edit')
            ->buttons(["Create", "Cancel"])
            ->variables(compact('employees', 'types', 'priorities', 'clients', 'job'))
            ->render();
        }
    }

    public function update(JobRequest $request, $id)
    {
        $data  =  $request->all();

        $this->jobService->update($id,
           $data
        );
        return ModalMessage::success("Tasks successfully updated!");
    }

    public function lists()
    {
        $jobs =  $this->jobService->all();

        return view('pages.jobs')
            ->with(compact('jobs'));
    }

    public function view($category, $id)
    {
        $job = $this->jobService->findFullDetails($id);
         
         
        return view('pages.job_view')
            ->with(compact('employees','priorities','types', 'job', 'category'));
    }

    public function logWork(Request $request)
    {
        return [
            'total' => $this->jobService->logTime(
                $request->all()
            )
        ];
    }

    //delete task
    public function delete($id)
    {
         $job = $this->jobService->find($id);
        return Modal::create()
            ->title("Delete Task")
            ->template('modal.forms.task-delete')
            ->buttons(["Delete", "Cancel"])
            ->variables(compact('job'))
            ->render();
    }

    public function remove($id)
    {
         $this->jobService->remove($id);

        return ModalMessage::success("Task successfully deleted!");
    }

    public function saveComment( Request $request,$id)
    {
        $this->jobService->saveComment( $id,
                $request->all());
        if(user()->isAdmin())
        return redirect('jobs/all/'.$id);
    else
        return redirect('jobs/my/'.$id)->with('message', 'Message sent!');;
     // return ModalMessage::success("Task successfully deleted!");
    }

    
}
