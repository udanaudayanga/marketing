<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use URL;
use Session;
use Redirect;
use Input;

use App\Services\InvoiceManager;
use App\Constants\InvoiceStatus;


use PayPal\Rest\ApiContext;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use App\Mail\InvoiceReceipt;
use Facades\App\Foundation\Modal\Modal;
use Facades\App\Foundation\Modal\ModalMessage;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{
	    /**
     * @var InvoiceManager
     */
    private $invoiceManager;

    //
    public function __construct(InvoiceManager $invoiceManager)
    {
/** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

         $this->invoiceManager = $invoiceManager;
}


	public function payWithpaypal(Request $request)
    {
    		$invoice = $this->invoiceManager->find($request->get('invo_id'));
			$payer = new Payer();
			$payer->setPaymentMethod('paypal');
			
			$item_1 = new Item();
			$item_1->setName($invoice->no) /** item name **/
			            ->setCurrency('USD')
			            ->setQuantity(1)
			            ->setPrice($request->get('amount')); /** unit price **/

			$item_list = new ItemList();
			$item_list->setItems(array($item_1));

			$amount = new Amount();
			$amount->setCurrency('USD')
			       ->setTotal($request->get('amount'));

			$transaction = new Transaction();
			$transaction->setAmount($amount)
			            ->setItemList($item_list)
			            ->setDescription($invoice->no);


			$redirect_urls = new RedirectUrls();
			$redirect_urls->setReturnUrl(URL::route('payment_status',$request->get('invo_id'))) /** Specify return URL **/
			              ->setCancelUrl(URL::route('payment_status',$request->get('invo_id')));
			$payment = new Payment();
			$payment->setIntent('Sale')
			            ->setPayer($payer)
			            ->setRedirectUrls($redirect_urls)
			            ->setTransactions(array($transaction));

			        // dd($payment->create($this->_api_context));exit;
		    try {
				$payment->create($this->_api_context);
			} catch (\PayPal\Exception\PPConnectionException $ex) {
				if (\Config::get('app.debug')) {
					\Session::put('error', 'Connection timeout');
				    return Redirect::route('non_subscribe_payments',$request->get('invo_id'));
				} else {
					\Session::put('error', 'Some error occur, sorry for inconvenient');
				    return Redirect::route('non_subscribe_payments',$request->get('invo_id'));
				}
			}
			foreach ($payment->getLinks() as $link) {
				if ($link->getRel() == 'approval_url') {
					$redirect_url = $link->getHref();
			    break;
			}
		}
			/** add payment ID to session **/
			Session::put('paypal_payment_id', $payment->getId());
			

			if (isset($redirect_url)) {
			/** redirect to paypal **/
			   return Redirect::away($redirect_url);
			}
			\Session::put('error', 'Unknown error occurred');
			return Redirect::route('non_subscribe_payments',$request->get('invo_id'));
}

	public function getPaymentStatus($id)
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
         $invo_id = Session::get('invo_id');

         if ( $payment_id == null)
         {
         	\Session::put('error', 'Payment failed');
		return Redirect::route('invoice_list');

	    }

		/** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
			\Session::put('error', 'Payment failed');
            return Redirect::route('invoice_list');
		}
		$payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

		/**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
		if ($result->getState() == 'approved') {
		\Session::put('success', 'Payment success');
			$invoice = $this->invoiceManager->find($id);
			$invoice->transaction_id = $payment_id;
			$invoice->status = InvoiceStatus::PAID;
			$invoice->save();

			$this->sendinvoicemail($id);
		    return Redirect::route('invoice_list');
		}
		\Session::put('error', 'Payment failed');
		return Redirect::route('invoice_list');
	}



	public function sendinvoicemail($id)
    {
        $invoice = $this->invoiceManager->find($id);
        $client = $this->invoiceManager->getClient();

        $mails  = $client->reciptEmails()->get();
       
        $toMails = [];

        foreach ( $mails as $mail)
        {
        	$toMails[] = $mail->email; 
        }
       
        $fileName = "receipt-".$invoice->no.".pdf";
        \PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif','isHtml5ParserEnabled'=>true]); 
        $pdf = \PDF::loadView('pdf.receipt', compact('invoice'))
            ->setPaper(array(0, 0, 595, 841), 'portrait');
       
        $pdf->save('./receipts/'.$fileName);

        Mail::to($toMails)
        
           ->bcc('james@jplms.com.au')
           ->send(new InvoiceReceipt($invoice));

        return ModalMessage::success("Invoice detail emailed successfully!");
        
     
       
    }



	
}
