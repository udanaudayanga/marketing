<?php

namespace App\Http\Controllers;

use App\Constants\Priority;
use App\Http\Requests\TaskRequest;
use App\Services\JobService;
use App\Services\TaskService;
use App\Services\UserService;
use App\Services\ClientService;
use Facades\App\Foundation\Modal\Modal;
use Facades\App\Foundation\Modal\ModalMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TasksController extends Controller
{
    /**
     * @var JobService
     */
    private $jobService;
    /**
     * @var TaskService
     */
    private $taskService;
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var ClientService
     */
      private $clientService;

    public function __construct(JobService $jobService, TaskService $taskService, UserService $userService, ClientService $clientService)
    {
        $this->jobService = $jobService;
        $this->taskService = $taskService;
        $this->userService = $userService;
        $this->clientService = $clientService;
    }

    public function show($category)
    {
        $filters['client'] = Input::get('client');
        $jobs =  $this->jobService->get($category, $filters);
        $employees = $this->userService->employeesList();
        $priorities = constants_to_array(Priority::class);
        $clients    = $this->clientService->lists();
        return Modal::create()
            ->title("Add New Task")
            ->template('modal.forms.task')
            ->buttons(["Create", "Cancel"])
            ->variables(compact('jobs','employees', 'priorities','clients'))
            ->render();
    }

    public function create(TaskRequest $request)
    {
        $attributes = $request->all();
        $this->jobService->createSubTasks($attributes);
        return ModalMessage::success("Task successfully created!");
    }

    public function view($id)
    {
        $job = $this->jobService->findFullDetails($id);
        $priorities = constants_to_array(Priority::class);
        $category = 'n/a';
        return view('pages.sub_task_view')
            ->with(compact( 'job','category', 'priorities'));
    }

   
}
