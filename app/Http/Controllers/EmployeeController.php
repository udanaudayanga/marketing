<?php

namespace App\Http\Controllers;


use App\Constants\UserRoles;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\EmployeeRegistered;
use App\Http\Requests\EmployeeEditRequest;
use App\Http\Requests\EmployeeRequest;
use App\Models\User;
use App\Services\EmployeeSerivce;
use App\Services\UserService;
use Facades\App\Foundation\Modal\Modal;
use Facades\App\Foundation\Modal\ModalMessage;

class EmployeeController extends Controller
{

    private $userService;

    public function __construct(UserService $service)
    {
        $this->userService = $service;
    }


     function getClient()
    {
       //$client  = Client::find(Auth::user()->client_id);
       if ( Auth::user()->role == 'admin')
       {
            $client_id = session('client_id');
            $client    = Client::find( $client_id);
       }else
       {
             $client  = Client::find(Auth::user()->client_id);
       }
       return $client;
    }


    public function show()
    {
        $roles = constants_to_array(UserRoles::class);
        return Modal::create()
            ->title("Add New Task")
            ->template('modal.forms.employee')
            ->buttons(["Create", "Cancel"])
            ->variables(compact('roles'))
            ->render();
    }

    public function edit($id)
    {
        $roles = constants_to_array(UserRoles::class);
        $user = $this->userService->find($id);
        return Modal::create()
            ->title("Edit Employees")
            ->template('modal.forms.employees-edit')
            ->buttons(["Save", "Cancel"])
            ->variables(compact('user','roles'))
            ->render();
    }

    public function delete($id)
    {
        $user = $this->userService->find($id);
        return Modal::create()
            ->title("Delete Employees")
            ->template('modal.forms.employees-delete')
            ->buttons(["Delete", "Cancel"])
            ->variables(compact('user'))
            ->render();
    }

    public function remove($id)
    {
         $this->userService->remove($id);

        return ModalMessage::success("Employee successfully deleted!");
    }

    public function update(EmployeeEditRequest $request, $id)
    {
        // dd( $request->all() );
        $this->userService->update($id, $request->all());

        return ModalMessage::success("Employee successfully updated!");
    }

    public function lists()
    {
        $client = $this->getClient();
        $employees = $this->userService->employees($client->id);
        return view('pages.employee_list')
            ->with(compact('employees'));
    }

    public function create(EmployeeRequest $request)
    {
         if ( Auth::user()->role == 'admin')
       {
            $client_id = session('client_id');
       }else
       {
             $client_id  = Auth::user()->client_id;
       }
        $data               = $request->all();
        $data['role']       = UserRoles::CLIENT;
        $data['client_id'] = $client_id;
        $data['pin']        = $this->userService->generateUniquePin();
        $user               = $this->userService->create($data);
        event(new EmployeeRegistered($user, $data['pin']));
        return ModalMessage::success("Employee successfully created!");
    }
}
