<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Analytics;
use Spatie\Analytics\Period;
use Illuminate\Http\Response;

class GoogleAnalyticsController extends Controller {

    private $reports = array('visits','visitsByDevice','topViewedPages');

    /**
     *
     */
    public function index() {

        $user_id = Auth::id();

        /* Get data from client */
        $client = Client::find($user_id);

        // config(['app.analytics_view_id' => '146642296']);
        Analytics::setViewId('138658544');

        //return response()->json($result);

        return view('google_analytics.dashboard',['session_data' => '']);
    }

    public function ajaxInitial() {

        $return = array(
            'status' => false,
            'data' => '',
            'message' => ''
        );

        $user_id = Auth::id();
        /* Get data from client */
        $client = Client::find($user_id);

        Analytics::setViewId('138658544');

        foreach ($this->reports as $report) {
            $result = $this->$report();
            if ($result['status']) {
                $return['data'][$report] = $result['data'];
            }
        }
        $return['status'] = true;

        return response()->json($return);
    }

    /**
     * @function
     * @param $period
     * @chart Line Chart
     */
    public function visits($period = 7) {

        $return = array(
            'status' => false,
            'data' => '',
            'message' => ''
        );

        $analyticsData = Analytics::performQuery(
            Period::days($period),
            'ga:visits',
            array(
                'dimensions' => 'ga:date',
                'sort' => '-ga:date'
            )
        );

        $labels = array();
        $data = array();
        $series = array('Users');
        $table = array();

        if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
            $return['status'] = true;
            foreach ($analyticsData['rows'] as $row) {
                $labels[] = date('M-d',strtotime($row[0]));
                $data[] = $row[1];
                $table[] = array(
                    'date' => date('M-d',strtotime($row[0])),
                    'visit_count' => $row[1]
                );
            }
            $return['data'] = array(
                'label' => $labels,
                'series' => $series,
                'data' => array($data),
                'table' => $table
            );
        }

        return $return;
    }

    /**
     * Doughnut Chart
     */
    public function userTypes() {

    }

    /**
     * @param $period
     */
    public function topViewedPages($period = 7) {
        $return = array(
            'status' => false,
            'data' => '',
            'message' => ''
        );

        $analyticsData = Analytics::performQuery(
            Period::days($period),
            'ga:pageviews',
            array(
                'dimensions' => 'ga:pagePath',
                'sort' => '-ga:pageviews'
            )
        );

        $table = array();

        $return['status'] = true;
        $return['data'] = $analyticsData;
        if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
            $return['status'] = true;
            foreach ($analyticsData['rows'] as $row) {
                $table[] = array(
                    'page_path' => $row[0],
                    'view_count' => $row[1]
                );
            }
            $return['data'] = array(
                'table' => $table
            );
        }

        return $return;
    }

    /**
     *
     */
    public function topKeywords() {

    }

    /**
     *
     */
    public function referralWebsites() {

    }

    /**
     *
     */
    public function visitsByDevice($period = 7) {
        $return = array(
            'status' => false,
            'data' => '',
            'message' => ''
        );

        $analyticsData = Analytics::performQuery(
            Period::days($period),
            'ga:visits',
            array(
                'dimensions' => 'ga:deviceCategory'
            )
        );

        $labels = array();
        $data = array();
        $table = array();

        if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
            $return['status'] = true;
            foreach ($analyticsData['rows'] as $row) {
                $labels[] = $row[0];
                $data[] = $row[1];
                $table[] = array(
                    'category' => $row[0],
                    'visits' => $row[1]
                );
            }
            $return['data'] = array(
                'label' => $labels,
                'data' => array($data),
                'table' => $table
            );
        }

        return $return;
    }
}
