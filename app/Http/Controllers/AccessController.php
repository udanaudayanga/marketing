<?php

namespace App\Http\Controllers;

use App\Constants\AccessTypes;
use App\Constants\JobTypes;
use App\Http\Requests\AccessRequest;
use App\Services\AccessService;
use Facades\App\Foundation\Modal\Modal;
use Facades\App\Foundation\Modal\ModalMessage;
use Illuminate\Http\Request;

class AccessController extends Controller
{
    /**
     * @var AccessService
     */
    private $accessService;

    public function __construct(AccessService $accessService)
    {
        $this->accessService = $accessService;
    }

    public function lists()
    {
        //$category = constants_to_array(JobTypes::class);
        $accesses = $this->accessService->lists();
        return view('pages.access')
                ->with(compact('accesses'));
    }

    public function show()
    {
          $types = constants_to_array(AccessTypes::class);
        return Modal::create()
            ->title("Add an access")
            ->template('modal.forms.access')
            ->buttons(["Create", "Cancel"])
            ->variables(compact('types'))
            ->render();
    }
    public function create(AccessRequest $request)
    {
        $this->accessService->create($request->all());
        return ModalMessage::success("Access successfully created!");
    }
    public function edit($id)
    {
        $access = $this->accessService->find($id);
        return Modal::create()
            ->title("Edit an access")
            ->template('modal.forms.access-edit')
            ->variables(compact('access'))
            ->buttons(["Edit", "Cancel"])
            ->render();
    }

    public function update(AccessRequest $request)
    {
        $this->accessService->update($request->all());
        return ModalMessage::success("Access successfully updated!");
    }

    public function delete($id)
    {
        $access = $this->accessService->find($id);
        return Modal::create()
            ->title("Delete Client")
            ->template('modal.forms.access-delete')
            ->buttons(["Delete", "Cancel"])
            ->variables(compact('access'))
            ->render();
    }

    public function destroy($id)
    {
        $this->accessService->destroy($id);
        return ModalMessage::success("Access successfully deleted!");
    }

}
