<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Client;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Analytics;
use Spatie\Analytics\Period;
use Illuminate\Http\Response;

class WebAnalyticsController extends Controller {

     public function __construct()
    {
        
    }

     function getClient()
    {
       
       if ( Auth::user()->role == 'admin')
       {
            $client_id = session('client_id');
            $client    = Client::find( $client_id);
        
       }else
       {
             $client  = Client::find(Auth::user()->client_id);
       }
       return $client;
    }

    public function index( ) 
    {

       $client     = $this->getClient();
       $websites   = $client->websites;

       if ( !isset( $websites[0]))
       {
          return view('web_analytics.message');
       }else{

        $viewId = $websites[0]->view_id; 
         //$website = new Website();
       // $website = $website->websiteByViewId($viewId)->get();
         return view('web_analytics.dashboard')->with(compact('websites','viewId'));
       
       }
       
    }

     public function info( $viewId = null) 
    {
     

       $client     = $this->getClient();

      $websites   = $client->websites;

      if ( $viewId == null)
         $viewId = $websites[0]->view_id; 

       $website = new Website();
        $website1 = $website->websiteByViewId($viewId);


        //dd($website1);
      return view('web_analytics.dashboard')->with(compact('websites','viewId','website1'));
    }

    function visitors( )
    {
      if (isset($_GET['duration']))
      $duration = $_GET['duration'];
      else
        $duration = 'month_to_date';
     
       $period   = $this->currentPeriod( $duration );
       $prevoius = $this->previousPeriod( $duration );

       $viewId     = $_GET['view_id']; 
      
       Analytics::setViewId( $viewId);

        $analyticsData = Analytics::performQuery(
            $period,
            'ga:users',
            array(
                'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );

        $analyticsDataPrev = Analytics::performQuery(
            $prevoius,
            'ga:users',
            array(
                'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );

        $rows = array();
        $table = array();

        $table['cols'] = array(
         array(
          'label' => 'Date', 
          'type' => 'date'
         ),
         array(
          'label' => 'Users', 
          'type' => 'number'
         ),

          array(
          'label' => 'Previous', 
          'type' => 'number',

         ),
           array(
          
          'type' => 'string',
          'role' => 'tooltip',
          'p' => array('isHtml' => true )
         
         )
        );


        if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
            
            foreach ($analyticsData['rows'] as $key=>$row) {

              $year = date('Y',strtotime($row[0]));
              $month = date('m',strtotime($row[0]))-1;
              $date = date('d',strtotime($row[0]));

              $sub_array = array();
              $sub_array[] =  array(
              "v" => "Date( $year,$month,$date)"
             );
              $sub_array[] =  array(
              "v" => $row[1]
             );

               if (array_key_exists($key, $analyticsDataPrev['rows']))
               {
                  $sub_array[] =  array(
                  "v" => $analyticsDataPrev['rows'][$key][1]
                 );
               
                  $sub_array[] =  array(
                  "v" => date('M-d,Y',strtotime($analyticsDataPrev['rows'][$key][0])).' Visitors :'.$analyticsDataPrev['rows'][$key][1]
                 );

                }

               $rows[] =  array(
                 "c" => $sub_array
               );
           
            }
          
        }
        
        $table['rows'] = $rows;
   
        return  response()->json($table);


    }


    //Bounce Rate
     function bounceRate( )
    {
      if (isset($_GET['duration']))
        $duration = $_GET['duration'];
      else
        $duration = 'month_to_date';

       $period   = $this->currentPeriod( $duration );
       $prevoius = $this->previousPeriod( $duration );

        /*$client     = new Client;
        $client     = $client->findClient(Auth::user()->id);
        $viewId     = $client->ga_view_id;*/ 

       // Analytics::setViewId('138264813');
       // Analytics::setViewId( $viewId);

        $viewId     = $_GET['view_id']; 
      
       Analytics::setViewId( $viewId);

        $analyticsData = Analytics::performQuery(
            $period,
            'ga:bounceRate',
            array(
                'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );

        $analyticsDataPrev = Analytics::performQuery(
            $prevoius,
            'ga:bounceRate',
            array(
                'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );

        $rows = array();
        $table = array();

        $table['cols'] = array(
         array(
          'label' => 'Date', 
          'type' => 'date'
         ),
         array(
          'label' => 'Bounce Rate', 
          'type' => 'number'
         ),

          array(
          'label' => 'Previous', 
          'type' => 'number',

         ),
           array(
          
          'type' => 'string',
          'role' => 'tooltip',
          'p' => array('isHtml' => true )
         
         )
        );

        if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
            
            foreach ($analyticsData['rows'] as $key=>$row) {

              $year = date('Y',strtotime($row[0]));
              $month = date('m',strtotime($row[0]))-1;
              $date = date('d',strtotime($row[0]));
          
           
                  $sub_array = array();
                  $sub_array[] =  array(


              "v" => "Date( $year,$month,$date)"
             );
                   $sub_array[] =  array(
              "v" => $row[1]
             );

           if (array_key_exists($key, $analyticsDataPrev['rows']))
           {
             
              $sub_array[] =  array(
              "v" => $analyticsDataPrev['rows'][$key][1]
             );
           
         $sub_array[] =  array(
              "v" => date('M-d,Y',strtotime($analyticsDataPrev['rows'][$key][0])).' Bounce Rate :'.$analyticsDataPrev['rows'][$key][1]
             );
        }

             $rows[] =  array(
             "c" => $sub_array
            );
             
            }
          
        }
        
        $table['rows'] = $rows;
        return  response()->json($table);


    }

    
    //sessions
    function sessionsInfo()
    {
      if (isset($_GET['duration']))
      	$duration = $_GET['duration'];
      else
        $duration = 'month_to_date';

       $period   = $this->currentPeriod( $duration );
       $prevoius = $this->previousPeriod( $duration );

      $viewId     = $_GET['view_id']; 
      
       Analytics::setViewId( $viewId);
  

        $analyticsData = Analytics::performQuery(
            $period,
            'ga:sessions',
            array(
                'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );

        $analyticsDataPrev = Analytics::performQuery(
            $prevoius,
            'ga:sessions',
            array(
                'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );


        $rows = array();
        $table = array();

        $table['cols'] = array(
         array(
          'label' => 'Date', 
          'type' => 'date'
         ),
         array(
          'label' => 'Sessions', 
          'type' => 'number'
         ),

          array(
          'label' => 'Previous', 
          'type' => 'number',

         ),
           array(
          
          'type' => 'string',
          'role' => 'tooltip',
          'p' => array('isHtml' => true )
         
         )
        );


        if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
            
            foreach ($analyticsData['rows'] as $key=>$row) {

              $year = date('Y',strtotime($row[0]));
              $month = date('m',strtotime($row[0]))-1;
              $date = date('d',strtotime($row[0]));
          
           
                  $sub_array = array();
                  $sub_array[] =  array(


              "v" => "Date( $year,$month,$date)"
             );
                   $sub_array[] =  array(
              "v" => $row[1]
             );

           if (array_key_exists($key, $analyticsDataPrev['rows']))
           {
             
            $sub_array[] =  array(
              "v" => $analyticsDataPrev['rows'][$key][1]


             );
           
         $sub_array[] =  array(
              "v" => date('M-d,Y',strtotime($analyticsDataPrev['rows'][$key][0])).' Sessions :'.$analyticsDataPrev['rows'][$key][1]


             );
        }

             $rows[] =  array(
             "c" => $sub_array
            );
             
            }
          
        }
        
        $table['rows'] = $rows;
        return  response()->json($table);
    }

    //6. Sessions by channel

     function sessionsByChannelInfo( )
    {

       if (isset($_GET['duration']))
        $duration = $_GET['duration'];
      else
        $duration = 'month_to_date';

       $period   = $this->currentPeriod( $duration );
       $prevoius = $this->previousPeriod( $duration );

      /*  $client     = new Client;
        $client     = $client->findClient(Auth::user()->id);
        $viewId     = '138264813';//$client->ga_view_id; 
        
       Analytics::setViewId( $viewId);*/
        $viewId     = $_GET['view_id']; 
      
       Analytics::setViewId( $viewId);
   
       $analyticsData = Analytics::performQuery(
           $period,
            'ga:entrances',
            array(
                'dimensions' => 'ga:channelGrouping'
              
            )
        );

      $rows = array();
    $table = array();

    $table['cols'] = array(
     array(
           'id'=> '',
      'label' => 'Channel', 
          'pattern'=>'',
      'type' => 'string'
     ),
     array(
             'id'=> '',
      'label' => 'Visitors', 
           'pattern'=>'',
      'type' => 'number'
     ),

     
    );


    if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
            
            foreach ($analyticsData['rows'] as $key=>$row) {
               $sub_array = array();
                $sub_array[] =  array(
          "v" => $row[0],
              "f" => null
         );
                 $sub_array[] =  array(
          "v" => intval($row[1]),
               "f" => null
         );

         $rows[] =  array(
         "c" => $sub_array
        );

             
            }
          
        }

           $table['rows'] = $rows;
        return  response()->json($table);

    }


//sessions
    function newSessionsRateInfo()
    {
        if (isset($_GET['duration']))
      $duration = $_GET['duration'];
      else
        $duration = 'month_to_date';
     

       $period   = $this->currentPeriod( $duration );
       $prevoius = $this->previousPeriod( $duration );


         $viewId     = $_GET['view_id']; 
      
       Analytics::setViewId( $viewId);

        $analyticsData = Analytics::performQuery(
            $period,
            'ga:percentNewSessions',
            array(
                'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );

        $analyticsDataPrev = Analytics::performQuery(
            $prevoius,
            'ga:percentNewSessions',
            array(
                'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );


        $rows = array();
        $table = array();

        $table['cols'] = array(
         array(
          'label' => 'Date', 
          'type' => 'date'
         ),
         array(
          'label' => '% New Sessions', 
          'type' => 'number'
         ),

          array(
          'label' => 'Previous', 
          'type' => 'number',

         ),
           array(
          
          'type' => 'string',
          'role' => 'tooltip',
          'p' => array('isHtml' => true )
         
         )
        );


        if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
            
            foreach ($analyticsData['rows'] as $key=>$row) {

              $year = date('Y',strtotime($row[0]));
              $month = date('m',strtotime($row[0]))-1;
              $date = date('d',strtotime($row[0]));
          
           
                  $sub_array = array();
                  $sub_array[] =  array(


              "v" => "Date( $year,$month,$date)"
             );
                   $sub_array[] =  array(
              "v" => $row[1]
             );

           if (array_key_exists($key, $analyticsDataPrev['rows']))
           {
             
            $sub_array[] =  array(
              "v" => $analyticsDataPrev['rows'][$key][1]


             );
           
         $sub_array[] =  array(
              "v" => date('M-d,Y',strtotime($analyticsDataPrev['rows'][$key][0])).' New Session Rate :'.$analyticsDataPrev['rows'][$key][1]


             );
        }

             $rows[] =  array(
             "c" => $sub_array
            );
             
            }
          
        }
        
        $table['rows'] = $rows;
        return  response()->json($table);
    }

    //pages per session
    function pagesForSessionInfo()
    {
       if (isset($_GET['duration']))
        $duration = $_GET['duration'];
      else
        $duration = 'month_to_date';

       $period = $this->currentPeriod( $duration );
       $prevoius = $this->previousPeriod( $duration );

         $viewId     = $_GET['view_id']; 
      
       Analytics::setViewId( $viewId);

        $analyticsData = Analytics::performQuery(
            $period,
            'ga:pageviewsPerSession',
            array(
                'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );

        $analyticsDataPrev = Analytics::performQuery(
            $prevoius,
            'ga:pageviewsPerSession',
            array(
                'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );


        $rows = array();
        $table = array();

        $table['cols'] = array(
         array(
          'label' => 'Date', 
          'type' => 'date'
         ),
         array(
          'label' => '% New Sessions', 
          'type' => 'number'
         ),

          array(
          'label' => 'Previous', 
          'type' => 'number',

         ),
           array(
          
          'type' => 'string',
          'role' => 'tooltip',
          'p' => array('isHtml' => true )
         
         )
        );


        if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
            
            foreach ($analyticsData['rows'] as $key=>$row) {

              $year = date('Y',strtotime($row[0]));
              $month = date('m',strtotime($row[0]))-1;
              $date = date('d',strtotime($row[0]));
          
           
                  $sub_array = array();
                  $sub_array[] =  array(


              "v" => "Date( $year,$month,$date)"
             );
                   $sub_array[] =  array(
              "v" => $row[1]
             );

           if (array_key_exists($key, $analyticsDataPrev['rows']))
           {
             
            $sub_array[] =  array(
              "v" => $analyticsDataPrev['rows'][$key][1]


             );
           
         $sub_array[] =  array(
              "v" => date('M-d,Y',strtotime($analyticsDataPrev['rows'][$key][0])).' Pages / Session :'.$analyticsDataPrev['rows'][$key][1]


             );
        }

             $rows[] =  array(
             "c" => $sub_array
            );
             
            }
          
        }
        
        $table['rows'] = $rows;
        return  response()->json($table);
    }


    function currentPeriod( $duration )
    {
      switch( $duration)
      {
        case 'month_to_date':
       
         $period = 30;
        
        break;
        case 'last_7_days':
         $period = 7;
         
        break;

        case 'last_10_days':
         $period = 30;
        
        break;
      }

      return  Period::days($period);

    }

    function previousPeriod( $duration )
    {

      switch( $duration)
      {
       
       case 'month_to_date':

         $startDate = Carbon::now()->subMonth(2);
         $endDate   = Carbon::now()->subMonth(1);
         $prevoius  = Period::create($startDate, $endDate);
        break;

        case 'last_7_days':
        
         $startDate = Carbon::now()->subWeek(2);
         $endDate   = Carbon::now()->subWeek(1);
         $prevoius  = Period::create($startDate, $endDate);
        break;

        case 'last_30_days':
        
         $startDate = Carbon::now()->subMonth(2);
         $endDate = Carbon::now()->subMonth(1);
         $prevoius = Period::create($startDate, $endDate);
        break;

      }
    
      return $prevoius;
    }


    function users( $period, $viewId)
    {

    	 Analytics::setViewId( $viewId );
        $analyticsData = Analytics::performQuery(
            $period,
            'ga:users'
           
        );

        return  $analyticsData['rows'][0][0];


    }

     function sessions( $period, $viewId)
    {

    	 Analytics::setViewId( $viewId );
        $analyticsData = Analytics::performQuery(
            $period,
            'ga:sessions'
           
        );

        return  $analyticsData['rows'][0][0];
    }


     function newSessionsPercentage( $period, $viewId)
    {

       Analytics::setViewId( $viewId );
        $analyticsData = Analytics::performQuery(
            $period,
            'ga:percentNewSessions'
           
        );

        return  $analyticsData['rows'][0][0];
    }

      function newUsers( $period, $viewId)
    {

    	 Analytics::setViewId( $viewId );
        $analyticsData = Analytics::performQuery(
            $period,
            'ga:newUsers'
           
        );
        
        return  $analyticsData['rows'][0][0];
    }

    function sessionsPerUser( $period, $viewId)
    {

    	 Analytics::setViewId( $viewId );
        $analyticsData = Analytics::performQuery(
            $period,
            'ga:sessionsPerUser'
           
        );

        return  $analyticsData['rows'][0][0];
    }

     function sessionsPerPage( $period, $viewId)
    {

    	 Analytics::setViewId( $viewId );
        $analyticsData = Analytics::performQuery(
            $period,
            'ga:pageviewsPerSession'
           
        );

        return  round($analyticsData['rows'][0][0],2);
    }


     function pageviews( $period, $viewId)
    {

    	 Analytics::setViewId( $viewId );
        $analyticsData = Analytics::performQuery(
            $period,
            'ga:pageviews'
           
        );

        return  $analyticsData['rows'][0][0];
    }


     function avgSessionDuration( $period, $viewId)
    {

    	 Analytics::setViewId( $viewId );
        $analyticsData = Analytics::performQuery(
            $period,
            'ga:avgSessionDuration'
           
        );

        $time = $analyticsData['rows'][0][0];


        return  date("H:i:s", mktime(0,0, round($time) % (24*3600)));
    }

     function avgSessionDurationSec( $period, $viewId)
    {

       Analytics::setViewId( $viewId );
        $analyticsData = Analytics::performQuery(
            $period,
            'ga:avgSessionDuration'
           
        );

        return  $analyticsData['rows'][0][0];
    }


      function bounceRateInfo( $period, $viewId)
    {

       Analytics::setViewId( $viewId );
        $analyticsData = Analytics::performQuery(
            $period,
            'ga:bounceRate'
           
        );

        return  round($analyticsData['rows'][0][0],2);
    }


    function audienceView()
    {

      if (isset($_GET['duration']))
        $duration = $_GET['duration'];
      else
        $duration = 'month_to_date';



        $period    = $this->currentPeriod( $duration );
        $prevoius  = $this->previousPeriod( $duration );
         $viewId     = $_GET['view_id']; 
      
       Analytics::setViewId( $viewId);

        $data   = [];

        $users            = $this->users( $period, $viewId);
        $data['users']    = $users;
        $prevUsers        = $this->users( $prevoius, $viewId);
        $userPercentage   = round(($users-$prevUsers)*100/$prevUsers,2);
        if ( $userPercentage>0)
        {
          $data['users_compare']      = '<i class="fa fa-caret-up" aria-hidden="true"></i>'.$userPercentage.'&#37;';
          $data['user_count_comapre'] = '<span>'. $users .'<em class="success"><i class="fa fa-caret-up" aria-hidden="true"></i> '.$userPercentage.'%</em> <small>Previous'.$prevUsers.'</small> </span>';
        }
         if ( $userPercentage<0)
        {
         $data['users_compare']      = '<i class="fa fa-caret-down" aria-hidden="true"></i>'.$userPercentage.'&#37;';
         $data['user_count_comapre'] = '<span>'. $users .'<em class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i> '.$userPercentage.'%</em> <small>Previous'.$prevUsers.'</small> </span>';
        }


 




        $sessions                   = $this->sessions( $period, $viewId);
        $data['sessions']           = $sessions;
        $prevSessions       = $this->sessions( $prevoius, $viewId);
        $sessPercentage   = round(($sessions-$prevSessions)*100/$prevSessions,2);

        if ( $sessPercentage>0)
        {
          $data['session_compare']      = '<i class="fa fa-caret-up" aria-hidden="true"></i>'.$sessPercentage.'&#37;';
          $data['session_count_comapre'] = '<span>'.$sessions .'<em class="success"><i class="fa fa-caret-up" aria-hidden="true"></i> '.$sessPercentage.'%</em> <small>Previous'.$prevSessions.'</small> </span>';
        }
         if ( $sessPercentage<0)
        {
         $data['session_compare']      = '<i class="fa fa-caret-down" aria-hidden="true"></i>'.$sessPercentage.'&#37;';
         $data['session_count_comapre'] = '<span>'.$sessions .'<em class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i> '.$sessPercentage.'%</em> <small>Previous'.$prevSessions.'</small> </span>';
        }




        
        $newUsers                   = $this->newUsers( $period, $viewId);
        $data['newUsers']           = $newUsers;
        $prevNewUsers               = $this->newUsers( $prevoius, $viewId);
         $newuserPercentage         = round(($newUsers-$prevNewUsers)*100/$prevNewUsers,2);
        if ( $newuserPercentage>0)
        {
        $data['newusers_compare']   = '<div class="success"><i class="fa fa-caret-up" aria-hidden="true"></i>'.$newuserPercentage.'&#37;</div>';
        $data['newuser_count_comapre'] = '<span>'. $users .'<em class="success"><i class="fa fa-caret-up" aria-hidden="true"></i> '.$newuserPercentage.'%</em> <small>Previous'.$prevNewUsers.'</small> </span>';
        }
         if ( $newuserPercentage<0)
         {
        $data['newusers_compare']   = '<div class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i>'.$newuserPercentage.'&#37;</div>';
        $data['newuser_count_comapre'] = '<span>'. $users .'<em class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i> '.$newuserPercentage.'%</em> <small>Previous'.$prevNewUsers.'</small> </span>';
        }

        
        $sessionPerUsers                     = $this->sessionsPerUser( $period, $viewId);
        $data['sessionPerUsers']             = $sessionPerUsers;
        $prevSessionPerUsers                 = $this->sessionsPerUser( $prevoius, $viewId);
         $sessionPerUsersPercentage          = round(($sessionPerUsers-$prevSessionPerUsers)*100/$prevSessionPerUsers,2);
        if ( $sessionPerUsersPercentage>0)
        $data['sessionperusers_compare']     = '<div class="success"><i class="fa fa-caret-up" aria-hidden="true"></i>'.$sessionPerUsersPercentage.'&#37;</div>';
         if ( $sessionPerUsersPercentage<0)
        $data['sessionperusers_compare']     = '<div class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i>'.$sessionPerUsersPercentage.'&#37;</div>';


        
        $sessionPerPage = $this->sessionsPerPage( $period, $viewId);
        $data['sessionPerPage']  = $sessionPerPage;

        $prevSessionPerPage               = $this->sessionsPerPage( $prevoius, $viewId);
         $prevSessionPercentage           = round(($sessionPerPage-$prevSessionPerPage)*100/$prevSessionPerPage,2);
        if ( $prevSessionPercentage>0)
        $data['sessionperpage_compare']   = '<div class="success"><i class="fa fa-caret-up" aria-hidden="true"></i>'.$prevSessionPercentage.'&#37;</div>';
         if ( $prevSessionPercentage<0)
        $data['sessionperpage_compare']   = '<div class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i>'.$prevSessionPercentage.'&#37;</div>';


        
        $pageViews                      = $this->pageviews( $period, $viewId);
        $data['pageViews']              = $pageViews;
        $prevPageViews                  = $this->pageviews( $prevoius, $viewId);
        $pageViewsPercentage            = round(($pageViews-$prevPageViews)*100/$prevPageViews,2);
        if ( $pageViewsPercentage>0)
        {
          $data['pageViewsPercentage']    = '<div class="success"><i class="fa fa-caret-up" aria-hidden="true"></i>'.$pageViewsPercentage.'&#37;</div>';
          $data['pageview_count_comapre'] = '<span>'.$pageViews .'<em class="success"><i class="fa fa-caret-up" aria-hidden="true"></i> '.$pageViewsPercentage.'%</em> <small>Previous'. $prevPageViews.'</small> </span>';
        }
        if ( $pageViewsPercentage<0){
          $data['pageViewsPercentage']    = '<div class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i>'.$pageViewsPercentage.'&#37;</div>';
          $data['pageview_count_comapre'] = '<span>'.$pageViews .'<em class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i> '.$pageViewsPercentage.'%</em> <small>Previous'. $prevPageViews.'</small> </span>';
        }


        
        $avgSessionDuration                 = $this->avgSessionDuration( $period, $viewId);
        $data['avgSessionDuration']         = $avgSessionDuration;
        $avgSessionDurationSec              = $this->avgSessionDurationSec( $period, $viewId);
        $prevAvgSessionDuration             = $this->avgSessionDurationSec( $prevoius, $viewId);
        $prevAvgSessionPercentage           = round(($avgSessionDurationSec-$prevAvgSessionDuration)*100/$prevAvgSessionDuration,2); 
        if ( $prevAvgSessionPercentage>0)
        $data['prevAvgSessionPercentage']   = '<div class="success"><i class="fa fa-caret-up" aria-hidden="true"></i>'.$prevAvgSessionPercentage.'&#37;</div>';
         if ( $prevAvgSessionPercentage<0)
        $data['prevAvgSessionPercentage']   = '<div class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i>'.$prevAvgSessionPercentage.'&#37;</div>';



        $bounceRate                      = $this->bounceRateInfo( $period, $viewId);
        $data['bounceRate']              = $bounceRate;
        $prevBounceRate                  = $this->bounceRateInfo( $prevoius, $viewId);
        $bounceRatePercentage            = round(($bounceRate-$prevBounceRate)*100/$prevBounceRate,2);
        if ( $bounceRatePercentage>0)
        {
        $data['bounceRatePercentage']    = '<div class="success"><i class="fa fa-caret-up" aria-hidden="true"></i>'.$bounceRatePercentage.'&#37;</div>';
         $data['bouncerate_count_comapre'] = '<span>'.$bounceRate .'<em class="success"><i class="fa fa-caret-up" aria-hidden="true"></i> '.$bounceRatePercentage.'%</em> <small>Previous'.$prevBounceRate.'</small> </span>';
        }
      
         if ( $bounceRatePercentage<0){
        $data['bounceRatePercentage']    = '<div class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i>'.$bounceRatePercentage.'&#37;</div>';
         $data['bouncerate_count_comapre'] = '<span>'. $bounceRate .'<em class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i> '.$bounceRatePercentage.'%</em> <small>Previous'.$prevBounceRate.'</small> </span>';
        }


        $newSessionPer      = $this->newSessionsPercentage( $period, $viewId);
        $newSessionsPerPrev = $this->newSessionsPercentage( $prevoius, $viewId);
        $newSessionPercentage = round(($newSessionPer-$newSessionsPerPrev)*100/$newSessionsPerPrev,2);

         if ( $newSessionPercentage>0)
        {
        
         $data['newsess_count_comapre'] = '<span>'. round($newSessionPer,2) .'<em class="success"><i class="fa fa-caret-up" aria-hidden="true"></i> '.$newSessionPercentage.'%</em> <small>Previous'. round($newSessionsPerPrev,2).'</small> </span>';
        }
      
         if ( $newSessionPercentage<0){
       
         $data['newsess_count_comapre'] = '<span>'.  round($newSessionPer,2).'<em class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i> '. round($newSessionPercentage,2).'%</em> <small>Previous'.$newSessionsPerPrev.'</small> </span>';
        }

     

        return response()->json($data);


    }

     function topPageViews()
    {
      if (isset($_GET['duration']))
        $duration = $_GET['duration'];
      else
        $duration = 'month_to_date';

        $period    = $this->currentPeriod( $duration );
        $prevoius  = $this->previousPeriod( $duration );

           $viewId     = $_GET['view_id']; 
      
       Analytics::setViewId( $viewId);
        
      /* $client     = new Client;
       $client     = $client->findClient(Auth::user()->id);
       $viewId     = $client->ga_view_id; 

        Analytics::setViewId('138264813');*/
       
        $analyticsData = Analytics::performQuery(
           $period,
            'ga:pageviews',
            array(
                'dimensions' => 'ga:pagePath',
                //'max-results'=> 10,
                'sort' => '-ga:pageviews'
            )
        );

          $analyticsDataPrev = Analytics::performQuery(
           $prevoius,
            'ga:pageviews',
            array(
                'dimensions' => 'ga:pagePath',
                'sort' => '-ga:pageviews'
            )
        );
   
      $return = [];
      if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
           
            foreach ($analyticsData['rows'] as $key=>$row) {
              /*  $pagePath = '';
                if (strlen($row[0])<=10){
                  $pagePath = $row[0];
                }else{
                  $arrString = str_split($row[0],10);
                    $lineCount = count($arrString) ;
                    for( $i=0;$i<$lineCount;$i++)
                    {
                      if ($i ==($lineCount-1))
                         $pagePath = $arrString[$i];
                      else
                         $pagePath .= $arrString[$i].'<br/>';
                    }
                }*/
               
                       
        
                if (array_key_exists($key, $analyticsDataPrev['rows']))
                    {
                 
                       $prevViewCount =  $analyticsDataPrev['rows'][$key][1];
                       $percentage = ($row[1]- $prevViewCount)*100/ $prevViewCount;
                       if ( $percentage>0)
                          $page_path_percentage = '<div class="success"><i class="fa fa-caret-up" aria-hidden="true"></i>'.round(($row[1]- $prevViewCount)*100/ $prevViewCount,2).'&#37;</div>';
                        if ( $percentage<0)
                           $page_path_percentage = '<div class="danger"><i class="fa fa-caret-down" aria-hidden="true"></i>'.round(($row[1]- $prevViewCount)*100/ $prevViewCount,2).'&#37;</div>';
                    }else{
                      $page_path_percentage = '';
                    }
             
                $table[] = array(
                    'page_path' =>  $row[0] ,
                    'page_path_percentage' => $page_path_percentage,

                   'view_count' => $row[1]
                   
                );
            }
         
        }

        return response()->json($table);


    }

    function pageViewsInfo()
    {
      if (isset($_GET['duration']))
        $duration = $_GET['duration'];
      else
        $duration = 'month_to_date';

        $period    = $this->currentPeriod( $duration );
        $prevoius  = $this->previousPeriod( $duration );

            $viewId     = $_GET['view_id']; 
      
       Analytics::setViewId( $viewId);
        
     /*  $client     = new Client;
       $client     = $client->findClient(Auth::user()->id);
       $viewId     = $client->ga_view_id; 

        Analytics::setViewId('138264813');*/
       
        $analyticsData = Analytics::performQuery(
           $period,
            'ga:pageviews',
            array(
               'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );

          $analyticsDataPrev = Analytics::performQuery(
           $prevoius,
            'ga:pageviews',
            array(
                'dimensions' => 'ga:date',
                'sort' => 'ga:date'
            )
        );
   
     $rows = array();
        $table = array();

        $table['cols'] = array(
         array(
          'label' => 'Date', 
          'type' => 'date'
         ),
         array(
          'label' => 'Page Views', 
          'type' => 'number'
         ),

          array(
          'label' => 'Previous', 
          'type' => 'number',

         ),
           array(
          
          'type' => 'string',
          'role' => 'tooltip',
          'p' => array('isHtml' => true )
         
         )
        );


        if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
            
            foreach ($analyticsData['rows'] as $key=>$row) {

              $year = date('Y',strtotime($row[0]));
              $month = date('m',strtotime($row[0]))-1;
              $date = date('d',strtotime($row[0]));

              $sub_array = array();
              $sub_array[] =  array(
              "v" => "Date( $year,$month,$date)"
             );
              $sub_array[] =  array(
              "v" => $row[1]
             );

               if (array_key_exists($key, $analyticsDataPrev['rows']))
               {
                  $sub_array[] =  array(
                  "v" => $analyticsDataPrev['rows'][$key][1]
                 );
               
                  $sub_array[] =  array(
                  "v" => date('M-d,Y',strtotime($analyticsDataPrev['rows'][$key][0])).' Page Views :'.$analyticsDataPrev['rows'][$key][1]
                 );

                }

               $rows[] =  array(
                 "c" => $sub_array
               );
           
            }
          
        }
        
        $table['rows'] = $rows; 
   
        return  response()->json($table);


    }


    public function visitsByDevice() {

      if (isset($_GET['duration']))
        $duration = $_GET['duration'];
      else
        $duration = 'month_to_date';

        $period    = $this->currentPeriod( $duration );
        $prevoius  = $this->previousPeriod( $duration );

       $viewId     = $_GET['view_id']; 

       Analytics::setViewId( $viewId);

       
        $analyticsData = Analytics::performQuery(
           $period,
            'ga:users',
            array(
               'dimensions' => 'ga:deviceCategory'
            )
        );




      $rows = array();
    $table = array();

    $table['cols'] = array(
     array(
           'id'=> '',
      'label' => 'Devices', 
          'pattern'=>'',
      'type' => 'string'
     ),
     array(
             'id'=> '',
      'label' => 'Visitors', 
           'pattern'=>'',
      'type' => 'number'
     ),

     
    );


    if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
            
            foreach ($analyticsData['rows'] as $key=>$row) {
               $sub_array = array();
                $sub_array[] =  array(
          "v" => $row[0],
              "f" => null
         );
                 $sub_array[] =  array(
          "v" => intval($row[1]),
               "f" => null
         );

         $rows[] =  array(
         "c" => $sub_array
        );

             
            }
          
        }

           $table['rows'] = $rows;
        return  response()->json($table);

   

         
    }


    public function visitsByCountries() {

      if (isset($_GET['duration']))
        $duration = $_GET['duration'];
      else
        $duration = 'month_to_date';

        $period    = $this->currentPeriod( $duration );
        $prevoius  = $this->previousPeriod( $duration );

        $viewId     = $_GET['view_id']; 
      
       Analytics::setViewId( $viewId);

       
        $analyticsData = Analytics::performQuery(
           $period,
            'ga:sessions',
            array(
               'dimensions' => 'ga:country',
               'max-results'=>10

            )
        );
 $return = [];
      if (isset($analyticsData['rows']) && !empty($analyticsData['rows'])) {
           
            foreach ($analyticsData['rows'] as $key=>$row) {
              
                    
                $table[] = array(
                    'country' =>  $row[0] ,
                   'view_count' => $row[1]
                   
                );
            }
         
        }

        return response()->json($table);
        
         
    }

      function goalInfo( $period = 7)
    {
         $client     = new Client;
         $client     = $client->findClient(Auth::user()->id);
         //$viewId    = $client->ga_view_id; 

         $viewId = '138658544';
       Analytics::setViewId( $viewId);
      // Analytics::setViewId('138658544');
         $period = 7;



       $analyticsData = Analytics::performQuery(
            Period::days($period),
            'ga:goalStartsAll',
            array(
                'dimensions' => 'ga:goalCompletionLocation'
              
            )
        );

       dd($analyticsData);
     }

  
}
