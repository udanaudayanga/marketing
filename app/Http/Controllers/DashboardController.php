<?php

namespace App\Http\Controllers;


use Auth;
use App\Models\Logger;
use App\Models\Client;
use App\Models\EmailAddress;
use App\Services\UserService;
use App\Services\ClientService;
use App\Services\AccessService;
use App\Services\JobService;
use App\Services\InvoiceManager;
use Facades\App\Foundation\Modal\Modal;
use Facades\App\Foundation\Modal\ModalMessage;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
	 /**
     * @var EmailAddress
     */
    private $emailAddress;

    private $userService;

    private $accessService;

    private $jobService;

    private $invoiceManager;

    public function __construct(EmailAddress $emailAddress, UserService $userService, AccessService $accessService, JobService $jobService, InvoiceManager $invoiceManager,ClientService $clientService)
    {
       $this->emailAddress   = $emailAddress;
       $this->userService    = $userService;
       $this->accessService  = $accessService;
       $this->jobService     = $jobService;
       $this->invoiceManager = $invoiceManager;
       $this->clientService = $clientService;
  
    }


    public function index( )
    {
        $client             = $this->getClient();

       if ( $client == null)
       { 
              return redirect('/logout');
       }

        $employees          = $this->userService->employees($client->id)->take(5);
        $emailAddresses     = $this->emailAddress->take(5)->where('client_id', $client->id)->get();
        $access             = $this->accessService->firstRecord($client->id);
        $jobs               = $this->jobService->activeJobs()->take(5);
        $invoices           = $this->invoiceManager->lists()->where('status','UNPAID')->take(3);
      
    	return view('dashboard.index')->with(compact('client','employees','emailAddresses','access','jobs','invoices'));
      
    }

    function getClient()
    {
        if ( Auth::user()->role == 'admin')
       {

            $client_id = session('client_id');
            $client    = Client::find( $client_id);
          
       }else
       {
             $client  = Client::find(Auth::user()->client_id);
             
       }
       return $client;
      
    }


    //save card info in db
     public function showAddEmail()
    {
        $client = $this->getClient();

        return Modal::create()
            ->title("Add an access")
            ->template('modal.forms.email-address-list')
            ->buttons(["Create", "Cancel"])
            ->variables(compact('client'))
            ->render();
    }

	 public function saveEmailAddress(Request $request)
    {
         $attributes = $request->all();
  

         $emailAddress = new EmailAddress();
         $emailAddress->create($attributes);
        return ModalMessage::success("Email Address successfully saved!");

     

    }

    public function emailList()
    {
    	$client = $this->getClient();
        $emailAddresses = $this->emailAddress->where('client_id', $client->id)->get();
        return view('pages.email_addresses_list')
                ->with(compact('emailAddresses'));
    }


    //update client info
     public function editClient()
    {
        $client = $this->getClient();

        return Modal::create()
            ->title("Add an access")
            ->template('modal.forms.client-edit')
            ->buttons(["Create", "Cancel"])
            ->variables(compact('client'))
            ->render();
    }

     public function updateClient(Request $request)
    {
        $client     = $this->getClient();
        $attributes = $request->all();
        $this->clientService->update($client->id,$attributes);

        return ModalMessage::success("Client updated successfully saved!");

     

    }
}
