<?php

namespace App\Http\Requests;

use App\Constants\UserRoles;
use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ( user()->role == UserRoles::CLIENT || user()->role == UserRoles::ADMIN )
            return true;
        //return user()->actAs(UserRoles::ADMIN);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required',
            'invoice_date' => 'required',
            'due_date' => 'required',
            'terms' => 'required',
        ];
    }
}
