<?php

namespace App\Http\Requests;
use App\Constants\UserRoles;

use Illuminate\Foundation\Http\FormRequest;

class AccessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
       // return user()->isClient();
        if ( user()->role == UserRoles::CLIENT || user()->role == UserRoles::ADMIN )
            return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'organization' => 'required'
        ];
    }
}
