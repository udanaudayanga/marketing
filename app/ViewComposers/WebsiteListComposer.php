<?php

namespace App\ViewComposers;

use App\Models\Client;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

class WebsiteListComposer
{

    protected $client;


    public function __construct(Client $client)
    {
        $this->client= $client;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if (Auth::User() != null){

           if ( Auth::user()->role == 'admin')
           {
                $client_id = session('client_id');
                $this->client    = Client::find( $client_id);
           }else
           {
                  $this->client = Client::find(Auth::user()->client_id);
           }
 
        $websiteList = $this->client->websites;
     
        $view->with('websiteList',  $websiteList);
    }
    }
}
